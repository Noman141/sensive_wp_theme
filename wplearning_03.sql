-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2019 at 10:40 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wplearning_03`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_b2s_posts`
--

CREATE TABLE `wp_b2s_posts` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `blog_user_id` int(11) NOT NULL,
  `last_edit_blog_user_id` int(11) NOT NULL,
  `user_timezone` tinyint(4) NOT NULL DEFAULT 0,
  `sched_details_id` int(11) NOT NULL,
  `sched_type` tinyint(4) NOT NULL DEFAULT 0,
  `sched_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sched_date_utc` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_link` varchar(255) NOT NULL,
  `publish_error_code` varchar(100) NOT NULL,
  `network_details_id` int(11) NOT NULL,
  `post_for_relay` tinyint(4) NOT NULL DEFAULT 0,
  `post_for_approve` tinyint(4) NOT NULL DEFAULT 0,
  `relay_primary_post_id` int(11) NOT NULL DEFAULT 0,
  `relay_delay_min` int(11) NOT NULL DEFAULT 0,
  `hook_action` tinyint(4) NOT NULL DEFAULT 0,
  `hide` tinyint(4) NOT NULL DEFAULT 0,
  `v2_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_b2s_posts_drafts`
--

CREATE TABLE `wp_b2s_posts_drafts` (
  `id` int(11) NOT NULL,
  `last_save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `blog_user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_b2s_posts_network_details`
--

CREATE TABLE `wp_b2s_posts_network_details` (
  `id` int(11) NOT NULL,
  `network_id` tinyint(4) NOT NULL,
  `network_type` tinyint(4) NOT NULL,
  `network_auth_id` int(11) NOT NULL,
  `network_display_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_b2s_posts_sched_details`
--

CREATE TABLE `wp_b2s_posts_sched_details` (
  `id` int(11) NOT NULL,
  `sched_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_b2s_user`
--

CREATE TABLE `wp_b2s_user` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `blog_user_id` int(11) NOT NULL,
  `feature` tinyint(2) NOT NULL,
  `state_url` tinyint(2) NOT NULL,
  `register_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_b2s_user`
--

INSERT INTO `wp_b2s_user` (`id`, `token`, `blog_user_id`, `feature`, `state_url`, `register_date`) VALUES
(1, 'v3_NGlFYnhTSk5aeTNTSDF3d1NYODZ2Qk9mS2M2QlltTzRZZm9GRm5Xd01hVkJ6eWFiR0hmSnRqOXBPRjh1YjEveWJUeHM3RmNEdFhWdnA1RXo0Q2liOE1uenI0aTVjMUF1SmRodkhtMG8xbWMxOHcrbmNUYWVha094ZlJOMDBSaEo1NDNrN3FLSW1Mc3Rmc3E2TEE0Qk53PT06OmhrZDkrZTNsckNkdXZKdTM,', 1, 0, 0, '2019-11-07 12:17:56');

-- --------------------------------------------------------

--
-- Table structure for table `wp_b2s_user_contact`
--

CREATE TABLE `wp_b2s_user_contact` (
  `id` int(11) NOT NULL,
  `blog_user_id` int(11) NOT NULL,
  `name_mandant` varchar(100) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name_presse` varchar(100) NOT NULL,
  `anrede_presse` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0=Frau,1=Herr 2=keine Angabe',
  `vorname_presse` varchar(50) NOT NULL,
  `nachname_presse` varchar(50) NOT NULL,
  `strasse_presse` varchar(100) NOT NULL,
  `nummer_presse` varchar(5) NOT NULL DEFAULT '',
  `plz_presse` varchar(10) NOT NULL,
  `ort_presse` varchar(75) NOT NULL,
  `land_presse` varchar(3) NOT NULL DEFAULT 'DE',
  `email_presse` varchar(75) NOT NULL,
  `telefon_presse` varchar(30) NOT NULL,
  `fax_presse` varchar(30) NOT NULL,
  `url_presse` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_b2s_user_network_settings`
--

CREATE TABLE `wp_b2s_user_network_settings` (
  `id` int(11) NOT NULL,
  `blog_user_id` int(11) NOT NULL,
  `mandant_id` int(11) NOT NULL,
  `network_auth_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-10-30 18:10:04', '2019-10-30 18:10:04', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 97, 'admin', 'nmnaba14@gmail.com', '', '::1', '2019-11-20 06:01:41', '2019-11-20 06:01:41', 'Hello Admin', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', '', 0, 1),
(3, 97, 'admin', 'nmnaba14@gmail.com', '', '::1', '2019-11-20 07:31:47', '2019-11-20 07:31:47', 'Hi User', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', '', 2, 1),
(4, 97, 'admin', 'nmnaba14@gmail.com', '', '::1', '2019-11-20 07:34:18', '2019-11-20 07:34:18', 'Test Comment without reply', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', '', 0, 1),
(5, 97, 'admin', 'nmnaba14@gmail.com', '', '::1', '2019-11-20 07:39:09', '2019-11-20 07:39:09', 'Hi', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', '', 2, 1),
(6, 97, 'admin', 'nmnaba14@gmail.com', '', '::1', '2019-11-20 07:39:28', '2019-11-20 07:39:28', 'Your Reply', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', '', 4, 1),
(7, 1, 'admin', 'nmnaba14@gmail.com', '', '::1', '2019-11-20 07:40:47', '2019-11-20 07:40:47', 'Hello', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', '', 1, 1),
(8, 97, 'admin', 'nmnaba14@gmail.com', '', '::1', '2019-11-20 13:57:41', '2019-11-20 13:57:41', 'hello dear', 0, '1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', '', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_fblb`
--

CREATE TABLE `wp_fblb` (
  `id` mediumint(9) NOT NULL,
  `display` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `beforeafter` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `except_ids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `where_like` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `btn_size` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb_app_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb_app_admin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `mobile` int(11) DEFAULT NULL,
  `kid` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `share` int(11) DEFAULT NULL,
  `faces` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_fblb`
--

INSERT INTO `wp_fblb` (`id`, `display`, `width`, `beforeafter`, `except_ids`, `where_like`, `layout`, `action`, `color`, `btn_size`, `position`, `language`, `fb_app_id`, `fb_app_admin`, `url`, `default_image`, `status`, `mobile`, `kid`, `user_id`, `active`, `share`, `faces`, `created`, `last_modified`) VALUES
(1, 8, 450, 'after', '', 'eachpage', 'standard', 'like', 'light', 'small', 'left', 'en_US', '', '', '', '', 1, 1, 0, 0, 1, 0, 1, '2019-11-08 14:36:59', '2019-11-08 14:47:21');

-- --------------------------------------------------------

--
-- Table structure for table `wp_hustle_entries`
--

CREATE TABLE `wp_hustle_entries` (
  `entry_id` bigint(20) UNSIGNED NOT NULL,
  `entry_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_hustle_entries_meta`
--

CREATE TABLE `wp_hustle_entries_meta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `entry_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_hustle_modules`
--

CREATE TABLE `wp_hustle_modules` (
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `module_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) DEFAULT 1,
  `module_mode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_hustle_modules`
--

INSERT INTO `wp_hustle_modules` (`module_id`, `blog_id`, `module_name`, `module_type`, `active`, `module_mode`) VALUES
(1, 0, 'Social', 'social_sharing', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_hustle_modules_meta`
--

CREATE TABLE `wp_hustle_modules_meta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_hustle_modules_meta`
--

INSERT INTO `wp_hustle_modules_meta` (`meta_id`, `module_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'content', '{\"module_name\":\"\",\"title\":\"\",\"sub_title\":\"\",\"main_content\":\"\",\"feature_image\":\"\",\"show_never_see_link\":\"0\",\"never_see_link_text\":\"Never see this message again.\",\"show_cta\":\"0\",\"cta_label\":\"\",\"cta_url\":\"\",\"cta_target\":\"blank\",\"social_icons\":{\"facebook\":{\"platform\":\"facebook\",\"label\":\"Facebook\",\"type\":\"click\",\"counter\":\"0\",\"link\":\"\"},\"twitter\":{\"platform\":\"twitter\",\"label\":\"Twitter\",\"type\":\"click\",\"counter\":\"0\",\"link\":\"\"},\"linkedin\":{\"platform\":\"linkedin\",\"label\":\"LinkedIn\",\"type\":\"click\",\"counter\":\"0\",\"link\":\"\"},\"instagram\":{\"platform\":\"instagram\",\"label\":\"Instagram\",\"type\":\"click\",\"counter\":\"0\",\"link\":\"\"}},\"counter_enabled\":\"0\"}'),
(2, 1, 'design', '{\"form_layout\":\"one\",\"style\":\"minimal\",\"feature_image_position\":\"left\",\"feature_image_fit\":\"contain\",\"feature_image_horizontal\":\"center\",\"feature_image_horizontal_px\":\"-100\",\"feature_image_vertical\":\"center\",\"feature_image_vertical_px\":\"-100\",\"feature_image_hide_on_mobile\":\"0\",\"form_fields_style\":\"flat\",\"form_fields_border_radius\":\"5\",\"form_fields_border_weight\":\"2\",\"form_fields_border_type\":\"solid\",\"form_fields_icon\":\"static\",\"form_fields_proximity\":\"joined\",\"button_style\":\"flat\",\"button_border_radius\":\"5\",\"button_border_weight\":\"2\",\"button_border_type\":\"solid\",\"gdpr_checkbox_style\":\"flat\",\"gdpr_border_radius\":\"5\",\"gdpr_border_weight\":\"2\",\"gdpr_border_type\":\"solid\",\"cta_style\":\"flat\",\"cta_border_radius\":\"5\",\"cta_border_weight\":\"2\",\"cta_border_type\":\"solid\",\"color_palette\":\"gray_slate\",\"customize_colors\":\"0\",\"main_bg_color\":\"#38454E\",\"image_container_bg\":\"#35414A\",\"form_area_bg\":\"#5D7380\",\"title_color\":\"#FFFFFF\",\"title_color_alt\":\"#ADB5B7\",\"subtitle_color\":\"#FFFFFF\",\"subtitle_color_alt\":\"#ADB5B7\",\"content_color\":\"#ADB5B7\",\"ol_counter\":\"#ADB5B7\",\"ul_bullets\":\"#ADB5B7\",\"blockquote_border\":\"#38C5B5\",\"link_static_color\":\"#38C5B5\",\"link_hover_color\":\"#49E2D1\",\"link_active_color\":\"#49E2D1\",\"cta_button_static_bo\":\"#2CAE9F\",\"cta_button_static_bg\":\"#38C5B5\",\"cta_button_static_color\":\"#FFFFFF\",\"cta_button_hover_bo\":\"#39CDBD\",\"cta_button_hover_bg\":\"#49E2D1\",\"cta_button_hover_color\":\"#FFFFFF\",\"cta_button_active_bo\":\"#39CDBD\",\"cta_button_active_bg\":\"#49E2D1\",\"cta_button_active_color\":\"#FFFFFF\",\"optin_input_icon\":\"#AAAAAA\",\"optin_input_static_bo\":\"#B0BEC6\",\"optin_input_static_bg\":\"#FFFFFF\",\"optin_form_field_text_static_color\":\"#5D7380\",\"optin_placeholder_color\":\"#AAAAAA\",\"optin_input_icon_hover\":\"#5D7380\",\"optin_input_hover_bo\":\"#4F5F6B\",\"optin_input_hover_bg\":\"#FFFFFF\",\"optin_input_icon_focus\":\"#5D7380\",\"optin_input_active_bo\":\"#4F5F6B\",\"optin_input_active_bg\":\"#FFFFFF\",\"optin_input_icon_error\":\"#D43858\",\"optin_input_error_border\":\"#D43858\",\"optin_input_error_background\":\"#FFFFFF\",\"optin_check_radio_bo\":\"#B0BEC6\",\"optin_check_radio_bg\":\"#FFFFFF\",\"optin_mailchimp_labels_color\":\"#FFFFFF\",\"optin_check_radio_bo_checked\":\"#4F5F6B\",\"optin_check_radio_bg_checked\":\"#FFFFFF\",\"optin_check_radio_tick_color\":\"#38C5B5\",\"gdpr_chechbox_border_static\":\"#B0BEC6\",\"gdpr_chechbox_background_static\":\"#FFFFFF\",\"gdpr_content\":\"#FFFFFF\",\"gdpr_content_link\":\"#FFFFFF\",\"gdpr_chechbox_border_active\":\"#4F5F6B\",\"gdpr_checkbox_background_active\":\"#FFFFFF\",\"gdpr_checkbox_icon\":\"#38C5B5\",\"gdpr_checkbox_border_error\":\"#D43858\",\"gdpr_checkbox_background_error\":\"#FFFFFF\",\"optin_select_border\":\"#B0BEC6\",\"optin_select_background\":\"#FFFFFF\",\"optin_select_icon\":\"#38C5B5\",\"optin_select_label\":\"#5D7380\",\"optin_select_placeholder\":\"#AAAAAA\",\"optin_select_border_hover\":\"#4F5F6B\",\"optin_select_background_hover\":\"#FFFFFF\",\"optin_select_icon_hover\":\"#49E2D1\",\"optin_select_border_open\":\"#4F5F6B\",\"optin_select_background_open\":\"#FFFFFF\",\"optin_select_icon_open\":\"#49E2D1\",\"optin_select_border_error\":\"#FFFFFF\",\"optin_select_background_error\":\"#FFFFFF\",\"optin_select_icon_error\":\"#D43858\",\"optin_dropdown_background\":\"#FFFFFF\",\"optin_dropdown_option_color\":\"#5D7380\",\"optin_dropdown_option_color_hover\":\"#FFFFFF\",\"optin_dropdown_option_bg_hover\":\"#ADB5B7\",\"optin_dropdown_option_color_active\":\"#FFFFFF\",\"optin_dropdown_option_bg_active\":\"#38C5B5\",\"optin_calendar_background\":\"#FFFFFF\",\"optin_calendar_title\":\"#35414A\",\"optin_calendar_arrows\":\"#5D7380\",\"optin_calendar_thead\":\"#35414A\",\"optin_calendar_cell_background\":\"#FFFFFF\",\"optin_calendar_cell_color\":\"#5D7380\",\"optin_calendar_arrows_hover\":\"#5D7380\",\"optin_calendar_cell_bg_hover\":\"#38C5B5\",\"optin_calendar_cell_color_hover\":\"#FFFFFF\",\"optin_calendar_arrows_active\":\"#5D7380\",\"optin_calendar_cell_bg_active\":\"#38C5B5\",\"optin_calendar_cell_color_active\":\"#FFFFFF\",\"optin_submit_button_static_bo\":\"#2CAE9F\",\"optin_submit_button_static_bg\":\"#38C5B5\",\"optin_submit_button_static_color\":\"#FFFFFF\",\"optin_submit_button_hover_bo\":\"#39CDBD\",\"optin_submit_button_hover_bg\":\"#49E2D1\",\"optin_submit_button_hover_color\":\"#FFFFFF\",\"optin_submit_button_active_bo\":\"#39CDBD\",\"optin_submit_button_active_bg\":\"#49E2D1\",\"optin_submit_button_active_color\":\"#FFFFFF\",\"optin_mailchimp_title_color\":\"#FFFFFF\",\"custom_section_bg\":\"#35414A\",\"optin_error_text_bg\":\"#FFFFFF\",\"optin_error_text_border\":\"#D43858\",\"optin_error_text_color\":\"#D43858\",\"optin_success_background\":\"#38454E\",\"optin_success_tick_color\":\"#38C5B5\",\"optin_success_content_color\":\"#ADB5B7\",\"overlay_bg\":\"rgba(51,51,51,0.9)\",\"close_button_static_color\":\"#38C5B5\",\"never_see_link_static\":\"#38C5B5\",\"close_button_hover_color\":\"#49E2D1\",\"never_see_link_hover\":\"#49E2D1\",\"close_button_active_color\":\"#49E2D1\",\"never_see_link_active\":\"#49E2D1\",\"border\":\"0\",\"border_radius\":\"5\",\"border_weight\":\"2\",\"border_type\":\"solid\",\"border_color\":\"#DADADA\",\"drop_shadow\":\"0\",\"drop_shadow_x\":\"0\",\"drop_shadow_y\":\"0\",\"drop_shadow_blur\":\"0\",\"drop_shadow_spread\":\"0\",\"drop_shadow_color\":\"rgba(0,0,0,0.4)\",\"customize_size\":\"0\",\"apply_custom_size_to\":\"desktop\",\"custom_width\":\"600\",\"custom_height\":\"300\",\"customize_css\":\"0\",\"custom_css\":\"\",\"icon_style\":\"squared\",\"floating_customize_colors\":\"0\",\"floating_icon_bg_color\":\"rgba(146, 158, 170, 1)\",\"floating_icon_color\":\"rgba(255, 255, 255, 1)\",\"floating_bg_color\":\"rgba(4, 48, 69, 1)\",\"floating_counter_border\":\"rgba(146, 158, 170, 1)\",\"floating_counter_color\":\"rgba(255, 255, 255, 1)\",\"floating_animate_icons\":\"0\",\"floating_drop_shadow\":\"0\",\"floating_drop_shadow_x\":\"0\",\"floating_drop_shadow_y\":\"0\",\"floating_drop_shadow_blur\":\"0\",\"floating_drop_shadow_spread\":\"0\",\"floating_drop_shadow_color\":\"rgba(0,0,0,0.2)\",\"floating_inline_count\":\"0\",\"widget_customize_colors\":\"0\",\"widget_icon_bg_color\":\"rgba(146, 158, 170, 1)\",\"widget_icon_color\":\"rgba(255, 255, 255, 1)\",\"widget_bg_color\":\"rgba(146, 158, 170, 1)\",\"widget_animate_icons\":\"1\",\"widget_drop_shadow\":\"0\",\"widget_drop_shadow_x\":\"0\",\"widget_drop_shadow_y\":\"0\",\"widget_drop_shadow_blur\":\"0\",\"widget_drop_shadow_spread\":\"0\",\"widget_drop_shadow_color\":\"rgba(0,0,0,0.2)\",\"widget_inline_count\":\"0\",\"widget_counter_border\":\"rgba(146, 158, 170, 1)\",\"widget_counter_color\":\"rgba(255, 255, 255, 1)\"}'),
(3, 1, 'display', '{\"inline_enabled\":\"1\",\"inline_position\":\"below\",\"widget_enabled\":\"1\",\"shortcode_enabled\":\"1\",\"inline_align\":\"left\",\"float_desktop_enabled\":\"1\",\"float_desktop_position\":\"center\",\"float_desktop_offset\":\"screen\",\"float_desktop_offset_x\":\"0\",\"float_desktop_position_y\":\"top\",\"float_desktop_offset_y\":\"0\",\"float_desktop_css_selector\":\"\",\"float_mobile_enabled\":\"0\",\"float_mobile_position\":\"center\",\"float_mobile_offset\":\"screen\",\"float_mobile_position_x\":\"left\",\"float_mobile_offset_x\":\"0\",\"float_mobile_position_y\":\"top\",\"float_mobile_offset_y\":\"0\",\"float_mobile_css_selector\":\"\"}'),
(4, 1, 'visibility', '{\"conditions\":{\"16e45dec0cb\":{\"group_id\":\"16e45dec0cb\",\"filter_type\":\"all\",\"posts\":{\"filter_type\":\"except\"}}}}'),
(5, 1, 'shortcode_id', 'Social'),
(6, 1, 'edit_roles', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `wp_hustle_tracking`
--

CREATE TABLE `wp_hustle_tracking` (
  `tracking_id` bigint(20) UNSIGNED NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `page_id` bigint(20) UNSIGNED NOT NULL,
  `module_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `counter` mediumint(8) UNSIGNED NOT NULL DEFAULT 1,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_actions`
--

CREATE TABLE `wp_ig_actions` (
  `contact_id` bigint(20) UNSIGNED DEFAULT NULL,
  `message_id` bigint(20) UNSIGNED DEFAULT NULL,
  `campaign_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `count` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `link_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `list_id` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `updated_at` int(11) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_ig_actions`
--

INSERT INTO `wp_ig_actions` (`contact_id`, `message_id`, `campaign_id`, `type`, `count`, `link_id`, `list_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 1, 0, 0, 1572860652, 1572860652),
(2, 1, 1, 2, 1, 0, 0, 1572860653, 1572860653),
(1, NULL, NULL, 1, 1, 0, 2, 1572860901, 1572860901),
(1, 2, 2, 2, 1, 0, 0, 1572861552, 1572861552),
(2, 2, 2, 2, 1, 0, 0, 1572861554, 1572861554);

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_blocked_emails`
--

CREATE TABLE `wp_ig_blocked_emails` (
  `id` int(10) NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_campaigns`
--

CREATE TABLE `wp_ig_campaigns` (
  `id` int(10) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `parent_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_to_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_to_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `list_ids` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_template_id` int(10) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `meta` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_ig_campaigns`
--

INSERT INTO `wp_ig_campaigns` (`id`, `slug`, `name`, `type`, `parent_id`, `parent_type`, `subject`, `body`, `from_name`, `from_email`, `reply_to_name`, `reply_to_email`, `categories`, `list_ids`, `base_template_id`, `status`, `meta`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'welcome-to-email-subscribers', 'Welcome To Email Subscribers', 'newsletter', NULL, NULL, NULL, '', 'My New Wordpress Website', 'nmnaba14@gmail.com', 'My New Wordpress Website', 'nmnaba14@gmail.com', '', '1', 93, 1, NULL, '2019-11-04 09:29:37', NULL, NULL),
(2, 'new-post-published-posttitle', 'New Post Published - {{POSTTITLE}}', 'post_notification', NULL, NULL, NULL, '', 'nmnaba14@gmail.com', 'My New Wordpress Website', 'nmnaba14@gmail.com', 'My New Wordpress Website', '##4##5##1##', '1', 94, 1, NULL, '2019-11-04 09:29:40', NULL, '2019-11-04 11:16:16');

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_contactmeta`
--

CREATE TABLE `wp_ig_contactmeta` (
  `meta_id` bigint(20) NOT NULL,
  `contact_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_contacts`
--

CREATE TABLE `wp_ig_contacts` (
  `id` int(10) NOT NULL,
  `wp_user_id` int(10) NOT NULL DEFAULT 0,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_id` int(10) NOT NULL DEFAULT 0,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unsubscribed` tinyint(1) NOT NULL DEFAULT 0,
  `hash` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_verified` tinyint(1) DEFAULT 0,
  `is_disposable` tinyint(1) DEFAULT 0,
  `is_rolebased` tinyint(1) DEFAULT 0,
  `is_webmail` tinyint(1) DEFAULT 0,
  `is_deliverable` tinyint(1) DEFAULT 0,
  `is_sendsafely` tinyint(1) DEFAULT 0,
  `meta` longtext CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_contacts_ips`
--

CREATE TABLE `wp_ig_contacts_ips` (
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_contact_meta`
--

CREATE TABLE `wp_ig_contact_meta` (
  `id` int(10) NOT NULL,
  `contact_id` bigint(10) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_forms`
--

CREATE TABLE `wp_ig_forms` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `styles` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `af_id` int(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_ig_forms`
--

INSERT INTO `wp_ig_forms` (`id`, `name`, `body`, `settings`, `styles`, `created_at`, `updated_at`, `deleted_at`, `af_id`) VALUES
(1, 'First Form', 'a:4:{i:0;a:5:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:4:\"Name\";s:2:\"id\";s:4:\"name\";s:6:\"params\";a:4:{s:5:\"label\";s:0:\"\";s:12:\"place_holder\";s:0:\"\";s:4:\"show\";b:0;s:8:\"required\";b:0;}s:8:\"position\";i:1;}i:1;a:5:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:5:\"Email\";s:2:\"id\";s:5:\"email\";s:6:\"params\";a:4:{s:5:\"label\";s:0:\"\";s:12:\"place_holder\";s:16:\"Enter Your Email\";s:4:\"show\";b:1;s:8:\"required\";b:1;}s:8:\"position\";i:2;}i:2;a:5:{s:4:\"type\";s:8:\"checkbox\";s:4:\"name\";s:5:\"Lists\";s:2:\"id\";s:5:\"lists\";s:6:\"params\";a:4:{s:5:\"label\";s:5:\"Lists\";s:4:\"show\";b:0;s:8:\"required\";b:1;s:6:\"values\";a:1:{i:0;s:1:\"2\";}}s:8:\"position\";i:3;}i:3;a:5:{s:4:\"type\";s:6:\"submit\";s:4:\"name\";s:6:\"submit\";s:2:\"id\";s:6:\"submit\";s:6:\"params\";a:3:{s:5:\"label\";s:0:\"\";s:4:\"show\";b:1;s:8:\"required\";b:1;}s:8:\"position\";i:4;}}', 'a:3:{s:5:\"lists\";a:1:{i:0;s:1:\"2\";}s:4:\"desc\";s:0:\"\";s:12:\"form_version\";s:3:\"1.0\";}', NULL, '2019-11-04 09:28:56', '2019-11-04 09:49:41', NULL, 0),
(2, 'Subscribe', 'a:4:{i:0;a:5:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:4:\"Name\";s:2:\"id\";s:4:\"name\";s:6:\"params\";a:4:{s:5:\"label\";s:0:\"\";s:12:\"place_holder\";s:0:\"\";s:4:\"show\";b:0;s:8:\"required\";b:0;}s:8:\"position\";i:1;}i:1;a:5:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:5:\"Email\";s:2:\"id\";s:5:\"email\";s:6:\"params\";a:4:{s:5:\"label\";s:0:\"\";s:12:\"place_holder\";s:16:\"Enter Your Email\";s:4:\"show\";b:1;s:8:\"required\";b:1;}s:8:\"position\";i:2;}i:2;a:5:{s:4:\"type\";s:8:\"checkbox\";s:4:\"name\";s:5:\"Lists\";s:2:\"id\";s:5:\"lists\";s:6:\"params\";a:4:{s:5:\"label\";s:5:\"Lists\";s:4:\"show\";b:0;s:8:\"required\";b:1;s:6:\"values\";a:1:{i:0;s:1:\"2\";}}s:8:\"position\";i:3;}i:3;a:5:{s:4:\"type\";s:6:\"submit\";s:4:\"name\";s:6:\"submit\";s:2:\"id\";s:6:\"submit\";s:6:\"params\";a:3:{s:5:\"label\";s:9:\"Subscribe\";s:4:\"show\";b:1;s:8:\"required\";b:1;}s:8:\"position\";i:4;}}', 'a:3:{s:5:\"lists\";a:1:{i:0;s:1:\"2\";}s:4:\"desc\";s:0:\"\";s:12:\"form_version\";s:3:\"1.0\";}', NULL, '2019-11-04 11:24:47', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_links`
--

CREATE TABLE `wp_ig_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message_id` int(10) UNSIGNED NOT NULL,
  `campaign_id` int(10) UNSIGNED NOT NULL,
  `link` varchar(2083) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `i` tinyint(1) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_lists`
--

CREATE TABLE `wp_ig_lists` (
  `id` int(10) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_ig_lists`
--

INSERT INTO `wp_ig_lists` (`id`, `slug`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 'Test', '2019-11-04 09:28:54', NULL, NULL),
(2, 'main', 'Main', '2019-11-04 09:28:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_lists_contacts`
--

CREATE TABLE `wp_ig_lists_contacts` (
  `id` int(10) NOT NULL,
  `list_id` int(10) NOT NULL,
  `contact_id` int(10) NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `optin_type` tinyint(4) NOT NULL,
  `subscribed_at` datetime DEFAULT NULL,
  `subscribed_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unsubscribed_at` datetime DEFAULT NULL,
  `unsubscribed_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_mailing_queue`
--

CREATE TABLE `wp_ig_mailing_queue` (
  `id` int(10) NOT NULL,
  `hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `campaign_id` int(10) NOT NULL DEFAULT 0,
  `subject` text COLLATE utf8mb4_unicode_ci DEFAULT '',
  `body` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_at` datetime DEFAULT NULL,
  `finish_at` datetime DEFAULT NULL,
  `meta` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_ig_mailing_queue`
--

INSERT INTO `wp_ig_mailing_queue` (`id`, `hash`, `campaign_id`, `subject`, `body`, `count`, `status`, `start_at`, `finish_at`, `meta`, `created_at`, `updated_at`) VALUES
(1, 'rpwiuh-rlieuq-bvhewl-slnxmw-dufnzo', 1, 'Welcome To Email Subscribers', '<strong style=\"color: #990000\">What can you achieve using Email Subscribers?</strong><p>Add subscription forms on website, send HTML newsletters & automatically notify subscribers about new blog posts once it is published. You can also Import or Export subscribers from any list to Email Subscribers.</p> <strong style=\"color: #990000\">Plugin Features</strong><ol> <li>Send notification emails to subscribers when new blog posts are published.</li> <li>Subscribe form available with 3 options to setup.</li> <li>Double Opt-In and Single Opt-In support.</li> <li>Email notification to admin when a new user signs up (Optional).</li> <li>Automatic welcome email to subscriber.</li> <li>Auto add unsubscribe link in the email.</li> <li>Import/Export subscriber emails to migrate to any lists.</li> <li>Default WordPress editor to create emails.</li> </ol> <strong>Thanks & Regards,</strong><br>Admin', 2, 'Sent', '2019-11-04 09:29:38', '2019-11-04 09:44:13', NULL, '2019-11-04 09:29:38', '2019-11-04 09:29:38'),
(2, 'reailg-etwhvq-sindjk-oeikuf-ksmubl', 2, 'New Post Published - Where does it come from?', '<p>Hello {{NAME}},</p>\n<p>We have published a new blog article on our website : Where does it come from?<br />\n<a href=\'http://localhost/WpLearning_03/2019/11/02/where-does-it-come-from/\' target=\'_blank\'><img width=\"150\" height=\"150\" src=\"http://localhost/WpLearning_03/wp-content/uploads/2019/11/cat-post-1-150x150.jpg\" class=\"attachment-thumbnail size-thumbnail wp-post-image\" alt=\"\" /></a></p>\n<p>You can view it from this link : <a href=\'http://localhost/WpLearning_03/2019/11/02/where-does-it-come-from/\' target=\'_blank\'>http://localhost/WpLearning_03/2019/11/02/where-does-it-come-from/</a></p>\n<p>Thanks &#038; Regards,<br />\nAdmin</p>\n<p>You received this email because in the past you have provided us your email address : {{EMAIL}} to receive notifications when new updates are posted.</p>\n', 2, 'Sent', '2019-11-04 09:29:41', '2019-11-04 09:59:14', NULL, '2019-11-04 09:29:41', '2019-11-04 09:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_queue`
--

CREATE TABLE `wp_ig_queue` (
  `contact_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `campaign_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `requeued` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `added` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `timestamp` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `sent_at` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `priority` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `count` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `error` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `ignore_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `options` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tags` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ig_sending_queue`
--

CREATE TABLE `wp_ig_sending_queue` (
  `id` int(10) NOT NULL,
  `mailing_queue_id` int(10) NOT NULL DEFAULT 0,
  `mailing_queue_hash` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `campaign_id` int(10) NOT NULL DEFAULT 0,
  `contact_id` int(10) NOT NULL DEFAULT 0,
  `contact_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `links` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opened` int(1) DEFAULT NULL,
  `sent_at` datetime DEFAULT NULL,
  `opened_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_ig_sending_queue`
--

INSERT INTO `wp_ig_sending_queue` (`id`, `mailing_queue_id`, `mailing_queue_hash`, `campaign_id`, `contact_id`, `contact_hash`, `email`, `status`, `links`, `opened`, `sent_at`, `opened_at`) VALUES
(1, 1, 'rpwiuh-rlieuq-bvhewl-slnxmw-dufnzo', 1, 1, 'ifalkq-xmazyi-njelix-wxrebg-askrgj', 'nmnaba14@gmail.com', 'Sent', '', 0, '2019-11-04 09:44:13', '0000-00-00 00:00:00'),
(2, 1, 'rpwiuh-rlieuq-bvhewl-slnxmw-dufnzo', 1, 2, 'fijnav-pkdqfo-wgtvoj-mjopgu-vbwakn', 'nmnaba14@gmail.com', 'Sent', '', 0, '2019-11-04 09:44:13', '0000-00-00 00:00:00'),
(3, 2, 'reailg-etwhvq-sindjk-oeikuf-ksmubl', 2, 1, 'ifalkq-xmazyi-njelix-wxrebg-askrgj', 'nmnaba14@gmail.com', 'Sent', '', 0, '2019-11-04 09:59:14', '0000-00-00 00:00:00'),
(4, 2, 'reailg-etwhvq-sindjk-oeikuf-ksmubl', 2, 2, 'fijnav-pkdqfo-wgtvoj-mjopgu-vbwakn', 'nmnaba14@gmail.com', 'Sent', '', 0, '2019-11-04 09:59:14', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wp_like_dislike_btn_details`
--

CREATE TABLE `wp_like_dislike_btn_details` (
  `id` int(11) NOT NULL,
  `btn_container` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likeDislikeType` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_one_home` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `on_pages` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `on_product_page` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `onshowShare` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_like_dislike_btn_details`
--

INSERT INTO `wp_like_dislike_btn_details` (`id`, `btn_container`, `likeDislikeType`, `show_one_home`, `on_pages`, `on_product_page`, `onshowShare`) VALUES
(1, '<div class=\"button-container-likes-dislike\">\n    <button class=\"btn-start-1\" id=\"post-like-btn\">\n      <i class=\"fa fa-thumbs-up\"></i><span>Like</span><b>224</b>\n    </button>\n        <button class=\"btn-start-1\" id=\"post-dislike-btn\">\n      <i class=\"fa fa-thumbs-down\"></i><span>Dislike</span><b>28</b>\n    </button>\n    </div>', 'cookie-check', 'no', 'no', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_maxbuttonsv3`
--

CREATE TABLE `wp_maxbuttonsv3` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'publish',
  `cache` text DEFAULT NULL,
  `basic` text DEFAULT NULL,
  `color` text DEFAULT NULL,
  `dimension` text DEFAULT NULL,
  `border` text DEFAULT NULL,
  `gradient` text DEFAULT NULL,
  `text` text DEFAULT NULL,
  `container` text DEFAULT NULL,
  `advanced` text DEFAULT NULL,
  `responsive` text DEFAULT NULL,
  `meta` text DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp_maxbuttons_collections`
--

CREATE TABLE `wp_maxbuttons_collections` (
  `meta_id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `collection_key` varchar(255) DEFAULT NULL,
  `collection_value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_maxbuttons_collections`
--

INSERT INTO `wp_maxbuttons_collections` (`meta_id`, `collection_id`, `collection_key`, `collection_value`) VALUES
(1, 1, 'general', '{\"name\":\"\",\"active\":\"1\"}'),
(2, 1, 'network', '{\"network_active\":[\"facebook\",\"twitter\",\"linkedin\",\"instagram\"],\"mbcustom\":[]}'),
(3, 1, 'display', '{\"display_page_options\":[],\"display_post_options\":[],\"display_homepage_options\":[],\"display_archive_options\":[],\"show_desktop\":0,\"show_mobile\":0}'),
(4, 1, 'display_page', 'hidden'),
(5, 1, 'display_post', 'hidden'),
(6, 1, 'display_homepage', 'hidden'),
(7, 1, 'display_archive', 'hidden'),
(8, 1, 'style', '{\"mbs-style\":\"round\",\"mbs-width\":\"40\",\"mbs-height\":\"40\"}'),
(9, 1, 'layout', '{\"margin_left\":\"0\",\"margin_right\":\"0\",\"margin_top\":\"0\",\"margin_bottom\":\"0\",\"orientation\":\"auto\",\"font\":\"\",\"font_label_size\":\"12\",\"font_label_style\":\"normal\",\"font_label_weight\":\"normal\",\"font_label_upper\":\"none\",\"font_icon_size\":\"16\",\"font_icon_style\":\"normal\",\"font_icon_weight\":\"normal\",\"use_background\":\"1\",\"background_color\":\"#000\",\"background_color_hover\":\"#000\",\"color\":\"#fff\",\"color_hover\":\"#fff\",\"button_spacing\":\"10\",\"ignore_container\":0,\"webfonts\":null}'),
(10, 1, 'count', '{\"count_active\":0,\"share_min_count\":\"5\",\"show_total_count\":\"0\",\"total_count_label\":\"Shares\",\"font_count_size\":\"16\",\"font_count_style\":\"normal\",\"font_count_weight\":\"normal\",\"total_count_color\":\"\"}'),
(11, 1, 'effect', '{\"effect_type\":\"transform\",\"scale\":\"120\"}'),
(12, 1, 'profile', '{\"profile_pinterest\":\"\",\"useprofile_pinterest\":0,\"profile_facebook\":\"\",\"useprofile_facebook\":0,\"profile_linkedin\":\"\",\"useprofile_linkedin\":0,\"profile_reddit\":\"\",\"useprofile_reddit\":0,\"profile_instagram\":\"\",\"profile_youtube\":\"\",\"profile_snapchat\":\"\",\"profile_rss\":\"\",\"profile_vimeo\":\"\",\"profile_twitter\":\"\",\"useprofile_twitter\":0,\"profile_vkontakte\":\"\",\"useprofile_vkontakte\":0}'),
(13, 1, 'twitter', '{\"twitter_handle\":\"\",\"twitter_hash\":\"\"}'),
(14, 1, 'email', '{\"email_subject\":\"\",\"email_content\":\"\"}'),
(22, 1, 'collection_type', 'social_share');

-- --------------------------------------------------------

--
-- Table structure for table `wp_maxbuttons_collections_trans`
--

CREATE TABLE `wp_maxbuttons_collections_trans` (
  `name` varchar(1000) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp_newsletter`
--

CREATE TABLE `wp_newsletter` (
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `token` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'S',
  `id` int(11) NOT NULL,
  `profile` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated` int(11) NOT NULL DEFAULT 0,
  `last_activity` int(11) NOT NULL DEFAULT 0,
  `followup_step` tinyint(4) NOT NULL DEFAULT 0,
  `followup_time` bigint(20) NOT NULL DEFAULT 0,
  `followup` tinyint(4) NOT NULL DEFAULT 0,
  `surname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sex` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'n',
  `feed_time` bigint(20) NOT NULL DEFAULT 0,
  `feed` tinyint(4) NOT NULL DEFAULT 0,
  `referrer` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `wp_user_id` int(11) NOT NULL DEFAULT 0,
  `http_referer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `geo` tinyint(4) NOT NULL DEFAULT 0,
  `country` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `region` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bounce_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bounce_time` int(11) NOT NULL DEFAULT 0,
  `unsub_email_id` int(11) NOT NULL DEFAULT 0,
  `unsub_time` int(11) NOT NULL DEFAULT 0,
  `list_1` tinyint(4) NOT NULL DEFAULT 0,
  `list_2` tinyint(4) NOT NULL DEFAULT 0,
  `list_3` tinyint(4) NOT NULL DEFAULT 0,
  `list_4` tinyint(4) NOT NULL DEFAULT 0,
  `list_5` tinyint(4) NOT NULL DEFAULT 0,
  `list_6` tinyint(4) NOT NULL DEFAULT 0,
  `list_7` tinyint(4) NOT NULL DEFAULT 0,
  `list_8` tinyint(4) NOT NULL DEFAULT 0,
  `list_9` tinyint(4) NOT NULL DEFAULT 0,
  `list_10` tinyint(4) NOT NULL DEFAULT 0,
  `list_11` tinyint(4) NOT NULL DEFAULT 0,
  `list_12` tinyint(4) NOT NULL DEFAULT 0,
  `list_13` tinyint(4) NOT NULL DEFAULT 0,
  `list_14` tinyint(4) NOT NULL DEFAULT 0,
  `list_15` tinyint(4) NOT NULL DEFAULT 0,
  `list_16` tinyint(4) NOT NULL DEFAULT 0,
  `list_17` tinyint(4) NOT NULL DEFAULT 0,
  `list_18` tinyint(4) NOT NULL DEFAULT 0,
  `list_19` tinyint(4) NOT NULL DEFAULT 0,
  `list_20` tinyint(4) NOT NULL DEFAULT 0,
  `list_21` tinyint(4) NOT NULL DEFAULT 0,
  `list_22` tinyint(4) NOT NULL DEFAULT 0,
  `list_23` tinyint(4) NOT NULL DEFAULT 0,
  `list_24` tinyint(4) NOT NULL DEFAULT 0,
  `list_25` tinyint(4) NOT NULL DEFAULT 0,
  `list_26` tinyint(4) NOT NULL DEFAULT 0,
  `list_27` tinyint(4) NOT NULL DEFAULT 0,
  `list_28` tinyint(4) NOT NULL DEFAULT 0,
  `list_29` tinyint(4) NOT NULL DEFAULT 0,
  `list_30` tinyint(4) NOT NULL DEFAULT 0,
  `list_31` tinyint(4) NOT NULL DEFAULT 0,
  `list_32` tinyint(4) NOT NULL DEFAULT 0,
  `list_33` tinyint(4) NOT NULL DEFAULT 0,
  `list_34` tinyint(4) NOT NULL DEFAULT 0,
  `list_35` tinyint(4) NOT NULL DEFAULT 0,
  `list_36` tinyint(4) NOT NULL DEFAULT 0,
  `list_37` tinyint(4) NOT NULL DEFAULT 0,
  `list_38` tinyint(4) NOT NULL DEFAULT 0,
  `list_39` tinyint(4) NOT NULL DEFAULT 0,
  `list_40` tinyint(4) NOT NULL DEFAULT 0,
  `profile_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_6` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_7` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_8` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_9` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_10` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_11` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_12` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_13` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_14` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_15` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_16` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_17` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_18` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_19` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `profile_20` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `test` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_newsletter`
--

INSERT INTO `wp_newsletter` (`name`, `email`, `token`, `language`, `status`, `id`, `profile`, `created`, `updated`, `last_activity`, `followup_step`, `followup_time`, `followup`, `surname`, `sex`, `feed_time`, `feed`, `referrer`, `ip`, `wp_user_id`, `http_referer`, `geo`, `country`, `region`, `city`, `bounce_type`, `bounce_time`, `unsub_email_id`, `unsub_time`, `list_1`, `list_2`, `list_3`, `list_4`, `list_5`, `list_6`, `list_7`, `list_8`, `list_9`, `list_10`, `list_11`, `list_12`, `list_13`, `list_14`, `list_15`, `list_16`, `list_17`, `list_18`, `list_19`, `list_20`, `list_21`, `list_22`, `list_23`, `list_24`, `list_25`, `list_26`, `list_27`, `list_28`, `list_29`, `list_30`, `list_31`, `list_32`, `list_33`, `list_34`, `list_35`, `list_36`, `list_37`, `list_38`, `list_39`, `list_40`, `profile_1`, `profile_2`, `profile_3`, `profile_4`, `profile_5`, `profile_6`, `profile_7`, `profile_8`, `profile_9`, `profile_10`, `profile_11`, `profile_12`, `profile_13`, `profile_14`, `profile_15`, `profile_16`, `profile_17`, `profile_18`, `profile_19`, `profile_20`, `test`) VALUES
('', 'nmnaba14@gmail.com', '292bcbb2d7', '', 'C', 1, NULL, '2019-11-04 10:13:48', 0, 0, 0, 0, 0, '', 'n', 0, 0, '', '', 0, '', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_newsletter_emails`
--

CREATE TABLE `wp_newsletter_emails` (
  `id` int(11) NOT NULL,
  `language` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message2` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('new','sending','sent','paused') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'new',
  `total` int(11) NOT NULL DEFAULT 0,
  `last_id` int(11) NOT NULL DEFAULT 0,
  `sent` int(11) NOT NULL DEFAULT 0,
  `track` int(11) NOT NULL DEFAULT 0,
  `list` int(11) NOT NULL DEFAULT 0,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `query` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editor` tinyint(4) NOT NULL DEFAULT 0,
  `sex` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `theme` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message_text` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preferences` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_on` int(11) NOT NULL DEFAULT 0,
  `token` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `options` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `private` tinyint(1) NOT NULL DEFAULT 0,
  `click_count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `version` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `open_count` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_newsletter_sent`
--

CREATE TABLE `wp_newsletter_sent` (
  `email_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `open` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `error` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_newsletter_stats`
--

CREATE TABLE `wp_newsletter_stats` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `email_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `link_id` int(11) NOT NULL DEFAULT 0,
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `country` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_newsletter_user_logs`
--

CREATE TABLE `wp_newsletter_user_logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `ip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `source` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `data` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_nls_subscribers`
--

CREATE TABLE `wp_nls_subscribers` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `terms` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `act_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deact_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/WpLearning_03', 'yes'),
(2, 'home', 'http://localhost/WpLearning_03', 'yes'),
(3, 'blogname', 'My New Wordpress Website', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'nmnaba14@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '4', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:89:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:8:{i:0;s:36:\"contact-form-7/wp-contact-form-7.php\";i:1;s:33:\"instagram-feed/instagram-feed.php\";i:2;s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";i:3;s:25:\"maxbuttons/maxbuttons.php\";i:4;s:29:\"share-button/share-button.php\";i:5;s:17:\"top-10/top-10.php\";i:6;s:31:\"wp-google-maps/wpGoogleMaps.php\";i:7;s:41:\"wp-like-button/crudlab-fb-like-button.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:5:{i:0;s:83:\"D:\\xampp\\htdocs\\WpLearning_03/wp-content/uploads/bws-custom-code/bws-custom-code.js\";i:1;s:84:\"D:\\xampp\\htdocs\\WpLearning_03/wp-content/uploads/bws-custom-code/bws-custom-code.php\";i:2;s:84:\"D:\\xampp\\htdocs\\WpLearning_03/wp-content/uploads/bws-custom-code/bws-custom-code.css\";i:4;s:65:\"D:\\xampp\\htdocs\\WpLearning_03/wp-content/themes/sensive/style.css\";i:5;s:0:\"\";}', 'no'),
(40, 'template', 'sensive', 'yes'),
(41, 'stylesheet', 'sensive', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:6;a:4:{s:5:\"title\";s:8:\"Category\";s:5:\"count\";i:1;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:10:{i:1;a:0:{}i:2;a:4:{s:5:\"title\";s:8:\"About Us\";s:4:\"text\";s:121:\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:3;a:4:{s:5:\"title\";s:15:\"Instragram Feed\";s:4:\"text\";s:16:\"[instagram-feed]\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:4;a:4:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:221:\"Copyright ©<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> by <a href=\"https://www.fiverr.com/noman141\">Noman</a>\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:5;a:4:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:18:\"[maxsocial id=\"1\"]\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:6;a:4:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:8:\"[fblike]\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:7;a:4:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:15:\"[wpgmza id=\"1\"]\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:8;a:4:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:930:\"<div class=\"media contact-info\">\r\n            <span class=\"contact-info__icon\"><i class=\"ti-home\"></i></span>\r\n            <div class=\"media-body\">\r\n              <h3>California United States</h3>\r\n              <p>Santa monica bullevard</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"media contact-info\">\r\n            <span class=\"contact-info__icon\"><i class=\"ti-headphone\"></i></span>\r\n            <div class=\"media-body\">\r\n              <h3><a href=\"tel:454545654\">00 (440) 9865 562</a></h3>\r\n              <p>Mon to Fri 9am to 6pm</p>\r\n            </div>\r\n          </div>\r\n          <div class=\"media contact-info\">\r\n            <span class=\"contact-info__icon\"><i class=\"ti-email\"></i></span>\r\n            <div class=\"media-body\">\r\n              <h3><a href=\"mailto:support@colorlib.com\">support@colorlib.com</a></h3>\r\n              <p>Send us your query anytime!</p>\r\n            </div>\r\n          </div>\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:9;a:4:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:41:\"[contact-form-7 id=\"111\" title=\"contact\"]\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:8:{s:33:\"instagram-feed/instagram-feed.php\";s:22:\"sb_instagram_uninstall\";s:27:\"shareaholic/shareaholic.php\";a:2:{i:0;s:11:\"Shareaholic\";i:1;s:9:\"uninstall\";}s:58:\"ultimate-social-media-plus/ultimate_social_media_icons.php\";a:2:{i:0;s:15:\"Account\\Account\";i:1;s:25:\"onUninstallPluginListener\";}s:59:\"ultimate-social-media-icons/ultimate_social_media_icons.php\";s:20:\"sfsi_Unistall_plugin\";s:27:\"blog2social/blog2social.php\";s:15:\"uninstallPlugin\";s:41:\"wp-like-button/crudlab-fb-like-button.php\";a:2:{i:0;s:8:\"CLFBLBtn\";i:1;s:19:\"fblb_uninstall_hook\";}s:31:\"wti-like-post/wti_like_post.php\";s:23:\"UnsetOptionsWtiLikePost\";s:53:\"aspexi-facebook-like-box/aspexi-facebook-like-box.php\";a:2:{i:0;s:15:\"AspexiFBlikebox\";i:1;s:9:\"uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:69:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:29:\"manage_instagram_feed_options\";b:1;s:18:\"blog2social_access\";b:1;s:11:\"hustle_menu\";b:1;s:18:\"hustle_edit_module\";b:1;s:13:\"hustle_create\";b:1;s:24:\"hustle_edit_integrations\";b:1;s:20:\"hustle_access_emails\";b:1;s:20:\"hustle_edit_settings\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"blog2social_access\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:11:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:18:\"blog2social_access\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:6:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:18:\"blog2social_access\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:3:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;s:18:\"blog2social_access\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:14:{s:16:\"post_like_widget\";a:1:{i:0;s:6:\"text-6\";}s:19:\"wp_inactive_widgets\";a:0:{}s:15:\"category_widget\";a:1:{i:0;s:12:\"categories-6\";}s:19:\"popular_post_widget\";a:1:{i:0;s:17:\"widget_tptn_pop-2\";}s:11:\"tags_widget\";a:1:{i:0;s:11:\"tag_cloud-3\";}s:12:\"about_widget\";a:1:{i:0;s:6:\"text-2\";}s:16:\"instagram_widget\";a:1:{i:0;s:6:\"text-3\";}s:16:\"subscribe_widget\";a:1:{i:0;s:19:\"mc4wp_form_widget-2\";}s:16:\"copyright_widget\";a:1:{i:0;s:6:\"text-4\";}s:18:\"socialshare_widget\";a:1:{i:0;s:6:\"text-5\";}s:17:\"google_map_widget\";a:1:{i:0;s:6:\"text-7\";}s:14:\"address_widget\";a:1:{i:0;s:6:\"text-8\";}s:19:\"contact_form_widget\";a:1:{i:0;s:6:\"text-9\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'cron', 'a:10:{i:1575133809;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1575135417;a:1:{s:38:\"hustle_general_data_protection_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1575137407;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575137409;a:1:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1575137410;a:2:{s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1575137419;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575137423;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575194770;a:1:{s:36:\"sfsi_plus_sf_instagram_count_fetcher\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575197080;a:1:{s:17:\"mashsb_cron_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:2:{i:3;a:3:{s:5:\"title\";s:4:\"Tags\";s:5:\"count\";i:0;s:8:\"taxonomy\";s:8:\"post_tag\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'recovery_keys', 'a:0:{}', 'yes'),
(114, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1572525873;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(115, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.3.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.3-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.3-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.3\";s:7:\"version\";s:3:\"5.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.3.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.3-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.3-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.3\";s:7:\"version\";s:3:\"5.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1575132225;s:15:\"version_checked\";s:5:\"5.2.4\";s:12:\"translations\";a:0:{}}', 'no'),
(120, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1575132232;s:7:\"checked\";a:4:{s:7:\"sensive\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(140, 'can_compress_scripts', '1', 'no'),
(157, 'current_theme', 'Sensive WP Theme', 'yes'),
(158, 'theme_mods_sensive', 'a:5:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:8:\"mainmenu\";i:2;s:10:\"socialmenu\";i:3;}s:18:\"custom_css_post_id\";i:-1;s:11:\"header_text\";i:1;s:11:\"custom_logo\";i:75;}', 'yes'),
(159, 'theme_switched', '', 'yes'),
(164, 'recovery_mode_email_last_sent', '1574312530', 'yes'),
(168, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(171, 'recently_activated', 'a:4:{s:62:\"simple-google-maps-short-code/simple-google-map-short-code.php\";i:1574317302;s:29:\"acf-extended/acf-extended.php\";i:1573976952;s:30:\"advanced-custom-fields/acf.php\";i:1573976814;s:53:\"custom-content-shortcode/custom-content-shortcode.php\";i:1573976623;}', 'yes'),
(176, 'responsive_menu_version', '3.1.24', 'yes'),
(179, 'wpr_optionsframework', 'a:1:{s:2:\"id\";s:15:\"wprmenu_options\";}', 'yes'),
(182, 'wprmenu_options', 'a:85:{s:7:\"enabled\";s:1:\"1\";s:16:\"wpr_live_preview\";s:1:\"1\";s:21:\"search_box_menu_block\";s:1:\"1\";s:17:\"wpr_enable_widget\";b:0;s:18:\"search_box_menubar\";b:0;s:7:\"rtlview\";b:0;s:14:\"submenu_opened\";b:0;s:24:\"fullwidth_menu_container\";b:0;s:4:\"menu\";s:0:\"\";s:4:\"hide\";s:0:\"\";s:15:\"search_box_text\";s:9:\"Search...\";s:9:\"bar_title\";s:4:\"MENU\";s:8:\"bar_logo\";s:0:\"\";s:12:\"bar_logo_pos\";s:4:\"left\";s:9:\"logo_link\";s:30:\"http://localhost/WpLearning_03\";s:5:\"swipe\";s:3:\"yes\";s:7:\"zooming\";s:2:\"no\";s:12:\"parent_click\";s:3:\"yes\";s:23:\"wpr_enable_external_css\";s:2:\"no\";s:17:\"wpr_enable_minify\";s:2:\"no\";s:27:\"content_before_menu_element\";s:0:\"\";s:26:\"content_after_menu_element\";s:0:\"\";s:18:\"header_menu_height\";s:2:\"42\";s:9:\"menu_type\";s:7:\"default\";s:15:\"custom_menu_top\";s:1:\"0\";s:15:\"menu_symbol_pos\";s:4:\"left\";s:16:\"custom_menu_left\";s:1:\"0\";s:20:\"custom_menu_bg_color\";s:7:\"#CCCCCC\";s:19:\"menu_icon_animation\";s:17:\"hamburger--slider\";s:10:\"slide_type\";s:9:\"bodyslide\";s:8:\"position\";s:4:\"left\";s:10:\"from_width\";s:3:\"768\";s:8:\"how_wide\";s:2:\"80\";s:14:\"menu_max_width\";s:3:\"400\";s:15:\"menu_title_size\";s:2:\"20\";s:17:\"menu_title_weight\";s:6:\"normal\";s:14:\"menu_font_size\";s:2:\"15\";s:16:\"menu_font_weight\";s:6:\"normal\";s:19:\"menu_font_text_type\";s:9:\"uppercase\";s:17:\"submenu_alignment\";s:4:\"left\";s:18:\"sub_menu_font_size\";s:2:\"15\";s:20:\"sub_menu_font_weight\";s:6:\"normal\";s:23:\"sub_menu_font_text_type\";s:9:\"uppercase\";s:30:\"cart_contents_bubble_text_size\";s:2:\"12\";s:23:\"menu_border_bottom_show\";s:3:\"yes\";s:23:\"menu_border_top_opacity\";s:4:\"0.05\";s:26:\"menu_border_bottom_opacity\";s:4:\"0.05\";s:7:\"menu_bg\";s:0:\"\";s:12:\"menu_bg_size\";s:5:\"cover\";s:11:\"menu_bg_rep\";s:6:\"repeat\";s:11:\"menu_bar_bg\";s:0:\"\";s:16:\"menu_bar_bg_size\";s:5:\"cover\";s:15:\"menu_bar_bg_rep\";s:6:\"repeat\";s:14:\"enable_overlay\";s:1:\"1\";s:31:\"menu_background_overlay_opacity\";s:4:\"0.83\";s:21:\"widget_menu_font_size\";s:2:\"28\";s:24:\"widget_menu_top_position\";s:1:\"0\";s:22:\"widget_menu_icon_color\";s:7:\"#FFFFFF\";s:29:\"widget_menu_icon_active_color\";s:7:\"#FFFFFF\";s:20:\"widget_menu_bg_color\";s:7:\"#c82d2d\";s:22:\"widget_menu_text_color\";s:7:\"#FFFFFF\";s:26:\"widget_menu_open_direction\";s:4:\"left\";s:7:\"bar_bgd\";s:7:\"#C92C2C\";s:9:\"bar_color\";s:7:\"#FFFFFF\";s:8:\"menu_bgd\";s:7:\"#c82d2d\";s:10:\"menu_color\";s:7:\"#FFFFFF\";s:16:\"menu_color_hover\";s:7:\"#FFFFFF\";s:15:\"menu_textovrbgd\";s:7:\"#d53f3f\";s:17:\"active_menu_color\";s:7:\"#FFFFFF\";s:20:\"active_menu_bg_color\";s:7:\"#d53f3f\";s:15:\"menu_icon_color\";s:7:\"#FFFFFF\";s:21:\"menu_icon_hover_color\";s:7:\"#FFFFFF\";s:15:\"menu_border_top\";s:7:\"#FFFFFF\";s:18:\"menu_border_bottom\";s:7:\"#FFFFFF\";s:17:\"social_icon_color\";s:7:\"#FFFFFF\";s:23:\"social_icon_hover_color\";s:7:\"#FFFFFF\";s:17:\"search_icon_color\";s:7:\"#FFFFFF\";s:23:\"search_icon_hover_color\";s:7:\"#FFFFFF\";s:16:\"google_font_type\";s:0:\"\";s:18:\"google_font_family\";s:0:\"\";s:22:\"google_web_font_family\";s:0:\"\";s:14:\"menu_icon_type\";s:7:\"default\";s:21:\"custom_menu_font_size\";s:2:\"40\";s:20:\"custom_menu_icon_top\";s:2:\"-7\";s:21:\"social_icon_font_size\";s:2:\"16\";}', 'yes'),
(189, 'widget_maxmegamenu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(190, 'megamenu_version', '2.7.2', 'yes'),
(191, 'megamenu_initial_version', '2.7.2', 'yes'),
(192, 'megamenu_multisite_share_themes', 'false', 'yes'),
(193, 'megamenu_settings', 'a:3:{s:6:\"prefix\";s:8:\"disabled\";s:12:\"descriptions\";s:7:\"enabled\";s:12:\"second_click\";s:2:\"go\";}', 'yes'),
(196, 'su_option_custom-formatting', 'on', 'no'),
(197, 'su_option_skip', 'on', 'no'),
(198, 'su_option_prefix', 'su_', 'no'),
(199, 'su_option_custom-css', '', 'no'),
(200, 'su_option_supported_blocks', 'a:3:{i:0;s:14:\"core/paragraph\";i:1;s:14:\"core/shortcode\";i:2;s:13:\"core/freeform\";}', 'no'),
(201, 'su_option_generator_access', 'manage_options', 'no'),
(202, 'su_option_enable_shortcodes_in', 'a:1:{i:0;s:16:\"term_description\";}', 'no'),
(203, 'su_option_hide_deprecated', 'on', 'no'),
(204, 'widget_shortcodes-ultimate', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(205, 'su_option_version', '5.6.0', 'no'),
(206, 'su_option_dismissed_notices', 'a:1:{s:4:\"rate\";i:1573144556;}', 'yes'),
(286, 'category_children', 'a:0:{}', 'yes'),
(332, '_transient_health-check-site-status-result', '{\"good\":\"12\",\"recommended\":\"5\",\"critical\":\"0\"}', 'yes'),
(362, 'do_activate', '0', 'yes'),
(367, '_transient_timeout_jetpack_file_data_7.8', '1575287145', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(368, '_transient_jetpack_file_data_7.8', 'a:51:{s:32:\"212a162108f1dc20cc6c768d5b47d4f2\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"d3576702faeb399eb47ad20f586c3804\";a:14:{s:4:\"name\";s:8:\"Carousel\";s:11:\"description\";s:75:\"Display images and galleries in a gorgeous, full-screen browsing experience\";s:4:\"sort\";s:2:\"22\";s:20:\"recommendation_order\";s:2:\"12\";s:10:\"introduced\";s:3:\"1.5\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:17:\"Photos and Videos\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:80:\"gallery, carousel, diaporama, slideshow, images, lightbox, exif, metadata, image\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"55409a5f8388b8d33e2350ef80de3ea3\";a:14:{s:4:\"name\";s:13:\"Comment Likes\";s:11:\"description\";s:64:\"Increase visitor engagement by adding a Like button to comments.\";s:4:\"sort\";s:2:\"39\";s:20:\"recommendation_order\";s:2:\"17\";s:10:\"introduced\";s:3:\"5.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:37:\"like widget, like button, like, likes\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"e914e6d31cb61f5a9ef86e1b9573430e\";a:14:{s:4:\"name\";s:8:\"Comments\";s:11:\"description\";s:81:\"Let visitors use a WordPress.com, Twitter, Facebook, or Google account to comment\";s:4:\"sort\";s:2:\"20\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:53:\"comments, comment, facebook, twitter, google+, social\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"f1b8c61705fb18eb8c8584c9f9cdffd9\";a:14:{s:4:\"name\";s:12:\"Contact Form\";s:11:\"description\";s:81:\"Add a customizable contact form to any post or page using the Jetpack Form Block.\";s:4:\"sort\";s:2:\"15\";s:20:\"recommendation_order\";s:2:\"14\";s:10:\"introduced\";s:3:\"1.3\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:5:\"Other\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:214:\"contact, form, grunion, feedback, submission, contact form, email, feedback, contact form plugin, custom form, custom form plugin, form builder, forms, form maker, survey, contact by jetpack, contact us, forms free\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"4fca6eb23a793155d69fdb119a094926\";a:14:{s:4:\"name\";s:9:\"Copy Post\";s:11:\"description\";s:77:\"Enable the option to copy entire posts and pages, including tags and settings\";s:4:\"sort\";s:2:\"15\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"7.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:15:\"copy, duplicate\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"cfdac01e3c3c529f93a8f49edef1f5db\";a:14:{s:4:\"name\";s:20:\"Custom content types\";s:11:\"description\";s:74:\"Display different types of content on your site with custom content types.\";s:4:\"sort\";s:2:\"34\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"3.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:72:\"cpt, custom post types, portfolio, portfolios, testimonial, testimonials\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"4b9137ecf507290743735fb1f94535df\";a:14:{s:4:\"name\";s:10:\"Custom CSS\";s:11:\"description\";s:88:\"Adds options for CSS preprocessor use, disabling the theme\'s CSS, or custom image width.\";s:4:\"sort\";s:1:\"2\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.7\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:108:\"css, customize, custom, style, editor, less, sass, preprocessor, font, mobile, appearance, theme, stylesheet\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"95d75b38d76d2ee1b5b537026eadb8ff\";a:14:{s:4:\"name\";s:21:\"Enhanced Distribution\";s:11:\"description\";s:27:\"Increase reach and traffic.\";s:4:\"sort\";s:1:\"5\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:6:\"Public\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:54:\"google, seo, firehose, search, broadcast, broadcasting\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"f1bb571a95c5de1e6adaf9db8567c039\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"822f9ef1281dace3fb7cc420c77d24e0\";a:14:{s:4:\"name\";s:16:\"Google Analytics\";s:11:\"description\";s:56:\"Set up Google Analytics without touching a line of code.\";s:4:\"sort\";s:2:\"37\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"4.5\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:37:\"webmaster, google, analytics, console\";s:12:\"plan_classes\";s:17:\"business, premium\";}s:32:\"c167275f926ef0eefaec9a679bd88d34\";a:14:{s:4:\"name\";s:19:\"Gravatar Hovercards\";s:11:\"description\";s:58:\"Enable pop-up business cards over commenters’ Gravatars.\";s:4:\"sort\";s:2:\"11\";s:20:\"recommendation_order\";s:2:\"13\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:18:\"Social, Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:20:\"gravatar, hovercards\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"58cbd4585a74829a1c88aa9c295f3993\";a:14:{s:4:\"name\";s:15:\"Infinite Scroll\";s:11:\"description\";s:53:\"Automatically load new content when a visitor scrolls\";s:4:\"sort\";s:2:\"26\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:33:\"scroll, infinite, infinite scroll\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"d4a35eabc948caefad71a0d3303b95c8\";a:14:{s:4:\"name\";s:8:\"JSON API\";s:11:\"description\";s:51:\"Allow applications to securely access your content.\";s:4:\"sort\";s:2:\"19\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:6:\"Public\";s:11:\"module_tags\";s:19:\"Writing, Developers\";s:7:\"feature\";s:7:\"General\";s:25:\"additional_search_queries\";s:50:\"api, rest, develop, developers, json, klout, oauth\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"7b0c670bc3f8209dc83abb8610e23a89\";a:14:{s:4:\"name\";s:14:\"Beautiful Math\";s:11:\"description\";s:74:\"Use the LaTeX markup language to write mathematical equations and formulas\";s:4:\"sort\";s:2:\"12\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:47:\"latex, math, equation, equations, formula, code\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"b00e4e6c109ce6f77b5c83fbaaaead4c\";a:14:{s:4:\"name\";s:11:\"Lazy Images\";s:11:\"description\";s:137:\"Speed up your site and create a smoother viewing experience by loading images as visitors scroll down the screen, instead of all at once.\";s:4:\"sort\";s:2:\"24\";s:20:\"recommendation_order\";s:2:\"14\";s:10:\"introduced\";s:5:\"5.6.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:23:\"Appearance, Recommended\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:150:\"mobile, theme, fast images, fast image, image, lazy, lazy load, lazyload, images, lazy images, thumbnail, image lazy load, lazy loading, load, loading\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"8e46c72906c928eca634ac2c8b1bc84f\";a:14:{s:4:\"name\";s:5:\"Likes\";s:11:\"description\";s:63:\"Give visitors an easy way to show they appreciate your content.\";s:4:\"sort\";s:2:\"23\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:26:\"like, likes, wordpress.com\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"2df2264a07aff77e0556121e33349dce\";a:14:{s:4:\"name\";s:8:\"Markdown\";s:11:\"description\";s:50:\"Write posts or pages in plain-text Markdown syntax\";s:4:\"sort\";s:2:\"31\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.8\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:12:\"md, markdown\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"0337eacae47d30c946cb9fc4e5ece649\";a:14:{s:4:\"name\";s:21:\"WordPress.com Toolbar\";s:11:\"description\";s:91:\"Replaces the admin bar with a useful toolbar to quickly manage your site via WordPress.com.\";s:4:\"sort\";s:2:\"38\";s:20:\"recommendation_order\";s:2:\"16\";s:10:\"introduced\";s:3:\"4.8\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"General\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:19:\"adminbar, masterbar\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"cb5d81445061b89d19cb9c7754697a39\";a:14:{s:4:\"name\";s:12:\"Mobile Theme\";s:11:\"description\";s:31:\"Enable the Jetpack Mobile theme\";s:4:\"sort\";s:2:\"21\";s:20:\"recommendation_order\";s:2:\"11\";s:10:\"introduced\";s:3:\"1.8\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:31:\"Appearance, Mobile, Recommended\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:24:\"mobile, theme, minileven\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"ea0fbbd64080c81a90a784924603588c\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"5c53fdb3633ba3232f60180116900273\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"40b97d9ce396339d3e8e46b833a045b5\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"0739df64747f2d02c140f23ce6c19cd8\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"c54bb0a65b39f1316da8632197a88a4e\";a:14:{s:4:\"name\";s:7:\"Monitor\";s:11:\"description\";s:118:\"Jetpack’s downtime monitoring will continuously watch your site, and alert you the moment that downtime is detected.\";s:4:\"sort\";s:2:\"28\";s:20:\"recommendation_order\";s:2:\"10\";s:10:\"introduced\";s:3:\"2.6\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:11:\"Recommended\";s:7:\"feature\";s:8:\"Security\";s:25:\"additional_search_queries\";s:123:\"monitor, uptime, downtime, monitoring, maintenance, maintenance mode, offline, site is down, site down, down, repair, error\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"cc013f4c5480c7bdc1e7edb2f410bf3c\";a:14:{s:4:\"name\";s:13:\"Notifications\";s:11:\"description\";s:57:\"Receive instant notifications of site comments and likes.\";s:4:\"sort\";s:2:\"13\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:5:\"Other\";s:7:\"feature\";s:7:\"General\";s:25:\"additional_search_queries\";s:62:\"notification, notifications, toolbar, adminbar, push, comments\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"b3b34928b1e549bb52f866accc0450c5\";a:14:{s:4:\"name\";s:9:\"Asset CDN\";s:11:\"description\";s:154:\"Jetpack’s Site Accelerator loads your site faster by optimizing your images and serving your images and static files from our global network of servers.\";s:4:\"sort\";s:2:\"26\";s:20:\"recommendation_order\";s:1:\"1\";s:10:\"introduced\";s:3:\"6.6\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:42:\"Photos and Videos, Appearance, Recommended\";s:7:\"feature\";s:23:\"Recommended, Appearance\";s:25:\"additional_search_queries\";s:160:\"site accelerator, accelerate, static, assets, javascript, css, files, performance, cdn, bandwidth, content delivery network, pagespeed, combine js, optimize css\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"714284944f56d6936a40f3309900bc8e\";a:14:{s:4:\"name\";s:9:\"Image CDN\";s:11:\"description\";s:141:\"Mirrors and serves your images from our free and fast image CDN, improving your site’s performance with no additional load on your servers.\";s:4:\"sort\";s:2:\"25\";s:20:\"recommendation_order\";s:1:\"1\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:42:\"Photos and Videos, Appearance, Recommended\";s:7:\"feature\";s:23:\"Recommended, Appearance\";s:25:\"additional_search_queries\";s:171:\"photon, photo cdn, image cdn, speed, compression, resize, responsive images, responsive, content distribution network, optimize, page speed, image optimize, photon jetpack\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"348754bc914ee02c72d9af445627784c\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"041704e207c4c59eea93e0499c908bff\";a:14:{s:4:\"name\";s:13:\"Post by email\";s:11:\"description\";s:33:\"Publish posts by sending an email\";s:4:\"sort\";s:2:\"14\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:7:\"Writing\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:20:\"post by email, email\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"26e6cb3e08a6cfd0811c17e7c633c72c\";a:14:{s:4:\"name\";s:7:\"Protect\";s:11:\"description\";s:151:\"Enabling brute force protection will prevent bots and hackers from attempting to log in to your website with common username and password combinations.\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:1:\"4\";s:10:\"introduced\";s:3:\"3.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:11:\"Recommended\";s:7:\"feature\";s:8:\"Security\";s:25:\"additional_search_queries\";s:173:\"security, jetpack protect, secure, protection, botnet, brute force, protect, login, bot, password, passwords, strong passwords, strong password, wp-login.php,  protect admin\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"915a504082f797395713fd01e0e2e713\";a:14:{s:4:\"name\";s:9:\"Publicize\";s:11:\"description\";s:128:\"Publicize makes it easy to share your site’s posts on several social media networks automatically when you publish a new post.\";s:4:\"sort\";s:2:\"10\";s:20:\"recommendation_order\";s:1:\"7\";s:10:\"introduced\";s:3:\"2.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:19:\"Social, Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:220:\"facebook, jetpack publicize, twitter, tumblr, linkedin, social, tweet, connections, sharing, social media, automated, automated sharing, auto publish, auto tweet and like, auto tweet, facebook auto post, facebook posting\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"a7b21cc562ee9ffa357bba19701fe45b\";a:14:{s:4:\"name\";s:20:\"Progressive Web Apps\";s:11:\"description\";s:85:\"Speed up and improve the reliability of your site using the latest in web technology.\";s:4:\"sort\";s:2:\"38\";s:20:\"recommendation_order\";s:2:\"18\";s:10:\"introduced\";s:5:\"5.6.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Developers\";s:7:\"feature\";s:7:\"Traffic\";s:25:\"additional_search_queries\";s:26:\"manifest, pwa, progressive\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"9243c1a718566213f4eaf3b44cf14b07\";a:14:{s:4:\"name\";s:13:\"Related posts\";s:11:\"description\";s:113:\"Keep visitors engaged on your blog by highlighting relevant and new content at the bottom of each published post.\";s:4:\"sort\";s:2:\"29\";s:20:\"recommendation_order\";s:1:\"9\";s:10:\"introduced\";s:3:\"2.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:11:\"Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:360:\"related, jetpack related posts, related posts for wordpress, related posts, popular posts, popular, related content, related post, contextual, context, contextual related posts, related articles, similar posts, easy related posts, related page, simple related posts, free related posts, related thumbnails, similar, engagement, yet another related posts plugin\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"583e4cda5596ee1b28a19cde33f438be\";a:14:{s:4:\"name\";s:6:\"Search\";s:11:\"description\";s:87:\"Enhanced search, powered by Elasticsearch, a powerful replacement for WordPress search.\";s:4:\"sort\";s:2:\"34\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"5.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:5:\"false\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:6:\"Search\";s:25:\"additional_search_queries\";s:110:\"search, elastic, elastic search, elasticsearch, fast search, search results, search performance, google search\";s:12:\"plan_classes\";s:8:\"business\";}s:32:\"15346c1f7f2a5f29d34378774ecfa830\";a:14:{s:4:\"name\";s:9:\"SEO Tools\";s:11:\"description\";s:50:\"Better results on search engines and social media.\";s:4:\"sort\";s:2:\"35\";s:20:\"recommendation_order\";s:2:\"15\";s:10:\"introduced\";s:3:\"4.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:18:\"Social, Appearance\";s:7:\"feature\";s:7:\"Traffic\";s:25:\"additional_search_queries\";s:81:\"search engine optimization, social preview, meta description, custom title format\";s:12:\"plan_classes\";s:17:\"business, premium\";}s:32:\"72a0ff4cfae86074a7cdd2dcd432ef11\";a:14:{s:4:\"name\";s:7:\"Sharing\";s:11:\"description\";s:120:\"Add Twitter, Facebook and Google+ buttons at the bottom of each post, making it easy for visitors to share your content.\";s:4:\"sort\";s:1:\"7\";s:20:\"recommendation_order\";s:1:\"6\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:3:\"1.2\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:19:\"Social, Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:229:\"share, sharing, sharedaddy, social buttons, buttons, share facebook, share twitter, social media sharing, social media share, social share, icons, email, facebook, twitter, linkedin, pinterest, pocket, social widget, social media\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"bb8c6c190aaec212a7ab6e940165af4d\";a:14:{s:4:\"name\";s:16:\"Shortcode Embeds\";s:11:\"description\";s:177:\"Shortcodes are WordPress-specific markup that let you add media from popular sites. This feature is no longer necessary as the editor now handles media embeds rather gracefully.\";s:4:\"sort\";s:1:\"3\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:3:\"1.2\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:46:\"Photos and Videos, Social, Writing, Appearance\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:236:\"shortcodes, shortcode, embeds, media, bandcamp, dailymotion, facebook, flickr, google calendars, google maps, google+, polldaddy, recipe, recipes, scribd, slideshare, slideshow, slideshows, soundcloud, ted, twitter, vimeo, vine, youtube\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"1abd31fe07ae4fb0f8bb57dc24592219\";a:14:{s:4:\"name\";s:16:\"WP.me Shortlinks\";s:11:\"description\";s:82:\"Generates shorter links so you can have more space to write on social media sites.\";s:4:\"sort\";s:1:\"8\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:17:\"shortlinks, wp.me\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"cae5f097f8d658e0b0ae50733d7c6476\";a:14:{s:4:\"name\";s:8:\"Sitemaps\";s:11:\"description\";s:50:\"Make it easy for search engines to find your site.\";s:4:\"sort\";s:2:\"13\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"3.9\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:20:\"Recommended, Traffic\";s:7:\"feature\";s:11:\"Recommended\";s:25:\"additional_search_queries\";s:39:\"sitemap, traffic, search, site map, seo\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"e9b8318133b2f95e7906cedb3557a87d\";a:14:{s:4:\"name\";s:14:\"Secure Sign On\";s:11:\"description\";s:63:\"Allow users to log in to this site using WordPress.com accounts\";s:4:\"sort\";s:2:\"30\";s:20:\"recommendation_order\";s:1:\"5\";s:10:\"introduced\";s:3:\"2.6\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Developers\";s:7:\"feature\";s:8:\"Security\";s:25:\"additional_search_queries\";s:34:\"sso, single sign on, login, log in\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"17e66a12031ccf11d8d45ceee0955f05\";a:14:{s:4:\"name\";s:10:\"Site Stats\";s:11:\"description\";s:44:\"Collect valuable traffic stats and insights.\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:1:\"2\";s:10:\"introduced\";s:3:\"1.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:23:\"Site Stats, Recommended\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:54:\"statistics, tracking, analytics, views, traffic, stats\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"346cf9756e7c1252acecb9a8ca81a21c\";a:14:{s:4:\"name\";s:13:\"Subscriptions\";s:11:\"description\";s:58:\"Let visitors subscribe to new posts and comments via email\";s:4:\"sort\";s:1:\"9\";s:20:\"recommendation_order\";s:1:\"8\";s:10:\"introduced\";s:3:\"1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:6:\"Social\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:74:\"subscriptions, subscription, email, follow, followers, subscribers, signup\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"4f84d218792a6efa06ed6feae09c4dd5\";a:14:{s:4:\"name\";s:0:\"\";s:11:\"description\";s:0:\"\";s:4:\"sort\";s:0:\"\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:0:\"\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:0:\"\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:0:\"\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"ca086af79d0d9dccacc934ccff5b4fd7\";a:14:{s:4:\"name\";s:15:\"Tiled Galleries\";s:11:\"description\";s:61:\"Display image galleries in a variety of elegant arrangements.\";s:4:\"sort\";s:2:\"24\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.1\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:17:\"Photos and Videos\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:43:\"gallery, tiles, tiled, grid, mosaic, images\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"43c24feb7c541c376af93e0251c1a261\";a:14:{s:4:\"name\";s:20:\"Backups and Scanning\";s:11:\"description\";s:100:\"Protect your site with daily or real-time backups and automated virus scanning and threat detection.\";s:4:\"sort\";s:2:\"32\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:5:\"0:1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:5:\"false\";s:4:\"free\";s:5:\"false\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:16:\"Security, Health\";s:25:\"additional_search_queries\";s:386:\"backup, cloud backup, database backup, restore, wordpress backup, backup plugin, wordpress backup plugin, back up, backup wordpress, backwpup, vaultpress, backups, off-site backups, offsite backup, offsite, off-site, antivirus, malware scanner, security, virus, viruses, prevent viruses, scan, anti-virus, antimalware, protection, safe browsing, malware, wp security, wordpress security\";s:12:\"plan_classes\";s:27:\"personal, business, premium\";}s:32:\"b9396d8038fc29140b499098d2294d79\";a:14:{s:4:\"name\";s:17:\"Site verification\";s:11:\"description\";s:58:\"Establish your site\'s authenticity with external services.\";s:4:\"sort\";s:2:\"33\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"3.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:3:\"Yes\";s:11:\"module_tags\";s:0:\"\";s:7:\"feature\";s:10:\"Engagement\";s:25:\"additional_search_queries\";s:56:\"webmaster, seo, google, bing, pinterest, search, console\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"afe184082e106c1bdfe1ee844f98aef3\";a:14:{s:4:\"name\";s:10:\"VideoPress\";s:11:\"description\";s:101:\"Save on hosting storage and bandwidth costs by streaming fast, ad-free video from our global network.\";s:4:\"sort\";s:2:\"27\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.5\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:5:\"false\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:0:\"\";s:11:\"module_tags\";s:17:\"Photos and Videos\";s:7:\"feature\";s:7:\"Writing\";s:25:\"additional_search_queries\";s:118:\"video, videos, videopress, video gallery, video player, videoplayer, mobile video, vimeo, youtube, html5 video, stream\";s:12:\"plan_classes\";s:17:\"business, premium\";}s:32:\"44637d43460370af9a1b31ce3ccec0cd\";a:14:{s:4:\"name\";s:17:\"Widget Visibility\";s:11:\"description\";s:42:\"Control where widgets appear on your site.\";s:4:\"sort\";s:2:\"17\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"2.4\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:10:\"Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:54:\"widget visibility, logic, conditional, widgets, widget\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"694c105a5c3b659acfcddad220048d08\";a:14:{s:4:\"name\";s:21:\"Extra Sidebar Widgets\";s:11:\"description\";s:49:\"Provides additional widgets for use on your site.\";s:4:\"sort\";s:1:\"4\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:3:\"1.2\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:2:\"No\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:18:\"Social, Appearance\";s:7:\"feature\";s:10:\"Appearance\";s:25:\"additional_search_queries\";s:65:\"widget, widgets, facebook, gallery, twitter, gravatar, image, rss\";s:12:\"plan_classes\";s:0:\"\";}s:32:\"ae15da72c5802d72f320640bad669561\";a:14:{s:4:\"name\";s:3:\"Ads\";s:11:\"description\";s:60:\"Earn income by allowing Jetpack to display high quality ads.\";s:4:\"sort\";s:1:\"1\";s:20:\"recommendation_order\";s:0:\"\";s:10:\"introduced\";s:5:\"4.5.0\";s:7:\"changed\";s:0:\"\";s:10:\"deactivate\";s:0:\"\";s:4:\"free\";s:0:\"\";s:19:\"requires_connection\";s:3:\"Yes\";s:13:\"auto_activate\";s:2:\"No\";s:11:\"module_tags\";s:19:\"Traffic, Appearance\";s:7:\"feature\";s:0:\"\";s:25:\"additional_search_queries\";s:26:\"advertising, ad codes, ads\";s:12:\"plan_classes\";s:17:\"premium, business\";}}', 'no'),
(410, 'jetpack_constants_sync_checksum', 'a:20:{s:16:\"EMPTY_TRASH_DAYS\";i:2473281379;s:17:\"WP_POST_REVISIONS\";i:4261170317;s:26:\"AUTOMATIC_UPDATER_DISABLED\";i:634125391;s:7:\"ABSPATH\";i:4293097813;s:14:\"WP_CONTENT_DIR\";i:1047791152;s:9:\"FS_METHOD\";i:634125391;s:18:\"DISALLOW_FILE_EDIT\";i:634125391;s:18:\"DISALLOW_FILE_MODS\";i:634125391;s:19:\"WP_AUTO_UPDATE_CORE\";i:634125391;s:22:\"WP_HTTP_BLOCK_EXTERNAL\";i:634125391;s:19:\"WP_ACCESSIBLE_HOSTS\";i:634125391;s:16:\"JETPACK__VERSION\";i:3774078356;s:12:\"IS_PRESSABLE\";i:634125391;s:15:\"DISABLE_WP_CRON\";i:634125391;s:17:\"ALTERNATE_WP_CRON\";i:634125391;s:20:\"WP_CRON_LOCK_TIMEOUT\";i:3994858278;s:11:\"PHP_VERSION\";i:787594639;s:15:\"WP_MEMORY_LIMIT\";i:3065409971;s:19:\"WP_MAX_MEMORY_LIMIT\";i:1474498405;s:8:\"WP_DEBUG\";i:734881840;}', 'yes'),
(411, 'jetpack_sync_https_history_main_network_site_url', 'a:1:{i:0;s:4:\"http\";}', 'yes'),
(412, 'jetpack_sync_https_history_site_url', 'a:2:{i:0;s:4:\"http\";i:1;s:4:\"http\";}', 'yes'),
(413, 'jetpack_sync_https_history_home_url', 'a:2:{i:0;s:4:\"http\";i:1;s:4:\"http\";}', 'yes'),
(451, 'jetpack_callables_sync_checksum', 'a:33:{s:18:\"wp_max_upload_size\";i:677931734;s:15:\"is_main_network\";i:734881840;s:13:\"is_multi_site\";i:734881840;s:17:\"main_network_site\";i:3862738268;s:8:\"site_url\";i:3862738268;s:8:\"home_url\";i:3862738268;s:16:\"single_user_site\";i:4261170317;s:7:\"updates\";i:3425443202;s:28:\"has_file_system_write_access\";i:4261170317;s:21:\"is_version_controlled\";i:734881840;s:10:\"taxonomies\";i:1746598580;s:10:\"post_types\";i:3055645231;s:18:\"post_type_features\";i:3143842698;s:10:\"shortcodes\";i:2566769823;s:27:\"rest_api_allowed_post_types\";i:2830172746;s:32:\"rest_api_allowed_public_metadata\";i:223132457;s:24:\"sso_is_two_step_required\";i:734881840;s:26:\"sso_should_hide_login_form\";i:734881840;s:18:\"sso_match_by_email\";i:4261170317;s:21:\"sso_new_user_override\";i:734881840;s:29:\"sso_bypass_default_login_form\";i:734881840;s:10:\"wp_version\";i:3920828771;s:11:\"get_plugins\";i:657124646;s:24:\"get_plugins_action_links\";i:223132457;s:14:\"active_modules\";i:223132457;s:16:\"hosting_provider\";i:769900095;s:6:\"locale\";i:110763218;s:13:\"site_icon_url\";i:734881840;s:5:\"roles\";i:2074191413;s:8:\"timezone\";i:3808505409;s:24:\"available_jetpack_blocks\";i:2992033451;s:13:\"paused_themes\";i:223132457;s:14:\"paused_plugins\";i:223132457;}', 'no'),
(452, 'jpsq_sync_checkout', '0:0', 'no'),
(455, 'jetpack_plugin_api_action_links', 'a:1:{s:19:\"jetpack/jetpack.php\";a:3:{s:7:\"Jetpack\";s:62:\"http://localhost/WpLearning_03/wp-admin/admin.php?page=jetpack\";s:8:\"Settings\";s:72:\"http://localhost/WpLearning_03/wp-admin/admin.php?page=jetpack#/settings\";s:7:\"Support\";s:71:\"http://localhost/WpLearning_03/wp-admin/admin.php?page=jetpack-debugger\";}}', 'yes'),
(481, 'tptn_db_version', '6.0', 'no'),
(482, 'widget_widget_tptn_pop', 'a:2:{i:2;a:14:{s:5:\"title\";s:13:\"Popular Posts\";s:5:\"limit\";s:1:\"3\";s:6:\"offset\";s:0:\"\";s:5:\"daily\";s:7:\"overall\";s:11:\"daily_range\";s:0:\"\";s:10:\"hour_range\";s:0:\"\";s:15:\"disp_list_count\";b:0;s:12:\"show_excerpt\";b:0;s:11:\"show_author\";b:1;s:9:\"show_date\";b:1;s:13:\"post_thumb_op\";s:5:\"after\";s:12:\"thumb_height\";s:5:\"134px\";s:11:\"thumb_width\";s:5:\"300px\";s:10:\"post_types\";s:9:\"post=post\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(483, 'widget_widget_tptn_count', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(484, 'tptn_settings', 'a:67:{s:8:\"trackers\";a:2:{s:7:\"overall\";s:7:\"overall\";s:5:\"daily\";s:5:\"daily\";}s:5:\"cache\";i:0;s:10:\"cache_time\";s:4:\"3600\";s:14:\"daily_midnight\";i:1;s:10:\"range_desc\";i:0;s:11:\"daily_range\";s:1:\"1\";s:10:\"hour_range\";s:1:\"0\";s:23:\"uninstall_clean_options\";i:1;s:22:\"uninstall_clean_tables\";i:0;s:12:\"show_metabox\";i:1;s:19:\"show_metabox_admins\";i:1;s:11:\"show_credit\";i:0;s:6:\"add_to\";a:2:{s:6:\"single\";s:6:\"single\";s:4:\"page\";s:4:\"page\";}s:15:\"count_disp_form\";s:55:\"(Visited %totalcount% times, %dailycount% visits today)\";s:20:\"count_disp_form_zero\";s:13:\"No visits yet\";s:19:\"number_format_count\";i:1;s:18:\"dynamic_post_count\";i:0;s:12:\"tracker_type\";s:11:\"query_based\";s:17:\"tracker_all_pages\";i:0;s:11:\"track_users\";a:2:{s:7:\"editors\";s:7:\"editors\";s:6:\"admins\";s:6:\"admins\";}s:9:\"logged_in\";i:1;s:19:\"exclude_on_post_ids\";s:0:\"\";s:11:\"pv_in_admin\";i:1;s:21:\"show_count_non_admins\";i:1;s:10:\"debug_mode\";i:0;s:5:\"limit\";s:2:\"10\";s:7:\"how_old\";s:1:\"0\";s:10:\"post_types\";s:15:\"post,attachment\";s:16:\"exclude_post_ids\";s:0:\"\";s:17:\"exclude_cat_slugs\";s:0:\"\";s:18:\"exclude_categories\";s:0:\"\";s:23:\"customize_output_header\";i:0;s:5:\"title\";s:23:\"<h3>Popular posts:</h3>\";s:11:\"title_daily\";s:28:\"<h3>Currently trending:</h3>\";s:12:\"blank_output\";s:5:\"blank\";s:17:\"blank_output_text\";s:16:\"No top posts yet\";s:12:\"show_excerpt\";i:0;s:14:\"excerpt_length\";s:2:\"10\";s:9:\"show_date\";i:1;s:11:\"show_author\";i:1;s:15:\"disp_list_count\";i:0;s:12:\"title_length\";s:2:\"60\";s:15:\"link_new_window\";i:0;s:13:\"link_nofollow\";i:0;s:19:\"html_wrapper_header\";i:0;s:11:\"before_list\";s:61:\"<div class=\"popular-post-list\"><div class=\"single-post-list\">\";s:10:\"after_list\";s:12:\"</div></div>\";s:16:\"before_list_item\";s:19:\"<div class=\"thumb\">\";s:15:\"after_list_item\";s:6:\"</div>\";s:13:\"post_thumb_op\";s:9:\"text_only\";s:10:\"thumb_size\";s:9:\"thumbnail\";s:11:\"thumb_width\";s:3:\"250\";s:12:\"thumb_height\";s:3:\"250\";s:10:\"thumb_crop\";i:1;s:18:\"thumb_create_sizes\";i:1;s:10:\"thumb_html\";s:4:\"html\";s:10:\"thumb_meta\";s:10:\"post-image\";s:11:\"scan_images\";i:1;s:18:\"thumb_default_show\";i:1;s:13:\"thumb_default\";s:68:\"http://localhost/WpLearning_03/wp-content/plugins/top-10/default.png\";s:11:\"tptn_styles\";s:8:\"no_style\";s:10:\"custom_css\";s:0:\"\";s:7:\"cron_on\";i:0;s:15:\"cron_range_desc\";i:0;s:9:\"cron_hour\";i:0;s:8:\"cron_min\";i:0;s:15:\"cron_recurrence\";s:6:\"weekly\";}', 'yes'),
(551, 'sbi_rating_notice', 'pending', 'no'),
(552, 'sbi_statuses', 'a:1:{s:13:\"first_install\";i:1572854776;}', 'no'),
(554, 'sbi_db_version', '1.3', 'yes'),
(557, 'sb_instagram_settings', 'a:51:{s:15:\"sb_instagram_at\";s:0:\"\";s:20:\"sb_instagram_user_id\";a:1:{i:0;s:5:\"false\";}s:30:\"sb_instagram_preserve_settings\";s:2:\"on\";s:23:\"sb_instagram_cache_time\";s:1:\"1\";s:28:\"sb_instagram_cache_time_unit\";s:5:\"hours\";s:16:\"sbi_caching_type\";s:4:\"page\";s:23:\"sbi_cache_cron_interval\";s:7:\"12hours\";s:19:\"sbi_cache_cron_time\";s:1:\"1\";s:20:\"sbi_cache_cron_am_pm\";s:2:\"am\";s:18:\"sb_instagram_width\";i:100;s:23:\"sb_instagram_width_unit\";s:1:\"%\";s:28:\"sb_instagram_feed_width_resp\";s:0:\"\";s:19:\"sb_instagram_height\";s:0:\"\";s:16:\"sb_instagram_num\";i:8;s:24:\"sb_instagram_height_unit\";s:2:\"px\";s:17:\"sb_instagram_cols\";s:1:\"2\";s:27:\"sb_instagram_disable_mobile\";s:0:\"\";s:26:\"sb_instagram_image_padding\";i:5;s:31:\"sb_instagram_image_padding_unit\";s:2:\"px\";s:17:\"sb_instagram_sort\";s:6:\"random\";s:23:\"sb_instagram_background\";s:0:\"\";s:21:\"sb_instagram_show_btn\";s:2:\"on\";s:27:\"sb_instagram_btn_background\";s:0:\"\";s:27:\"sb_instagram_btn_text_color\";s:0:\"\";s:21:\"sb_instagram_btn_text\";s:12:\"Load More...\";s:22:\"sb_instagram_image_res\";s:5:\"thumb\";s:24:\"sb_instagram_show_header\";s:0:\"\";s:24:\"sb_instagram_header_size\";s:5:\"small\";s:25:\"sb_instagram_header_color\";s:0:\"\";s:28:\"sb_instagram_show_follow_btn\";s:2:\"on\";s:33:\"sb_instagram_folow_btn_background\";s:0:\"\";s:34:\"sb_instagram_follow_btn_text_color\";s:0:\"\";s:28:\"sb_instagram_follow_btn_text\";s:19:\"Follow on Instagram\";s:23:\"sb_instagram_custom_css\";s:0:\"\";s:22:\"sb_instagram_custom_js\";s:0:\"\";s:17:\"sb_instagram_cron\";s:2:\"no\";s:19:\"sb_instagram_backup\";s:2:\"on\";s:15:\"sb_ajax_initial\";s:0:\"\";s:24:\"enqueue_css_in_shortcode\";s:0:\"\";s:23:\"sb_instagram_ajax_theme\";s:0:\"\";s:27:\"sb_instagram_disable_resize\";s:0:\"\";s:24:\"sb_instagram_favor_local\";s:0:\"\";s:19:\"sb_instagram_minnum\";i:0;s:24:\"disable_js_image_loading\";s:0:\"\";s:18:\"enqueue_js_in_head\";s:0:\"\";s:30:\"sb_instagram_disable_mob_swipe\";b:0;s:15:\"sbi_font_method\";s:3:\"svg\";s:28:\"sb_instagram_disable_awesome\";s:0:\"\";s:15:\"custom_template\";s:0:\"\";s:18:\"connected_accounts\";a:2:{i:15243772486;a:7:{s:12:\"access_token\";s:70:\"15243772486.M2E4MWE5Zg==.ZGQwYzkxZTQ0YWN.hNDVlMmFkMGMyMWZiY2QxZDI1MDA=\";s:7:\"user_id\";s:11:\"15243772486\";s:8:\"username\";s:10:\"alnoman141\";s:8:\"is_valid\";b:1;s:12:\"last_checked\";i:1572854816;s:15:\"profile_picture\";s:184:\"https://scontent.cdninstagram.com/vp/0b3f469d38f40bd8f330cd1a6eef82dd/5E41C390/t51.2885-19/s150x150/64379235_2267993340181031_4712706986418896896_n.jpg?_nc_ht=scontent.cdninstagram.com\";s:12:\"local_avatar\";b:1;}s:77:\"1428c7e60c541a2fc481e29573/apps/share-buttons/code?id=28743734&just-created=1\";a:6:{s:12:\"access_token\";s:77:\"1428c7e60c541a2fc481e29573/apps/share-buttons/code?id=28743734&just-created=1\";s:7:\"user_id\";b:0;s:8:\"username\";b:0;s:8:\"is_valid\";b:0;s:12:\"last_checked\";i:1573118920;s:15:\"profile_picture\";N;}}s:21:\"sb_instagram_show_bio\";s:0:\"\";}', 'yes'),
(561, '!sbi_15243772486#8', '{\"data\":[{\"id\":\"2096651942797224840_15243772486\",\"user\":{\"id\":\"15243772486\",\"full_name\":\"Abdullah Al Noman\",\"profile_picture\":\"https:\\/\\/scontent.cdninstagram.com\\/vp\\/0b3f469d38f40bd8f330cd1a6eef82dd\\/5E41C390\\/t51.2885-19\\/s150x150\\/64379235_2267993340181031_4712706986418896896_n.jpg?_nc_ht=scontent.cdninstagram.com\",\"username\":\"alnoman141\"},\"images\":{\"thumbnail\":{\"width\":150,\"height\":150,\"url\":\"https:\\/\\/scontent.cdninstagram.com\\/vp\\/7cbfabe58c12dd909bc66874de30b243\\/5E585FDC\\/t51.2885-15\\/e35\\/s150x150\\/66421366_111804106814964_7995586576395090816_n.jpg?_nc_ht=scontent.cdninstagram.com\"},\"low_resolution\":{\"width\":320,\"height\":320,\"url\":\"https:\\/\\/scontent.cdninstagram.com\\/vp\\/70ed936620428882aa515c875b7e03b1\\/5E59372C\\/t51.2885-15\\/e35\\/s320x320\\/66421366_111804106814964_7995586576395090816_n.jpg?_nc_ht=scontent.cdninstagram.com\"},\"standard_resolution\":{\"width\":640,\"height\":640,\"url\":\"https:\\/\\/scontent.cdninstagram.com\\/vp\\/f17de9a7c6655fef9841246c5a053a64\\/5E5AA47B\\/t51.2885-15\\/sh0.08\\/e35\\/s640x640\\/66421366_111804106814964_7995586576395090816_n.jpg?_nc_ht=scontent.cdninstagram.com\"}},\"created_time\":\"1564160410\",\"caption\":null,\"user_has_liked\":false,\"likes\":{\"count\":4},\"tags\":[],\"filter\":\"Perpetua\",\"comments\":{\"count\":0},\"type\":\"image\",\"link\":\"https:\\/\\/www.instagram.com\\/p\\/B0YzdVcD6uI\\/\",\"location\":null,\"attribution\":null,\"users_in_photo\":[]}],\"pagination\":false}', 'no'),
(564, '!sbi_header_15243772486#8', '{\"id\":\"15243772486\",\"username\":\"alnoman141\",\"profile_picture\":\"https:\\/\\/scontent.cdninstagram.com\\/vp\\/0b3f469d38f40bd8f330cd1a6eef82dd\\/5E41C390\\/t51.2885-19\\/s150x150\\/64379235_2267993340181031_4712706986418896896_n.jpg?_nc_ht=scontent.cdninstagram.com\",\"full_name\":\"Abdullah Al Noman\",\"bio\":\"I\'m here...\",\"website\":\"https:\\/\\/www.fiverr.com\\/noman141\",\"is_business\":false,\"counts\":{\"media\":1,\"follows\":71,\"followed_by\":22},\"local_avatar\":\"http:\\/\\/localhost\\/WpLearning_03\\/wp-content\\/uploads\\/sb-instagram-feed-images\\/alnoman141.jpg\"}', 'no'),
(567, 'sb_instagram_ajax_status', 'a:2:{s:6:\"tested\";b:1;s:10:\"successful\";b:1;}', 'yes'),
(583, 'ig_es_from_name', 'My New Wordpress Website', 'no'),
(584, 'ig_es_from_email', 'nmnaba14@gmail.com', 'no'),
(585, 'ig_es_admin_new_contact_email_subject', 'One more contact joins our tribe!', 'no'),
(586, 'ig_es_admin_new_contact_email_content', 'Hi,\r\n\r\nYour friendly Email Subscribers notification bot here!\r\n\r\n{{NAME}} ({{EMAIL}}) joined our tribe just now.\r\n\r\nWhich list/s? {{LIST}}\r\n\r\nIf you know this person, or if they are an influencer, you may want to reach out to them personally!\r\n\r\nLater...', 'no'),
(587, 'ig_es_admin_emails', 'nmnaba14@gmail.com', 'no'),
(588, 'ig_es_confirmation_mail_subject', 'Thanks!', 'no'),
(589, 'ig_es_confirmation_mail_content', 'Hi {{NAME}},\r\n\r\nJust one more step before we share the awesomeness from {{SITENAME}}!\r\n\r\nPlease confirm your subscription by clicking on this link:\r\n\r\n{{SUBSCRIBE-LINK}}\r\n\r\nThanks!', 'no'),
(590, 'ig_es_enable_welcome_email', 'yes', 'no'),
(591, 'ig_es_welcome_email_subject', 'Welcome to {{SITENAME}}', 'no'),
(592, 'ig_es_welcome_email_content', 'Hi {{NAME}},\r\n\r\nJust wanted to send you a quick note...\r\n\r\nThank you for joining the awesome {{SITENAME}} tribe.\r\n\r\nOnly valuable emails from me, promise!\r\n\r\nThanks!', 'no'),
(593, 'ig_es_enable_cron_admin_email', 'yes', 'no'),
(594, 'ig_es_cron_admin_email', 'Hi Admin,\r\n\r\nCron URL has been triggered successfully on {{DATE}} for the email \'{{SUBJECT}}\'. And it sent email to {{COUNT}} recipient(s).\r\n\r\nBest,\r\nMy New Wordpress Website', 'no'),
(595, 'ig_es_cronurl', 'http://localhost/WpLearning_03?es=cron&guid=qvzdlw-omxqjg-xqpilv-qagcbp-gqtjpy', 'no'),
(596, 'ig_es_hourly_email_send_limit', '300', 'no'),
(597, 'ig_es_sent_report_subject', 'Your email has been sent', 'no'),
(598, 'ig_es_sent_report_content', 'Hi Admin,\n\nEmail has been sent successfully to {{COUNT}} email(s). Please find the details below:\n\nUnique ID: {{UNIQUE}}\nStart Time: {{STARTTIME}}\nEnd Time: {{ENDTIME}}\nFor more information, login to your dashboard and go to Reports menu in Email Subscribers.\n\nThank You.', 'no'),
(599, 'ig_es_unsubscribe_link', 'http://localhost/WpLearning_03/?es=unsubscribe&db={{DBID}}&email={{EMAIL}}&guid={{GUID}}', 'no'),
(600, 'ig_es_optin_link', 'http://localhost/WpLearning_03/?es=optin&db={{DBID}}&email={{EMAIL}}&guid={{GUID}}', 'no'),
(601, 'ig_es_unsubscribe_link_content', 'I\'d be sad to see you go. But if you want to, you can unsubscribe from here: {{UNSUBSCRIBE-LINK}}', 'no'),
(602, 'ig_es_email_type', 'wp_html_mail', 'no'),
(603, 'ig_es_notify_admin', 'yes', 'no'),
(604, 'ig_es_optin_type', 'double_opt_in', 'no'),
(605, 'ig_es_subscription_error_messsage', 'Hmm.. Something\'s amiss..\r\n\r\nCould not complete your request. That email address  is probably already subscribed. Or worse blocked!!\r\n\r\nPlease try again after some time - or contact us if the problem persists.\r\n\r\n', 'no'),
(606, 'ig_es_subscription_success_message', 'You have been successfully subscribed.', 'no'),
(607, 'ig_es_unsubscribe_error_message', 'Urrgh.. Something\'s wrong..\r\n\r\nAre you sure that email address is on our file? There was some problem in completing your request.\r\n\r\nPlease try again after some time - or contact us if the problem persists.\r\n\r\n', 'no'),
(608, 'ig_es_unsubscribe_success_message', '<h2>Unsubscribed.</h2><p>You will no longer hear from us. ☹️ Sorry to see you go!</p>', 'no'),
(609, 'ig_es_post_image_size', 'thumbnail', 'no'),
(610, 'ig_es_db_version', '4.2.4', 'no'),
(611, 'ig_es_current_version_date_details', 'a:2:{s:18:\"es_current_version\";s:5:\"4.2.4\";s:15:\"es_current_date\";s:19:\"2019-11-04 11:23:51\";}', 'no'),
(612, 'ig_es_enable_captcha', '', 'no'),
(613, 'ig_es_roles_and_capabilities', '', 'no'),
(614, 'ig_es_sample_data_imported', 'no', 'no'),
(615, 'ig_es_default_subscriber_imported', 'no', 'no'),
(616, 'ig_es_set_widget', '', 'no'),
(617, 'ig_es_sync_wp_users', '', 'no'),
(618, 'ig_es_blocked_domains', 'mail.ru', 'no'),
(619, 'ig_es_disable_wp_cron', 'no', 'no'),
(620, 'ig_es_track_email_opens', 'yes', 'no'),
(621, 'ig_es_installed_on', '2019-11-04 09:28:50', 'no'),
(622, 'ig_es_form_submission_success_message', 'Your subscription was successful! Kindly check your mailbox and confirm your subscription. If you don\'t see the email within a few minutes, check the spam/junk folder.', 'no'),
(623, 'ig_es_db_update_history', 'a:1:{s:5:\"4.2.4\";s:19:\"2019-11-04 09:28:50\";}', 'no'),
(624, 'ig_es_email_sent_data', 'a:1:{s:10:\"2019-11-04\";a:1:{s:2:\"09\";i:4;}}', 'no'),
(625, 'ig_es_mailer_settings', 'a:2:{s:6:\"mailer\";s:6:\"wpmail\";s:8:\"pepipost\";a:1:{s:7:\"api_key\";s:9:\"123456789\";}}', 'no'),
(627, 'ig_admin_notices', 'a:0:{}', 'yes'),
(628, 'widget_email-subscribers-form', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(630, 'ig_es_onboarding_test_campaign_error', 'yes', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(631, 'ig_es_onboarding_complete', 'yes', 'yes'),
(632, 'ig_es_ob_skip_email_send_error', 'yes', 'yes'),
(637, 'ig_es_sequence_release_notice_seen', 'yes', 'yes'),
(638, 'ig_es_last_cron_run', '1572861555', 'yes'),
(646, 'widget_s2_form_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(647, 'mo_dismiss_adnotice', 'true', 'yes'),
(650, 'newsletter_logger_secret', '4ad65002', 'yes'),
(652, 'newsletter_main_first_install_time', '1572862418', 'no'),
(653, 'newsletter_main', 'a:31:{s:11:\"return_path\";s:0:\"\";s:8:\"reply_to\";s:0:\"\";s:12:\"sender_email\";s:20:\"newsletter@localhost\";s:11:\"sender_name\";s:24:\"My New Wordpress Website\";s:6:\"editor\";i:0;s:13:\"scheduler_max\";i:100;s:9:\"phpmailer\";i:0;s:5:\"debug\";i:0;s:5:\"track\";i:1;s:3:\"css\";s:0:\"\";s:12:\"css_disabled\";i:0;s:2:\"ip\";s:0:\"\";s:4:\"page\";i:95;s:19:\"disable_cron_notice\";i:0;s:13:\"do_shortcodes\";i:0;s:11:\"header_logo\";s:0:\"\";s:12:\"header_title\";s:24:\"My New Wordpress Website\";s:10:\"header_sub\";s:27:\"Just another WordPress site\";s:12:\"footer_title\";s:0:\"\";s:14:\"footer_contact\";s:0:\"\";s:12:\"footer_legal\";s:0:\"\";s:12:\"facebook_url\";s:0:\"\";s:11:\"twitter_url\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:14:\"googleplus_url\";s:0:\"\";s:13:\"pinterest_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:10:\"tumblr_url\";s:0:\"\";s:11:\"youtube_url\";s:0:\"\";s:9:\"vimeo_url\";s:0:\"\";s:14:\"soundcloud_url\";s:0:\"\";}', 'yes'),
(654, 'newsletter_main_info', 'a:16:{s:11:\"header_logo\";a:1:{s:2:\"id\";i:0;}s:12:\"header_title\";s:24:\"My New Wordpress Website\";s:10:\"header_sub\";s:27:\"Just another WordPress site\";s:12:\"footer_title\";s:0:\"\";s:14:\"footer_contact\";s:0:\"\";s:12:\"footer_legal\";s:0:\"\";s:12:\"facebook_url\";s:0:\"\";s:11:\"twitter_url\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:14:\"googleplus_url\";s:0:\"\";s:13:\"pinterest_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:10:\"tumblr_url\";s:0:\"\";s:11:\"youtube_url\";s:0:\"\";s:9:\"vimeo_url\";s:0:\"\";s:14:\"soundcloud_url\";s:0:\"\";}', 'yes'),
(655, 'newsletter_main_smtp', 'a:7:{s:7:\"enabled\";i:0;s:4:\"host\";s:0:\"\";s:4:\"user\";s:0:\"\";s:4:\"pass\";s:0:\"\";s:4:\"port\";i:25;s:6:\"secure\";s:0:\"\";s:12:\"ssl_insecure\";i:0;}', 'yes'),
(656, 'newsletter_main_version', '1.5.1', 'yes'),
(657, 'newsletter_subscription_first_install_time', '1572862420', 'no'),
(658, 'newsletter', 'a:20:{s:14:\"noconfirmation\";i:1;s:9:\"antiflood\";i:10;s:12:\"ip_blacklist\";a:0:{}s:17:\"address_blacklist\";a:0:{}s:12:\"domain_check\";i:0;s:7:\"akismet\";i:0;s:7:\"captcha\";i:0;s:12:\"notify_email\";s:18:\"nmnaba14@gmail.com\";s:8:\"multiple\";i:1;s:6:\"notify\";i:0;s:10:\"error_text\";s:102:\"<p>You cannot subscribe with the email address you entered, please contact the site administrator.</p>\";s:17:\"subscription_text\";s:19:\"{subscription_form}\";s:17:\"confirmation_text\";s:104:\"<p>A confirmation email is on the way. Follow the instructions and check the spam folder. Thank you.</p>\";s:20:\"confirmation_subject\";s:32:\"Please confirm your subscription\";s:21:\"confirmation_tracking\";s:0:\"\";s:20:\"confirmation_message\";s:94:\"<p>Please confirm your subscription <a href=\"{subscription_confirm_url}\">clicking here</a></p>\";s:14:\"confirmed_text\";s:43:\"<p>Your subscription has been confirmed</p>\";s:17:\"confirmed_subject\";s:7:\"Welcome\";s:17:\"confirmed_message\";s:130:\"<p>This message confirms your subscription to our newsletter. Thank you!</p><hr><p><a href=\"{profile_url}\">Change your profile</p>\";s:18:\"confirmed_tracking\";s:0:\"\";}', 'yes'),
(659, 'newsletter_subscription_lists', 'a:160:{s:6:\"list_1\";s:0:\"\";s:13:\"list_1_status\";i:0;s:14:\"list_1_checked\";i:0;s:13:\"list_1_forced\";i:0;s:6:\"list_2\";s:0:\"\";s:13:\"list_2_status\";i:0;s:14:\"list_2_checked\";i:0;s:13:\"list_2_forced\";i:0;s:6:\"list_3\";s:0:\"\";s:13:\"list_3_status\";i:0;s:14:\"list_3_checked\";i:0;s:13:\"list_3_forced\";i:0;s:6:\"list_4\";s:0:\"\";s:13:\"list_4_status\";i:0;s:14:\"list_4_checked\";i:0;s:13:\"list_4_forced\";i:0;s:6:\"list_5\";s:0:\"\";s:13:\"list_5_status\";i:0;s:14:\"list_5_checked\";i:0;s:13:\"list_5_forced\";i:0;s:6:\"list_6\";s:0:\"\";s:13:\"list_6_status\";i:0;s:14:\"list_6_checked\";i:0;s:13:\"list_6_forced\";i:0;s:6:\"list_7\";s:0:\"\";s:13:\"list_7_status\";i:0;s:14:\"list_7_checked\";i:0;s:13:\"list_7_forced\";i:0;s:6:\"list_8\";s:0:\"\";s:13:\"list_8_status\";i:0;s:14:\"list_8_checked\";i:0;s:13:\"list_8_forced\";i:0;s:6:\"list_9\";s:0:\"\";s:13:\"list_9_status\";i:0;s:14:\"list_9_checked\";i:0;s:13:\"list_9_forced\";i:0;s:7:\"list_10\";s:0:\"\";s:14:\"list_10_status\";i:0;s:15:\"list_10_checked\";i:0;s:14:\"list_10_forced\";i:0;s:7:\"list_11\";s:0:\"\";s:14:\"list_11_status\";i:0;s:15:\"list_11_checked\";i:0;s:14:\"list_11_forced\";i:0;s:7:\"list_12\";s:0:\"\";s:14:\"list_12_status\";i:0;s:15:\"list_12_checked\";i:0;s:14:\"list_12_forced\";i:0;s:7:\"list_13\";s:0:\"\";s:14:\"list_13_status\";i:0;s:15:\"list_13_checked\";i:0;s:14:\"list_13_forced\";i:0;s:7:\"list_14\";s:0:\"\";s:14:\"list_14_status\";i:0;s:15:\"list_14_checked\";i:0;s:14:\"list_14_forced\";i:0;s:7:\"list_15\";s:0:\"\";s:14:\"list_15_status\";i:0;s:15:\"list_15_checked\";i:0;s:14:\"list_15_forced\";i:0;s:7:\"list_16\";s:0:\"\";s:14:\"list_16_status\";i:0;s:15:\"list_16_checked\";i:0;s:14:\"list_16_forced\";i:0;s:7:\"list_17\";s:0:\"\";s:14:\"list_17_status\";i:0;s:15:\"list_17_checked\";i:0;s:14:\"list_17_forced\";i:0;s:7:\"list_18\";s:0:\"\";s:14:\"list_18_status\";i:0;s:15:\"list_18_checked\";i:0;s:14:\"list_18_forced\";i:0;s:7:\"list_19\";s:0:\"\";s:14:\"list_19_status\";i:0;s:15:\"list_19_checked\";i:0;s:14:\"list_19_forced\";i:0;s:7:\"list_20\";s:0:\"\";s:14:\"list_20_status\";i:0;s:15:\"list_20_checked\";i:0;s:14:\"list_20_forced\";i:0;s:7:\"list_21\";s:0:\"\";s:14:\"list_21_status\";i:0;s:15:\"list_21_checked\";i:0;s:14:\"list_21_forced\";i:0;s:7:\"list_22\";s:0:\"\";s:14:\"list_22_status\";i:0;s:15:\"list_22_checked\";i:0;s:14:\"list_22_forced\";i:0;s:7:\"list_23\";s:0:\"\";s:14:\"list_23_status\";i:0;s:15:\"list_23_checked\";i:0;s:14:\"list_23_forced\";i:0;s:7:\"list_24\";s:0:\"\";s:14:\"list_24_status\";i:0;s:15:\"list_24_checked\";i:0;s:14:\"list_24_forced\";i:0;s:7:\"list_25\";s:0:\"\";s:14:\"list_25_status\";i:0;s:15:\"list_25_checked\";i:0;s:14:\"list_25_forced\";i:0;s:7:\"list_26\";s:0:\"\";s:14:\"list_26_status\";i:0;s:15:\"list_26_checked\";i:0;s:14:\"list_26_forced\";i:0;s:7:\"list_27\";s:0:\"\";s:14:\"list_27_status\";i:0;s:15:\"list_27_checked\";i:0;s:14:\"list_27_forced\";i:0;s:7:\"list_28\";s:0:\"\";s:14:\"list_28_status\";i:0;s:15:\"list_28_checked\";i:0;s:14:\"list_28_forced\";i:0;s:7:\"list_29\";s:0:\"\";s:14:\"list_29_status\";i:0;s:15:\"list_29_checked\";i:0;s:14:\"list_29_forced\";i:0;s:7:\"list_30\";s:0:\"\";s:14:\"list_30_status\";i:0;s:15:\"list_30_checked\";i:0;s:14:\"list_30_forced\";i:0;s:7:\"list_31\";s:0:\"\";s:14:\"list_31_status\";i:0;s:15:\"list_31_checked\";i:0;s:14:\"list_31_forced\";i:0;s:7:\"list_32\";s:0:\"\";s:14:\"list_32_status\";i:0;s:15:\"list_32_checked\";i:0;s:14:\"list_32_forced\";i:0;s:7:\"list_33\";s:0:\"\";s:14:\"list_33_status\";i:0;s:15:\"list_33_checked\";i:0;s:14:\"list_33_forced\";i:0;s:7:\"list_34\";s:0:\"\";s:14:\"list_34_status\";i:0;s:15:\"list_34_checked\";i:0;s:14:\"list_34_forced\";i:0;s:7:\"list_35\";s:0:\"\";s:14:\"list_35_status\";i:0;s:15:\"list_35_checked\";i:0;s:14:\"list_35_forced\";i:0;s:7:\"list_36\";s:0:\"\";s:14:\"list_36_status\";i:0;s:15:\"list_36_checked\";i:0;s:14:\"list_36_forced\";i:0;s:7:\"list_37\";s:0:\"\";s:14:\"list_37_status\";i:0;s:15:\"list_37_checked\";i:0;s:14:\"list_37_forced\";i:0;s:7:\"list_38\";s:0:\"\";s:14:\"list_38_status\";i:0;s:15:\"list_38_checked\";i:0;s:14:\"list_38_forced\";i:0;s:7:\"list_39\";s:0:\"\";s:14:\"list_39_status\";i:0;s:15:\"list_39_checked\";i:0;s:14:\"list_39_forced\";i:0;s:7:\"list_40\";s:0:\"\";s:14:\"list_40_status\";i:0;s:15:\"list_40_checked\";i:0;s:14:\"list_40_forced\";i:0;}', 'yes'),
(660, 'newsletter_subscription_template', 'a:1:{s:8:\"template\";s:2365:\"<!DOCTYPE html>\n<html>\n    <head>\n        <!-- General styles, not used by all email clients -->\n        <style type=\"text/css\" media=\"all\">\n            a {\n                text-decoration: none;\n                color: #0088cc;\n            }\n            hr {\n                border-top: 1px solid #999;\n            }\n        </style>\n    </head>\n\n    <!-- KEEP THE LAYOUT SIMPLE: THOSE ARE SERVICE MESSAGES. -->\n    <body style=\"margin: 0; padding: 0;\">\n\n        <!-- Top title with dark background -->\n        <table style=\"background-color: #444; width: 100%;\" cellspacing=\"0\" cellpadding=\"0\">\n            <tr>\n                <td style=\"padding: 20px; text-align: center; font-family: sans-serif; color: #fff; font-size: 28px\">\n                    {blog_title}\n                </td>\n            </tr>\n        </table>\n\n        <!-- Main table 100% wide with background color #eee -->    \n        <table style=\"background-color: #eee; width: 100%;\">\n            <tr>\n                <td align=\"center\" style=\"padding: 15px;\">\n\n                    <!-- Content table with backgdound color #fff, width 500px -->\n                    <table style=\"background-color: #fff; max-width: 600px; width: 100%; border: 1px solid #ddd;\">\n                        <tr>\n                            <td style=\"padding: 15px; color: #333; font-size: 16px; font-family: sans-serif\">\n                                <!-- The \"message\" tag below is replaced with one of confirmation, welcome or goodbye messages -->\n                                <!-- Messages content can be configured on Newsletter List Building panels --> \n\n                                {message}\n\n                                <hr>\n                                <!-- Signature if not already added to single messages (surround with <p>) -->\n                                <p>\n                                    <small>\n                                        <a href=\"{blog_url}\">{blog_url}</a><br>\n                                        {company_name}<br>\n                                        {company_address}\n                                    </small>\n                                </p>\n                                \n\n                            </td>\n                        </tr>\n                    </table>\n\n                </td>\n            </tr>\n        </table>\n\n    </body>\n</html>\";}', 'yes'),
(661, 'newsletter_profile', 'a:184:{s:5:\"email\";s:5:\"Email\";s:11:\"email_error\";s:28:\"Email address is not correct\";s:4:\"name\";s:23:\"First name or full name\";s:10:\"name_error\";s:16:\"Name is required\";s:11:\"name_status\";i:0;s:10:\"name_rules\";i:0;s:7:\"surname\";s:9:\"Last name\";s:13:\"surname_error\";s:21:\"Last name is required\";s:14:\"surname_status\";i:0;s:10:\"sex_status\";i:0;s:3:\"sex\";s:3:\"I\'m\";s:7:\"privacy\";s:44:\"By continuing, you accept the privacy policy\";s:13:\"privacy_error\";s:34:\"You must accept the privacy policy\";s:14:\"privacy_status\";i:0;s:11:\"privacy_url\";s:0:\"\";s:18:\"privacy_use_wp_url\";i:0;s:9:\"subscribe\";s:9:\"Subscribe\";s:12:\"title_female\";s:3:\"Ms.\";s:10:\"title_male\";s:3:\"Mr.\";s:10:\"title_none\";s:4:\"Dear\";s:8:\"sex_male\";s:3:\"Man\";s:10:\"sex_female\";s:5:\"Woman\";s:8:\"sex_none\";s:13:\"Not specified\";s:13:\"profile_error\";s:34:\"A mandatory field is not filled in\";s:16:\"profile_1_status\";i:0;s:9:\"profile_1\";s:0:\"\";s:14:\"profile_1_type\";s:4:\"text\";s:21:\"profile_1_placeholder\";s:0:\"\";s:15:\"profile_1_rules\";i:0;s:17:\"profile_1_options\";s:0:\"\";s:16:\"profile_2_status\";i:0;s:9:\"profile_2\";s:0:\"\";s:14:\"profile_2_type\";s:4:\"text\";s:21:\"profile_2_placeholder\";s:0:\"\";s:15:\"profile_2_rules\";i:0;s:17:\"profile_2_options\";s:0:\"\";s:16:\"profile_3_status\";i:0;s:9:\"profile_3\";s:0:\"\";s:14:\"profile_3_type\";s:4:\"text\";s:21:\"profile_3_placeholder\";s:0:\"\";s:15:\"profile_3_rules\";i:0;s:17:\"profile_3_options\";s:0:\"\";s:16:\"profile_4_status\";i:0;s:9:\"profile_4\";s:0:\"\";s:14:\"profile_4_type\";s:4:\"text\";s:21:\"profile_4_placeholder\";s:0:\"\";s:15:\"profile_4_rules\";i:0;s:17:\"profile_4_options\";s:0:\"\";s:16:\"profile_5_status\";i:0;s:9:\"profile_5\";s:0:\"\";s:14:\"profile_5_type\";s:4:\"text\";s:21:\"profile_5_placeholder\";s:0:\"\";s:15:\"profile_5_rules\";i:0;s:17:\"profile_5_options\";s:0:\"\";s:16:\"profile_6_status\";i:0;s:9:\"profile_6\";s:0:\"\";s:14:\"profile_6_type\";s:4:\"text\";s:21:\"profile_6_placeholder\";s:0:\"\";s:15:\"profile_6_rules\";i:0;s:17:\"profile_6_options\";s:0:\"\";s:16:\"profile_7_status\";i:0;s:9:\"profile_7\";s:0:\"\";s:14:\"profile_7_type\";s:4:\"text\";s:21:\"profile_7_placeholder\";s:0:\"\";s:15:\"profile_7_rules\";i:0;s:17:\"profile_7_options\";s:0:\"\";s:16:\"profile_8_status\";i:0;s:9:\"profile_8\";s:0:\"\";s:14:\"profile_8_type\";s:4:\"text\";s:21:\"profile_8_placeholder\";s:0:\"\";s:15:\"profile_8_rules\";i:0;s:17:\"profile_8_options\";s:0:\"\";s:16:\"profile_9_status\";i:0;s:9:\"profile_9\";s:0:\"\";s:14:\"profile_9_type\";s:4:\"text\";s:21:\"profile_9_placeholder\";s:0:\"\";s:15:\"profile_9_rules\";i:0;s:17:\"profile_9_options\";s:0:\"\";s:17:\"profile_10_status\";i:0;s:10:\"profile_10\";s:0:\"\";s:15:\"profile_10_type\";s:4:\"text\";s:22:\"profile_10_placeholder\";s:0:\"\";s:16:\"profile_10_rules\";i:0;s:18:\"profile_10_options\";s:0:\"\";s:17:\"profile_11_status\";i:0;s:10:\"profile_11\";s:0:\"\";s:15:\"profile_11_type\";s:4:\"text\";s:22:\"profile_11_placeholder\";s:0:\"\";s:16:\"profile_11_rules\";i:0;s:18:\"profile_11_options\";s:0:\"\";s:17:\"profile_12_status\";i:0;s:10:\"profile_12\";s:0:\"\";s:15:\"profile_12_type\";s:4:\"text\";s:22:\"profile_12_placeholder\";s:0:\"\";s:16:\"profile_12_rules\";i:0;s:18:\"profile_12_options\";s:0:\"\";s:17:\"profile_13_status\";i:0;s:10:\"profile_13\";s:0:\"\";s:15:\"profile_13_type\";s:4:\"text\";s:22:\"profile_13_placeholder\";s:0:\"\";s:16:\"profile_13_rules\";i:0;s:18:\"profile_13_options\";s:0:\"\";s:17:\"profile_14_status\";i:0;s:10:\"profile_14\";s:0:\"\";s:15:\"profile_14_type\";s:4:\"text\";s:22:\"profile_14_placeholder\";s:0:\"\";s:16:\"profile_14_rules\";i:0;s:18:\"profile_14_options\";s:0:\"\";s:17:\"profile_15_status\";i:0;s:10:\"profile_15\";s:0:\"\";s:15:\"profile_15_type\";s:4:\"text\";s:22:\"profile_15_placeholder\";s:0:\"\";s:16:\"profile_15_rules\";i:0;s:18:\"profile_15_options\";s:0:\"\";s:17:\"profile_16_status\";i:0;s:10:\"profile_16\";s:0:\"\";s:15:\"profile_16_type\";s:4:\"text\";s:22:\"profile_16_placeholder\";s:0:\"\";s:16:\"profile_16_rules\";i:0;s:18:\"profile_16_options\";s:0:\"\";s:17:\"profile_17_status\";i:0;s:10:\"profile_17\";s:0:\"\";s:15:\"profile_17_type\";s:4:\"text\";s:22:\"profile_17_placeholder\";s:0:\"\";s:16:\"profile_17_rules\";i:0;s:18:\"profile_17_options\";s:0:\"\";s:17:\"profile_18_status\";i:0;s:10:\"profile_18\";s:0:\"\";s:15:\"profile_18_type\";s:4:\"text\";s:22:\"profile_18_placeholder\";s:0:\"\";s:16:\"profile_18_rules\";i:0;s:18:\"profile_18_options\";s:0:\"\";s:17:\"profile_19_status\";i:0;s:10:\"profile_19\";s:0:\"\";s:15:\"profile_19_type\";s:4:\"text\";s:22:\"profile_19_placeholder\";s:0:\"\";s:16:\"profile_19_rules\";i:0;s:18:\"profile_19_options\";s:0:\"\";s:17:\"profile_20_status\";i:0;s:10:\"profile_20\";s:0:\"\";s:15:\"profile_20_type\";s:4:\"text\";s:22:\"profile_20_placeholder\";s:0:\"\";s:16:\"profile_20_rules\";i:0;s:18:\"profile_20_options\";s:0:\"\";s:13:\"list_1_forced\";i:0;s:13:\"list_2_forced\";i:0;s:13:\"list_3_forced\";i:0;s:13:\"list_4_forced\";i:0;s:13:\"list_5_forced\";i:0;s:13:\"list_6_forced\";i:0;s:13:\"list_7_forced\";i:0;s:13:\"list_8_forced\";i:0;s:13:\"list_9_forced\";i:0;s:14:\"list_10_forced\";i:0;s:14:\"list_11_forced\";i:0;s:14:\"list_12_forced\";i:0;s:14:\"list_13_forced\";i:0;s:14:\"list_14_forced\";i:0;s:14:\"list_15_forced\";i:0;s:14:\"list_16_forced\";i:0;s:14:\"list_17_forced\";i:0;s:14:\"list_18_forced\";i:0;s:14:\"list_19_forced\";i:0;s:14:\"list_20_forced\";i:0;s:14:\"list_21_forced\";i:0;s:14:\"list_22_forced\";i:0;s:14:\"list_23_forced\";i:0;s:14:\"list_24_forced\";i:0;s:14:\"list_25_forced\";i:0;s:14:\"list_26_forced\";i:0;s:14:\"list_27_forced\";i:0;s:14:\"list_28_forced\";i:0;s:14:\"list_29_forced\";i:0;s:14:\"list_30_forced\";i:0;s:14:\"list_31_forced\";i:0;s:14:\"list_32_forced\";i:0;s:14:\"list_33_forced\";i:0;s:14:\"list_34_forced\";i:0;s:14:\"list_35_forced\";i:0;s:14:\"list_36_forced\";i:0;s:14:\"list_37_forced\";i:0;s:14:\"list_38_forced\";i:0;s:14:\"list_39_forced\";i:0;s:14:\"list_40_forced\";i:0;}', 'yes'),
(662, 'newsletter_subscription_version', '2.1.7', 'yes'),
(663, 'newsletter_unsubscription_first_install_time', '1572862421', 'no'),
(664, 'newsletter_unsubscription', 'a:6:{s:16:\"unsubscribe_text\";s:103:\"<p>Please confirm you want to unsubscribe <a href=\"{unsubscription_confirm_url}\">clicking here</a>.</p>\";s:10:\"error_text\";s:99:\"<p>Subscriber not found, it probably has already been removed. No further actions are required.</p>\";s:17:\"unsubscribed_text\";s:124:\"<p>Your subscription has been deleted. If that was an error you can <a href=\"{reactivate_url}\">subscribe again here</a>.</p>\";s:20:\"unsubscribed_subject\";s:7:\"Goodbye\";s:20:\"unsubscribed_message\";s:87:\"<p>This message confirms that you have unsubscribed from our newsletter. Thank you.</p>\";s:16:\"reactivated_text\";s:46:\"<p>Your subscription has been reactivated.</p>\";}', 'yes'),
(665, 'newsletter_unsubscription_version', '1.0.0', 'yes'),
(666, 'newsletter_profile_first_install_time', '1572862421', 'no'),
(667, 'newsletter_profile_main', 'a:8:{s:4:\"text\";s:188:\"{profile_form}\n    <p>If you change your email address, a confirmation email will be sent to activate it.</p>\n    <p><a href=\"{unsubscription_confirm_url}\">Cancel your subscription</a></p>\";s:13:\"email_changed\";s:81:\"Your email has been changed, an activation email has been sent with instructions.\";s:5:\"error\";s:42:\"Your email is not valid or already in use.\";s:10:\"save_label\";s:4:\"Save\";s:13:\"privacy_label\";s:21:\"Read our privacy note\";s:5:\"saved\";s:14:\"Profile saved.\";s:18:\"export_newsletters\";i:0;s:3:\"url\";s:0:\"\";}', 'yes'),
(668, 'newsletter_profile_version', '1.1.0', 'yes'),
(669, 'newsletter_emails_first_install_time', '1572862421', 'no'),
(670, 'newsletter_emails', 'a:1:{s:5:\"theme\";s:7:\"default\";}', 'yes'),
(671, 'newsletter_emails_theme_default', 'a:0:{}', 'no'),
(672, 'newsletter_emails_version', '1.1.5', 'yes'),
(673, 'newsletter_users_first_install_time', '1572862422', 'no'),
(674, 'newsletter_users', 'a:0:{}', 'yes'),
(675, 'newsletter_users_version', '1.3.0', 'yes'),
(676, 'newsletter_statistics_first_install_time', '1572862422', 'no'),
(677, 'newsletter_statistics', 'a:1:{s:3:\"key\";s:32:\"8538c72716dd03c5b32b649e7c74eae2\";}', 'yes'),
(678, 'newsletter_statistics_version', '1.1.8', 'yes'),
(679, 'newsletter_install_time', '1572862423', 'no'),
(680, 'widget_newsletterwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(681, 'widget_newsletterwidgetminimal', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(684, 'newsletter_page', '95', 'no'),
(686, 'newsletter_diagnostic_cron_calls', 'a:4:{i:0;i:1572862442;i:1;i:1572862489;i:2;i:1572862789;i:3;i:1572863062;}', 'no'),
(687, 'newsletter_diagnostic_cron_data', '', 'no'),
(691, 'newsletter_users_search', 'a:0:{}', 'yes'),
(692, 'newsletter_forms', 'a:10:{s:6:\"form_1\";s:768:\"<form target=\"_blank\" novalidate=\"true\" action=\"\"\r\n                method=\"get\" class=\"form-inline\">\r\n\r\n                <div class=\"d-flex flex-row\">\r\n\r\n                  <input class=\"form-control\" name=\"EMAIL\" placeholder=\"Enter Email\" onfocus=\"this.placeholder = \'\'\" onblur=\"this.placeholder = \'Enter Email \'\"\r\n                    required=\"\" type=\"email\">\r\n\r\n\r\n                  <button class=\"click-btn btn btn-default\"><span class=\"lnr lnr-arrow-right\"></span></button>\r\n                  <div style=\"position: absolute; left: -5000px;\">\r\n                    <input name=\"b_36c4fd991d266f23781ded980_aefe40901a\" tabindex=\"-1\" value=\"\" type=\"text\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"info\"></div>\r\n              </form>\";s:6:\"form_2\";s:0:\"\";s:6:\"form_3\";s:0:\"\";s:6:\"form_4\";s:0:\"\";s:6:\"form_5\";s:0:\"\";s:6:\"form_6\";s:0:\"\";s:6:\"form_7\";s:0:\"\";s:6:\"form_8\";s:0:\"\";s:6:\"form_9\";s:0:\"\";s:7:\"form_10\";s:0:\"\";}', 'yes'),
(706, 'ig_es_cron_admin_email_subject', 'Campaign Sent!', 'no'),
(788, 'weblizar_nls_options', 'a:71:{s:15:\"select_template\";s:16:\"select_template1\";s:19:\"theme_color_schemes\";s:13:\"default_color\";s:20:\"custom_color_schemes\";s:7:\"#FF2E34\";s:21:\"default_color_schemes\";s:7:\"#eb5054\";s:17:\"theme_font_family\";s:5:\"Khand\";s:20:\"section_heading_size\";s:2:\"30\";s:24:\"section_sub_heading_size\";s:2:\"18\";s:24:\"section_description_size\";s:2:\"18\";s:17:\"section_icon_size\";s:2:\"30\";s:24:\"sub_section_heading_size\";s:2:\"20\";s:28:\"sub_section_sub_heading_size\";s:2:\"17\";s:28:\"sub_section_description_size\";s:2:\"15\";s:21:\"sub_section_icon_size\";s:2:\"28\";s:16:\"social_link_size\";s:2:\"14\";s:18:\"social_icons_onoff\";s:2:\"on\";s:13:\"social_icon_1\";s:15:\"fab fa-facebook\";s:13:\"social_icon_2\";s:18:\"fab fa-google-plus\";s:13:\"social_icon_3\";s:15:\"fab fa-linkedin\";s:13:\"social_icon_4\";s:14:\"fab fa-twitter\";s:13:\"social_icon_5\";s:16:\"fab fa-instagram\";s:13:\"social_link_1\";s:1:\"#\";s:13:\"social_link_2\";s:1:\"#\";s:13:\"social_link_3\";s:1:\"#\";s:13:\"social_link_4\";s:1:\"#\";s:13:\"social_link_5\";s:1:\"#\";s:15:\"social_icolor_1\";s:7:\"#3b5998\";s:15:\"social_icolor_2\";s:7:\"#c92228\";s:15:\"social_icolor_3\";s:7:\"#3b5998\";s:15:\"social_icolor_4\";s:7:\"#00aced\";s:15:\"social_icolor_5\";s:7:\"#3f729b\";s:17:\"social_link_tab_1\";s:3:\"off\";s:17:\"social_link_tab_2\";s:3:\"off\";s:17:\"social_link_tab_3\";s:3:\"off\";s:17:\"social_link_tab_4\";s:3:\"off\";s:17:\"social_link_tab_5\";s:3:\"off\";s:18:\"total_Social_links\";s:1:\"5\";s:16:\"social_icon_list\";s:0:\"\";s:15:\"subscriber_form\";s:2:\"on\";s:21:\"subscriber_form_title\";s:23:\"SUBSCRIBE TO NEWSLETTER\";s:20:\"subscriber_form_icon\";s:15:\"fas fa-envelope\";s:25:\"subscriber_form_sub_title\";s:65:\"Subscribe to our mailing list to get updates to your email inbox.\";s:23:\"subscriber_form_message\";s:22:\"GET MONTHLY NEWSLETTER\";s:17:\"sub_form_bg_color\";s:7:\"#333333\";s:20:\"sub_form_button_text\";s:9:\"Subscribe\";s:14:\"wl_gdpr_select\";s:2:\"no\";s:22:\"sub_form_button_f_name\";s:10:\"First Name\";s:22:\"sub_form_button_l_name\";s:9:\"Last Name\";s:24:\"sub_form_subscribe_title\";s:5:\"Email\";s:24:\"sub_form_button_ht_color\";s:7:\"#333333\";s:24:\"sub_form_button_hb_color\";s:7:\"#FFFFFF\";s:22:\"sub_form_ph_text_color\";s:0:\"\";s:9:\"user_sets\";s:14:\"$user_sets_all\";s:35:\"sub_form_subscribe_seuccess_message\";s:42:\"Thank you! We will be back with the quote.\";s:34:\"sub_form_subscribe_invalid_message\";s:28:\"You have already subscribed.\";s:19:\"subscriber_msg_body\";s:0:\"\";s:42:\"sub_form_subscribe_confirm_success_message\";s:83:\"Thank You!!! Subscription has been confirmed. We will notify when the site is live.\";s:42:\"sub_form_subscribe_already_confirm_message\";s:73:\"You subscription is already active. We will notify when the site is live.\";s:37:\"sub_form_invalid_confirmation_message\";s:36:\"Error: Invalid subscription details.\";s:16:\"subscribe_select\";s:7:\"wp_mail\";s:16:\"wp_mail_email_id\";s:18:\"nmnaba14@gmail.com\";s:13:\"php_user_name\";s:5:\"admin\";s:17:\"php_user_email_id\";s:18:\"nmnaba14@gmail.com\";s:23:\"confirm_email_subscribe\";s:3:\"off\";s:17:\"mailchimp_api_key\";s:0:\"\";s:17:\"mailchimp_list_id\";s:0:\"\";s:16:\"madmimi_username\";s:0:\"\";s:15:\"madmimi_api_key\";s:0:\"\";s:19:\"madmimi_list_option\";s:0:\"\";s:28:\"subscriber_users_mail_option\";s:9:\"all_users\";s:23:\"subscriber_mail_subject\";s:0:\"\";s:23:\"subscriber_mail_message\";s:0:\"\";}', 'yes'),
(789, 'weblizar_nls_mailchimp_key', 's:7:\"s:0:\"\";\";', 'yes'),
(790, 'weblizar_nls_madmimi_list', 's:7:\"s:0:\"\";\";', 'yes'),
(792, 'widget_nls_form_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(797, 'wpcf7-mce-post-id', '5829', 'no'),
(798, 'wpcf7-mce-post-update', '2019-10-10T19:57:01', 'no'),
(799, 'mce_conten_panel_master', '<p class=\"about-description\">Easier setup to get you up and running in no time. Please <a href=\"https://renzojohnson.com/contact\" target=\"_blank\" rel=\"noopener noreferrer\">lets us know</a> what kind of features you would like to see added <a href=\"https://renzojohnson.com/contact\" target=\"_blank\" rel=\"noopener noreferrer\">HERE</a>.</p>\n<div class=\"welcome-panel-column-container\">\n<div class=\"welcome-panel-column\">\n<h3>Get Started</h3>\n<p>Make sure it works as you expect <br /><a class=\"button button-primary button-hero load-customize\" href=\"/wp-admin/admin.php?page=wpcf7&amp;post=8&amp;active-tab=4\">Review your settings <span alt=\"f111\" class=\"dashicons dashicons-admin-generic\" style=\"font-size: 17px;vertical-align: middle;\"> </span> </a></p>\n</div>\n<div class=\"welcome-panel-column\">\n<h3>Next Steps</h3>\n<p>Help me develop the plugin and provide support by <br /><a class=\"donate button button-primary button-hero load-customize\" href=\"https://www.paypal.me/renzojohnson\" target=\"_blank\" rel=\"noopener noreferrer\">Donating even a small sum <span alt=\"f524\" class=\"dashicons dashicons-tickets-alt\"> </span></a></p>\n</div>\n</div>\n', 'no'),
(800, 'mce_conten_tittle_master', 'ChimpMatic Lite is now 0.5!', 'no'),
(806, 'widget_mc4wp_form_widget', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(807, 'mc4wp_version', '4.7', 'yes'),
(808, 'mc4wp_flash_messages', 'a:0:{}', 'no'),
(811, 'mc4wp', 'a:6:{s:19:\"grecaptcha_site_key\";s:0:\"\";s:21:\"grecaptcha_secret_key\";s:0:\"\";s:7:\"api_key\";s:36:\"3dc366519137397deed826667350cc4b-us5\";s:20:\"allow_usage_tracking\";i:0;s:15:\"debug_log_level\";s:7:\"warning\";s:18:\"first_activated_on\";i:1572871646;}', 'yes'),
(812, 'mc4wp_default_form_id', '96', 'yes'),
(813, 'mc4wp_form_stylesheets', 'a:0:{}', 'yes'),
(999, 'sb_instagram_errors', 'a:1:{s:12:\"image_editor\";a:2:{i:0;N;i:1;s:69:\"Error editing image. error_loading_image- File doesn&#8217;t exist? |\";}}', 'no'),
(1004, 'widget_a2a_share_save_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1005, 'widget_a2a_follow_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1006, 'addtoany_options', 'a:34:{s:8:\"position\";s:6:\"bottom\";s:30:\"display_in_posts_on_front_page\";s:2:\"-1\";s:33:\"display_in_posts_on_archive_pages\";s:2:\"-1\";s:19:\"display_in_excerpts\";s:2:\"-1\";s:16:\"display_in_posts\";s:1:\"1\";s:16:\"display_in_pages\";s:2:\"-1\";s:22:\"display_in_attachments\";s:2:\"-1\";s:15:\"display_in_feed\";s:1:\"1\";s:7:\"onclick\";s:2:\"-1\";s:9:\"icon_size\";s:2:\"32\";s:7:\"icon_bg\";s:11:\"transparent\";s:13:\"icon_bg_color\";s:7:\"#2a2a2a\";s:7:\"icon_fg\";s:8:\"original\";s:13:\"icon_fg_color\";s:7:\"#ffffff\";s:6:\"button\";s:10:\"A2A_SVG_32\";s:13:\"button_custom\";s:0:\"\";s:17:\"button_show_count\";s:2:\"-1\";s:6:\"header\";s:0:\"\";s:23:\"additional_js_variables\";s:0:\"\";s:14:\"additional_css\";s:0:\"\";s:12:\"custom_icons\";s:2:\"-1\";s:16:\"custom_icons_url\";s:1:\"/\";s:17:\"custom_icons_type\";s:3:\"png\";s:18:\"custom_icons_width\";s:0:\"\";s:19:\"custom_icons_height\";s:0:\"\";s:5:\"cache\";s:2:\"-1\";s:21:\"display_in_cpt_banner\";s:2:\"-1\";s:11:\"button_text\";s:5:\"Share\";s:24:\"special_facebook_options\";a:1:{s:10:\"show_count\";s:2:\"-1\";}s:25:\"special_pinterest_options\";a:1:{s:10:\"show_count\";s:2:\"-1\";}s:15:\"active_services\";a:1:{i:0;s:13:\"facebook_like\";}s:29:\"special_facebook_like_options\";a:2:{s:10:\"show_count\";s:2:\"-1\";s:4:\"verb\";s:4:\"like\";}s:29:\"special_twitter_tweet_options\";a:1:{s:10:\"show_count\";s:2:\"-1\";}s:29:\"special_pinterest_pin_options\";a:1:{s:10:\"show_count\";s:2:\"-1\";}}', 'yes'),
(1010, 'analyst_cache', 's:6:\"a:0:{}\";', 'yes'),
(1014, 'sfsi_custom_icons', 'no', 'yes'),
(1020, 'sfsi_plus_new_show_notification', 'yes', 'yes'),
(1021, 'sfsi_plus_show_premium_cumulative_count_notification', 'yes', 'yes'),
(1022, 'sfsi_plus_custom_icons', 'no', 'yes'),
(1023, 'sfsi_plus_section1_options', 's:617:\"a:15:{s:21:\"sfsi_plus_rss_display\";s:2:\"no\";s:23:\"sfsi_plus_email_display\";s:2:\"no\";s:26:\"sfsi_plus_facebook_display\";s:3:\"yes\";s:25:\"sfsi_plus_twitter_display\";s:3:\"yes\";s:25:\"sfsi_plus_youtube_display\";s:2:\"no\";s:27:\"sfsi_plus_pinterest_display\";s:2:\"no\";s:26:\"sfsi_plus_linkedin_display\";s:2:\"no\";s:27:\"sfsi_plus_instagram_display\";s:3:\"yes\";s:20:\"sfsi_plus_ok_display\";s:2:\"no\";s:26:\"sfsi_plus_telegram_display\";s:3:\"yes\";s:20:\"sfsi_plus_vk_display\";s:2:\"no\";s:24:\"sfsi_plus_wechat_display\";s:2:\"no\";s:23:\"sfsi_plus_weibo_display\";s:2:\"no\";s:23:\"sfsi_plus_houzz_display\";s:2:\"no\";s:17:\"sfsi_custom_files\";s:0:\"\";}\";', 'yes'),
(1024, 'sfsi_plus_section2_options', 's:2490:\"a:48:{s:17:\"sfsi_plus_rss_url\";s:36:\"http://localhost/WpLearning_03/feed/\";s:19:\"sfsi_plus_rss_icons\";s:9:\"subscribe\";s:19:\"sfsi_plus_email_url\";s:297:\"https://www.specificfeeds.com/widgets/emailSubscribeEncFeed/R2ZCNjJ4Yk9ncHFTbHkyNnBYL0RjUnFwcnBIZ1VxYmdiSk54QjhNa1NRdUp6SUE4b1RZc2lpalBIN3h2Si9wZmdzTUtRa0VpekhJWWtTSTJmeXRqdkJXWGhsMmF5YjR0L09lNGVmMDIwU3pvRlUzQlhvV21zSXdFbGJVRk84OUx8T3NiWUFFMnQ5RTRlWG00Tm1SaVkvTUlyMG1ZamxBRzVVRS96YXZsVDNZVT0=/OA==\";s:29:\"sfsi_plus_facebookPage_option\";s:2:\"no\";s:26:\"sfsi_plus_facebookPage_url\";s:0:\"\";s:29:\"sfsi_plus_facebookLike_option\";s:3:\"yes\";s:30:\"sfsi_plus_facebookShare_option\";s:3:\"yes\";s:26:\"sfsi_plus_twitter_followme\";s:2:\"no\";s:32:\"sfsi_plus_twitter_followUserName\";s:0:\"\";s:27:\"sfsi_plus_twitter_aboutPage\";s:3:\"yes\";s:22:\"sfsi_plus_twitter_page\";s:2:\"no\";s:25:\"sfsi_plus_twitter_pageURL\";s:0:\"\";s:31:\"sfsi_plus_twitter_aboutPageText\";s:82:\"Hey, check out this cool site I found: www.yourname.com #Topic via@my_twitter_name\";s:25:\"sfsi_plus_youtube_pageUrl\";s:0:\"\";s:22:\"sfsi_plus_youtube_page\";s:2:\"no\";s:24:\"sfsi_plus_youtube_follow\";s:2:\"no\";s:29:\"sfsi_plus_youtubeusernameorid\";s:4:\"name\";s:22:\"sfsi_plus_ytube_chnlid\";s:0:\"\";s:20:\"sfsi_plus_ytube_user\";s:0:\"\";s:24:\"sfsi_plus_pinterest_page\";s:2:\"no\";s:27:\"sfsi_plus_pinterest_pageUrl\";s:0:\"\";s:28:\"sfsi_plus_pinterest_pingBlog\";s:0:\"\";s:24:\"sfsi_plus_instagram_page\";s:2:\"no\";s:27:\"sfsi_plus_instagram_pageUrl\";s:0:\"\";s:23:\"sfsi_plus_houzz_pageUrl\";s:0:\"\";s:23:\"sfsi_plus_linkedin_page\";s:2:\"no\";s:26:\"sfsi_plus_linkedin_pageURL\";s:0:\"\";s:25:\"sfsi_plus_linkedin_follow\";s:2:\"no\";s:32:\"sfsi_plus_linkedin_followCompany\";s:0:\"\";s:28:\"sfsi_plus_linkedin_SharePage\";s:3:\"yes\";s:35:\"sfsi_plus_linkedin_recommendBusines\";s:2:\"no\";s:35:\"sfsi_plus_linkedin_recommendCompany\";s:0:\"\";s:37:\"sfsi_plus_linkedin_recommendProductId\";s:0:\"\";s:24:\"sfsi_plus_okVisit_option\";s:2:\"no\";s:21:\"sfsi_plus_okVisit_url\";s:0:\"\";s:28:\"sfsi_plus_okSubscribe_option\";s:2:\"no\";s:28:\"sfsi_plus_okSubscribe_userid\";s:0:\"\";s:23:\"sfsi_plus_okLike_option\";s:2:\"no\";s:29:\"sfsi_plus_wechatFollow_option\";s:2:\"no\";s:28:\"sfsi_plus_wechatShare_option\";s:2:\"no\";s:30:\"sfsi_plus_telegramShare_option\";s:2:\"no\";s:32:\"sfsi_plus_telegramMessage_option\";s:2:\"no\";s:26:\"sfsi_plus_telegram_message\";s:0:\"\";s:27:\"sfsi_plus_telegram_username\";s:0:\"\";s:26:\"sfsi_plus_CustomIcon_links\";s:0:\"\";s:27:\"sfsi_plus_premium_email_box\";s:3:\"yes\";s:30:\"sfsi_plus_premium_facebook_box\";s:3:\"yes\";s:29:\"sfsi_plus_premium_twitter_box\";s:3:\"yes\";}\";', 'yes'),
(1025, 'sfsi_plus_section3_options', 's:708:\"a:15:{s:23:\"sfsi_plus_actvite_theme\";s:7:\"default\";s:19:\"sfsi_plus_mouseOver\";s:2:\"no\";s:26:\"sfsi_plus_mouseOver_effect\";s:7:\"fade_in\";s:31:\"sfsi_plus_mouseover_effect_type\";s:10:\"same_icons\";s:23:\"sfsi_plus_shuffle_icons\";s:2:\"no\";s:27:\"sfsi_plus_shuffle_Firstload\";s:2:\"no\";s:26:\"sfsi_plus_shuffle_interval\";s:2:\"no\";s:30:\"sfsi_plus_shuffle_intervalTime\";i:0;s:31:\"sfsi_plus_specialIcon_animation\";s:0:\"\";s:31:\"sfsi_plus_specialIcon_MouseOver\";s:2:\"no\";s:31:\"sfsi_plus_specialIcon_Firstload\";s:2:\"no\";s:37:\"sfsi_plus_specialIcon_Firstload_Icons\";s:3:\"all\";s:30:\"sfsi_plus_specialIcon_interval\";s:2:\"no\";s:34:\"sfsi_plus_specialIcon_intervalTime\";s:0:\"\";s:35:\"sfsi_plus_specialIcon_intervalIcons\";s:3:\"all\";}\";', 'yes'),
(1026, 'sfsi_plus_section4_options', 's:2211:\"a:49:{s:24:\"sfsi_plus_display_counts\";s:2:\"no\";s:29:\"sfsi_plus_email_countsDisplay\";s:2:\"no\";s:26:\"sfsi_plus_email_countsFrom\";s:6:\"source\";s:28:\"sfsi_plus_email_manualCounts\";s:2:\"20\";s:27:\"sfsi_plus_rss_countsDisplay\";s:2:\"no\";s:26:\"sfsi_plus_rss_manualCounts\";s:2:\"20\";s:27:\"sfsi_plus_facebook_PageLink\";s:0:\"\";s:32:\"sfsi_plus_facebook_countsDisplay\";s:2:\"no\";s:29:\"sfsi_plus_facebook_countsFrom\";s:6:\"manual\";s:31:\"sfsi_plus_facebook_manualCounts\";s:2:\"20\";s:31:\"sfsi_plus_twitter_countsDisplay\";s:2:\"no\";s:28:\"sfsi_plus_twitter_countsFrom\";s:6:\"manual\";s:30:\"sfsi_plus_twitter_manualCounts\";s:2:\"20\";s:32:\"sfsi_plus_linkedIn_countsDisplay\";s:2:\"no\";s:29:\"sfsi_plus_linkedIn_countsFrom\";s:6:\"manual\";s:31:\"sfsi_plus_linkedIn_manualCounts\";s:2:\"20\";s:20:\"sfsi_plus_ln_api_key\";s:0:\"\";s:23:\"sfsi_plus_ln_secret_key\";s:0:\"\";s:29:\"sfsi_plus_ln_oAuth_user_token\";s:0:\"\";s:20:\"sfsi_plus_ln_company\";s:0:\"\";s:22:\"sfsi_plus_youtube_user\";s:0:\"\";s:27:\"sfsi_plus_youtube_channelId\";s:0:\"\";s:31:\"sfsi_plus_youtube_countsDisplay\";s:2:\"no\";s:28:\"sfsi_plus_youtube_countsFrom\";s:6:\"manual\";s:30:\"sfsi_plus_youtube_manualCounts\";s:2:\"20\";s:33:\"sfsi_plus_pinterest_countsDisplay\";s:2:\"no\";s:30:\"sfsi_plus_pinterest_countsFrom\";s:6:\"manual\";s:32:\"sfsi_plus_pinterest_manualCounts\";s:2:\"20\";s:24:\"sfsi_plus_pinterest_user\";s:0:\"\";s:25:\"sfsi_plus_pinterest_board\";s:0:\"\";s:30:\"sfsi_plus_instagram_countsFrom\";s:6:\"manual\";s:33:\"sfsi_plus_instagram_countsDisplay\";s:2:\"no\";s:32:\"sfsi_plus_instagram_manualCounts\";s:2:\"20\";s:24:\"sfsi_plus_instagram_User\";s:0:\"\";s:28:\"sfsi_plus_instagram_clientid\";s:0:\"\";s:26:\"sfsi_plus_instagram_appurl\";s:0:\"\";s:25:\"sfsi_plus_instagram_token\";s:0:\"\";s:29:\"sfsi_plus_houzz_countsDisplay\";s:2:\"no\";s:26:\"sfsi_plus_houzz_countsFrom\";s:6:\"manual\";s:28:\"sfsi_plus_houzz_manualCounts\";s:2:\"20\";s:26:\"sfsi_plus_ok_countsDisplay\";s:2:\"no\";s:26:\"sfsi_plus_vk_countsDisplay\";s:2:\"no\";s:32:\"sfsi_plus_telegram_countsDisplay\";s:2:\"no\";s:29:\"sfsi_plus_weibo_countsDisplay\";s:2:\"no\";s:25:\"sfsi_plus_ok_manualCounts\";s:2:\"20\";s:25:\"sfsi_plus_vk_manualCounts\";s:2:\"20\";s:31:\"sfsi_plus_telegram_manualCounts\";s:2:\"20\";s:28:\"sfsi_plus_weibo_manualCounts\";s:2:\"20\";s:27:\"sfsi_plus_premium_count_box\";s:3:\"yes\";}\";', 'yes'),
(1027, 'sfsi_plus_section5_options', 's:1804:\"a:41:{s:20:\"sfsi_plus_icons_size\";i:40;s:23:\"sfsi_plus_icons_spacing\";i:5;s:25:\"sfsi_plus_icons_Alignment\";s:4:\"left\";s:22:\"sfsi_plus_icons_perRow\";i:5;s:31:\"sfsi_plus_follow_icons_language\";s:12:\"Follow_en_US\";s:33:\"sfsi_plus_facebook_icons_language\";s:14:\"Visit_us_en_US\";s:32:\"sfsi_plus_twitter_icons_language\";s:14:\"Visit_us_en_US\";s:24:\"sfsi_plus_icons_language\";s:5:\"en_US\";s:29:\"sfsi_plus_icons_ClickPageOpen\";s:3:\"yes\";s:21:\"sfsi_plus_icons_float\";s:2:\"no\";s:28:\"sfsi_plus_disable_floaticons\";s:2:\"no\";s:26:\"sfsi_plus_disable_viewport\";s:2:\"no\";s:29:\"sfsi_plus_icons_floatPosition\";s:12:\"center-right\";s:21:\"sfsi_plus_icons_stick\";s:2:\"no\";s:27:\"sfsi_plus_rss_MouseOverText\";s:0:\"\";s:29:\"sfsi_plus_email_MouseOverText\";s:0:\"\";s:31:\"sfsi_plus_twitter_MouseOverText\";s:7:\"Twitter\";s:32:\"sfsi_plus_facebook_MouseOverText\";s:8:\"Facebook\";s:32:\"sfsi_plus_linkedIn_MouseOverText\";s:0:\"\";s:33:\"sfsi_plus_pinterest_MouseOverText\";s:0:\"\";s:31:\"sfsi_plus_youtube_MouseOverText\";s:0:\"\";s:33:\"sfsi_plus_instagram_MouseOverText\";s:9:\"Instagram\";s:29:\"sfsi_plus_houzz_MouseOverText\";s:0:\"\";s:27:\"sfsi_plus_CustomIcons_order\";s:0:\"\";s:23:\"sfsi_plus_rssIcon_order\";i:1;s:25:\"sfsi_plus_emailIcon_order\";i:2;s:28:\"sfsi_plus_facebookIcon_order\";i:3;s:27:\"sfsi_plus_twitterIcon_order\";i:4;s:27:\"sfsi_plus_youtubeIcon_order\";i:5;s:29:\"sfsi_plus_pinterestIcon_order\";i:7;s:29:\"sfsi_plus_instagramIcon_order\";i:9;s:25:\"sfsi_plus_houzzIcon_order\";i:10;s:22:\"sfsi_plus_okIcon_order\";i:24;s:28:\"sfsi_plus_telegramIcon_order\";i:0;s:22:\"sfsi_plus_vkIcon_order\";i:23;s:25:\"sfsi_plus_weiboIcon_order\";i:25;s:26:\"sfsi_plus_wechatIcon_order\";i:26;s:28:\"sfsi_plus_linkedinIcon_order\";i:8;s:31:\"sfsi_plus_custom_MouseOverTexts\";s:0:\"\";s:28:\"sfsi_plus_custom_social_hide\";s:2:\"no\";s:32:\"sfsi_pplus_icons_suppress_errors\";s:2:\"no\";}\";', 'yes'),
(1028, 'sfsi_plus_section6_options', 's:343:\"a:7:{s:22:\"sfsi_plus_show_Onposts\";s:2:\"no\";s:23:\"sfsi_plus_show_Onbottom\";s:2:\"no\";s:27:\"sfsi_plus_icons_postPositon\";s:6:\"source\";s:25:\"sfsi_plus_icons_alignment\";s:12:\"center-right\";s:27:\"sfsi_plus_rss_countsDisplay\";s:2:\"no\";s:25:\"sfsi_plus_textBefor_icons\";s:26:\"Please follow and like us:\";s:29:\"sfsi_plus_icons_DisplayCounts\";s:2:\"no\";}\";', 'yes'),
(1029, 'sfsi_plus_section7_options', 's:653:\"a:13:{s:20:\"sfsi_plus_popup_text\";s:42:\"Enjoy this blog? Please spread the word :)\";s:20:\"sfsi_plus_popup_font\";s:26:\"Helvetica,Arial,sans-serif\";s:25:\"sfsi_plus_popup_fontColor\";s:7:\"#000000\";s:24:\"sfsi_plus_popup_fontSize\";i:30;s:25:\"sfsi_plus_popup_fontStyle\";s:0:\"\";s:32:\"sfsi_plus_popup_background_color\";s:7:\"#eff7f7\";s:28:\"sfsi_plus_popup_border_color\";s:7:\"#f3faf2\";s:32:\"sfsi_plus_popup_border_thickness\";i:1;s:29:\"sfsi_plus_popup_border_shadow\";s:3:\"yes\";s:22:\"sfsi_plus_Show_popupOn\";s:12:\"selectedpage\";s:30:\"sfsi_plus_Show_popupOn_PageIDs\";s:9:\"s:2:\"14\";\";s:19:\"sfsi_plus_Shown_pop\";s:8:\"ETscroll\";s:29:\"sfsi_plus_Shown_popupOnceTime\";i:0;}\";', 'yes'),
(1030, 'sfsi_plus_section8_options', 's:1953:\"a:28:{s:25:\"sfsi_plus_show_via_widget\";s:2:\"no\";s:23:\"sfsi_plus_float_on_page\";s:2:\"no\";s:29:\"sfsi_plus_float_page_position\";s:12:\"center-right\";s:31:\"sfsi_plus_icons_floatMargin_top\";i:0;s:34:\"sfsi_plus_icons_floatMargin_bottom\";i:0;s:32:\"sfsi_plus_icons_floatMargin_left\";i:0;s:33:\"sfsi_plus_icons_floatMargin_right\";i:0;s:29:\"sfsi_plus_place_item_manually\";s:2:\"no\";s:30:\"sfsi_plus_place_item_gutenberg\";s:2:\"no\";s:27:\"sfsi_plus_show_item_onposts\";s:3:\"yes\";s:29:\"sfsi_plus_display_button_type\";s:13:\"normal_button\";s:25:\"sfsi_plus_post_icons_size\";i:40;s:28:\"sfsi_plus_post_icons_spacing\";i:5;s:22:\"sfsi_plus_show_Onposts\";s:2:\"no\";s:25:\"sfsi_plus_textBefor_icons\";s:26:\"Please follow and like us:\";s:25:\"sfsi_plus_icons_alignment\";s:1:\"5\";s:29:\"sfsi_plus_icons_DisplayCounts\";s:2:\"no\";s:30:\"sfsi_plus_display_before_posts\";s:2:\"no\";s:29:\"sfsi_plus_display_after_posts\";s:2:\"no\";s:34:\"sfsi_plus_display_before_blogposts\";s:2:\"no\";s:33:\"sfsi_plus_display_after_blogposts\";s:2:\"no\";s:17:\"sfsi_plus_rectsub\";s:3:\"yes\";s:16:\"sfsi_plus_rectfb\";s:3:\"yes\";s:18:\"sfsi_plus_recttwtr\";s:3:\"yes\";s:19:\"sfsi_plus_rectpinit\";s:3:\"yes\";s:21:\"sfsi_plus_rectfbshare\";s:3:\"yes\";s:35:\"sfsi_plus_responsive_icons_end_post\";s:2:\"no\";s:26:\"sfsi_plus_responsive_icons\";a:2:{s:13:\"default_icons\";a:3:{s:8:\"facebook\";a:3:{s:6:\"active\";s:3:\"yes\";s:4:\"text\";s:17:\"Share on Facebook\";s:3:\"url\";s:0:\"\";}s:7:\"Twitter\";a:3:{s:6:\"active\";s:3:\"yes\";s:4:\"text\";s:5:\"Tweet\";s:3:\"url\";s:0:\"\";}s:6:\"Follow\";a:3:{s:6:\"active\";s:3:\"yes\";s:4:\"text\";s:9:\"Follow us\";s:3:\"url\";s:0:\"\";}}s:8:\"settings\";a:12:{s:9:\"icon_size\";s:6:\"Medium\";s:15:\"icon_width_type\";s:16:\"Fully responsive\";s:15:\"icon_width_size\";s:3:\"240\";s:9:\"edge_type\";s:5:\"Round\";s:11:\"edge_radius\";s:1:\"5\";s:5:\"style\";s:8:\"Gradient\";s:6:\"margin\";s:2:\"10\";s:10:\"text_align\";s:8:\"Centered\";s:10:\"show_count\";s:2:\"no\";s:13:\"counter_color\";s:7:\"#aaaaaa\";s:16:\"counter_bg_color\";s:4:\"#fff\";s:16:\"share_count_text\";s:6:\"SHARES\";}}}\";', 'yes'),
(1031, 'sfsi_plus_feed_id', 'R2ZCNjJ4Yk9ncHFTbHkyNnBYL0RjUnFwcnBIZ1VxYmdiSk54QjhNa1NRdUp6SUE4b1RZc2lpalBIN3h2Si9wZmdzTUtRa0VpekhJWWtTSTJmeXRqdkJXWGhsMmF5YjR0L09lNGVmMDIwU3pvRlUzQlhvV21zSXdFbGJVRk84OUx8T3NiWUFFMnQ5RTRlWG00Tm1SaVkvTUlyMG1ZamxBRzVVRS96YXZsVDNZVT0=', 'yes'),
(1032, 'sfsi_plus_redirect_url', 'https://www.specificfeeds.com/widgets/emailSubscribeEncFeed/R2ZCNjJ4Yk9ncHFTbHkyNnBYL0RjUnFwcnBIZ1VxYmdiSk54QjhNa1NRdUp6SUE4b1RZc2lpalBIN3h2Si9wZmdzTUtRa0VpekhJWWtTSTJmeXRqdkJXWGhsMmF5YjR0L09lNGVmMDIwU3pvRlUzQlhvV21zSXdFbGJVRk84OUx8T3NiWUFFMnQ5RTRlWG00Tm1SaVkvTUlyMG1ZamxBRzVVRS96YXZsVDNZVT0=/OA==', 'yes'),
(1033, 'sfsi_plus_installDate', '2019-11-07 10:06:01', 'yes'),
(1034, 'sfsi_plus_RatingDiv', 'no', 'yes'),
(1035, 'sfsi_plus_activate', '0', 'yes'),
(1036, 'sfsi_plus_instagram_sf_count', 's:141:\"a:4:{s:7:\"date_sf\";i:1573084800;s:14:\"date_instagram\";i:1573084800;s:18:\"sfsi_plus_sf_count\";s:0:\"\";s:25:\"sfsi_plus_instagram_count\";s:0:\"\";}\";', 'yes'),
(1037, 'adding_plustags', 'yes', 'yes'),
(1038, 'widget_sfsi-plus-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1039, 'widget_sfsiplus_subscriber_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1040, 'sfsi_plus_pluginVersion', '3.28', 'yes'),
(1041, 'sfsi_plus_serverphpVersionnotification', 'yes', 'yes'),
(1042, 'sfsi_plus_show_Setting_mobile_notification', 'yes', 'yes'),
(1043, 'sfsi_plus_show_premium_notification', 'yes', 'yes'),
(1044, 'sfsi_plus_show_notification', 'yes', 'yes'),
(1045, 'sfsi_plus_footer_sec', 'yes', 'yes'),
(1046, 'sfsi_plus_section9_options', 's:1317:\"a:26:{s:25:\"sfsi_plus_form_adjustment\";s:3:\"yes\";s:21:\"sfsi_plus_form_height\";i:180;s:20:\"sfsi_plus_form_width\";i:230;s:21:\"sfsi_plus_form_border\";s:3:\"yes\";s:31:\"sfsi_plus_form_border_thickness\";i:1;s:27:\"sfsi_plus_form_border_color\";s:7:\"#b5b5b5\";s:25:\"sfsi_plus_form_background\";s:7:\"#ffffff\";s:27:\"sfsi_plus_form_heading_text\";s:23:\"Get new posts by email:\";s:27:\"sfsi_plus_form_heading_font\";s:26:\"Helvetica,Arial,sans-serif\";s:32:\"sfsi_plus_form_heading_fontstyle\";s:4:\"bold\";s:32:\"sfsi_plus_form_heading_fontcolor\";s:7:\"#000000\";s:31:\"sfsi_plus_form_heading_fontsize\";i:16;s:32:\"sfsi_plus_form_heading_fontalign\";s:6:\"center\";s:25:\"sfsi_plus_form_field_text\";s:16:\"Enter your email\";s:25:\"sfsi_plus_form_field_font\";s:26:\"Helvetica,Arial,sans-serif\";s:30:\"sfsi_plus_form_field_fontstyle\";s:6:\"normal\";s:30:\"sfsi_plus_form_field_fontcolor\";s:0:\"\";s:29:\"sfsi_plus_form_field_fontsize\";i:14;s:30:\"sfsi_plus_form_field_fontalign\";s:6:\"center\";s:26:\"sfsi_plus_form_button_text\";s:9:\"Subscribe\";s:26:\"sfsi_plus_form_button_font\";s:26:\"Helvetica,Arial,sans-serif\";s:31:\"sfsi_plus_form_button_fontstyle\";s:4:\"bold\";s:31:\"sfsi_plus_form_button_fontcolor\";s:7:\"#000000\";s:30:\"sfsi_plus_form_button_fontsize\";i:16;s:31:\"sfsi_plus_form_button_fontalign\";s:6:\"center\";s:32:\"sfsi_plus_form_button_background\";s:7:\"#dedede\";}\";', 'yes'),
(1048, 'sfsi_plus_addThis_icon_removal_notice_dismissed', '1', 'yes'),
(1049, 'sfsi_plus_verificatiom_code', 'yEF9B30SnQKaKXn6RJb7', 'yes'),
(1052, 'analyst_accounts_data', 's:431:\"O:26:\"Account\\AccountDataFactory\":1:{s:11:\"\0*\0accounts\";a:1:{i:0;O:19:\"Account\\AccountData\":7:{s:5:\"\0*\0id\";s:16:\"w6l8b75dy5qkv9ze\";s:9:\"\0*\0secret\";s:40:\"39db55426579986bb6c79c6d94aa6ab82b67f9f5\";s:7:\"\0*\0path\";s:107:\"D:\\xampp\\htdocs\\WpLearning_03\\wp-content\\plugins\\ultimate-social-media-plus\\ultimate_social_media_icons.php\";s:14:\"\0*\0isInstalled\";b:0;s:12:\"\0*\0isOptedIn\";b:0;s:11:\"\0*\0isSigned\";b:0;s:20:\"\0*\0isInstallResolved\";N;}}}\";', 'yes'),
(1074, 'widget_sfsi-widget', 'a:2:{i:2;a:2:{s:5:\"showf\";i:1;s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(1082, 'sfsi_pplus_error_reporting_notice_dismissed', '1', 'yes'),
(1091, 'mashsb_settings', 'a:37:{s:16:\"visible_services\";s:1:\"2\";s:8:\"networks\";a:3:{i:0;a:3:{s:2:\"id\";s:8:\"facebook\";s:6:\"status\";s:1:\"1\";s:4:\"name\";s:0:\"\";}i:2;a:2:{s:2:\"id\";s:9:\"subscribe\";s:4:\"name\";s:0:\"\";}i:1;a:3:{s:2:\"id\";s:7:\"twitter\";s:6:\"status\";s:1:\"1\";s:4:\"name\";s:0:\"\";}}s:10:\"post_types\";a:1:{s:4:\"post\";s:4:\"post\";}s:19:\"mashsharer_position\";s:6:\"manual\";s:7:\"loadall\";s:1:\"1\";s:12:\"twitter_card\";s:1:\"1\";s:10:\"open_graph\";s:1:\"1\";s:18:\"mashsb_sharemethod\";s:11:\"sharedcount\";s:14:\"caching_method\";s:15:\"refresh_loading\";s:14:\"mashsu_methods\";s:8:\"disabled\";s:18:\"responsive_buttons\";s:1:\"1\";s:13:\"button_margin\";s:1:\"1\";s:17:\"text_align_center\";s:1:\"1\";s:16:\"mashsharer_round\";s:1:\"1\";s:16:\"mashsharer_cache\";s:3:\"300\";s:19:\"facebook_count_mode\";s:6:\"shares\";s:15:\"execution_order\";s:4:\"1000\";s:16:\"sharecount_title\";s:6:\"SHARES\";s:11:\"share_color\";s:7:\"#cccccc\";s:12:\"buttons_size\";s:10:\"mash-small\";s:12:\"button_width\";s:3:\"177\";s:13:\"border_radius\";s:7:\"default\";s:10:\"mash_style\";s:7:\"default\";s:18:\"subscribe_behavior\";s:7:\"content\";s:13:\"content_above\";s:0:\"\";s:13:\"content_below\";s:0:\"\";s:17:\"subscribe_content\";s:0:\"\";s:10:\"custom_css\";s:0:\"\";s:7:\"amp_css\";s:0:\"\";s:17:\"mashsharer_apikey\";s:0:\"\";s:10:\"fake_count\";s:0:\"\";s:15:\"hide_sharecount\";s:0:\"\";s:16:\"fb_publisher_url\";s:0:\"\";s:18:\"mashsharer_hashtag\";s:0:\"\";s:14:\"subscribe_link\";s:0:\"\";s:13:\"excluded_from\";s:0:\"\";s:18:\"bitly_access_token\";s:0:\"\";}', 'yes'),
(1092, 'mashsb_version', '3.7.1', 'yes'),
(1093, 'mashsb_installDate', '2019-11-07 10:44:40', 'yes'),
(1094, 'mashsb_RatingDiv', 'no', 'yes'),
(1095, 'mashsb_update_notice_101', 'yes', 'yes'),
(1096, 'mashsb_networks', 'a:3:{i:0;s:8:\"Facebook\";i:2;s:9:\"Subscribe\";i:1;s:7:\"Twitter\";}', 'yes'),
(1099, 'widget_mashsb_mostshared_posts_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1123, 'widget_heateor_sss_follow', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1124, 'heateor_sss_gdpr_notification_read', '1', 'yes'),
(1150, 'ssb_networks', 'a:1:{s:14:\"icon_selection\";s:31:\"fbshare,twitter,linkedin,fblike\";}', 'yes'),
(1151, 'ssb_themes', 'a:1:{s:10:\"icon_style\";s:12:\"simple-icons\";}', 'yes'),
(1152, 'ssb_positions', 'a:1:{s:8:\"position\";a:1:{s:6:\"inline\";s:6:\"inline\";}}', 'yes'),
(1153, 'ssb_inline', 'a:14:{s:8:\"location\";s:5:\"below\";s:14:\"icon_alignment\";s:4:\"left\";s:9:\"animation\";s:12:\"no-animation\";s:12:\"share_counts\";s:1:\"0\";s:11:\"total_share\";s:1:\"0\";s:10:\"icon_space\";s:1:\"0\";s:16:\"icon_space_value\";s:0:\"\";s:11:\"hide_mobile\";s:1:\"0\";s:16:\"show_on_category\";s:1:\"0\";s:15:\"show_on_archive\";s:1:\"0\";s:11:\"show_on_tag\";s:1:\"0\";s:14:\"show_on_search\";s:1:\"0\";s:11:\"share_title\";s:0:\"\";s:5:\"posts\";a:1:{s:4:\"post\";s:4:\"post\";}}', 'yes'),
(1155, 'widget_ssb_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1156, 'ssb_sidebar', 'a:8:{s:11:\"orientation\";s:4:\"left\";s:9:\"animation\";s:12:\"no-animation\";s:12:\"share_counts\";s:1:\"0\";s:11:\"total_share\";s:1:\"0\";s:10:\"icon_space\";s:1:\"0\";s:16:\"icon_space_value\";s:0:\"\";s:11:\"hide_mobile\";s:1:\"0\";s:5:\"posts\";a:2:{s:4:\"post\";s:4:\"post\";s:4:\"page\";s:4:\"page\";}}', 'yes'),
(1157, 'ssb_media', '', 'yes'),
(1158, 'ssb_popup', '', 'yes'),
(1159, 'ssb_flyin', '', 'yes'),
(1160, 'ssb_advanced', '', 'yes'),
(1161, 'ssb_active_time', '1573128081', 'no'),
(1174, 'B2S_PLUGIN_GENERAL_OPTIONS', 'a:2:{s:9:\"og_active\";i:1;s:11:\"card_active\";i:1;}', 'no'),
(1175, 'b2s_plugin_version', '590', 'no'),
(1176, 'B2S_PLUGIN_USER_VERSION_1', 'a:3:{s:23:\"B2S_PLUGIN_USER_VERSION\";i:0;s:18:\"B2S_PLUGIN_VERSION\";s:3:\"590\";s:36:\"B2S_PLUGIN_USER_VERSION_NEXT_REQUEST\";i:1573132677;}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1177, 'B2S_PLUGIN_PRIVACY_POLICY_USER_ACCEPT_1', 'YToyOntzOjI6ImRlIjtzOjE3NzUwOiI8Yj5EYXRlbnNjaHV0emVya2zkcnVuZzwvYj4gCjxicj5WZXJhbnR3b3J0bGljaGVyIGltIFNpbm5lIGRlciBEYXRlbnNjaHV0emdlc2V0emUgaXN0Ogo8YnI+RGlwbC4gSW5nLiBBbmRyZWFzIFdpbmtsZXIKPGJyPgo8YnI+PGI+RmFpcmVyIFVtZ2FuZyBtaXQgSWhyZW4gRGF0ZW48L2I+Cjxicj5EaWUgQURFTklPTiBHbWJIIHZlcnBmbGljaHRldCBzaWNoLCBhbGxlIHBlcnP2bmxpY2hlbiBEYXRlbiB3aWUgTmFtZSB1bmQgRS1NYWlsLUFkcmVzc2Ugdm9uIEt1bmRlbiB1bmQgTnV0emVybiB2ZXJ0cmF1bGljaCB6dSBiZWhhbmRlbG4uIERpZSBXZWl0ZXJnYWJlIGFuIERyaXR0ZSBlcmZvbGd0IG51ciB6dXIgRXJm/GxsdW5nIHZlcmVpbmJhcnRlciBad2Vja2UuIERpZXNlIERhdGVuc2NodXR6ZXJrbORydW5nIGb8aHJ0IGF1Ziwgd2llIHdpciBJaHJlIERhdGVuIG51dHplbiB1bmQgc2No/HR6ZW4uCjxicj4KPGJyPjxiPk51dHp1bmdzYmVkaW5ndW5nZW4gZGVyIFdlYnNpdGU8L2I+Cjxicj5Tb2Zlcm4ga2VpbiBnZXNvbmRlcnRlciBMb2ctSW4gZXJmb3JkZXJsaWNoIGlzdCwgZXJmb2xndCBkaWUgQW5lcmtlbm51bmcgZGVyIE51dHp1bmdzYmVkaW5ndW5nZW4gZvxyIGRpZSBOdXR6dW5nIGRlciBXZWJzaXRlIG1pdCBCZWdpbm4gZGVyIE51dHp1bmcgZGVyIFdlYi1TaXRlLgo8YnI+RXJmYXNzdW5nIGFsbGdlbWVpbmVyIEluZm9ybWF0aW9uZW4KPGJyPkRpZSBXZWJzaXRlIGVyaGVidCB1bmQgc3BlaWNoZXJ0IGF1dG9tYXRpc2NoIGluIGlocmVuIFNlcnZlciBMb2cgRmlsZSBJbmZvcm1hdGlvbmVuLCBkaWUgZGVyIEJyb3dzZXIgZGVzIE51dHplcnMgZGVyIFdlYnNpdGUgYW4gQWRlbmlvbiD8YmVybWl0dGVsdC4gCjxicj5EaWVzZSBJbmZvcm1hdGlvbmVuIChTZXJ2ZXItTG9nZmlsZXMpIGJlaW5oYWx0ZW4gZXR3YSBkaWUgQXJ0IGRlcyBXZWJicm93c2VycywgZGFzIHZlcndlbmRldGUgQmV0cmllYnNzeXN0ZW0sIGRlbiBEb21haW5uYW1lbiBJaHJlcyBJbnRlcm5ldCBTZXJ2aWNlIFByb3ZpZGVycyB1bmQgxGhubGljaGVzLiBIaWVyYmVpIGhhbmRlbHQgZXMgc2ljaCBhdXNzY2hsaWXfbGljaCB1bSBJbmZvcm1hdGlvbmVuLCB3ZWxjaGUga2VpbmUgUvxja3NjaGz8c3NlIGF1ZiBJaHJlIFBlcnNvbiB6dWxhc3Nlbi4gRGllc2UgSW5mb3JtYXRpb25lbiBzaW5kIHRlY2huaXNjaCBub3R3ZW5kaWcsIHVtIHZvbiBJaG5lbiBhbmdlZm9yZGVydGUgSW5oYWx0ZSB2b24gV2Vic2VpdGVuIGtvcnJla3QgYXVzenVsaWVmZXJuIHVuZCBmYWxsZW4gYmVpIE51dHp1bmcgZGVzIEludGVybmV0cyB6d2luZ2VuZCBhbi4gQW5vbnltZSBJbmZvcm1hdGlvbmVuIGRpZXNlciBBcnQgd2VyZGVuIHZvbiB1bnMgc3RhdGlzdGlzY2ggYXVzZ2V3ZXJ0ZXQsIHVtIHVuc2VyZW4gSW50ZXJuZXRhdWZ0cml0dCB1bmQgZGllIGRhaGludGVyc3RlaGVuZGUgVGVjaG5payB6dSBvcHRpbWllcmVuLgo8YnI+Cjxicj48Yj5Db29raWVzPC9iPgo8YnI+V2llIHZpZWxlIGFuZGVyZSBXZWJzZWl0ZW4gdmVyd2VuZGVuIHdpciBhdWNoIHNvIGdlbmFubnRlICJDb29raWVzIi4gQ29va2llcyBzaW5kIGtsZWluZSBUZXh0ZGF0ZWllbiwgZGllIHZvbiBlaW5lbSBXZWJzZWl0ZW5zZXJ2ZXIgYXVmIElocmUgRmVzdHBsYXR0ZSD8YmVydHJhZ2VuIHdlcmRlbi4gSGllcmR1cmNoIGVyaGFsdGVuIHdpciBhdXRvbWF0aXNjaCBiZXN0aW1tdGUgRGF0ZW4gd2llIHouIEIuIElQLUFkcmVzc2UsIHZlcndlbmRldGVyIEJyb3dzZXIsIEJldHJpZWJzc3lzdGVtIPxiZXIgSWhyZW4gQ29tcHV0ZXIgdW5kIElocmUgVmVyYmluZHVuZyB6dW0gSW50ZXJuZXQuCjxicj5BbmhhbmQgZGVyIGluIENvb2tpZXMgZW50aGFsdGVuZW4gSW5mb3JtYXRpb25lbiBr9m5uZW4gd2lyIElobmVuIGRpZSBOYXZpZ2F0aW9uIGVybGVpY2h0ZXJuIHVuZCBkaWUga29ycmVrdGUgQW56ZWlnZSB1bnNlcmVyIFdlYnNlaXRlbiBlcm32Z2xpY2hlbi4KPGJyPgo8YnI+TmF0/HJsaWNoIGv2bm5lbiBTaWUgdW5zZXJlIFdlYnNpdGUgZ3J1bmRz5HR6bGljaCBhdWNoIG9obmUgQ29va2llcyBiZXRyYWNodGVuLiBJbnRlcm5ldC1Ccm93c2VyIHNpbmQgcmVnZWxt5N9pZyBzbyBlaW5nZXN0ZWxsdCwgZGFzcyBzaWUgQ29va2llcyBha3plcHRpZXJlbi4gU2llIGv2bm5lbiBkaWUgVmVyd2VuZHVuZyB2b24gQ29va2llcyBqZWRlcnplaXQg/GJlciBkaWUgRWluc3RlbGx1bmdlbiBJaHJlcyBCcm93c2VycyBkZWFrdGl2aWVyZW4uIEJpdHRlIHZlcndlbmRlbiBTaWUgZGllIEhpbGZlZnVua3Rpb25lbiBJaHJlcyBJbnRlcm5ldGJyb3dzZXJzLCB1bSB6dSBlcmZhaHJlbiwgd2llIFNpZSBkaWVzZSBFaW5zdGVsbHVuZ2VuIORuZGVybiBr9m5uZW4uIEJpdHRlIGJlYWNodGVuIFNpZSwgZGFzcyBlaW56ZWxuZSBGdW5rdGlvbmVuIHVuc2VyZXIgV2Vic2l0ZSBt9mdsaWNoZXJ3ZWlzZSBuaWNodCBmdW5rdGlvbmllcmVuLCB3ZW5uIFNpZSBkaWUgVmVyd2VuZHVuZyB2b24gQ29va2llcyBkZWFrdGl2aWVydCBoYWJlbi4KPGJyPgo8YnI+PGI+UmVnaXN0cmllcnVuZyBhdWYgdW5zZXJlciBXZWJzaXRlPC9iPgo8YnI+TWl0IGRlciBBbm1lbGR1bmcsIFJlZ2lzdHJpZXJ1bmcsIGRlbSBEb3dubG9hZCwgYmV6aWVodW5nc3dlaXNlIFRlaWxuYWhtZSwgYXVzZ2Vkcvxja3QgZHVyY2ggZGFzIFZlcnNlbmRlbiBlaW5lcyBlbnRzcHJlY2hlbmRlbiBGb3JtdWxhcnMsIGVya2zkcmVuIFNpZSBzaWNoIGRhbWl0IGVpbnZlcnN0YW5kZW4sIGRhc3MgQURFTklPTiBwZXJz9m5saWNoZSBEYXRlbiwgZGllIFNpZSBpbSBGb3JtdWxhciBlaW5nZXRyYWdlbiBoYWJlbiwgc3BlaWNoZXJ0IHVuZCB2ZXJhcmJlaXRldC4gWnVz5HR6bGljaCBlcmts5HJlbiBTaWUgc2ljaCBkYW1pdCBlaW52ZXJzdGFuZGVuLCBkYXNzIFNpZSB2b24gdW5zIGluIHVucmVnZWxt5N9pZ2VuIEFic3TkbmRlbiBJbmZvcm1hdGlvbmVuIHp1IEJlaXRy5GdlbiwgUHJvZHVrdG5ldWVydW5nZW4gdW5kIEFrdGlvbmVuIHBlciBFLU1haWwgb2RlciBUZWxlZm9uIGVyaGFsdGVuLiBTaWUga/ZubmVuIGRpZXNlcyBFaW52ZXJzdORuZG5pcyBqZWRlcnplaXQgd2lkZXJydWZlbiAoc2llaGUgQWJzYXR6ID9JaHJlIFJlY2h0ZT8pLgo8YnI+Cjxicj48Yj5EYXRlbmVyZmFzc3VuZyBkdXJjaCBEcml0dGFuYmlldGVyPC9iPgo8YnI+Cjxicj48Yj5Hb29nbGUgQW5hbHl0aWNzPC9iPgo8YnI+RGllc2UgV2Vic2VpdGUgYmVudXR6dCBHb29nbGUgQW5hbHl0aWNzLCBlaW5lbiBXZWJhbmFseXNlZGllbnN0IGRlciBHb29nbGUgSW5jLiAoZm9sZ2VuZDogR29vZ2xlKS4gR29vZ2xlIEFuYWx5dGljcyB2ZXJ3ZW5kZXQgc29nLiAiQ29va2llcyIsIGFsc28gVGV4dGRhdGVpZW4sIGRpZSBhdWYgSWhyZW0gQ29tcHV0ZXIgZ2VzcGVpY2hlcnQgd2VyZGVuIHVuZCBkaWUgZWluZSBBbmFseXNlIGRlciBCZW51dHp1bmcgZGVyIFdlYnNlaXRlIGR1cmNoIFNpZSBlcm32Z2xpY2hlbi4gRGllIGR1cmNoIGRhcyBDb29raWUgZXJ6ZXVndGVuIEluZm9ybWF0aW9uZW4g/GJlciBJaHJlIEJlbnV0enVuZyBkaWVzZXIgV2Vic2VpdGUgd2VyZGVuIGluIGRlciBSZWdlbCBhbiBlaW5lbiBTZXJ2ZXIgdm9uIEdvb2dsZSBpbiBkZW4gVVNBIPxiZXJ0cmFnZW4gdW5kIGRvcnQgZ2VzcGVpY2hlcnQuIEF1ZmdydW5kIGRlciBBa3RpdmllcnVuZyBkZXIgSVAtQW5vbnltaXNpZXJ1bmcgYXVmIGRpZXNlbiBXZWJzZWl0ZW4sIHdpcmQgSWhyZSBJUC1BZHJlc3NlIHZvbiBHb29nbGUgamVkb2NoIGlubmVyaGFsYiB2b24gTWl0Z2xpZWRzdGFhdGVuIGRlciBFdXJvcORpc2NoZW4gVW5pb24gb2RlciBpbiBhbmRlcmVuIFZlcnRyYWdzc3RhYXRlbiBkZXMgQWJrb21tZW5zIPxiZXIgZGVuIEV1cm9w5GlzY2hlbiBXaXJ0c2NoYWZ0c3JhdW0genV2b3IgZ2Vr/HJ6dC4gTnVyIGluIEF1c25haG1lZuRsbGVuIHdpcmQgZGllIHZvbGxlIElQLUFkcmVzc2UgYW4gZWluZW4gU2VydmVyIHZvbiBHb29nbGUgaW4gZGVuIFVTQSD8YmVydHJhZ2VuIHVuZCBkb3J0IGdla/xyenQuIEltIEF1ZnRyYWcgZGVzIEJldHJlaWJlcnMgZGllc2VyIFdlYnNlaXRlIHdpcmQgR29vZ2xlIGRpZXNlIEluZm9ybWF0aW9uZW4gYmVudXR6ZW4sIHVtIElocmUgTnV0enVuZyBkZXIgV2Vic2VpdGUgYXVzenV3ZXJ0ZW4sIHVtIFJlcG9ydHMg/GJlciBkaWUgV2Vic2VpdGVuYWt0aXZpdOR0ZW4genVzYW1tZW56dXN0ZWxsZW4gdW5kIHVtIHdlaXRlcmUgbWl0IGRlciBXZWJzZWl0ZW5udXR6dW5nIHVuZCBkZXIgSW50ZXJuZXRudXR6dW5nIHZlcmJ1bmRlbmUgRGllbnN0bGVpc3R1bmdlbiBnZWdlbvxiZXIgZGVtIFdlYnNlaXRlbmJldHJlaWJlciB6dSBlcmJyaW5nZW4uIERpZSBpbSBSYWhtZW4gdm9uIEdvb2dsZSBBbmFseXRpY3Mgdm9uIElocmVtIEJyb3dzZXIg/GJlcm1pdHRlbHRlIElQLUFkcmVzc2Ugd2lyZCBuaWNodCBtaXQgYW5kZXJlbiBEYXRlbiB2b24gR29vZ2xlIHp1c2FtbWVuZ2Vm/GhydC4KPGJyPlNpZSBr9m5uZW4gZGllIFNwZWljaGVydW5nIGRlciBDb29raWVzIGR1cmNoIGVpbmUgZW50c3ByZWNoZW5kZSBFaW5zdGVsbHVuZyBJaHJlciBCcm93c2VyLVNvZnR3YXJlIHZlcmhpbmRlcm47IHdpciB3ZWlzZW4gU2llIGplZG9jaCBkYXJhdWYgaGluLCBkYXNzIFNpZSBpbiBkaWVzZW0gRmFsbCBnZWdlYmVuZW5mYWxscyBuaWNodCBz5G10bGljaGUgRnVua3Rpb25lbiBkaWVzZXIgV2Vic2VpdGUgdm9sbHVtZuRuZ2xpY2ggd2VyZGVuIG51dHplbiBr9m5uZW4uIFNpZSBr9m5uZW4gZGFy/GJlciBoaW5hdXMgZGllIEVyZmFzc3VuZyBkZXIgZHVyY2ggZGFzIENvb2tpZSBlcnpldWd0ZW4gdW5kIGF1ZiBJaHJlIE51dHp1bmcgZGVyIFdlYnNlaXRlIGJlem9nZW5lbiBEYXRlbiAoaW5rbC4gSWhyZXIgSVAtQWRyZXNzZSkgYW4gR29vZ2xlIHNvd2llIGRpZSBWZXJhcmJlaXR1bmcgZGllc2VyIERhdGVuIGR1cmNoIEdvb2dsZSB2ZXJoaW5kZXJuLCBpbmRlbSBzaWUgZGFzIHVudGVyIGRlbSBmb2xnZW5kZW4gTGluayB2ZXJm/GdiYXJlIEJyb3dzZXItUGx1Z2luIGhlcnVudGVybGFkZW4gdW5kIGluc3RhbGxpZXJlbjogQnJvd3Nlci1BZGQtb24genVyIERlYWt0aXZpZXJ1bmcgdm9uIEdvb2dsZSBBbmFseXRpY3MuCjxicj4KPGJyPjxiPkVpbmdlYmV0dGV0ZSBZb3VUdWJlLVZpZGVvczwvYj4KPGJyPkF1ZiBlaW5pZ2VuIHVuc2VyZXIgV2Vic2VpdGVuIGJldHRlbiB3aXIgWW91dHViZS1WaWRlb3MgZWluLiBCZXRyZWliZXIgZGVyIGVudHNwcmVjaGVuZGVuIFBsdWdpbnMgaXN0IGRpZSBZb3VUdWJlLCBMTEMsIDkwMSBDaGVycnkgQXZlLiwgU2FuIEJydW5vLCBDQSA5NDA2NiwgVVNBLiBXZW5uIFNpZSBlaW5lIFNlaXRlIG1pdCBkZW0gWW91VHViZS1QbHVnaW4gYmVzdWNoZW4sIHdpcmQgZWluZSBWZXJiaW5kdW5nIHp1IFNlcnZlcm4gdm9uIFlvdXR1YmUgaGVyZ2VzdGVsbHQuIERhYmVpIHdpcmQgWW91dHViZSBtaXRnZXRlaWx0LCB3ZWxjaGUgU2VpdGVuIFNpZSBiZXN1Y2hlbi4gV2VubiBTaWUgaW4gSWhyZW0gWW91dHViZS1BY2NvdW50IGVpbmdlbG9nZ3Qgc2luZCwga2FubiBZb3V0dWJlIElociBTdXJmdmVyaGFsdGVuIElobmVuIHBlcnP2bmxpY2ggenV6dW9yZG5lbi4gRGllcyB2ZXJoaW5kZXJuIFNpZSwgaW5kZW0gU2llIHNpY2ggdm9yaGVyIGF1cyBJaHJlbSBZb3V0dWJlLUFjY291bnQgYXVzbG9nZ2VuLgo8YnI+V2lyZCBlaW4gWW91dHViZS1WaWRlbyBnZXN0YXJ0ZXQsIHNldHp0IGRlciBBbmJpZXRlciBDb29raWVzIGVpbiwgZGllIEhpbndlaXNlIPxiZXIgZGFzIE51dHplcnZlcmhhbHRlbiBzYW1tZWxuLgo8YnI+V2VyIGRhcyBTcGVpY2hlcm4gdm9uIENvb2tpZXMgZvxyIGRhcyBHb29nbGUtQWQtUHJvZ3JhbW0gZGVha3RpdmllcnQgaGF0LCB3aXJkIGF1Y2ggYmVpbSBBbnNjaGF1ZW4gdm9uIFlvdXR1YmUtVmlkZW9zIG1pdCBrZWluZW4gc29sY2hlbiBDb29raWVzIHJlY2huZW4gbfxzc2VuLiBZb3V0dWJlIGxlZ3QgYWJlciBhdWNoIGluIGFuZGVyZW4gQ29va2llcyBuaWNodC1wZXJzb25lbmJlem9nZW5lIE51dHp1bmdzaW5mb3JtYXRpb25lbiBhYi4gTfZjaHRlbiBTaWUgZGllcyB2ZXJoaW5kZXJuLCBzbyBt/HNzZW4gU2llIGRhcyBTcGVpY2hlcm4gdm9uIENvb2tpZXMgaW0gQnJvd3NlciBibG9ja2llcmVuLgo8YnI+V2VpdGVyZSBJbmZvcm1hdGlvbmVuIHp1bSBEYXRlbnNjaHV0eiBiZWkgP1lvdXR1YmU/IGZpbmRlbiBTaWUgaW4gZGVyIERhdGVuc2NodXR6ZXJrbORydW5nIGRlcyBBbmJpZXRlcnMuIAo8YnI+Cjxicj48Yj5Tb2NpYWwgUGx1Z2luczwvYj4KPGJyPkRpZSBXZWJzaXRlIHZlcndlbmRldCBTb2NpYWwgUGx1Z2lucyAoIlBsdWdpbnMiKSBkZXMgc296aWFsZW4gTmV0endlcmtlcyBGYWNlYm9vaywgd2VsY2hlcyB2b24gZGVyIEZhY2Vib29rIEluYy4sIDE2MDEgUy4gQ2FsaWZvcm5pYSBBdmUsIFBhbG8gQWx0bywgQ0EgOTQzMDQsIFVTQSBiZXRyaWViZW4gd2lyZCAoIkZhY2Vib29rIikuIERpZSBQbHVnaW5zIHNpbmQgbWl0IGVpbmVtIEZhY2Vib29rIExvZ28gb2RlciBkZW0gWnVzYXR6ID9GYWNlYm9vayBTb2NpYWwgUGx1Z2luPyBnZWtlbm56ZWljaG5ldC4KPGJyPldlbm4gZGVyIFdlYnNpdGUgTnV0emVyIGVpbmUgV2Vic2VpdGUgZGVzIEludGVybmV0YXVmdHJpdHRzIGF1ZnJ1ZnQsIGRpZSBlaW4gc29sY2hlcyBQbHVnaW4gZW50aORsdCwgYmF1dCBkZXIgQnJvd3NlciBlaW5lIGRpcmVrdGUgVmVyYmluZHVuZyBtaXQgZGVuIFNlcnZlcm4gdm9uIEZhY2Vib29rIGF1Zi4gRGVyIEluaGFsdCBkZXMgUGx1Z2lucyB3aXJkIHZvbiBGYWNlYm9vayBkaXJla3QgYW4gZGVuIEJyb3dzZXIg/GJlcm1pdHRlbHQgdW5kIHZvbiBkaWVzZW0gaW4gZGllIFdlYnNlaXRlIGVpbmdlYnVuZGVuLgo8YnI+RHVyY2ggZGllIEVpbmJpbmR1bmcgZGVyIFBsdWdpbnMgZXJo5Gx0IEZhY2Vib29rIGRpZSBJbmZvcm1hdGlvbiwgZGFzcyBkZXIgV2Vic2l0ZSBOdXR6ZXIgZGllIGVudHNwcmVjaGVuZGUgU2VpdGUgZGVzIEludGVybmV0YXVmdHJpdHRzIGF1ZmdlcnVmZW4gaGF0LiBJc3QgZGVyIFdlYnNpdGUgTnV0emVyIGJlaSBGYWNlYm9vayBlaW5nZWxvZ2d0LCBrYW5uIEZhY2Vib29rIGRlbiBCZXN1Y2ggYXVmIHNlaW5lbSBGYWNlYm9vay1Lb250byB6dW9yZG5lbi4gV2VubiBkZXIgV2Vic2l0ZSBOdXR6ZXIgbWl0IGRlbiBQbHVnaW5zIGludGVyYWdpZXJ0LCB6dW0gQmVpc3BpZWwgZGVuID9HZWbkbGx0IG1pcj8gQnV0dG9uIGJldOR0aWd0IG9kZXIgZWluZW4gS29tbWVudGFyIGFiZ2lidCwgd2lyZCBkaWUgZW50c3ByZWNoZW5kZSBJbmZvcm1hdGlvbiB2b24gc2VpbmVtIEJyb3dzZXIgZGlyZWt0IGFuIEZhY2Vib29rIPxiZXJtaXR0ZWx0IHVuZCBkb3J0IGdlc3BlaWNoZXJ0Lgo8YnI+WndlY2sgdW5kIFVtZmFuZyBkZXIgRGF0ZW5lcmhlYnVuZyB1bmQgZGllIHdlaXRlcmUgVmVyYXJiZWl0dW5nIHVuZCBOdXR6dW5nIGRlciBEYXRlbiBkdXJjaCBGYWNlYm9vayBzb3dpZSBkaWVzYmV6/GdsaWNoZW4gUmVjaHRlIHVuZCBFaW5zdGVsbHVuZ3Nt9mdsaWNoa2VpdGVuIHp1bSBTY2h1dHogZGVyIFByaXZhdHNwaORyZSBr9m5uZW4gZGVuIERhdGVuc2NodXR6aGlud2Vpc2VuIHZvbiBGYWNlYm9vayBlbnRub21tZW4gd2VyZGVuLgo8YnI+V2VyIG5pY2h0IG32Y2h0ZSwgZGFzcyBGYWNlYm9vayD8YmVyIGRpZXNlIFdlYnNpdGUgRGF0ZW4g/GJlciBkZW4gV2Vic2l0ZSBOdXR6ZXIgc2FtbWVsdCwgZGFyZiBzaWNoIHZvciBkZW0gQmVzdWNoIGRpZXNlciBXZWJzaXRlIG5pY2h0IGJlaSBGYWNlYm9vayBlaW5sb2dnZW4uCjxicj5EaWUgV2Vic2l0ZSBlbnRo5Gx0IEZ1bmt0aW9uZW4gZGVyIFR3aXR0ZXIgSW5jLiwgNzk1IEZvbHNvbSBTdHJlZXQsIFN1aXRlIDYwMCwgU2FuIEZyYW5jaXNjbywgQ0EgOTQxMDcsIFVTQS4gV2VubiBTaWUgVHdpdHRlciB1bmQgaW5zYmVzb25kZXJlIGRpZSBGdW5rdGlvbiAiUmUtVHdlZXQiIHZlcndlbmRlbiwgdmVya278cGZ0IFR3aXR0ZXIgSWhyIFR3aXR0ZXItQWNjb3VudCBtaXQgZGVuIHZvbiBJaG5lbiBmcmVxdWVudGllcnRlbiBJbnRlcm5ldHNlaXRlbi4gRGllcyB3aXJkIGFuZGVyZW4gTnV0emVybiBiZWkgVHdpdHRlciwgaW5zYmVzb25kZXJlIElocmVuIEZvbGxvd2VybiBiZWthbm50IGdlZ2ViZW4uIEF1ZiBkaWVzZW0gV2VnIGZpbmRldCBhdWNoIGVpbmUgRGF0ZW78YmVydHJhZ3VuZyBhbiBUd2l0dGVyIHN0YXR0Lgo8YnI+V2lyLCBhbHMgQW5iaWV0ZXIgdW5zZXJlciBJbnRlcm5ldHNlaXRlLCB3ZXJkZW4gdm9uIFR3aXR0ZXIgbmljaHQg/GJlciBkZW4gSW5oYWx0IGRlciD8YmVydHJhZ2VuZW4gRGF0ZW4gYnp3LiBkZXIgRGF0ZW5udXR6dW5nIGluZm9ybWllcnQuIFVudGVyIGRlbSBuYWNoZm9sZ2VuZGVuIExpbmsga/ZubmVuIFNpZSBzaWNoIHdlaXRlcmdlaGVuZCD8YmVyIGRpZSBUd2l0dGVyIERhdGVuc2NodXR6ZXJrbORydW5nIGluZm9ybWllcmVuOiAKPGJyPkJpdHRlIGJlYWNodGVuIFNpZSBqZWRvY2gsIGRhc3MgU2llIGRpZSBN9mdsaWNoa2VpdCBoYWJlbiwgSWhyZSBEYXRlbnNjaHV0emVpbnN0ZWxsdW5nZW4gYmVpIFR3aXR0ZXIgaW4gSWhyZW4gZG9ydGlnZW4gS29udG8tRWluc3RlbGx1bmdlbiB1bnRlciA8YSBocmVmPSJodHRwOi8vdHdpdHRlci5jb20vYWNjb3VudC9zZXR0aW5ncyIgdGFyZ2V0PSJfYmxhbmsiPmh0dHA6Ly90d2l0dGVyLmNvbS9hY2NvdW50L3NldHRpbmdzPC9hPiB6dSB2ZXLkbmRlcm4uCjxicj4KPGJyPldpciBoYWJlbiBhdWYgdW5zZXJlciBXZWJzaXRlIGRpZSBTb2NpYWwtTWVkaWEtQnV0dG9ucyBmb2xnZW5kZXIgVW50ZXJuZWhtZW4gZWluZ2VidW5kZW46Cjxicj4KPGJyPi1GYWNlYm9vayBJbmMuICgxNjAxIFMuIENhbGlmb3JuaWEgQXZlIC0gUGFsbyBBbHRvIC0gQ0EgOTQzMDQgLSBVU0EpCjxicj4tVHdpdHRlciBJbmMuICg3OTUgRm9sc29tIFN0LiAtIFN1aXRlIDYwMCAtIFNhbiBGcmFuY2lzY28gLSBDQSA5NDEwNyAtIFVTQSkKPGJyPi1Hb29nbGUgUGx1cy9Hb29nbGUgSW5jLiAoMTYwMCBBbXBoaXRoZWF0cmUgUGFya3dheSAtIE1vdW50YWluIFZpZXcgLSBDQSA5NDA0MyAtIFVTQSkKPGJyPi1XaGF0c2FwcAo8YnI+LVBpbnRlcmVzdAo8YnI+LVhJTkcgQUcgKEfkbnNlbWFya3QgNDMgLSAyMDM1NCBIYW1idXJnIC0gR2VybWFueSkKPGJyPi1MaW5rZWRJbiBDb3Jwb3JhdGlvbiAoMjAyOSBTdGllcmxpbiBDb3VydCAtIE1vdW50YWluIFZpZXcgLSBDQSA5NDA0MyAtIFVTQSkKPGJyPi1SZWRkaXQKPGJyPi1TdHVtYmxldXBvbgo8YnI+LUZsYXR0cgo8YnI+Cjxicj48Yj5Hb29nbGUgQWRXb3JkczwvYj4KPGJyPlVuc2VyZSBXZWJzZWl0ZSBudXR6dCBkYXMgR29vZ2xlIENvbnZlcnNpb24tVHJhY2tpbmcuIFNpbmQgU2llIPxiZXIgZWluZSB2b24gR29vZ2xlIGdlc2NoYWx0ZXRlIEFuemVpZ2UgYXVmIHVuc2VyZSBXZWJzZWl0ZSBnZWxhbmd0LCB3aXJkIHZvbiBHb29nbGUgQWR3b3JkcyBlaW4gQ29va2llIGF1ZiBJaHJlbSBSZWNobmVyIGdlc2V0enQuIERhcyBDb29raWUgZvxyIENvbnZlcnNpb24tVHJhY2tpbmcgd2lyZCBnZXNldHp0LCB3ZW5uIGVpbiBOdXR6ZXIgYXVmIGVpbmUgdm9uIEdvb2dsZSBnZXNjaGFsdGV0ZSBBbnplaWdlIGtsaWNrdC4gRGllc2UgQ29va2llcyB2ZXJsaWVyZW4gbmFjaCBtYXhpbWFsIDkwIFRhZ2VuIGlocmUgR/xsdGlna2VpdCB1bmQgZGllbmVuIG5pY2h0IGRlciBwZXJz9m5saWNoZW4gSWRlbnRpZml6aWVydW5nLiBCZXN1Y2h0IGRlciBOdXR6ZXIgYmVzdGltbXRlIFNlaXRlbiB1bnNlcmVyIFdlYnNpdGUgdW5kIGRhcyBDb29raWUgaXN0IG5vY2ggbmljaHQgYWJnZWxhdWZlbiwga/ZubmVuIHdpciB1bmQgR29vZ2xlIGVya2VubmVuLCBkYXNzIGRlciBOdXR6ZXIgYXVmIGRpZSBBbnplaWdlIGdla2xpY2t0IGhhdCB1bmQgenUgZGllc2VyIFNlaXRlIHdlaXRlcmdlbGVpdGV0IHd1cmRlLiBKZWRlciBHb29nbGUgQWRXb3Jkcy1LdW5kZSBlcmjkbHQgZWluIGFuZGVyZXMgQ29va2llLiBDb29raWVzIGv2bm5lbiBzb21pdCBuaWNodCD8YmVyIGRpZSBXZWJzaXRlcyB2b24gQWRXb3Jkcy1LdW5kZW4gbmFjaHZlcmZvbGd0IHdlcmRlbi4gRGllIG1pdGhpbGZlIGRlcyBDb252ZXJzaW9uLUNvb2tpZXMgZWluZ2Vob2x0ZW4gSW5mb3JtYXRpb25lbiBkaWVuZW4gZGF6dSwgQ29udmVyc2lvbi1TdGF0aXN0aWtlbiBm/HIgQWRXb3Jkcy1LdW5kZW4genUgZXJzdGVsbGVuLCBkaWUgc2ljaCBm/HIgQ29udmVyc2lvbi1UcmFja2luZyBlbnRzY2hpZWRlbiBoYWJlbi4gRGllIEt1bmRlbiBlcmZhaHJlbiBkaWUgR2VzYW10YW56YWhsIGRlciBOdXR6ZXIsIGRpZSBhdWYgaWhyZSBBbnplaWdlIGdla2xpY2t0IGhhYmVuIHVuZCB6dSBlaW5lciBtaXQgZWluZW0gQ29udmVyc2lvbi1UcmFja2luZy1UYWcgdmVyc2VoZW5lbiBTZWl0ZSB3ZWl0ZXJnZWxlaXRldCB3dXJkZW4uIFNpZSBlcmhhbHRlbiBqZWRvY2gga2VpbmUgSW5mb3JtYXRpb25lbiwgbWl0IGRlbmVuIHNpY2ggTnV0emVyIHBlcnP2bmxpY2ggaWRlbnRpZml6aWVyZW4gbGFzc2VuLgo8YnI+TfZjaHRlbiBTaWUgbmljaHQgYW0gVHJhY2tpbmcgdGVpbG5laG1lbiwga/ZubmVuIFNpZSBkYXMgaGllcmb8ciBlcmZvcmRlcmxpY2hlIFNldHplbiBlaW5lcyBDb29raWVzIGFibGVobmVuIC0gZXR3YSBwZXIgQnJvd3Nlci1FaW5zdGVsbHVuZywgZGllIGRhcyBhdXRvbWF0aXNjaGUgU2V0emVuIHZvbiBDb29raWVzIGdlbmVyZWxsIGRlYWt0aXZpZXJ0IG9kZXIgSWhyZW4gQnJvd3NlciBzbyBlaW5zdGVsbGVuLCBkYXNzIENvb2tpZXMgdm9uIGRlciBEb21haW4gImdvb2dsZWxlYWRzZXJ2aWNlcy5jb20iIGJsb2NraWVydCB3ZXJkZW4uCjxicj5CaXR0ZSBiZWFjaHRlbiBTaWUsIGRhc3MgU2llIGRpZSBPcHQtb3V0LUNvb2tpZXMgbmljaHQgbPZzY2hlbiBk/HJmZW4sIHNvbGFuZ2UgU2llIGtlaW5lIEF1ZnplaWNobnVuZyB2b24gTWVzc2RhdGVuIHf8bnNjaGVuLiBIYWJlbiBTaWUgYWxsZSBJaHJlIENvb2tpZXMgaW0gQnJvd3NlciBnZWz2c2NodCwgbfxzc2VuIFNpZSBkYXMgamV3ZWlsaWdlIE9wdC1vdXQgQ29va2llIGVybmV1dCBzZXR6ZW4uCjxicj4KPGJyPjxiPkVpbnNhdHogdm9uIEdvb2dsZSBSZW1hcmtldGluZzwvYj4KPGJyPkRpZXNlIFdlYnNlaXRlIHZlcndlbmRldCBkaWUgUmVtYXJrZXRpbmctRnVua3Rpb24gZGVyIEdvb2dsZSBJbmMuIERpZSBGdW5rdGlvbiBkaWVudCBkYXp1LCBXZWJzZWl0ZW5iZXN1Y2hlcm4gaW5uZXJoYWxiIGRlcyBHb29nbGUtV2VyYmVuZXR6d2Vya3MgaW50ZXJlc3NlbmJlem9nZW5lIFdlcmJlYW56ZWlnZW4genUgcHLkc2VudGllcmVuLiBJbSBCcm93c2VyIGRlcyBXZWJzZWl0ZW5iZXN1Y2hlcnMgd2lyZCBlaW4gc29nLiA/Q29va2llPyBnZXNwZWljaGVydCwgZGVyIGVzIGVybfZnbGljaHQsIGRlbiBCZXN1Y2hlciB3aWVkZXJ6dWVya2VubmVuLCB3ZW5uIGRpZXNlciBXZWJzZWl0ZW4gYXVmcnVmdCwgZGllIGRlbSBXZXJiZW5ldHp3ZXJrIHZvbiBHb29nbGUgYW5nZWj2cmVuLiBBdWYgZGllc2VuIFNlaXRlbiBr9m5uZW4gZGVtIEJlc3VjaGVyIFdlcmJlYW56ZWlnZW4gcHLkc2VudGllcnQgd2VyZGVuLCBkaWUgc2ljaCBhdWYgSW5oYWx0ZSBiZXppZWhlbiwgZGllIGRlciBCZXN1Y2hlciB6dXZvciBhdWYgV2Vic2VpdGVuIGF1ZmdlcnVmZW4gaGF0LCBkaWUgZGllIFJlbWFya2V0aW5nIEZ1bmt0aW9uIHZvbiBHb29nbGUgdmVyd2VuZGVuLgo8YnI+TmFjaCBlaWdlbmVuIEFuZ2FiZW4gZXJoZWJ0IEdvb2dsZSBiZWkgZGllc2VtIFZvcmdhbmcga2VpbmUgcGVyc29uZW5iZXpvZ2VuZW4gRGF0ZW4uIFNvbGx0ZW4gU2llIGRpZSBGdW5rdGlvbiBSZW1hcmtldGluZyB2b24gR29vZ2xlIGRlbm5vY2ggbmljaHQgd/xuc2NoZW4sIGv2bm5lbiBTaWUgZGllc2UgZ3J1bmRz5HR6bGljaCBkZWFrdGl2aWVyZW4sIGluZGVtIFNpZSBkaWUgZW50c3ByZWNoZW5kZW4gRWluc3RlbGx1bmdlbiB1bnRlciA8YSBocmVmPSJodHRwOi8vd3d3Lmdvb2dsZS5jb20vc2V0dGluZ3MvYWRzIiB0YXJnZXQ9Il9ibGFuayI+aHR0cDovL3d3dy5nb29nbGUuY29tL3NldHRpbmdzL2FkczwvYT4gdm9ybmVobWVuLiBBbHRlcm5hdGl2IGv2bm5lbiBTaWUgZGVuIEVpbnNhdHogdm9uIENvb2tpZXMgZvxyIGludGVyZXNzZW5iZXpvZ2VuZSBXZXJidW5nIPxiZXIgZGllIFdlcmJlbmV0endlcmtpbml0aWF0aXZlIGRlYWt0aXZpZXJlbiwgaW5kZW0gU2llIGRlbiBBbndlaXN1bmdlbiB1bnRlciA8YSBocmVmPSJodHRwOi8vd3d3Lm5ldHdvcmthZHZlcnRpc2luZy5vcmcvbWFuYWdpbmcvb3B0X291dC5hc3AiIHRhcmdldD0iX2JsYW5rIj5odHRwOi8vd3d3Lm5ldHdvcmthZHZlcnRpc2luZy5vcmcvbWFuYWdpbmcvb3B0X291dC5hc3A8L2E+IGZvbGdlbi4KPGJyPjxiPlJlZ2lzdHJpZXJ1bmcgZvxyIHVuc2VyZSBQcm9kdWt0ZSB1bmQgU2VydmljZXM8L2I+Cjxicj5CZWkgZGVyIFJlZ2lzdHJpZXJ1bmcgZvxyIGRpZSBOdXR6dW5nIHVuc2VyZXIgcGVyc29uYWxpc2llcnRlbiBMZWlzdHVuZ2VuIHdlcmRlbiBlaW5pZ2UgcGVyc29uZW5iZXpvZ2VuZSBEYXRlbiBlcmhvYmVuLCB3aWUgTmFtZSwgQW5zY2hyaWZ0LCBLb250YWt0LSB1bmQgS29tbXVuaWthdGlvbnNkYXRlbiB3aWUgVGVsZWZvbm51bW1lciB1bmQgRS1NYWlsLUFkcmVzc2UuIFNpbmQgU2llIGJlaSB1bnMgcmVnaXN0cmllcnQsIGv2bm5lbiBTaWUgYXVmIEluaGFsdGUgdW5kIExlaXN0dW5nZW4genVncmVpZmVuLCBkaWUgd2lyIG51ciByZWdpc3RyaWVydGVuIE51dHplcm4gYW5iaWV0ZW4uIEFuZ2VtZWxkZXRlIE51dHplciBoYWJlbiB6dWRlbSBkaWUgTfZnbGljaGtlaXQsIGJlaSBCZWRhcmYgZGllIGJlaSBSZWdpc3RyaWVydW5nIGFuZ2VnZWJlbmVuIERhdGVuIGplZGVyemVpdCB6dSDkbmRlcm4gb2RlciB6dSBs9nNjaGVuLiBTZWxic3R2ZXJzdORuZGxpY2ggZXJ0ZWlsZW4gd2lyIElobmVuIGRhcvxiZXIgaGluYXVzIGplZGVyemVpdCBBdXNrdW5mdCD8YmVyIGRpZSB2b24gdW5zIPxiZXIgU2llIGdlc3BlaWNoZXJ0ZW4gcGVyc29uZW5iZXpvZ2VuZW4gRGF0ZW4uIEdlcm5lIGJlcmljaHRpZ2VuIGJ6dy4gbPZzY2hlbiB3aXIgZGllc2UgYXVjaCBhdWYgSWhyZW4gV3Vuc2NoLCBzb3dlaXQga2VpbmUgZ2VzZXR6bGljaGVuIEF1ZmJld2FocnVuZ3NwZmxpY2h0ZW4gZW50Z2VnZW5zdGVoZW4gb2RlciBkaWUgRGF0ZW4genVyIER1cmNoZvxocnVuZyB2ZXJ0cmFnbGljaGVyIExlaXN0dW5nZW4gZXJmb3JkZXJsaWNoIHNpbmQuIFp1ciBLb250YWt0YXVmbmFobWUgaW4gZGllc2VtIFp1c2FtbWVuaGFuZyBudXR6ZW4gU2llIGJpdHRlIGRpZSBhbSBFbmRlIGRpZXNlciBEYXRlbnNjaHV0emVya2zkcnVuZyBhbmdlZ2ViZW5lbiBLb250YWt0ZGF0ZW4uCjxicj4KPGJyPjxiPkVyYnJpbmd1bmcga29zdGVucGZsaWNodGlnZXIgTGVpc3R1bmdlbjwvYj4KPGJyPlp1ciBFcmJyaW5ndW5nIGtvc3RlbnBmbGljaHRpZ2VyIExlaXN0dW5nZW4gd2VyZGVuIHZvbiB1bnMgenVz5HR6bGljaGUgRGF0ZW4gZXJmcmFndCwgd2llIHouQi4gWmFobHVuZ3NhbmdhYmVuLgo8YnI+Cjxicj48Yj5TU0wtVmVyc2NobPxzc2VsdW5nPC9iPgo8YnI+VW0gZGllIFNpY2hlcmhlaXQgSWhyZXIgRGF0ZW4gYmVpIGRlciDcYmVydHJhZ3VuZyB6dSBzY2j8dHplbiwgdmVyd2VuZGVuIHdpciBkZW0gYWt0dWVsbGVuIFN0YW5kIGRlciBUZWNobmlrIGVudHNwcmVjaGVuZGUgVmVyc2NobPxzc2VsdW5nc3ZlcmZhaHJlbiAoei4gQi4gU1NMKSD8YmVyIEhUVFBTLgo8YnI+Cjxicj48Yj5WZXL2ZmZlbnRsaWNodW5nIHZvbiBQcmVzc2VtaXR0ZWlsdW5nZW4gLyBSZWRha3Rpb25lbGxlIC8gSm91cm5hbGlzdGlzY2hlIFB1Ymxpa2F0aW9uZW48L2I+Cjxicj5JbSBSYWhtZW4gdW5zZXJlciBQUi1EaWVuc3RlIHdpZSB6LkIuIFBSLUdhdGV3YXkgb2RlciB2ZXJzY2hpZWRlbmVyIE9ubGluZSBQcmVzc2Vwb3J0YWxlIHN0ZWxsdCBkZXIgTnV0emVyIGdnZi4gcGVyc/ZubGljaGUgRGF0ZW4gYmVyZWl0LCBkaWUgZvxyIGRpZSBQcmVzc2VhcmJlaXQgb2RlciB6dXIgVmVy9mZmZW50bGljaHVuZyBpbm5lcmhhbGIgdm9uIFByZXNzZW1pdHRlaWx1bmdlbiBiZXN0aW1tdCBzaW5kLiAoS29udGFrdGRhdGVuKSBEaWVzZSBEYXRlbiB3ZXJkZW4genVkZW0gYW4gZHJpdHRlIEludGVybmV0cG9ydGFsZSBlYmVuZmFsbHMgenVtIFp3ZWNrZSBkZXIgVmVy9mZmZW50bGljaHVuZyB3ZWl0ZXJnZWdlYmVuLiBTb2Zlcm4gZHVyY2ggZGVuIE51dHplciBhdXNnZXfkaGx0LCBr9m5uZW4gZGllc2UgRGF0ZW4gYXVjaCBhbiBQb3J0YWxlIHdlaXRlcmdlZ2ViZW4gd2VyZGVuLCBkaWUgYXXfZXJoYWxiIGRlciBFdXJvcORpc2NoZW4gVW5pb24gbGllZ2VuLgo8YnI+Cjxicj48Yj5WZXJ3ZW5kdW5nIGRlciBEYXRlbiBpbm5lcmhhbGIgZGVzIFBsdWdpbnMgdW5kIGRlciBXZWJhcHAgQmxvZzJTb2NpYWw8L2I+Cjxicj5CZWkgZGVyIFZlcndlbmR1bmcgZGVzIFBsdWdpbnMgQmxvZzJTb2NpYWwgd2VyZGVuIGF1ZiBkZXIgV29yZHByZXNzIFdlYnNpdGUgZGVzIEJsb2dpbmhhYmVycyBrZWluZXJsZWkgcGVyc/ZubGljaGVuIERhdGVuIGRlciBVc2VyIGRlcyBCbG9ncyBlcmhvYmVuLgo8YnI+SW5uZXJoYWxiIGRlciBTb2NpYWwgTWVkaWEgQW53ZW5kdW5nIEJsb2cyU29jaWFsIHNlbGJzdCB3ZXJkZW4gcGVyc/ZubGljaGUgRGF0ZW4gZGVzIE51dHplcnMgZXJob2Jlbi4gRXMgc2luZCBkaWVzIEUtTWFpbCBBZHJlc3NlLCBCbG9nLVVzZXItSUQgdW5kIEJsb2ctVVJMLiBBdXMgZGllc2VuIERhdGVuIHdpcmQgZvxyIHRlY2huaXNjaGUgWndlY2tlIGVpbmUgZWluZGV1dGlnZSBVc2VyLUlEIGdlbmVyaWVydC4KPGJyPkZvbGdlbmRlIERhdGVuIHdlcmRlbiBlcmhvYmVuLCB3ZW5uIGRlciBOdXR6ZXIgZGVzIFBsdWdpbnMvV2ViYXBwIGltIEFkbWluLUJlcmVpY2ggc2ljaCBtaXQgTmV0endlcmtlbiB2ZXJiaW5kZXQ6IE5ldHp3ZXJrLUFjY291bnQtSUQsIE5ldHp3ZXJrLUFjY291bnQtVG9rZW4sIFBhZ2UtSUQsIEdyb3VwLUlELCBQYWdlLU5hbWUsIEdyb3VwLU5hbWUsIE5ldHp3ZXJrLUFjY291bnQtTmFtZSB1bmQgZ2dmIFBhc3N3b3J0IGplIG5hY2ggTmV0endlcmsuCjxicj5QZXJz9m5saWNoZSBEYXRlbiB3ZXJkZW4gZXJob2Jlbiwgc29mZXJuIHNpY2ggZGVyIE51dHplciBkZXMgUGx1Z2lucyBpbSBBZG1pbmlzdHJhdG9yZW4gQmVyZWljaCBm/HIga29zdGVucGZsaWNodGlnZSBTZXJ2aWNlcyBlaW50cuRndCBvZGVyIGVpbmVuIE5ld3NsZXR0ZXIgYmVzdGVsbHQuIERpZXNlIERhdGVuIHdlcmRlbiB6dXIgRXJicmluZ3VuZyBkZXIgdmVydHJhZ2xpY2hlbiBMZWlzdHVuZyBlcmhvYmVuLiBFcyBzaW5kIGRpZXM6Cjxicj5FLU1haWwgQWRyZXNzZSwgTmFtZSwgQW5zY2hyaWZ0LCBnZ2YuIEtyZWRpdGthcnRlbmluZm9ybWF0aW9uZW4gb2RlciBLb250b2RhdGVuIGb8ciB6YWhsdW5nc3BmbGljaHRpZ2UgTGVpc3R1bmdlbi4uIERlciBWZXJ0cmFnc3BhcnRuZXIgZvxyIFphaGx1bmdzYWJ3aWNrbHVuZ2VuIGlzdCBQYXlwcm9HbG9iYWwuY29tIGluIFRvcm9udG8sIENhbmFkYS4KPGJyPgo8YnI+PGI+SWhyZSBSZWNodGUgYXVmIEF1c2t1bmZ0LCBCZXJpY2h0aWd1bmcsIFNwZXJyZSwgTPZzY2h1bmcgdW5kIFdpZGVyc3BydWNoPC9iPgo8YnI+U2llIGhhYmVuIGRhcyBSZWNodCwgamVkZXJ6ZWl0IEF1c2t1bmZ0IPxiZXIgSWhyZSBiZWkgdW5zIGdlc3BlaWNoZXJ0ZW4gcGVyc29uZW5iZXpvZ2VuZW4gRGF0ZW4genUgZXJoYWx0ZW4uIEViZW5zbyBoYWJlbiBTaWUgZGFzIFJlY2h0IGF1ZiBCZXJpY2h0aWd1bmcsIFNwZXJydW5nIG9kZXIsIGFiZ2VzZWhlbiB2b24gZGVyIHZvcmdlc2NocmllYmVuZW4gRGF0ZW5zcGVpY2hlcnVuZyB6dXIgR2VzY2jkZnRzYWJ3aWNrbHVuZyBvZGVyIGRlbSBCZXJlaXRoYWx0ZW4gdm9uIEltcHJlc3N1bXNhbmdhYmVuIGluIFByZXNzZW1pdHRlaWx1bmdlbiwgTPZzY2h1bmcgSWhyZXIgcGVyc29uZW5iZXpvZ2VuZW4gRGF0ZW4uIERhbWl0IGVpbmUgU3BlcnJlIHZvbiBEYXRlbiBqZWRlcnplaXQgYmVy/GNrc2ljaHRpZ3Qgd2VyZGVuIGthbm4sIG38c3NlbiBkaWVzZSBEYXRlbiB6dSBLb250cm9sbHp3ZWNrZW4gaW4gZWluZXIgU3BlcnJkYXRlaSB2b3JnZWhhbHRlbiB3ZXJkZW4uIFNpZSBr9m5uZW4gYXVjaCBkaWUgTPZzY2h1bmcgZGVyIERhdGVuIHZlcmxhbmdlbiwgc293ZWl0IGtlaW5lIGdlc2V0emxpY2hlIEFyY2hpdmllcnVuZ3N2ZXJwZmxpY2h0dW5nIGJlc3RlaHQsIGRpZSBEYXRlbiBm/HIgZGllIER1cmNoZvxocnVuZyBkaWVzZSB6dW0gWndlY2tlIGRlciB3ZWl0cmVpY2hlbmRlbiBWZXJicmVpdHVuZyBpbSBJbnRlcm5ldCD8YmVybGFzc2VuIHd1cmRlbiwgZGFzIE1lZGllbnByaXZpbGVnIGdpbHQgb2RlciBBZGVuaW9uIGtlaW5lbiB1bm1pdHRlbGJhcmVuIEVpbmZsdXNzIGF1ZiBkaWVzZSBEYXRlbiBoYXQuIChM9nNjaHVuZyB2b24gSW50ZXJuZXRwb3J0YWxlbikgU293ZWl0IGVpbmUgVmVycGZsaWNodHVuZyBiZXN0ZWh0LCBzcGVycmVuIHdpciBJaHJlIERhdGVuIGF1ZiBXdW5zY2guCjxicj5TaWUga/ZubmVuIMRuZGVydW5nZW4gb2RlciBkZW4gV2lkZXJydWYgZWluZXIgRWlud2lsbGlndW5nIGR1cmNoIGVudHNwcmVjaGVuZGUgTWl0dGVpbHVuZyBhbiB1bnMgbWl0IFdpcmt1bmcgZvxyIGRpZSBadWt1bmZ0IHZvcm5laG1lbi4KPGJyPk32Y2h0ZW4gU2llIGRpZXNlcyBFaW52ZXJzdORuZG5pcyB3aWRlcnJ1ZmVuLCBmaW5kZW4gU2llIGRhenUgaW4gamVkZXIgRS1NYWlsIGFtIHVudGVyZW4gUmFuZCBlaW5lbiBMaW5rLCB1bSBkaWUgSW5mb3JtYXRpb25lbiBhYnp1YmVzdGVsbGVuLiBPZGVyIFNpZSBzZW5kZW4gdW5zIGVpbmUgRS1NYWlsIHVuZCBuZW5uZW4gdW5zIGRpZSBFLU1haWwtQWRyZXNzZSwgYW4gZGllIFNpZSBrZWluZSBQcm9kdWt0bmV1ZXJ1bmdlbiBldGMuIG1laHIgZXJoYWx0ZW4gbfZjaHRlbi4gCjxicj4KPGJyPnVuc3Vic2NyaWJlQGFkZW5pb24uZGUgCjxicj4KPGJyPkFsdGVybmF0aXYgc2VuZGVuIFNpZSB1bnMgYml0dGUgZWluIEZheCBhbiArNDkgMjE4MSA3NTY5LTE5OSBvZGVyIHNjaHJlaWJlbiBTaWUgdW5zIGVpbmVuIEJyaWVmIGFuIAo8YnI+QURFTklPTiBHbWJICjxicj5NZXJrYXRvcnN0cmHfZSAyCjxicj40MTUxNSBHcmV2ZW5icm9pY2gKPGJyPkRldXRzY2hsYW5kIAo8YnI+Qml0dGUgZ2ViZW4gU2llIGF1ZiBqZWRlbiBGYWxsIGRpZSBFLU1haWwtQWRyZXNzZSBhbiwgbWl0IGRlciBTaWUgc2ljaCBhbmdlbWVsZGV0IGhhYmVuLgo8YnI+Cjxicj48Yj7EbmRlcnVuZyB1bnNlcmVyIERhdGVuc2NodXR6YmVzdGltbXVuZ2VuPC9iPgo8YnI+V2lyIGJlaGFsdGVuIHVucyB2b3IsIGRpZXNlIERhdGVuc2NodXR6ZXJrbORydW5nIGdlbGVnZW50bGljaCBhbnp1cGFzc2VuLCBkYW1pdCBzaWUgc3RldHMgZGVuIGFrdHVlbGxlbiByZWNodGxpY2hlbiBBbmZvcmRlcnVuZ2VuIGVudHNwcmljaHQgb2RlciB1bSDEbmRlcnVuZ2VuIHVuc2VyZXIgTGVpc3R1bmdlbiBpbiBkZXIgRGF0ZW5zY2h1dHplcmts5HJ1bmcgdW16dXNldHplbiwgei4gQi4gYmVpIGRlciBFaW5m/GhydW5nIG5ldWVyIFNlcnZpY2VzLiBG/HIgSWhyZW4gZXJuZXV0ZW4gQmVzdWNoIGdpbHQgZGFubiBkaWUgbmV1ZSBEYXRlbnNjaHV0emVya2zkcnVuZy4KPGJyPiI7czoyOiJlbiI7czoxNzYzNDoiPGI+UHJpdmFjeSBQb2xpY3k8L2I+Cjxicj5SZXNwb25zaWJsZSBib2R5IGluIHRlcm1zIG9mIGRhdGEgcHJvdGVjdGlvbiBsYXdzIGlzOgo8YnI+RGlwbC4gSW5nLiBBbmRyZWFzIFdpbmtsZXIKPGJyPgo8YnI+PGI+U2FmZWd1YXJkaW5nIG9mIHlvdXIgcGVyc29uYWwgZGF0YTwvYj4KPGJyPkFERU5JT04gR21iSCAoaGVyZWluYWZ0ZXIgY2FsbGVkIEFERU5JT04gb3IgIldlIikgaXMgY29tbWl0dGVkIHRvIHByb3RlY3QgeW91ciBwcml2YWN5IGFuZCB0cmVhdCBhbGwgcGVyc29uYWwgZGF0YSBzdWNoIGFzIG5hbWUsIGFkZHJlc3MgYW5kIGVtYWlsIGFkZHJlc3Mgb2YgY3VzdG9tZXJzIGFuZCB1c2VycyBjb25maWRlbnRpYWxseSBhbmQgb25seSBmb3IgdGhlIGZ1bGZpbGxtZW50IG9mIHRoZSBhZ3JlZWQgcHVycG9zZXMuIExpa2V3aXNlLCB0cmFuc21pc3Npb24gdG8gVGhpcmQgUGFydGllcyBvciBUaGlyZC1QYXJ0eSBBcHBzIGlzIGNvbmR1Y3RlZCBvbmx5IGZvciB0aGUgZnVsZmlsbG1lbnQgb2YgdGhlIGFncmVlZCBwdXJwb3Nlcy4gVGhlIGZvbGxvd2luZyBwcml2YWN5IHBvbGljeSBkZXNjcmliZXMgdGhlIHR5cGVzIG9mIGluZm9ybWF0aW9uIHdlIG1heSBjb2xsZWN0IGZyb20geW91IG9yIHRoYXQgeW91IG1heSBwcm92aWRlIHdoZW4geW91IHZpc2l0IG91ciBXZWJzaXRlcyBhbmQgU2VydmljZXMgYW5kIG91ciBwcmFjdGljZXMgZm9yIGNvbGxlY3RpbmcsIHVzaW5nLCBtYWludGFpbmluZywgcHJvdGVjdGluZyBhbmQgZGlzY2xvc2luZyB0aGF0IGluZm9ybWF0aW9uLgo8YnI+Cjxicj48Yj5HZW5lcmFsIFRlcm1zIG9mIFVzZSBmb3Igb3VyIFdlYnNpdGVzPC9iPgo8YnI+QnkgYWNjZXNzaW5nIGFuZCB1c2luZyBvdXIgV2Vic2l0ZXMgYW5kIFNlcnZpY2VzIHlvdSBhZ3JlZSB0byB0aGUgZm9sbG93aW5nIFByaXZhY3kgUG9saWN5IGFuZCB0aGUgY29ycmVzcG9uZGluZyBUZXJtcyBvZiBVc2UgZm9yIG91ciBXZWJzaXRlcyBhbmQgU2VydmljZXMuIElmIHlvdSBkbyBub3QgY29uc2VudCB0byBvdXIgVGVybXMgb2YgVXNlIGFuZCBQcml2YWN5IFBvbGljeSwgeW91IG11c3Qgbm90IHVzZSBvdXIgV2Vic2l0ZXMgYW5kIFNlcnZpY2VzLgo8YnI+Cjxicj48Yj5HZW5lcmFsIGluZm9ybWF0aW9uIGFuZCBkYXRhIHdlIGNvbGxlY3Qgb24gb3VyIFdlYnNpdGVzPC9iPgo8YnI+VGhlIEFERU5JT04gd2Vic2l0ZXMgYXV0b21hdGljYWxseSBjb2xsZWN0IGFuZCBzdG9yZSBpbmZvcm1hdGlvbiB0cmFuc21pdHRlZCBieSB0aGUgdXNlcidzIGJyb3dzZXIgaW4gc2VydmVyIGxvZyBmaWxlcy4KPGJyPlRoaXMgaW5mb3JtYXRpb24gKHNlcnZlciBsb2cgZmlsZXMpIGluY2x1ZGVzLCBmb3IgZXhhbXBsZSwgdGhlIHR5cGUgb2Ygd2ViIGJyb3dzZXIsIHRoZSBvcGVyYXRpbmcgc3lzdGVtIHVzZWQsIHRoZSBkb21haW4gbmFtZSBvZiB5b3VyIEludGVybmV0IHNlcnZpY2UgcHJvdmlkZXIgYW5kIHRoZSBsaWtlLiBUaGlzIGlzIG9ubHkgaW5mb3JtYXRpb24gdGhhdCBkb2VzIG5vdCBhbGxvdyBjb25jbHVzaW9ucyBhYm91dCB5b3VyIHBlcnNvbi4gVGhpcyBpbmZvcm1hdGlvbiBpcyB0ZWNobmljYWxseSBuZWNlc3NhcnkgdG8gY29ycmVjdGx5IGRlbGl2ZXIgdGhlIGNvbnRlbnRzIG9mIHdlYiBwYWdlcyByZXF1ZXN0ZWQgYnkgeW91IGFuZCBpcyBtYW5kYXRvcnkgd2hlbiB1c2luZyB0aGUgaW50ZXJuZXQuIEFub255bW91cyBpbmZvcm1hdGlvbiBvZiB0aGlzIGtpbmQgaXMgc3RhdGlzdGljYWxseSBldmFsdWF0ZWQgYnkgdXMsIGluIG9yZGVyIHRvIG9wdGltaXplIG91ciBpbnRlcm5ldCBhcHBlYXJhbmNlIGFuZCB0aGUgdGVjaG5vbG9neSBiZWhpbmQgaXQuCjxicj4KPGJyPjxiPkNvb2tpZXM8L2I+Cjxicj5MaWtlIG1hbnkgb3RoZXIgd2Vic2l0ZXMsIHdlIGFsc28gdXNlIHNvLWNhbGxlZCAiQ29va2llcyIuIENvb2tpZXMgYXJlIHNtYWxsIHRleHQgZmlsZXMgdGhhdCBhIHNpdGUgb3IgaXRzIHNlcnZpY2UgcHJvdmlkZXIgdHJhbnNmZXJzIHRvIGEgdXNlcj9zIGhhcmQgZHJpdmUgdGhyb3VnaCBhIFdlYiBicm93c2VyLiBJZiBjb29raWVzIGFyZSBhbGxvd2VkLCB0aGV5IGVuYWJsZSB0aGUgc2l0ZXMgb3Igc2VydmljZSBwcm92aWRlcnMgc3lzdGVtcyB0byByZWNvZ25pemUgeW91ciBicm93c2VyIGFuZCBjYXB0dXJlIGFuZCByZW1lbWJlciBjZXJ0YWluIGluZm9ybWF0aW9uLCBzdWNoIGFzIElQIGFkZHJlc3MsIHRoZSB0eXBlIG9mIGJyb3dzZXIgdXNlZCBmb3IgYWNjZXNzaW5nIHRoZSB3ZWJzaXRlLCBhIGNvbXB1dGVyP3Mgb3BlcmF0aW5nIHN5c3RlbSBhbmQgSW50ZXJuZXQgY29ubmVjdGlvbi4gQmFzZWQgb24gdGhlIGluZm9ybWF0aW9uIGNvbnRhaW5lZCBpbiBjb29raWVzLCB3ZSBjYW4gbW9uaXRvciBhbmQgYW5hbHl6ZSBzaXRlIHRyYWZmaWMsIGN1c3RvbWl6ZSBvdXIgc2l0ZXMgYW5kIHNlcnZpY2VzIHRvIGltcHJvdmUgc2l0ZSBuYXZpZ2F0aW9uIGFuZCB1c2VyIGV4cGVyaWVuY2UuIAo8YnI+T2YgY291cnNlLCB5b3UgY2FuIGFsc28gYWNjZXNzIG91ciBXZWJzaXRlcyBhbmQgU2VydmljZXMgd2l0aG91dCBjb29raWVzLiBJbnRlcm5ldCBicm93c2VycyBhcmUgc2V0IHRvIGFjY2VwdCBjb29raWVzIGJ5IGRlZmF1bHQuIFlvdSBjYW4gZGVhY3RpdmF0ZSB0aGUgdXNlIG9mIGNvb2tpZXMgYXQgYW55IHRpbWUgdmlhIHRoZSBzZXR0aW5nIG9mIHlvdXIgYnJvd3Nlci4gUGxlYXNlIHVzZSB0aGUgaGVscCBmZWF0dXJlcyBvZiB5b3VyIGludGVybmV0IGJyb3dzZXIgdG8gZmluZCBvdXQgaG93IHRvIGNoYW5nZSB0aGVzZSBzZXR0aW5ncy4gUGxlYXNlIG5vdGUgdGhhdCBzb21lIGZlYXR1cmVzIG9mIG91ciB3ZWJzaXRlIG1heSBub3Qgd29yayBpZiB5b3UgZGlzYWJsZSB0aGUgdXNlIG9mIGNvb2tpZXMuCjxicj4KPGJyPjxiPlJlZ2lzdHJhdGlvbiBvbiBvdXIgV2Vic2l0ZXM8L2I+Cjxicj5CeSByZWdpc3RlcmluZywgc3Vic2NyaWJpbmcsIGRvd25sb2FkaW5nIG9yIHBhcnRpY2lwYXRpbmcgdmlhIGEgY29ycmVzcG9uZGluZyBzdGFuZGFyZCBmb3JtLCB5b3UgYWdyZWUgdGhhdCBBREVOSU9OIHByb2Nlc3NlcyBhbmQgc3RvcmVzIHRoZSBnaXZlbiBwZXJzb25hbCBkYXRhIGZvciB0aGUgaW50ZW5kZWQgc2VydmljZS4gWW91IGFncmVlIHRoYXQgeW91IG1heSByZWNlaXZlIHNwb3JhZGljIGluZm9ybWF0aW9uIGFib3V0IG5ld3MsIHByb2R1Y3QgdXBkYXRlcyBhbmQgcHJvbW90aW9ucyBmcm9tIHVzIGJ5IGVtYWlsIG9yIHRlbGVwaG9uZS4gWW91IGNhbiByZXZva2UgeW91ciBjb25zZW50IGFuZCB1bnN1YnNjcmliZSBhbnkgdGltZSAoc2VlIGJlbG93IGluIHNlY3Rpb24gP0RhdGEgUmV0ZW50aW9uPykuCjxicj4KPGJyPjxiPkRhdGEgY29sbGVjdGVkIGJ5IFRoaXJkIFBhcnR5IEFwcHM8L2I+Cjxicj4KPGJyPjxiPkdvb2dsZSBBbmFseXRpY3MgQ29va2llczwvYj4KPGJyPk91ciBXZWJzaXRlcyBhbmQgU2VydmljZXMgdXNlIEdvb2dsZSBBbmFseXRpY3MsIGEgd2ViIGFuYWx5dGljcyBzZXJ2aWNlIHByb3ZpZGVkIGJ5IEdvb2dsZSBJbmMuIChoZXJlaW5hZnRlciBjYWxsZWQ6IEdvb2dsZSkuIEdvb2dsZSBBbmFseXRpY3MgdXNlcyBzby1jYWxsZWQgIkNvb2tpZXMiLCB0aHVzIHRleHQgZmlsZXMsIHRoYXQgYXJlIHN0b3JlZCBvbiB5b3VyIGNvbXB1dGVyIGFuZCB0aGF0IGFsbG93IHVzIHRvIHRyYWNrIGFuZCBhbmFseXplIHRoZSBwYWdlcyBvZiB0aGUgV2Vic2l0ZXMgYW5kIFNlcnZpY2VzIHlvdSB1c2UuIFdlIGRvIHRoaXMgb25seSB0byBiZXR0ZXIgdW5kZXJzdGFuZCBob3cgeW91IHVzZSBvdXIgV2Vic2l0ZXMgYW5kIFNlcnZpY2VzIHRvIGltcHJvdmUgb3VyIGJ1c2luZXNzIGFuZCBtYXJrZXRpbmcgYWN0aXZpdGllcyBmb3IgeW91IGFuZCBmb3Igb3RoZXIgdXNlcnMgYXMgd2VsbC4gR29vZ2xlIEFuYWx5dGljcyBjb29raWVzIGRvIG5vdCBwcm92aWRlIHVzIHdpdGggYW55IHBlcnNvbmFsIGluZm9ybWF0aW9uLiBUaGUgaW5mb3JtYXRpb24gYWJvdXQgeW91ciB1c2Ugb2Ygb3VyIFdlYnNpdGVzIGdlbmVyYXRlZCBieSB0aGUgR29vZ2xlIEFuYWx5dGljcyBjb29raWUgaXMgdXN1YWxseSB0cmFuc21pdHRlZCB0byBhIEdvb2dsZSBzZXJ2ZXIgaW4gdGhlIFVTIGFuZCBzdG9yZWQgdGhlcmUuIEhvd2V2ZXIsIGR1ZSB0byB0aGUgYWN0aXZhdGlvbiBvZiBJUCBhbm9ueW1pemF0aW9uIG9uIHRoZXNlIHdlYnNpdGVzLCB5b3VyIElQIGFkZHJlc3Mgd2lsbCBiZSBzaG9ydGVuZWQgYmVmb3JlaGFuZCBieSBHb29nbGUgd2l0aGluIG1lbWJlciBzdGF0ZXMgb2YgdGhlIEV1cm9wZWFuIFVuaW9uIG9yIGluIG90aGVyIGNvbnRyYWN0aW9uIHN0YXRlcyBvZiB0aGUgQWdyZWVtZW50IG9mIHRoZSBFdXJvcGVhbiBFY29ub21pYyBBcmVhLiBPbmx5IGluIGV4Y2VwdGlvbmFsIGNhc2VzIHdpbGwgdGhlIGZ1bGwgSVAgYWRkcmVzcyBiZSBzZW50IHRvIGEgR29vZ2xlIHNlcnZlciBpbiB0aGUgVVMgYW5kIHNob3J0ZW5lZCB0aGVyZS4gT24gYmVoYWxmIG9mIHRoZSB3ZWJzaXRlP3Mgb3BlcmF0b3IsIEdvb2dsZSB3aWxsIHVzZSB0aGlzIGluZm9ybWF0aW9uIHRvIGV2YWx1YXRlIHlvdXIgdXNlIG9mIHRoZSB3ZWJzaXRlLCB0byBjb21waWxlIHJlcG9ydHMgb24gd2Vic2l0ZSBhY3Rpdml0eSBhbmQgdG8gcHJvdmlkZSBvdGhlciBzZXJ2aWNlcyByZWxhdGVkIHRvIHdlYnNpdGUgYWN0aXZpdHkgYW5kIGludGVybmV0IHVzYWdlIHRvIHRoZSB3ZWJzaXRlIG9wZXJhdG9yLgo8YnI+VGhlIElQIGFkZHJlc3MgcHJvdmlkZWQgYnkgR29vZ2xlIEFuYWx5dGljcyBhcyBwYXJ0IG9mIEdvb2dsZSBBbmFseXRpY3Mgd2lsbCBub3QgYmUgbGlua2VkIHRvIG90aGVyIEdvb2dsZSBkYXRhLgo8YnI+WW91IGNhbiBwcmV2ZW50IHRoZSBzdG9yYWdlIG9mIGNvb2tpZXMgYnkgY2hhbmdpbmcgdGhlIHNldHRpbmdzIG9mIHlvdXIgYnJvd3NlciBzb2Z0d2FyZS4gSG93ZXZlciwgd2UgcG9pbnQgb3V0IHRoYXQgaW4gdGhpcyBjYXNlIHlvdSBtYXkgbm90IGJlIGFibGUgdG8gdGhlIGZ1bGwgdXNlIGFsbCBmdW5jdGlvbnMgb2Ygb3VyICBXZWJzaXRlcy4gWW91IG1heSBhbHNvIHByZXZlbnQgR29vZ2xlIGZyb20gY29sbGVjdGluZyBkYXRhIGdlbmVyYXRlZCBieSB0aGUgY29va2llLCBieSBkb3dubG9hZGluZyBhbmQgaW5zdGFsbGluZyB0aGUgYnJvd3NlciBwbHVnaW4gYXZhaWxhYmxlIHVuZGVyIHRoZSBmb2xsb3dpbmcgbGluazogTGVhcm4gbW9yZSBhYm91dCBwcml2YWN5IGF0IEdvb2dsZSBhbmQgdGhlIEdvb2dsZSBBbmFseXRpY3MgT3B0LW91dCBCcm93c2VyIEFkZC1vbi4KPGJyPiAKPGJyPjxiPkVtYmVkZGVkIFlvdVR1YmUgdmlkZW9zPC9iPgo8YnI+T24gc29tZSBvZiBvdXIgV2Vic2l0ZXMgYW5kIFNlcnZpY2VzIHdlIGVtYmVkIFlvdVR1YmUgdmlkZW9zLiBUaGUgY29ycmVzcG9uZGluZyBwbHVnaW5zIGFyZSBvcGVyYXRlZCBieSBZb3VUdWJlLCBMTEMsIDkwMSBDaGVycnkgQXZlLiwgU2FuIEJydW5vLCBDQSA5NDA2NiwgVVNBLiBXaGVuIHlvdSB2aXNpdCBhIHBhZ2Ugd2l0aCB0aGUgWW91VHViZSBwbHVnaW4sIGl0IHdpbGwgY29ubmVjdCB0byBZb3VUdWJlJ3Mgc2VydmVycy4gWW91VHViZSB3aWxsIGJlIGluZm9ybWVkIHdoaWNoIHBhZ2VzIHlvdSB2aXNpdC4gSWYgeW91IGFyZSBsb2dnZWQgaW50byB5b3VyIFlvdVR1YmUgYWNjb3VudCwgWW91VHViZSBjYW4gYXNzaWduIHlvdXIgc3VyZmluZyBiZWhhdmlvciB0byB5b3UgcGVyc29uYWxseS4gVGhpcyBjYW4gYmUgcHJldmVudGVkIGJ5IGxvZ2dpbmcgb3V0IG9mIHlvdXIgWW91VHViZSBhY2NvdW50IGJlZm9yZWhhbmQuCjxicj5JZiBhIFlvdVR1YmUgdmlkZW8gaXMgc3RhcnRlZCwgdGhlIHByb3ZpZGVyIHVzZXMgY29va2llcyB0aGF0IGNvbGxlY3QgaW5mb3JtYXRpb24gYWJvdXQgdXNlciBiZWhhdmlvci4KPGJyPkFueW9uZSB3aG8gaGFzIGRpc2FibGVkIHRoZSBzdG9yYWdlIG9mIGNvb2tpZXMgZm9yIHRoZSBHb29nbGUgQWQgcHJvZ3JhbSB3aWxsIG5vdCBoYXZlIHRvIGV4cGVjdCBzdWNoIGNvb2tpZXMgd2hlbiB3YXRjaGluZyBZb3VUdWJlIHZpZGVvcy4gQW55b25lIHdobyBoYXMgZGlzYWJsZWQgdGhlIHN0b3JhZ2Ugb2YgY29va2llcyBmb3IgdGhlIEdvb2dsZSBBZCBwcm9ncmFtIHdpbGwgbm90IGhhdmUgdG8gZXhwZWN0IHN1Y2ggY29va2llcyB3aGVuIHdhdGNoaW5nIFlvdVR1YmUgdmlkZW9zLiBZb3VUdWJlIGFsc28gc3RvcmVzIG5vbi1wZXJzb25hbCB1c2FnZSBpbmZvcm1hdGlvbiBpbiBvdGhlciBjb29raWVzLiBJZiB5b3Ugd2FudCB0byBwcmV2ZW50IHRoaXMsIHlvdSBtdXN0IGJsb2NrIHRoZSBzdG9yYWdlIG9mIGNvb2tpZXMgaW4gdGhlIGJyb3dzZXIuCjxicj5Gb3IgbW9yZSBpbmZvcm1hdGlvbiBvbiBkYXRhIHByb3RlY3Rpb24gYXQgWW91VHViZSwgc2VlIHRoZSBwcml2YWN5IHBvbGljeSBvZiB0aGUgcHJvdmlkZXIgYXQ6IDxhIGhyZWY9Imh0dHBzOi8vcG9saWNpZXMuZ29vZ2xlLmNvbS9wcml2YWN5IiB0YXJnZXQ9Il9ibGFuayI+aHR0cHM6Ly9wb2xpY2llcy5nb29nbGUuY29tL3ByaXZhY3k8L2E+Cjxicj4KPGJyPjxiPlNvY2lhbCBQbHVnaW5zPC9iPgo8YnI+T3VyIFdlYnNpdGVzIGFuZCBTZXJ2aWNlcyB1c2Ugc29jaWFsIHBsdWdpbnMgKCJwbHVnaW5zIikgb2YgdGhlIHNvY2lhbCBuZXR3b3JrIGZhY2Vib29rLmNvbSwgd2hpY2ggaXMgb3BlcmF0ZWQgYnkgRmFjZWJvb2sgSW5jLiwgMTYwMSBTLiBDYWxpZm9ybmlhIEF2ZS4sIFBhbG8gQWx0bywgQ0EgOTQzMDQsIFVTQSAoIkZhY2Vib29rIikuIFRoZSBwbHVnaW5zIGFyZSBtYXJrZWQgd2l0aCBhIEZhY2Vib29rIGxvZ28gb3IgdGhlIGFkZGl0aW9uICJGYWNlYm9vayBTb2NpYWwgUGx1Z2luIi4KPGJyPldoZW4gdGhlIHdlYnNpdGUgdXNlciBjYWxscyBhIHdlYiBwYWdlIG9mIHRoZSB3ZWJzaXRlIGNvbnRhaW5pbmcgc3VjaCBhIHBsdWdpbiwgdGhlIGJyb3dzZXIgZXN0YWJsaXNoZXMgYSBkaXJlY3QgY29ubmVjdGlvbiB3aXRoIHRoZSBGYWNlYm9vayBzZXJ2ZXJzLiBUaGUgY29udGVudCBvZiB0aGUgcGx1Z2luIGlzIHRyYW5zbWl0dGVkIGJ5IEZhY2Vib29rIGRpcmVjdGx5IHRvIHRoZSBicm93c2VyIGFuZCBlbWJlZGRlZCBieSBpdCBpbnRvIHRoZSB3ZWJzaXRlLiBCeSBpbnRlZ3JhdGluZyB0aGUgcGx1Z2lucywgRmFjZWJvb2sgcmVjZWl2ZXMgdGhlIGluZm9ybWF0aW9uIHRoYXQgdGhlIHdlYnNpdGUgdXNlciBoYXMgYWNjZXNzZWQgdGhlIGNvcnJlc3BvbmRpbmcgcGFnZSBvZiB0aGUgd2Vic2l0ZS4gSWYgdGhlIHdlYnNpdGUgdXNlciBpcyBsb2dnZWQgaW4gdG8gRmFjZWJvb2ssIEZhY2Vib29rIGNhbiBhc3NpZ24gdGhlIHZpc2l0IHRvIGhpcyBGYWNlYm9vayBhY2NvdW50LiBJZiB0aGUgd2Vic2l0ZSB1c2VyIGludGVyYWN0cyB3aXRoIHRoZSBwbHVnaW5zLCBmb3IgZXhhbXBsZSwgcHJlc3NlcyB0aGUgP0xpa2U/IGJ1dHRvbiBvciBsZWF2ZXMgYSBjb21tZW50LCB0aGUgY29ycmVzcG9uZGluZyBpbmZvcm1hdGlvbiBpcyB0cmFuc21pdHRlZCBmcm9tIGhpcyBicm93c2VyIGRpcmVjdGx5IHRvIEZhY2Vib29rIGFuZCBzdG9yZWQgdGhlcmUuCjxicj5UaGUgcHVycG9zZSBhbmQgaGFuZGxpbmcgb2YgdGhlIGRhdGEgY29sbGVjdGlvbiBhbmQgdGhlIGZ1cnRoZXIgcHJvY2Vzc2luZyBhbmQgdXNlIG9mIGRhdGEgYnkgRmFjZWJvb2ssIGFzIHdlbGwgYXMgcmVsYXRlZCByaWdodHMgYW5kIHByaXZhY3kgc2V0dGluZ3MsIGNhbiBiZSBmb3VuZCBpbiBGYWNlYm9vaydzIHByaXZhY3kgcG9saWN5Lgo8YnI+SWYgeW91IGRvIG5vdCB3YW50IEZhY2Vib29rIHRvIGNvbGxlY3QgZGF0YSBhYm91dCB5b3VyIHZpc2l0IG9uIHRoaXMgd2Vic2l0ZSwgcGxlYXNlIGxvZyBvdXQgb2YgRmFjZWJvb2sgYmVmb3JlIHZpc2l0aW5nIHRoaXMgd2Vic2l0ZS4KPGJyPlRoZSB3ZWJzaXRlIGluY2x1ZGVzIGZlYXR1cmVzIG9mIFR3aXR0ZXIgSW5jLiwgNzk1IEZvbHNvbSBTdHJlZXQsIFN1aXRlIDYwMCwgU2FuIEZyYW5jaXNjbywgQ0EgOTQxMDcsIFVTQS4gSWYgeW91IHVzZSBUd2l0dGVyIGFuZCBlc3BlY2lhbGx5IHRoZSBmdW5jdGlvbiAiUmUtVHdlZXQiLCBUd2l0dGVyIGxpbmtzIHlvdXIgVHdpdHRlciBhY2NvdW50IHdpdGggdGhlIHdlYnNpdGVzIHlvdSB1c2UuIFRoaXMgd2lsbCBiZSBhbm5vdW5jZWQgdG8gb3RoZXIgdXNlcnMgb24gVHdpdHRlciwgZXNwZWNpYWxseSB5b3VyIGZvbGxvd2Vycy4gSW4gdGhpcyB3YXksIHRoZXJlIGlzIGFsc28gYSBkYXRhIHRyYW5zZmVyIHRvIFR3aXR0ZXIuCjxicj5XZSBoYXZlIG5vIGFjY2VzcyB0byB0aGUgaW5mb3JtYXRpb24gY29sbGVjdGVkIGJ5IFR3aXR0ZXIsIG5vciBkbyB3ZSBrbm93IGFib3V0IHRoZSBleHRlbnQgb2YgdGhlIHRyYW5zbWl0dGVkIGRhdGEgb3IgdGhlIGRhdGEgdXNhZ2UgYnkgVHdpdHRlci4gUGxlYXNlIHZpc2l0IHRoZSBmb2xsb3dpbmcgbGluayBmb3IgbW9yZSBpbmZvcm1hdGlvbjogPGEgaHJlZj0iaHR0cHM6Ly90d2l0dGVyLmNvbS9wcml2YWN5IiB0YXJnZXQ9Il9ibGFuayI+aHR0cHM6Ly90d2l0dGVyLmNvbS9wcml2YWN5PC9hPi4gUGxlYXNlIG5vdGUsIHRoYXQgeW91IGhhdmUgdGhlIG9wdGlvbiB0byBjaGFuZ2UgeW91ciBwcml2YWN5IHNldHRpbmdzIGZvciBUd2l0dGVyIGluIHlvdXIgVHdpdHRlciBhY2NvdW50IHNldHRpbmdzOiA8YSBocmVmPSJodHRwczovL3R3aXR0ZXIuY29tL3NldHRpbmdzL2FjY291bnQiIHRhcmdldD0iX2JsYW5rIj5odHRwczovL3R3aXR0ZXIuY29tL3NldHRpbmdzL2FjY291bnQ8L2E+Lgo8YnI+IAo8YnI+V2UgaGF2ZSBpbnRlZ3JhdGVkIHRoZSBzb2NpYWwgbWVkaWEgYnV0dG9ucyBvZiB0aGUgZm9sbG93aW5nIGNvbXBhbmllcyBvbiBvdXIgd2Vic2l0ZToKPGJyPgo8YnI+LUZhY2Vib29rIEluYy4gKDE2MDEgUy4gQ2FsaWZvcm5pYSBBdmUgLSBQYWxvIEFsdG8gLSBDQSA5NDMwNCAtIFVTQSkKPGJyPi1Ud2l0dGVyIEluYy4gKDc5NSBGb2xzb20gU3QuIC0gU3VpdGUgNjAwIC0gU2FuIEZyYW5jaXNjbyAtIENBIDk0MTA3IC0gVVNBKQo8YnI+LUdvb2dsZSBQbHVzL0dvb2dsZSBJbmMuICgxNjAwIEFtcGhpdGhlYXRyZSBQYXJrd2F5IC0gTW91bnRhaW4gVmlldyAtIENBIDk0MDQzIC0gVVNBKQo8YnI+LVdoYXRzYXBwCjxicj4tUGludGVyZXN0Cjxicj4tWElORyBBRyAoR+Ruc2VtYXJrdCA0MyAtIDIwMzU0IEhhbWJ1cmcgLSBHZXJtYW55KQo8YnI+LUxpbmtlZEluIENvcnBvcmF0aW9uICgyMDI5IFN0aWVybGluIENvdXJ0IC0gTW91bnRhaW4gVmlldyAtIENBIDk0MDQzIC0gVVNBKQo8YnI+LVJlZGRpdAo8YnI+LVN0dW1ibGV1cG9uCjxicj4tRmxhdHRyCjxicj4KPGJyPjxiPkdvb2dsZSBBZFdvcmRzPC9iPgo8YnI+T3VyIFdlYnNpdGVzIHVzZSBHb29nbGUgQ29udmVyc2lvbiBUcmFja2luZy4gSWYgeW91IGhhdmUgYWNjZXNzZWQgb3VyIHdlYnNpdGUgdmlhIGFuIGFkdmVydGlzZW1lbnQgc2VudCBieSBHb29nbGUsIEdvb2dsZSBBZHdvcmRzIHdpbGwgc2V0IGEgY29va2llIG9uIHlvdXIgY29tcHV0ZXIuIFRoZSBjb252ZXJzaW9uIHRyYWNraW5nIGNvb2tpZSBpcyBzZXQgd2hlbiBhIHVzZXIgY2xvY2tzIG9uIGEgR29vZ2xlLXNlcnZlZCBhZC4gVGhlc2UgY29va2llcyBsb3NlIHRoZWlyIHZhbGlkaXR5IGFmdGVyIGEgbWF4aW11bSBvZiA5MCBkYXlzIGFuZCBhcmUgbm90IHVzZWQgZm9yIHBlcnNvbmFsIGlkZW50aWZpY2F0aW9uLiBJZiB0aGUgdXNlciB2aXNpdHMgY2VydGFpbiBwYWdlcyBvbiBvdXIgd2Vic2l0ZSBhbmQgdGhlIGNvb2tpZSBoYXMgbm90IGV4cGlyZWQsIHdlIGFuZCBHb29nbGUgY2FuIHJlY29nbml6ZSB0aGF0IHRoZSB1c2VyIGNsaWNrZWQgb24gdGhlIGFkIGFuZCB3YXMgcmVkaXJlY3RlZCB0byB0aGlzIHBhZ2UuIEVhY2ggR29vZ2xlIEFkV29yZHMgY3VzdG9tZXIgcmVjZWl2ZXMgYSBkaWZmZXJlbnQgY29va2llLiBDb29raWVzIGNhbm5vdCBiZSB0cmFja2VkIHRocm91Z2ggQWRXb3JkcyBhZHZlcnRpc2Vycz8gd2Vic2l0ZXMuIFRoZSBpbmZvcm1hdGlvbiBnYXRoZXJlZCBieSB1c2luZyB0aGUgY29udmVyc2lvbiBjb29raWUgaXMgdXNlZCB0byBnZW5lcmF0ZSBjb252ZXJzaW9uIHN0YXRpc3RpY3MgZm9yIEFkV29yZHMgYWR2ZXJ0aXNlcnMgd2hvIGhhdmUgb3B0ZWQgZm9yIGNvbnZlcnNpb24gdHJhY2tpbmcuIEFkdmVydGlzZXJzIGFyZSB0b2xkIHRoZSB0b3RhbCBudW1iZXIgb2YgdXNlcnMgd2hvIGNsaWNrZWQgb24gdGhlaXIgYWQgYW5kIHdlcmUgcmVkaXJlY3RlZCB0byBhIGNvbnZlcnNpb24gdHJhY2tpbmcgdGFnIHBhZ2UuIEhvd2V2ZXIsIHRoZXkgZG8gbm90IHJlY2VpdmUgaW5mb3JtYXRpb24gdGhhdCBwZXJzb25hbGx5IGlkZW50aWZpZXMgdXNlcnMuCjxicj5JZiB5b3UgZG8gbm90IHdhbnQgdG8gcGFydGljaXBhdGUgaW4gdGhlIGNvbnZlcnNpb24gdHJhY2tpbmcsIHlvdSBjYW4gcmVmdXNlIHRoZSByZXF1aXJlZCBzZXR0aW5nIG9mIGEgY29va2llIC0gZm9yIGV4YW1wbGUgdmlhIGEgYnJvd3NlciBzZXR0aW5nIHRoYXQgZ2VuZXJhbGx5IGRpc2FibGVzIHRoZSBhdXRvbWF0aWMgc2V0dGluZyBvZiBjb29raWVzIG9yIHNldHMgeW91ciBicm93c2VyIHRvIGJsb2NrIGNvb2tpZXMgZnJvbSB0aGUgZG9tYWluICJnb29nbGVsZWFkc2VydmljZXMuY29tIi4KPGJyPiBQbGVhc2Ugbm90ZSB0aGF0IHlvdSBjYW5ub3QgZGVsZXRlIHRoZSBvcHQtb3V0IGNvb2tpZSBhcyBsb25nIGFzIHlvdSB3YW50IHRvIHByZXZlbnQgR29vZ2xlIGZyb20gZ2F0aGVyaW5nIGRhdGEuIElmIHlvdSBoYXZlIGRlbGV0ZWQgYWxsIHlvdXIgY29va2llcyBpbiB5b3VyIGJyb3dzZXIsIHlvdSB3aWxsIGhhdmUgdG8gcmVzZXQgdGhlIGNvcnJlc3BvbmRpbmcgb3B0LW91dCBjb29raWUuCjxicj4KPGJyPjxiPkdvb2dsZSBSZW1hcmtldGluZzwvYj4KPGJyPk91ciBXZWJzaXRlcyB1c2UgR29vZ2xlIEluYy4ncyByZW1hcmtldGluZyBmZWF0dXJlLiBUaGlzIGZlYXR1cmUgaXMgZGVzaWduYXRlZCB0byBwcmVzZW50IGludGVyZXN0LWJhc2VkIGFkcyB0byB3ZWJzaXRlIHZpc2l0b3JzIHdpdGhpbiB0aGUgR29vZ2xlIE5ldHdvcmsuIEEgc28tY2FsbGVkID9jb29raWU/IGlzIHN0b3JlZCBpbiB0aGUgYnJvd3NlciBvZiB0aGUgd2Vic2l0ZSB2aXNpdG9yLCB3aGljaCBtYWtlcyBpdCBwb3NzaWJsZSB0byByZWNvZ25pemUgdGhlIHZpc2l0b3Igd2hlbiBoZSBvciBzaGUgdmlzaXRzIHdlYnBhZ2VzIHRoYXQgYmVsb25nIHRvIHRoZSBhZHZlcnRpc2luZyBuZXR3b3JrIG9mIEdvb2dsZS4gT24gdGhlc2UgcGFnZXMsIHZpc2l0b3JzIG1heSBiZSBwcmVzZW50ZWQgd2l0aCBhZHMgcmVsYXRlZCB0byBjb250ZW50IHRoYXQgdGhlIHZpc2l0b3IgcHJldmlvdXNseSB2aWV3ZWQgb24gd2ViIHBhZ2VzIHVzaW5nIEdvb2dsZSdzIHJlbWFya2V0aW5nIGZlYXR1cmUuIEdvb2dsZSBzYXlzIGl0IGRvZXMgbm90IGNvbGxlY3QgYW55IHBlcnNvbmFsIGluZm9ybWF0aW9uIGR1cmluZyB0aGlzIHByb2Nlc3MuIEhvd2V2ZXIsIHlvdSBjYW4gYWx3YXlzIGRpc2FibGUgdGhlIEdvb2dsZSBSZW1hcmtldGluZyBjb29raWUgYnkgY2hhbmdpbmcgdGhlIHNldHRpbmdzIGF0IDxhIGhyZWY9Imh0dHA6Ly93d3cuZ29vZ2xlLmNvbS9zZXR0aW5ncy9hZHMiIHRhcmdldD0iX2JsYW5rIj5odHRwOi8vd3d3Lmdvb2dsZS5jb20vc2V0dGluZ3MvYWRzPC9hPi4gQWx0ZXJuYXRpdmVseSwgeW91IGNhbiBkaXNhYmxlIHRoZSB1c2Ugb2YgY29va2llcyBmb3IgaW50ZXJlc3QtYmFzZWQgYWR2ZXJ0aXNpbmcgdGhyb3VnaCB0aGUgTmV0d29yayBJbml0aWF0aXZlIGJ5IGZvbGxvd2luZyB0aGUgaW5zdHJ1Y3Rpb25zIGF0OiA8YSBocmVmPSJodHRwOi8vd3d3Lm5ldHdvcmthZHZlcnRpc2luZy5vcmcvbWFuYWdpbmcvb3B0X291dC5hc3AiIHRhcmdldD0iX2JsYW5rIj5odHRwOi8vd3d3Lm5ldHdvcmthZHZlcnRpc2luZy5vcmcvbWFuYWdpbmcvb3B0X291dC5hc3A8L2E+Lgo8YnI+Cjxicj48Yj5SZWdpc3RyYXRpb24gZm9yIG91ciBBcHBzIGFuZCBTZXJ2aWNlczwvYj4KPGJyPldoZW4gcmVnaXN0ZXJpbmcgZm9yIG91ciBhcHBzIGFuZCBzZXJ2aWNlcywgc29tZSBwZXJzb25hbCBpbmZvcm1hdGlvbiB3aWxsIGJlIGNvbGxlY3RlZCwgc3VjaCBhcyBuYW1lLCBhZGRyZXNzLCBjb250YWN0IGFuZCBjb21tdW5pY2F0aW9uIGluZm9ybWF0aW9uIHN1Y2ggYXMgdGVsZXBob25lIG51bWJlciBhbmQgZW1haWwgYWRkcmVzcy4gSWYgeW91IHJlZ2lzdGVyIHdpdGggdXMsIHlvdSBjYW4gYWNjZXNzIGNvbnRlbnQgYW5kIHNlcnZpY2VzIHRoYXQgd2Ugb2ZmZXIgb25seSB0byByZWdpc3RlcmVkIHVzZXJzLiBSZWdpc3RlcmVkIHVzZXJzIGFsc28gaGF2ZSB0aGUgb3B0aW9uIG9mIGNoYW5naW5nIG9yIGRlbGV0aW5nIHRoZSBkYXRhIHNwZWNpZmllZCBkdXJpbmcgcmVnaXN0cmF0aW9uIGF0IGFueSB0aW1lLiBPZiBjb3Vyc2UsIHdlIGFsc28gcHJvdmlkZSB5b3Ugd2l0aCBpbmZvcm1hdGlvbiBhYm91dCB0aGUgcGVyc29uYWwgZGF0YSB3ZSBob2xkIGFib3V0IHlvdSBhdCBhbnkgdGltZS4gV2UgYXJlIGhhcHB5IHRvIGNvcnJlY3Qgb3IgZGVsZXRlIHRoZXNlIGRhdGEgYXQgeW91ciByZXF1ZXN0LCBhcyBmYXIgYXMgbm8gc3RhdHV0b3J5IHN0b3JhZ2UgcmVxdWlyZW1lbnRzIHByZWNsdWRlIG9yIHRoZSBkYXRhIGlzIHJlcXVpcmVkIHRvIGNhcnJ5IG91dCBjb250cmFjdHVhbCBzZXJ2aWNlcy4gVG8gY29udGFjdCB1cyBpbiB0aGlzIGNvbnRleHQsIHBsZWFzZSB1c2UgdGhlIGNvbnRhY3QgZGV0YWlscyBnaXZlbiBhdCB0aGUgZW5kIG9mIHRoaXMgUHJpdmFjeSBQb2xpY3kuCjxicj4KPGJyPjxiPlByb3Zpc2lvbiBvZiBQYWlkIFNlcnZpY2VzPC9iPgo8YnI+VG8gcHJvdmlkZSBQYWlkIFNlcnZpY2VzIHdlIGFzayBmb3IgYWRkaXRpb25hbCBkYXRhLCBzdWNoIGFzIHBheW1lbnQgZGV0YWlscy4KPGJyPgo8YnI+PGI+U1NMIEVuY3J5cHRpb248L2I+Cjxicj5UbyBwcm90ZWN0IHRoZSBzZWN1cml0eSBvZiB5b3VyIGRhdGEgZHVyaW5nIHRyYW5zbWlzc2lvbiwgd2UgdXNlIGVuY3J5cHRpb24gdGVjaG5pcXVlcyBhY2NvcmRpbmcgdG8gdGhlIGN1cnJlbnQgc3RhdHVzIChzdWNoIGFzIFNTTCkgb3ZlciBIVFRQUy4KPGJyPgo8YnI+PGI+UHVibGlzaGluZyBvZiBwcmVzcyByZWxlYXNlcyAvIGVkaXRvcmlhbCAvIGpvdXJuYWxpc3RpYyBwdWJsaWNhdGlvbnM8L2I+Cjxicj5CeSBuYXR1cmUsIG91ciBQUiBTZXJ2aWNlcywgc3VjaCBhcyBQUi1HYXRld2F5IGFuZCBhZmZpbGlhdGVkIG5ld3Mgc2l0ZXMsIGFyZSBpbnRlbmRlZCBmb3IgcHVibGlzaGluZyBpbmZvcm1hdGlvbi4gVGhlIHVzZXIgbWF5IHByb3ZpZGUgcGVyc29uYWwgaW5mb3JtYXRpb24gdGhhdCBpcyBpbnRlbmRlZCBmb3IgcHVibGljIHJlbGF0aW9ucyBvciB0aGF0IGlzIHBhcnQgb2YgdGhlIGNvcHkgb3IgY29udGFjdCBpbmZvcm1hdGlvbiB3aXRoaW4gdGhlIHByZXNzIHJlbGVhc2VzLiBUaGlzIGluZm9ybWF0aW9uIGlzIGFsc28gcGFzc2VkIG9uIHRvIFRoaXJkIFBhcnR5IHdlYiBzaXRlcyBmb3IgdGhlIHB1cnBvc2Ugb2YgcHVibGlzaGluZyBJZiBzZWxlY3RlZCBieSB0aGUgdXNlciwgdGhpcyBkYXRhIG1heSBhbHNvIGJlIHNoYXJlZCB3aXRoIHNpdGVzIGxvY2F0ZWQgb3V0c2lkZSB0aGUgRXVyb3BlYW4gVW5pb24uCjxicj4KPGJyPjxiPlVzZSBvZiB0aGUgZGF0YSB3aXRoaW4gdGhlIHBsdWdpbiBhbmQgdGhlIHdlYiBhcHAgQmxvZzJTb2NpYWw8L2I+Cjxicj5CbG9nMlNvY2lhbCBpc3QgYSBzb2NpYWwgbWVkaWEgbWFuYWdlbWVudCB0b29sLiBCeSBpdHMgbmF0dXJlLCB0aGUgc2VydmljZSBlbmFibGVzIHVzZXJzIHRvIHNoYXJlIGNvbnRlbnQgb24gc29jaWFsIG5ldHdvcmtzLiAKPGJyPldoZW4gdXNpbmcgdGhlIHBsdWdpbiBCbG9nMlNvY2lhbCBubyBwZXJzb25hbCB1c2VyIGRhdGEgb2YgdGhlIGJsb2cgaXMgY29sbGVjdGVkIG9uIHRoZSBXb3JkcHJlc3Mgd2Vic2l0ZSBvZiB0aGUgYmxvZyBvd25lci4KPGJyPldpdGhpbiB0aGUgc29jaWFsIG1lZGlhIGFwcGxpY2F0aW9uIEJsb2cyU29jaWFsLCBwZXJzb25hbCBkYXRhIG9mIHRoZSB1c2VyIGlzIGNvbGxlY3RlZC4gVGhlc2UgYXJlIGVtYWlsIGFkZHJlc3MsIGJsb2cgdXNlciBJRCBhbmQgYmxvZyBVUkwuIEEgdW5pcXVlIHVzZXIgSUQgaXMgZ2VuZXJhdGVkIGZyb20gdGhpcyBkYXRhIGZvciB0ZWNobmljYWwgcHVycG9zZXMuCjxicj5UaGUgc2VydmljZSBhbGxvd3MgdXNlcnMgdG8gY29ubmVjdCB0byBvdGhlciB0aGlyZCBwYXJ0eSBzZXJ2aWNlcyBpbmNsdWRpbmcgc3VwcG9ydGVkIHBsYXRmb3JtcyBzdWNoIGFzIFR3aXR0ZXIsIEZhY2Vib29rLCBMaW5rZWRJbiwgZXRjLiBXaGVuIHRoZSB1c2VyIG9mIHRoZSBwbHVnaW4vd2ViIGFwcCBjb25uZWN0cyB0byB0aGUgc29jaWFsIG5ldHdvcmtzIGluIHRoZSBhZG1pbiBhcmVhLCB0aGUgZm9sbG93aW5nIGRhdGEgd2lsbCBiZSBjb2xsZWN0ZWQ6IAo8YnI+Cjxicj4tbmV0d29yayBhY2NvdW50IElELCAKPGJyPi1uZXR3b3JrIGFjY291bnQgdG9rZW4sIAo8YnI+LXBhZ2UgSUQsIAo8YnI+LWdyb3VwIElELCAKPGJyPi1wYWdlIG5hbWUsIAo8YnI+LWdyb3VwIG5hbWUsIAo8YnI+LW5ldHdvcmsgYWNjb3VudCBuYW1lLCBhbmQgCjxicj4tcGFzc3dvcmQsIGlmIGFwcGxpY2FibGUsIGRlcGVuZGluZyBvbiB0aGUgbmV0d29yay4KPGJyPgo8YnI+V2hlbiB1c2VyIHJlZ2lzdGVyIGZvciBwcmVtaXVtL3BhaWQgc2VydmljZXMgb3Igc3Vic2NyaWJlcyB0byBvdXIgbmV3c2xldHRlciBpbiB0aGUgYWRtaW5pc3RyYXRvciBhcmVhLCB0aGUgZm9sbG93aW5nIHBlcnNvbmFsIGRhdGEgaXMgY29sbGVjdGVkIHRvIHByb3ZpZGUgdGhlIGNvbnRyYWN0dWFsIHNlcnZpY2U6IAo8YnI+Cjxicj4tZW1haWwgYWRkcmVzcwo8YnI+LW5hbWUKPGJyPi1hZGRyZXNzCjxicj4tY3JlZGl0IGNhcmQgaW5mb3JtYXRpb24gb3IgYWNjb3VudCBkYXRhIGZvciBwYXlhYmxlIHNlcnZpY2VzLgo8YnI+Cjxicj5UaGUgcGF5bWVudCBwcm9jZXNzaW5nIHBhcnRuZXIgaXMgUGF5cHJvR2xvYmFsLmNvbSBpbiBUb3JvbnRvLCBDYW5hZGEuCjxicj4KPGJyPjxiPkRhdGEgUmV0ZW50aW9uLCBVc2UsIEFjY2VzcywgQ29ycmVjdGlvbnMgYW5kIFJlbW92YWw8L2I+Cjxicj5Zb3UgaGF2ZSB0aGUgcmlnaHQgdG8gcmVxdWVzdCBpbmZvcm1hdGlvbiBhYm91dCB5b3VyIHN0b3JlZCBwZXJzb25hbCBkYXRhIGFueSB0aW1lLiBZb3UgYWxzbyBoYXZlIHRoZSByaWdodCB0byBhbWVuZCwgY29ycmVjdCBvciByZW1vdmUgeW91ciBwZXJzb25hbCBkYXRhIG9yIGNhbmNlbCB5b3VyIGFjY291bnRzIGFuZCBzdWJzY3JpcHRpb25zLCBleGNlcHQgZm9yIHRoZSByZXF1aXJlZCBkYXRhIHN0b3JlZCBmb3Igc3RpbGwgcGVuZGluZyBidXNpbmVzcyB0cmFuc2FjdGlvbnMgb3IgdGhlIGNvbnRhY3QgaW5mb3JtYXRpb24gaW4gcHJlc3MgcmVsZWFzZXMsIHJlcXVpcmVkIGZvciBwdWJsaXNoaW5nLgo8YnI+Cjxicj5CbG9ja2VkIGRhdGEgd2lsbCBiZSByZXF1aXJlZCB0byBiZSBzdG9yZWQgaW4gYSBsb2NrIGZpbGUgZm9yIHB1cnBvc2VzIG9mIHJlY29yZCBrZWVwaW5nLiBZb3UgbWF5IGFsc28gcmVxdWVzdCB0aGUgZGVsZXRpb24gb2YgdGhpcyBkYXRhLCBhcyBsb25nIGFzIHRoaXMgZGF0YQo8YnI+Cjxicj4taXMgbm90IHN1YmplY3QgdG8gYW55IGxlZ2FsIHJldGVudGlvbiBwZXJpb2Qgb3Igb2JsaWdhdGlvbiwgCjxicj4td2FzIG5vdCBnaXZlbiBmb3IgdGhlIGludGVuZGVkIHB1cnBvc2Ugb2YgcHVibGlzaGluZyBvbiB0aGUgSW50ZXJuZXQgb3Igc29jaWFsIG1lZGlhIG5ldHdvcmtzLCB3aGVyZSB0aGlzIGluZm9ybWF0aW9uIGlzIHB1YmxpYyBieSBuYXR1cmUsCjxicj4taXMgbm90IHN1YmplY3QgdG8gdGhlIG1lZGlhIHByaXZpbGVnZSwgb3IgCjxicj4KPGJyPkFERU5JT04gaGFzIG5vIGRpcmVjdCBhY2Nlc3MgZm9yIGRlbGV0aW5nIHRoZSBkYXRhLCBpLmcuIG9uIFRoaXJkIFBhcnR5IHNpdGVzIG9yIGFwcHMKPGJyPklmIHRoZXJlIGlzIGFuIG9ibGlnYXRpb24gdG8gbG9jayBvciBkZWxldGUgeW91ciBkYXRhLCB3ZSB3aWxsIGRvIHNvIG9uIHJlcXVlc3QuCjxicj5Ib3dldmVyLCB3ZSB3aWxsIHVzZSB5b3VyIGluZm9ybWF0aW9uIGFzIG5lY2Vzc2FyeSB0byBjb21wbHkgd2l0aCBvdXIgbGVnYWwgb2JsaWdhdGlvbnMsIHJlc29sdmUgZGlzcHV0ZXMsIGFuZCBlbmZvcmNlIG91ciBhZ3JlZW1lbnRzLCB1bmxlc3MgYSBsb25nZXIgcmV0ZW50aW9uIHBlcmlvZCBpcyByZXF1aXJlZCBvciBwZXJtaXR0ZWQgYnkgbGF3Lgo8YnI+WW91IGNhbiByZXZva2UgeW91ciBjb25zZW50IGFuZCBzdWJzY3JpcHRpb25zIGFueSB0aW1lLCB3aXRoIGVmZmVjdCBmb3IgdGhlIGZ1dHVyZSwgYnkgbm90aWZ5aW5nIHVzIHBlciBlbWFpbCwgZmF4IG9yIGxldHRlci4KPGJyPklmIHlvdSB3b3VsZCBsaWtlIHRvIHJldm9rZSB5b3VyIGNvbnNlbnQgb3IgdW5zdWJzY3JpYmUgZnJvbSBvdXIgZW1haWxzLCB5b3Ugd2lsbCBmaW5kIGEgbGluayB0byB1bnN1YnNjcmliZSBhdCB0aGUgZW5kIG9mIGVhY2ggZW1haWwgeW91IHJlY2VpdmUgZnJvbSB1cy4gT3IgeW91IGNhbiBzZW5kIGFuIGVtYWlsIGFuZCBpbmRpY2F0ZSB0aGUgZW1haWwgYWRkcmVzcyBmb3Igd2hpY2ggeW91IGRvIG5vIGxvbmdlciB3YW50IHRvIHJlY2VpdmUgYW55IGVtYWlsIGluZm9ybWF0aW9uLgo8YnI+Cjxicj51bnN1YnNjcmliZUBhZGVuaW9uLmRlIAo8YnI+Cjxicj5BbHRlcm5hdGl2ZWx5LCB5b3UgY2FuIHNlbmQgYSBmYXggdG8gKzQ5IDIxODEgNzU2OS0xOTkgb3Igd3JpdGUgYSBsZXR0ZXIgdG8KPGJyPgo8YnI+QURFTklPTiBHbWJICjxicj5NZXJrYXRvcnN0cmHfZSAyCjxicj40MTUxNSBHcmV2ZW5icm9pY2gKPGJyPkdlcm1hbnkKPGJyPgo8YnI+SW4gYW55IGNhc2UsIHBsZWFzZSBkbyBub3QgZm9yZ2V0IHRvIGluZGljYXRlIHRoZSBlbWFpbCBhZGRyZXNzIHdpdGggd2hpY2ggeW91IGhhdmUgcmVnaXN0ZXJlZCBvciB3aGljaCBlbWFpbCBhZGRyZXNzIHlvdSB3YW50IHRvIHVuc3Vic2NyaWJlLCBvciB3aGljaCBzZXJ2aWNlIHlvdSB3b3VsZCBsaWtlIHRvIHRlcm1pbmF0ZSBvciByZXZva2UuCjxicj4KPGJyPjxiPkNoYW5nZXMgb2Ygb3VyIFByaXZhY3kgUG9saWN5PC9iPgo8YnI+V2UgcmVzZXJ2ZSB0aGUgcmlnaHQgdG8gY2hhbmdlIHRoaXMgUHJpdmFjeSBQb2xpY3kgZnJvbSB0aW1lIHRvIHRpbWUgdG8gZW5zdXJlIHRoYXQgaXQgY29tcGxpZXMgd2l0aCBjdXJyZW50IGxlZ2FsIHJlcXVpcmVtZW50cyBvciB0byBpbXBsZW1lbnQgY2hhbmdlcyB0byBvdXIgc2VydmljZXMgaW4gdGhlIHByaXZhY3kgcG9saWN5LCBmb3IgZXhhbXBsZSwgd2hlbiBpbnRyb2R1Y2luZyBuZXcgc2VydmljZXMuIAo8YnI+WW91ciB2aXNpdCBvZiBvdXIgV2Vic2l0ZSBvciB1c2Ugb2Ygb3VyIFNlcnZpY2VzLCBmb2xsb3dpbmcgdGhlIGNoYW5nZXMsIGNvbnN0aXR1dGVzIGFjY2VwdGFuY2Ugb2YgbmV3IFByaXZhY3kgUG9saWN5Lgo8YnI+Ijt9', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1178, 'B2S_MULTI_WIDGET', 'a:2:{s:9:\"timestamp\";s:19:\"2019-11-07 12:18:02\";s:7:\"content\";s:9869:\"[{\"title\":\"Version 5.8: Google My Business auto posting\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2019\\/09\\/release-5-8-en.png\' style=\'width: 200px;height: 200px;\' alt=\'Release 5.8\'>\\r\\n<div style=\\\"margin: 0 0 5px 10px;\\\">\\r\\n<p style=\\\"padding-bottom: 15px; font-size: 20px\\\">\\r\\nUpdate your Blog2Social to version 5.8 to benefit from Google My Business (Pro & Business) auto posting for imported posts, easier draft management and more improvements and tweaks.\\r\\n<\\/p>\\r\\n<a href=\'https:\\/\\/www.blog2social.com\\/en\\/faq\\/index.php?action=news&newsid=66&newslang=en\' target=\'_blank\' class=\'btn btn-primary\' title=\'FAQ News Release 5.8\'>learn more<\\/a>\\r\\n<\\/div><\\/div>\"},{\"title\":\"New: Google My Business auto posting for imported posts\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2019\\/09\\/gmb-auto-posting-en.png\' style=\'width: 200px;height: 200px;\' alt=\'Release 5.8\'>\\r\\n<div style=\\\"margin: 0 0 5px 10px;\\\">\\r\\n<p style=\\\"padding-bottom: 15px; font-size: 20px\\\">\\r\\nAuto posting your content on social media helps to optimize your content creation and distribution process and saves valuable time and resources. Blog2Social 5.8. now adds Google My Business to the auto poster for imported posts (Pro & Business).\\r\\n<\\/p>\\r\\n<a href=\'https:\\/\\/www.blog2social.com\\/en\\/faq\\/index.php?action=news&newsid=66&newslang=en\' target=\'_blank\' class=\'btn btn-primary\' title=\'FAQ News Release 5.8\'>learn more<\\/a>\\r\\n<\\/div><\\/div>\"},{\"title\":\"New: Save your customizing work as drafts\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2019\\/08\\/feature-safe-post-draft.png\' style=\'width: 200px;height: 200px;\' alt=\'save social media posts as drafts\'>\\r\\n<div style=\\\"margin: 0 0 5px 10px;\\\">\\r\\n<p style=\\\"padding-bottom: 15px; font-size: 20px\\\">Saving your customized social media posts as drafts makes it easier for you to take a break whenever you like and continue working at a later time. Or, use your drafts for repurposing your social media posts.<\\/p>\\r\\n<a href=\'https:\\/\\/www.blog2social.com\\/en\\/faq\\/index.php?action=artikel&cat=4&id=155&artlang=en\' target=\'_blank\' class=\'btn btn-primary\' title=\'save social media posts as drafts\'>learn more<\\/a>\\r\\n<\\/div><\\/div>\"},{\"title\":\"A look into the tool with Jerry Banfield on YouTube\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2019\\/07\\/jerry-banfield-widget.png\' style=\'width: 200px;height: 200px;\' alt=\'A look into the tool with Jerry Banfield on YouTube\'>\\r\\n<div style=\\\"margin: 0 0 5px 10px;\\\">\\r\\n<p style=\\\"padding-bottom: 10px; font-size: 16px\\\">Jerry Banfield uploaded a 24-minute video with a look into the tool and explanation of all features of Blog2Social on YouTube:<\\/p>\\r\\n<p style=\\\"padding-bottom: 10px; font-size: 16px; font-style: italic\\\">\\u201eThis is giving me thousands of extra impressions on my blog every day. This is why I\\u2019ve taken the time to show this to you. \\u2013 I just signed up for this. I found it really helpful. So I thought to make a tutorial.\\u201c<\\/p>\\r\\n<p style=\\\"padding-bottom: 10px; font-size: 16px\\\">Watch the video: <a href=\\\"https:\\/\\/youtu.be\\/TknuQYnmHEU\\\" target=\\\"_blank\\\" title=\\\"Watch the video\\\">\\u00bb https:\\/\\/youtu.be\\/TknuQYnmHEU<\\/a><\\/p>\\r\\n<p style=\\\"padding-bottom: 10px; font-size: 16px\\\">More of Jerry\\u2019s social media strategy: <a href=\\\"https:\\/\\/youtu.be\\/pJuGiJymBEY\\\" target=\\\"_blank\\\" title=\\\"Watch the video\\\">\\u00bb https:\\/\\/youtu.be\\/pJuGiJymBEY<\\/a><\\/p>\\r\\n<\\/div><\\/div>\"},{\"title\":\"New in: The Blog2Social Troubleshooting-Tool\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2019\\/04\\/troubleshooting-en.png\' style=\'width: 200px;height: 200px;\' alt=\'Troubleshooting\'>\\r\\n<div style=\\\"margin: 0 0 5px 10px;\\\">\\r\\n<p style=\\\"padding-bottom: 15px; font-size: 16px\\\">With every new feature, new questions arise. The Troubleshooting-Tool assists you with all questions regarding the system requirements to run Blog2Social. Our FAQ will show you how to use the troubleshooting tool.<\\/p>\\r\\n<a href=\'https:\\/\\/www.blog2social.com\\/en\\/faq\\/index.php?action=artikel&cat=9&id=147&artlang=en\' target=\'_blank\' class=\'btn btn-primary\' title=\\\"\\\">learn more<\\/a>\\r\\n<\\/div><\\/div>\"},{\"title\":\"Business Blogs: Why Blogs are Important for Your Business\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2019\\/04\\/business-blogs-why-blogs-are-important-for-your-business.png\' style=\'width: 200px;height: 200px;\' alt=\'Business Blogs: Why Blogs are Important for Your Business\'>\\r\\n<div style=\\\"margin: 0 0 5px 10px;\\\">\\r\\n<p style=\\\"padding-bottom: 15px; font-size: 16px\\\">Business blogs have become an integral part of corporate social media communication. A successful corporate blog can increase a company\\u2019s reach and visibility in search engines, strengthen its reputation, and serve as a content center for internal and external communication.<\\/p>\\r\\n<a href=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/business-blogs-important\\/\' target=\'_blank\' class=\'btn btn-primary\' title=\\\"\\\">read blogpost<\\/a>\\r\\n<\\/div><\\/div>\"},{\"title\":\"Ultimate Guide: How to Write, Optimize and Promote Blog Posts\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2019\\/04\\/ultimate-guide-write-optimize-and-promote-blog-posts.png\' style=\'width: 200px;height: 200px;\' alt=\'Ultimate Guide: How to Write, Optimize and Promote Blog Posts + Checklist\'>\\r\\n<div style=\\\"margin: 0 0 5px 10px;\\\">\\r\\n<p style=\\\"padding-bottom: 15px; font-size: 16px\\\">Creating unique content that stands out from millions of other reading options has become more difficult than ever. Just take a look at the internet life stats on the millions of blog posts that are published per day. Every blogger has to work hard to get to the top of the ever-growing blogosphere.<\\/p>\\r\\n<a href=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/ultimate-guide-write-optimize-promote-blog-posts-checklist\\/\' target=\'_blank\' class=\'btn btn-primary\' title=\\\"\\\">read blogpost<\\/a>\\r\\n<\\/div><\\/div>\"},{\"title\":\"How to Use the Power of Visuals to Drive More Traffic to Your Blog\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2019\\/04\\/how-to-use-the-power-of-visuals-to-drive-more-traffic-to-your-blog.png\' style=\'width: 200px;height: 200px;\' alt=\'How to Use the Power of Visuals to Drive More Traffic to Your Blog\'>\\r\\n<div style=\\\"margin: 0 0 5px 10px;\\\">\\r\\n<p style=\\\"padding-bottom: 5px; font-size: 22px;\\\">7 tips and tools for your blog marketing with visual content<\\/p>\\r\\n<p style=\\\"padding-bottom: 15px; font-size: 16px;\\\">Visuals can have an enormous impact on the success of your blog and your social media campaigns. Photos, videos or infographics grab user attention much faster and more effectively than text.<\\/p>\\r\\n<a href=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/visuals-more-traffic-on-your-blog\\/\' target=\'_blank\' class=\'btn btn-primary\' title=\\\"\\\">read blogpost<\\/a>\\r\\n<\\/div><\\/div>\"},{\"title\":\"How to connect and automatically post to Facebook groups\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2019\\/01\\/automated-posting-to-facebook-groups-e1547799312116.png\' style=\'width: 200px;height: 200px;\'>\\r\\n\\r\\n<div style=\\\";margin-left: 10px;\\\">\\r\\n<span style=\'font-size:18px;margin-bottom: 5px; margin-top: 5px\'>\\r\\nSchedule and publish your blog posts automatically in Facebook groups. There you will find active communities for your topics.\\r\\n<br><br>\\r\\n<\\/span>\\r\\n\\r\\n<a href=\'https:\\/\\/www.blog2social.com\\/en\\/faq\\/index.php?action=artikel&lang=en&cat=3&id=143&artlang=en\' target=\'_blank\' class=\'btn btn-primary\'>learn more<\\/a><\\/div><\\/div>\"},{\"title\":\"10 Tips & Tools to Promote your Blog\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/wp-content\\/uploads\\/2017\\/11\\/Preisanpassung-9.png\' style=\'width: 200px; height: 200px\'>\\r\\n<div>\\r\\n<span style=\'font-size: 12px\'>\\r\\n<div style=\\\";margin-left: 10px;\\\">\\r\\n<span class=\\\"glyphicon glyphicon-bookmark glyphicon-success\\\">\\r\\n<\\/span> Posts from Blog2Social<\\/span><\\/div>\\r\\n<br>\\r\\n<div style=\\\";margin-left: 10px;\\\">\\r\\n<span style=\'font-size:16px;margin-bottom: 5px; margin-top: 5px\'>\\r\\nLooking for tips and tools to promote your blog post? Here are 10 things you can do to <strong>promote your blog post<\\/strong>, after hitting the publish-button. Drive readers to your blog and increase the performance of your post!\\r\\n<br>\\r\\n<br>\\r\\n<a href=\'https:\\/\\/www.blog2social.com\\/en\\/blog\\/10-tips-tools-promote-blog-post-checklist\\/\' target=\'_blank\' class=\'btn btn-primary\'>read more<\\/a>\\r\\n<\\/div><\\/div>\"},{\"title\":\"Recommend Blog2Social and Earn Cash\",\"content\":\"<div class=\'b2s-widget-block-image-content\'><img src=\'https:\\/\\/files.blog2social.com\\/wp\\/images\\/en\\/affiliate.png\' style=\'width: 200px; height: 200px\'><div><span style=\'color:#79B232;display:inline-block;width:45px;text-align:right;\'>15%<\\/span> commission per sale<br><span style=\'color:#79B232;display:inline-block;width:45px;text-align:right;\'>5%<\\/span> lifetime comission<br><span style=\'font-size:16px;margin-bottom: 5px;\'>Sign up now in less than 5 minutes and get paid for each referral.<br><br><\\/span><a href=\'https:\\/\\/www.blog2social.com\\/en\\/affiliate\\/\' target=\'_blank\' class=\'btn btn-primary\'>learn more<\\/a><\\/div><\\/div>\"}]\";}', 'no'),
(1179, 'B2S_PLUGIN_DEACTIVATE_SCHED_POST', '1', 'no'),
(1184, 'mozedia-social-sharing-facebook', '1', 'yes'),
(1185, 'mozedia-social-sharing-twitter', '1', 'yes'),
(1186, 'mozedia-social-sharing-twitter-name', '', 'yes'),
(1187, 'mozedia-social-sharing-pinterest', '1', 'yes'),
(1188, 'mozedia-social-sharing-linkedin', '1', 'yes'),
(1189, 'mozedia-social-sharing-whatsapp', '', 'yes'),
(1190, 'mozedia-social-sharing-rel-nofollow', '1', 'yes'),
(1191, 'mozedia-social-sharing-custom-label', '', 'yes'),
(1192, 'mozedia-social-sharing-email', '', 'yes'),
(1193, 'mozedia-social-sharing-post-page-global', '1', 'yes'),
(1200, 'social_share_button_subscribe_redirect_status', 'off', 'yes'),
(1201, 'wpsocialarrow-enable-post', '1', 'yes'),
(1202, 'wpsocialarrow-enable-plugin', '1', 'yes'),
(1203, 'wpsocialarrow-positioning', 'afterpost', 'yes'),
(1204, 'wpsocialarrow-skins', 'default-skin', 'yes'),
(1207, 'wpsocialarrow-enable-page', '', 'yes'),
(1208, 'wpsocialarrow-enable-home', '', 'yes'),
(1209, 'wpsocialarrow-message-selection', 'None', 'yes'),
(1210, 'wpsocialarrow-custom-message', '', 'yes'),
(1211, 'wpsocialarrow-alignment', 'alignleft', 'yes'),
(1212, 'wpsocialarrow-facebook', 'wpsocialarrow-facebook', 'yes'),
(1213, 'wpsocialarrow-twitter', 'wpsocialarrow-twitter', 'yes'),
(1214, 'wpsocialarrow-linkedin', 'wpsocialarrow-linkedin', 'yes'),
(1215, 'wpsocialarrow-pinterest', '', 'yes'),
(1216, 'wpsocialarrow-whatsapp', '', 'yes'),
(1217, 'wpsocialarrow-reddit', '', 'yes'),
(1218, 'wpsocialarrow-telegram', '', 'yes'),
(1219, 'wpsocialarrow-tumbler', '', 'yes'),
(1220, 'wpsocialarrow-vkontakte', '', 'yes'),
(1221, 'wpsocialarrow-wechat', '', 'yes'),
(1222, 'wpsocialarrow-email', '', 'yes'),
(1223, 'wpsocialarrow-line', '', 'yes'),
(1224, 'wpsocialarrow-gfonts', '1', 'yes'),
(1225, 'wpsocialarrow-custom-order', 'soc-facebook,soc-twitter,soc-linkedin', 'yes'),
(1226, 'wpsocialarrow-beforepost', '', 'yes'),
(1227, 'wpsocialarrow-floatingleft', '', 'yes'),
(1228, 'wpsocialarrow-floatingright', '', 'yes'),
(1233, 'socialsnap_version', '1.1.6', 'yes'),
(1234, 'socialsnap_activated', 'a:1:{s:4:\"lite\";i:1573129844;}', 'yes'),
(1237, 'widget_socialsnap-social-followers-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1238, 'widget_socialsnap-ctt-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1239, 'socialsnap_settings', 'a:59:{s:24:\"ss_social_share_networks\";a:4:{s:8:\"facebook\";a:3:{s:4:\"text\";s:0:\"\";s:18:\"desktop_visibility\";s:2:\"on\";s:17:\"mobile_visibility\";s:2:\"on\";}s:7:\"twitter\";a:3:{s:4:\"text\";s:0:\"\";s:18:\"desktop_visibility\";s:2:\"on\";s:17:\"mobile_visibility\";s:2:\"on\";}s:8:\"linkedin\";a:3:{s:4:\"text\";s:0:\"\";s:18:\"desktop_visibility\";s:2:\"on\";s:17:\"mobile_visibility\";s:2:\"on\";}s:5:\"order\";s:25:\"facebook;twitter;linkedin\";}s:29:\"ss_ss_facebook_count_provider\";s:9:\"authorize\";s:31:\"ss_ss_facebook_shared_count_api\";s:0:\"\";s:27:\"ss_ss_facebook_access_token\";s:0:\"\";s:21:\"ss_ss_sidebar_enabled\";s:2:\"on\";s:22:\"ss_ss_sidebar_position\";s:4:\"left\";s:26:\"ss_ss_sidebar_button_shape\";s:7:\"rounded\";s:25:\"ss_ss_sidebar_button_size\";s:7:\"regular\";s:24:\"ss_ss_sidebar_post_types\";a:5:{s:4:\"home\";s:2:\"on\";s:4:\"post\";s:2:\"on\";s:4:\"blog\";s:0:\"\";s:4:\"page\";s:0:\"\";s:6:\"banner\";s:0:\"\";}s:26:\"ss_ss_sidebar_all_networks\";s:2:\"on\";s:27:\"ss_ss_sidebar_label_tooltip\";s:2:\"on\";s:28:\"ss_ss_inline_content_enabled\";s:2:\"on\";s:29:\"ss_ss_inline_content_location\";s:5:\"below\";s:29:\"ss_ss_inline_content_position\";s:4:\"left\";s:32:\"ss_ss_inline_content_share_label\";s:10:\"Share via:\";s:33:\"ss_ss_inline_content_button_shape\";s:7:\"rounded\";s:32:\"ss_ss_inline_content_button_size\";s:5:\"small\";s:33:\"ss_ss_inline_content_button_label\";s:5:\"label\";s:31:\"ss_ss_inline_content_post_types\";a:5:{s:4:\"post\";s:2:\"on\";s:4:\"home\";s:0:\"\";s:4:\"blog\";s:0:\"\";s:4:\"page\";s:0:\"\";s:6:\"banner\";s:0:\"\";}s:35:\"ss_ss_inline_content_button_spacing\";s:2:\"on\";s:33:\"ss_ss_inline_content_all_networks\";s:2:\"on\";s:39:\"ss_ss_inline_content_all_networks_label\";s:4:\"More\";s:19:\"ss_ss_on_media_type\";s:6:\"pin_it\";s:27:\"ss_ss_on_media_button_shape\";s:6:\"circle\";s:26:\"ss_ss_on_media_button_size\";s:7:\"regular\";s:20:\"ss_ss_on_media_hover\";s:6:\"always\";s:23:\"ss_ss_on_media_position\";s:8:\"top-left\";s:23:\"ss_ss_on_media_minwidth\";s:3:\"250\";s:24:\"ss_ss_on_media_minheight\";s:3:\"250\";s:25:\"ss_ss_on_media_post_types\";a:5:{s:4:\"post\";s:2:\"on\";s:4:\"page\";s:2:\"on\";s:4:\"home\";s:0:\"\";s:4:\"blog\";s:0:\"\";s:6:\"banner\";s:0:\"\";}s:29:\"ss_ss_on_media_button_spacing\";s:2:\"on\";s:33:\"ss_social_follow_connect_networks\";a:7:{s:5:\"order\";s:47:\"facebook;twitter;pinterest;instagram;tumblr;mix\";s:8:\"facebook\";a:3:{s:7:\"profile\";a:2:{s:8:\"username\";s:0:\"\";s:3:\"url\";s:0:\"\";}s:5:\"label\";s:21:\"Follow us on Facebook\";s:16:\"manual_followers\";s:0:\"\";}s:7:\"twitter\";a:3:{s:7:\"profile\";a:2:{s:8:\"username\";s:0:\"\";s:3:\"url\";s:0:\"\";}s:5:\"label\";s:20:\"Follow us on Twitter\";s:16:\"manual_followers\";s:0:\"\";}s:9:\"pinterest\";a:2:{s:7:\"profile\";a:2:{s:8:\"username\";s:0:\"\";s:3:\"url\";s:0:\"\";}s:5:\"label\";s:22:\"Follow us on Pinterest\";}s:9:\"instagram\";a:3:{s:7:\"profile\";a:2:{s:8:\"username\";s:0:\"\";s:3:\"url\";s:0:\"\";}s:5:\"label\";s:22:\"Follow us on Instagram\";s:16:\"manual_followers\";s:0:\"\";}s:6:\"tumblr\";a:3:{s:7:\"profile\";a:2:{s:8:\"username\";s:0:\"\";s:3:\"url\";s:0:\"\";}s:5:\"label\";s:19:\"Follow us on Tumblr\";s:16:\"manual_followers\";s:0:\"\";}s:3:\"mix\";a:3:{s:7:\"profile\";a:2:{s:8:\"username\";s:0:\"\";s:3:\"url\";s:0:\"\";}s:5:\"label\";s:16:\"Follow us on Mix\";s:16:\"manual_followers\";s:0:\"\";}}s:17:\"ss_sf_button_size\";s:7:\"regular\";s:20:\"ss_sf_button_columns\";s:1:\"1\";s:22:\"ss_sf_button_followers\";s:2:\"on\";s:19:\"ss_sf_button_labels\";s:2:\"on\";s:19:\"ss_sf_button_scheme\";s:7:\"default\";s:18:\"ss_ctt_include_via\";s:2:\"on\";s:19:\"ss_ctt_include_link\";s:2:\"on\";s:14:\"ss_ctt_related\";a:2:{i:0;a:2:{s:8:\"username\";s:0:\"\";s:4:\"desc\";s:0:\"\";}i:1;a:2:{s:8:\"username\";s:0:\"\";s:4:\"desc\";s:0:\"\";}}s:20:\"ss_ctt_preview_style\";s:1:\"1\";s:19:\"ss_twitter_username\";s:0:\"\";s:28:\"ss_ss_sidebar_button_spacing\";s:0:\"\";s:25:\"ss_ss_sidebar_total_count\";s:0:\"\";s:25:\"ss_ss_sidebar_share_count\";s:0:\"\";s:28:\"ss_ss_sidebar_hide_on_mobile\";s:0:\"\";s:33:\"ss_ss_inline_content_full_content\";s:0:\"\";s:35:\"ss_ss_inline_content_hide_on_mobile\";s:0:\"\";s:32:\"ss_ss_inline_content_total_count\";s:0:\"\";s:22:\"ss_ss_on_media_enabled\";s:0:\"\";s:29:\"ss_ss_on_media_hide_on_mobile\";s:0:\"\";s:20:\"ss_sf_button_spacing\";s:0:\"\";s:21:\"ss_sf_button_vertical\";s:0:\"\";s:21:\"ss_sf_total_followers\";s:0:\"\";s:18:\"ss_ctt_hide_mobile\";s:0:\"\";s:17:\"ss_remove_notices\";s:0:\"\";s:19:\"ss_uninstall_delete\";s:0:\"\";s:17:\"ss_remove_cookies\";s:0:\"\";s:19:\"ss_remove_user_data\";s:0:\"\";}', 'yes'),
(1240, 'socialsnap_rating', 'a:2:{s:4:\"time\";i:1573129845;s:9:\"dismissed\";b:0;}', 'yes'),
(1246, 'hustle_30_migrated', '1', 'yes'),
(1247, 'wdev-frash', 'a:3:{s:7:\"plugins\";a:1:{s:27:\"wordpress-popup/popover.php\";i:1573130217;}s:5:\"queue\";a:2:{s:32:\"bba87391710944d67f24d8c1729a5356\";a:3:{s:6:\"plugin\";s:27:\"wordpress-popup/popover.php\";s:4:\"type\";s:5:\"email\";s:7:\"show_at\";i:1573130217;}s:32:\"c6788b5eb6b55e757c7822ca974a16bb\";a:3:{s:6:\"plugin\";s:27:\"wordpress-popup/popover.php\";s:4:\"type\";s:4:\"rate\";s:7:\"show_at\";i:1573735017;}}s:4:\"done\";a:0:{}}', 'no'),
(1248, 'hustle_activated_flag', '1', 'yes'),
(1249, 'hustle_database_version', '4.0', 'yes'),
(1250, 'widget_hustle_module_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1251, 'widget_inc_opt_widget', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:9:\"module_id\";s:1:\"1\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(1253, 'hustle_ss_refresh_counters', '1573142665', 'yes'),
(1262, 'MBFREE_CREATED', '1573143118', 'yes'),
(1263, 'MBFREE_HOMEURL', 'http://localhost/WpLearning_03', 'yes'),
(1264, 'maxbuttons_user_level', 'manage_options', 'yes'),
(1267, 'maxbuttons_version', '7.13.3', 'yes'),
(1270, 'wti_like_post_drop_settings_table', '0', 'yes'),
(1271, 'wti_like_post_voting_period', 'once', 'yes'),
(1272, 'wti_like_post_voting_style', 'style3', 'yes'),
(1273, 'wti_like_post_alignment', 'left', 'yes'),
(1274, 'wti_like_post_position', 'bottom', 'yes'),
(1275, 'wti_like_post_login_required', '1', 'yes'),
(1276, 'wti_like_post_login_message', 'Please login to vote.', 'yes'),
(1277, 'wti_like_post_thank_message', 'Thanks for your vote.', 'yes'),
(1278, 'wti_like_post_voted_message', 'You have already voted.', 'yes'),
(1279, 'wti_like_post_allowed_posts', '', 'yes'),
(1280, 'wti_like_post_excluded_posts', '', 'yes'),
(1281, 'wti_like_post_excluded_categories', '', 'yes'),
(1282, 'wti_like_post_excluded_sections', '', 'yes'),
(1283, 'wti_like_post_show_on_pages', '0', 'yes'),
(1284, 'wti_like_post_show_on_widget', '1', 'yes'),
(1285, 'wti_like_post_show_symbols', '0', 'yes'),
(1286, 'wti_like_post_show_dislike', '0', 'yes'),
(1287, 'wti_like_post_title_text', 'Like', 'yes'),
(1288, 'wti_like_post_db_version', '1.4.4', 'yes'),
(1289, 'widget_mostlikedpostswidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1290, 'widget_recentlylikedpostswidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2463, 'widget_likebtnlikebuttonmostlikedwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2464, 'widget_likebtnlikebuttonmostlikedbyuserwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2719, 'wp_ulike_general', 'a:14:{s:11:\"button_type\";s:4:\"text\";s:11:\"button_text\";s:4:\"Like\";s:13:\"button_text_u\";s:5:\"Liked\";s:10:\"button_url\";i:0;s:12:\"button_url_u\";i:0;s:15:\"permission_text\";s:33:\"You have not permission to unlike\";s:10:\"login_type\";s:6:\"button\";s:10:\"login_text\";s:36:\"You Should Login To Submit Your Like\";s:12:\"plugin_files\";s:128:\"{\"single\":1,\"home\":0,\"page\":0,\"archive\":0,\"category\":0,\"search\":0,\"tag\":0,\"author\":0,\"buddypress\":0,\"bbpress\":0,\"woocommerce\":0}\";s:13:\"format_number\";i:0;s:13:\"notifications\";i:1;s:9:\"anonymise\";i:0;s:11:\"like_notice\";s:23:\"Thanks! You Liked This.\";s:13:\"unlike_notice\";s:24:\"Sorry! You unliked this.\";}', 'yes'),
(2720, 'wp_ulike_posts', 'a:11:{s:5:\"theme\";s:15:\"wpulike-default\";s:12:\"auto_display\";i:1;s:21:\"auto_display_position\";s:6:\"bottom\";s:20:\"google_rich_snippets\";i:0;s:21:\"only_registered_users\";i:0;s:14:\"logging_method\";s:11:\"by_username\";s:15:\"users_liked_box\";i:1;s:23:\"disable_likers_pophover\";i:0;s:27:\"users_liked_box_avatar_size\";d:64;s:15:\"number_of_users\";d:10;s:24:\"users_liked_box_template\";s:149:\"<div class=\"wp-ulike-likers-list\">%START_WHILE%<span class=\"wp-ulike-liker\"><a href=\"#\" title=\"%USER_NAME%\">%USER_AVATAR%</a></span>%END_WHILE%</div>\";}', 'yes'),
(2721, 'wp_ulike_comments', 'a:10:{s:5:\"theme\";s:13:\"wpulike-heart\";s:12:\"auto_display\";i:1;s:21:\"auto_display_position\";s:6:\"bottom\";s:21:\"only_registered_users\";i:0;s:14:\"logging_method\";s:11:\"by_username\";s:15:\"users_liked_box\";i:1;s:23:\"disable_likers_pophover\";i:0;s:27:\"users_liked_box_avatar_size\";d:64;s:15:\"number_of_users\";d:10;s:24:\"users_liked_box_template\";s:149:\"<div class=\"wp-ulike-likers-list\">%START_WHILE%<span class=\"wp-ulike-liker\"><a href=\"#\" title=\"%USER_NAME%\">%USER_AVATAR%</a></span>%END_WHILE%</div>\";}', 'yes'),
(2722, 'wp_ulike_buddypress', 'a:0:{}', 'yes'),
(2723, 'wp_ulike_bbpress', 'a:0:{}', 'yes'),
(2724, 'wp_ulike_customize', 'a:9:{s:12:\"custom_style\";i:0;s:6:\"btn_bg\";s:0:\"\";s:10:\"btn_border\";s:0:\"\";s:9:\"btn_color\";s:0:\"\";s:10:\"counter_bg\";s:0:\"\";s:14:\"counter_border\";s:0:\"\";s:13:\"counter_color\";s:0:\"\";s:17:\"loading_animation\";i:0;s:10:\"custom_css\";s:0:\"\";}', 'yes'),
(2725, 'widget_wp_ulike', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2732, 'wpulike_lastvisit', '2019-11-08 05:27:23', 'yes'),
(2735, 'wpfblike_db_version', '7', 'yes'),
(2736, 'crudlab_fblb_install', '1573223819', 'yes'),
(2744, 'test_db_version', '', 'yes'),
(2751, 'widget_aio_facebook_like_widget', 'a:2:{i:2;a:10:{s:5:\"title\";s:7:\"Like us\";s:9:\"page_name\";s:11:\"quoteshirts\";s:5:\"width\";s:3:\"240\";s:6:\"height\";s:4:\"auto\";s:8:\"language\";s:5:\"en_US\";s:4:\"tabs\";s:0:\"\";s:10:\"show_faces\";b:1;s:11:\"show_stream\";b:0;s:11:\"show_header\";b:1;s:12:\"small_header\";b:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(2752, 'widget_easy_facebook_like_box', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2755, 'widget_sfp_page_plugin_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2764, 'wti_like_post_lite_notify_author', '0', 'yes'),
(2776, 'pld_settings', 'a:2:{s:14:\"basic_settings\";a:7:{s:10:\"post_types\";a:1:{i:0;s:4:\"post\";}s:21:\"like_dislike_position\";s:5:\"after\";s:20:\"like_dislike_display\";s:4:\"both\";s:26:\"like_dislike_resistriction\";s:2:\"ip\";s:13:\"display_order\";s:12:\"like-dislike\";s:15:\"like_hover_text\";s:4:\"Like\";s:18:\"dislike_hover_text\";s:0:\"\";}s:15:\"design_settings\";a:5:{s:8:\"template\";s:10:\"template-2\";s:9:\"like_icon\";s:0:\"\";s:12:\"dislike_icon\";s:0:\"\";s:10:\"icon_color\";s:7:\"#dd3333\";s:11:\"count_color\";s:0:\"\";}}', 'yes'),
(2779, 'widget_spsmiter_facebook', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2832, 'acf_version', '5.8.7', 'yes'),
(2881, '_transient_timeout_instagram_feed_rating_notice_waiting', '1575488514', 'no'),
(2882, '_transient_instagram_feed_rating_notice_waiting', 'waiting', 'no'),
(2906, 'wpgmza_db_version', '8.0.9', 'yes'),
(2907, 'wpgmza_global_settings', '{\"wpgmza_google_maps_api_key\":\"AIzaSyCQRIIju0It_namVD3g5OcCv9tYdctoL8A\",\"engine\":\"google-maps\",\"google_maps_api_key\":false,\"default_marker_icon\":\"http:\\/\\/localhost\\/WpLearning_03\\/wp-content\\/plugins\\/wp-google-maps\\/images\\/spotlight-poi2.png\",\"developer_mode\":false,\"user_interface_style\":\"default\",\"wpgmza_gdpr_enabled\":1,\"wpgmza_gdpr_default_notice\":\"<p>\\n\\tI agree for my personal data to be processed by <span name=\\\"wpgmza_gdpr_company_name\\\"><\\/span>, for the purpose(s) of <span name=\\\"wpgmza_gdpr_retention_purpose\\\"><\\/span>.\\n<\\/p>\\n\\n<p>\\t\\n\\tI agree for my personal data, provided via map API calls, to be processed by the API provider, for the purposes of geocoding (converting addresses to coordinates), reverse geocoding and\\tgenerating directions.\\n<\\/p>\\n<p>\\n\\tSome visual components of WP Google Maps use 3rd party libraries which are loaded over the network. At present the libraries are Google Maps, Open Street Map, jQuery DataTables and FontAwesome. When loading resources over a network, the 3rd party server will receive your IP address and User Agent string amongst other details. Please refer to the Privacy Policy of the respective libraries for details on how they use data and the process to exercise your rights under the GDPR regulations.\\n<\\/p>\\n<p>\\n\\tWP Google Maps uses jQuery DataTables to display sortable, searchable tables, such as that seen in the Advanced Marker Listing and on the Map Edit Page. jQuery DataTables in certain circumstances uses a cookie to save and later recall the \\\"state\\\" of a given table - that is, the search term, sort column and order and current page. This data is held in local storage and retained until this is cleared manually. No libraries used by WP Google Maps transmit this information.\\n<\\/p>\\n<p>\\n\\tPlease <a href=\\\"https:\\/\\/developers.google.com\\/maps\\/terms\\\">see here<\\/a> and <a href=\\\"https:\\/\\/maps.google.com\\/help\\/terms_maps.html\\\">here<\\/a> for Google\'s terms. Please also see <a href=\\\"https:\\/\\/policies.google.com\\/privacy?hl=en-GB&amp;gl=uk\\\">Google\'s Privacy Policy<\\/a>. We do not send the API provider any personally identifying information, or information that could uniquely identify your device.\\n<\\/p>\\n<p>\\n\\tWhere this notice is displayed in place of a map, agreeing to this notice will store a cookie recording your agreement so you are not prompted again.\\n<\\/p>\",\"wpgmza_gdpr_company_name\":\"My New Wordpress Website\",\"wpgmza_gdpr_retention_purpose\":\"displaying map tiles, geocoding addresses and calculating and display directions.\",\"wpgmza_settings_marker_pull\":\"0\"}', 'yes'),
(2908, 'WPGMZA_OTHER_SETTINGS', 'a:11:{s:26:\"wpgmza_google_maps_api_key\";s:39:\"AIzaSyCQRIIju0It_namVD3g5OcCv9tYdctoL8A\";s:6:\"engine\";s:11:\"google-maps\";s:19:\"google_maps_api_key\";b:0;s:19:\"default_marker_icon\";s:90:\"http://localhost/WpLearning_03/wp-content/plugins/wp-google-maps/images/spotlight-poi2.png\";s:14:\"developer_mode\";b:0;s:20:\"user_interface_style\";s:7:\"default\";s:19:\"wpgmza_gdpr_enabled\";i:1;s:26:\"wpgmza_gdpr_default_notice\";s:1954:\"<p>\n	I agree for my personal data to be processed by <span name=\"wpgmza_gdpr_company_name\"></span>, for the purpose(s) of <span name=\"wpgmza_gdpr_retention_purpose\"></span>.\n</p>\n\n<p>	\n	I agree for my personal data, provided via map API calls, to be processed by the API provider, for the purposes of geocoding (converting addresses to coordinates), reverse geocoding and	generating directions.\n</p>\n<p>\n	Some visual components of WP Google Maps use 3rd party libraries which are loaded over the network. At present the libraries are Google Maps, Open Street Map, jQuery DataTables and FontAwesome. When loading resources over a network, the 3rd party server will receive your IP address and User Agent string amongst other details. Please refer to the Privacy Policy of the respective libraries for details on how they use data and the process to exercise your rights under the GDPR regulations.\n</p>\n<p>\n	WP Google Maps uses jQuery DataTables to display sortable, searchable tables, such as that seen in the Advanced Marker Listing and on the Map Edit Page. jQuery DataTables in certain circumstances uses a cookie to save and later recall the \"state\" of a given table - that is, the search term, sort column and order and current page. This data is held in local storage and retained until this is cleared manually. No libraries used by WP Google Maps transmit this information.\n</p>\n<p>\n	Please <a href=\"https://developers.google.com/maps/terms\">see here</a> and <a href=\"https://maps.google.com/help/terms_maps.html\">here</a> for Google\'s terms. Please also see <a href=\"https://policies.google.com/privacy?hl=en-GB&amp;gl=uk\">Google\'s Privacy Policy</a>. We do not send the API provider any personally identifying information, or information that could uniquely identify your device.\n</p>\n<p>\n	Where this notice is displayed in place of a map, agreeing to this notice will store a cookie recording your agreement so you are not prompted again.\n</p>\";s:24:\"wpgmza_gdpr_company_name\";s:24:\"My New Wordpress Website\";s:29:\"wpgmza_gdpr_retention_purpose\";s:81:\"displaying map tiles, geocoding addresses and calculating and display directions.\";s:27:\"wpgmza_settings_marker_pull\";s:1:\"0\";}', 'yes'),
(2909, 'wpgmza_temp_api', 'AIzaSyDjyYKnTqGG2CAF9nmrfB6zgTBE6oPhMk4', 'yes'),
(2910, 'wpgmza_xml_location', '{uploads_dir}/wp-google-maps/', 'yes'),
(2911, 'wpgmza_xml_url', '{uploads_url}/wp-google-maps/', 'yes'),
(2912, 'wpgmaps_current_version', '8.0.9', 'yes'),
(2913, 'WPGM_V6_FIRST_TIME', '1', 'yes'),
(2914, 'widget_wpgmza_map_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(2915, 'WPGMZA_FIRST_TIME', '8.0.9', 'yes'),
(2918, 'wpgmza_stats', 'a:2:{s:15:\"list_maps_basic\";a:3:{s:5:\"views\";i:3;s:13:\"last_accessed\";s:19:\"2019-11-21 06:13:38\";s:14:\"first_accessed\";s:19:\"2019-11-21 06:13:19\";}s:9:\"dashboard\";a:3:{s:5:\"views\";i:2;s:13:\"last_accessed\";s:19:\"2019-11-21 06:24:05\";s:14:\"first_accessed\";s:19:\"2019-11-21 06:23:30\";}}', 'yes'),
(2919, 'wpgmza_google_maps_api_key', 'AIzaSyCQRIIju0It_namVD3g5OcCv9tYdctoL8A', 'yes'),
(2920, 'WPGMZA_SETTINGS', 'a:10:{s:24:\"map_default_starting_lat\";s:18:\"45.950464398418106\";s:24:\"map_default_starting_lng\";s:19:\"-109.81550500000003\";s:18:\"map_default_height\";s:3:\"400\";s:17:\"map_default_width\";s:3:\"100\";s:16:\"map_default_zoom\";i:2;s:20:\"map_default_max_zoom\";i:1;s:16:\"map_default_type\";i:1;s:21:\"map_default_alignment\";i:2;s:22:\"map_default_width_type\";s:2:\"\\%\";s:23:\"map_default_height_type\";s:2:\"px\";}', 'yes'),
(2929, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.5\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1574342432;s:7:\"version\";s:5:\"5.1.5\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(2933, '_site_transient_timeout_theme_roots', '1575134031', 'no'),
(2934, '_site_transient_theme_roots', 'a:4:{s:7:\"sensive\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(2935, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1575132234;s:7:\"checked\";a:9:{s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.5\";s:25:\"maxbuttons/maxbuttons.php\";s:6:\"7.13.3\";s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";s:3:\"4.7\";s:62:\"simple-google-maps-short-code/simple-google-map-short-code.php\";s:5:\"1.3.2\";s:33:\"instagram-feed/instagram-feed.php\";s:5:\"2.1.2\";s:17:\"top-10/top-10.php\";s:5:\"2.7.0\";s:29:\"share-button/share-button.php\";s:3:\"1.8\";s:31:\"wp-google-maps/wpGoogleMaps.php\";s:5:\"8.0.9\";s:41:\"wp-like-button/crudlab-fb-like-button.php\";s:5:\"1.6.8\";}s:8:\"response\";a:4:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.3\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:30:\"w.org/plugins/mailchimp-for-wp\";s:4:\"slug\";s:16:\"mailchimp-for-wp\";s:6:\"plugin\";s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";s:11:\"new_version\";s:5:\"4.7.2\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/mailchimp-for-wp/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/mailchimp-for-wp.4.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/mailchimp-for-wp/assets/icon-256x256.png?rev=1224577\";s:2:\"1x\";s:69:\"https://ps.w.org/mailchimp-for-wp/assets/icon-128x128.png?rev=1224577\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:71:\"https://ps.w.org/mailchimp-for-wp/assets/banner-772x250.png?rev=1184706\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:33:\"instagram-feed/instagram-feed.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/instagram-feed\";s:4:\"slug\";s:14:\"instagram-feed\";s:6:\"plugin\";s:33:\"instagram-feed/instagram-feed.php\";s:11:\"new_version\";s:5:\"2.1.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/instagram-feed/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/instagram-feed.2.1.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/instagram-feed/assets/icon-128x128.png?rev=2137676\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/instagram-feed/assets/banner-772x250.png?rev=2137676\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.3\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:31:\"wp-google-maps/wpGoogleMaps.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/wp-google-maps\";s:4:\"slug\";s:14:\"wp-google-maps\";s:6:\"plugin\";s:31:\"wp-google-maps/wpGoogleMaps.php\";s:11:\"new_version\";s:6:\"8.0.10\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-google-maps/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/wp-google-maps.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-google-maps/assets/icon-256x256.png?rev=970398\";s:2:\"1x\";s:66:\"https://ps.w.org/wp-google-maps/assets/icon-128x128.png?rev=970398\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/wp-google-maps/assets/banner-772x250.jpg?rev=792293\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:5:{s:25:\"maxbuttons/maxbuttons.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/maxbuttons\";s:4:\"slug\";s:10:\"maxbuttons\";s:6:\"plugin\";s:25:\"maxbuttons/maxbuttons.php\";s:11:\"new_version\";s:6:\"7.13.3\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/maxbuttons/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/maxbuttons.7.13.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/maxbuttons/assets/icon-128x128.png?rev=1378636\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/maxbuttons/assets/banner-772x250.png?rev=1271156\";}s:11:\"banners_rtl\";a:0:{}}s:62:\"simple-google-maps-short-code/simple-google-map-short-code.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:43:\"w.org/plugins/simple-google-maps-short-code\";s:4:\"slug\";s:29:\"simple-google-maps-short-code\";s:6:\"plugin\";s:62:\"simple-google-maps-short-code/simple-google-map-short-code.php\";s:11:\"new_version\";s:5:\"1.3.2\";s:3:\"url\";s:60:\"https://wordpress.org/plugins/simple-google-maps-short-code/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/simple-google-maps-short-code.1.3.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:82:\"https://ps.w.org/simple-google-maps-short-code/assets/icon-256x256.png?rev=1868328\";s:2:\"1x\";s:82:\"https://ps.w.org/simple-google-maps-short-code/assets/icon-128x128.png?rev=1868328\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:85:\"https://ps.w.org/simple-google-maps-short-code/assets/banner-1544x500.png?rev=1868523\";s:2:\"1x\";s:84:\"https://ps.w.org/simple-google-maps-short-code/assets/banner-772x250.png?rev=1868523\";}s:11:\"banners_rtl\";a:0:{}}s:17:\"top-10/top-10.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:20:\"w.org/plugins/top-10\";s:4:\"slug\";s:6:\"top-10\";s:6:\"plugin\";s:17:\"top-10/top-10.php\";s:11:\"new_version\";s:5:\"2.7.0\";s:3:\"url\";s:37:\"https://wordpress.org/plugins/top-10/\";s:7:\"package\";s:49:\"https://downloads.wordpress.org/plugin/top-10.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/top-10/assets/icon-256x256.png?rev=1193839\";s:2:\"1x\";s:59:\"https://ps.w.org/top-10/assets/icon-128x128.png?rev=1193839\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:62:\"https://ps.w.org/top-10/assets/banner-1544x500.png?rev=1245397\";s:2:\"1x\";s:61:\"https://ps.w.org/top-10/assets/banner-772x250.png?rev=1245397\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"share-button/share-button.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/share-button\";s:4:\"slug\";s:12:\"share-button\";s:6:\"plugin\";s:29:\"share-button/share-button.php\";s:11:\"new_version\";s:3:\"1.8\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/share-button/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/share-button.1.8.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/share-button/assets/icon-128x128.png?rev=1776265\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/share-button/assets/banner-772x250.png?rev=1776265\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"wp-like-button/crudlab-fb-like-button.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/wp-like-button\";s:4:\"slug\";s:14:\"wp-like-button\";s:6:\"plugin\";s:41:\"wp-like-button/crudlab-fb-like-button.php\";s:11:\"new_version\";s:5:\"1.6.8\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-like-button/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wp-like-button.1.6.8.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/wp-like-button/assets/icon-128x128.png?rev=2116306\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/wp-like-button/assets/banner-772x250.png?rev=2116306\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 1, '_edit_lock', '1572722719:1'),
(5, 6, '_menu_item_type', 'custom'),
(6, 6, '_menu_item_menu_item_parent', '0'),
(7, 6, '_menu_item_object_id', '6'),
(8, 6, '_menu_item_object', 'custom'),
(9, 6, '_menu_item_target', ''),
(10, 6, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(11, 6, '_menu_item_xfn', ''),
(12, 6, '_menu_item_url', 'http://localhost/WpLearning_03/'),
(13, 6, '_menu_item_orphaned', '1572538146'),
(14, 7, '_menu_item_type', 'post_type'),
(15, 7, '_menu_item_menu_item_parent', '0'),
(16, 7, '_menu_item_object_id', '2'),
(17, 7, '_menu_item_object', 'page'),
(18, 7, '_menu_item_target', ''),
(19, 7, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(20, 7, '_menu_item_xfn', ''),
(21, 7, '_menu_item_url', ''),
(22, 7, '_menu_item_orphaned', '1572538147'),
(23, 8, '_edit_lock', '1574317550:1'),
(24, 10, '_edit_lock', '1574312893:1'),
(25, 12, '_edit_lock', '1572538112:1'),
(26, 14, '_edit_lock', '1572538126:1'),
(27, 16, '_edit_lock', '1572538141:1'),
(91, 25, '_menu_item_type', 'custom'),
(92, 25, '_menu_item_menu_item_parent', '0'),
(93, 25, '_menu_item_object_id', '25'),
(94, 25, '_menu_item_object', 'custom'),
(95, 25, '_menu_item_target', ''),
(96, 25, '_menu_item_classes', 'a:1:{i:0;s:8:\"nav-item\";}'),
(97, 25, '_menu_item_xfn', ''),
(98, 25, '_menu_item_url', 'http://localhost/WpLearning_03/'),
(100, 26, '_menu_item_type', 'post_type'),
(101, 26, '_menu_item_menu_item_parent', '0'),
(102, 26, '_menu_item_object_id', '12'),
(103, 26, '_menu_item_object', 'page'),
(104, 26, '_menu_item_target', ''),
(105, 26, '_menu_item_classes', 'a:1:{i:0;s:8:\"nav-item\";}'),
(106, 26, '_menu_item_xfn', ''),
(107, 26, '_menu_item_url', ''),
(109, 27, '_menu_item_type', 'post_type'),
(110, 27, '_menu_item_menu_item_parent', '30'),
(111, 27, '_menu_item_object_id', '14'),
(112, 27, '_menu_item_object', 'page'),
(113, 27, '_menu_item_target', ''),
(114, 27, '_menu_item_classes', 'a:1:{i:0;s:8:\"nav-item\";}'),
(115, 27, '_menu_item_xfn', ''),
(116, 27, '_menu_item_url', ''),
(118, 28, '_menu_item_type', 'post_type'),
(119, 28, '_menu_item_menu_item_parent', '0'),
(120, 28, '_menu_item_object_id', '10'),
(121, 28, '_menu_item_object', 'page'),
(122, 28, '_menu_item_target', ''),
(123, 28, '_menu_item_classes', 'a:1:{i:0;s:8:\"nav-item\";}'),
(124, 28, '_menu_item_xfn', ''),
(125, 28, '_menu_item_url', ''),
(127, 29, '_menu_item_type', 'post_type'),
(128, 29, '_menu_item_menu_item_parent', '0'),
(129, 29, '_menu_item_object_id', '8'),
(130, 29, '_menu_item_object', 'page'),
(131, 29, '_menu_item_target', ''),
(132, 29, '_menu_item_classes', 'a:1:{i:0;s:8:\"nav-item\";}'),
(133, 29, '_menu_item_xfn', ''),
(134, 29, '_menu_item_url', ''),
(136, 30, '_menu_item_type', 'post_type'),
(137, 30, '_menu_item_menu_item_parent', '0'),
(138, 30, '_menu_item_object_id', '16'),
(139, 30, '_menu_item_object', 'page'),
(140, 30, '_menu_item_target', ''),
(141, 30, '_menu_item_classes', 'a:3:{i:0;s:8:\"nav-item\";i:1;s:7:\"submenu\";i:2;s:8:\"dropdown\";}'),
(142, 30, '_menu_item_xfn', ''),
(143, 30, '_menu_item_url', ''),
(145, 31, '_menu_item_type', 'post_type'),
(146, 31, '_menu_item_menu_item_parent', '30'),
(147, 31, '_menu_item_object_id', '2'),
(148, 31, '_menu_item_object', 'page'),
(149, 31, '_menu_item_target', ''),
(150, 31, '_menu_item_classes', 'a:1:{i:0;s:8:\"nav-item\";}'),
(151, 31, '_menu_item_xfn', ''),
(152, 31, '_menu_item_url', ''),
(154, 32, '_edit_lock', '1572541996:1'),
(155, 32, '_wp_trash_meta_status', 'publish'),
(156, 32, '_wp_trash_meta_time', '1572542033'),
(157, 33, '_edit_lock', '1572542056:1'),
(158, 33, '_wp_trash_meta_status', 'publish'),
(159, 33, '_wp_trash_meta_time', '1572542088'),
(162, 35, '_wp_trash_meta_status', 'publish'),
(163, 35, '_wp_trash_meta_time', '1572547533'),
(168, 38, '_edit_lock', '1572548340:1'),
(169, 38, '_wp_trash_meta_status', 'publish'),
(170, 38, '_wp_trash_meta_time', '1572548367'),
(171, 39, '_wp_trash_meta_status', 'publish'),
(172, 39, '_wp_trash_meta_time', '1572548381'),
(173, 40, '_edit_lock', '1572548502:1'),
(174, 40, '_wp_trash_meta_status', 'publish'),
(175, 40, '_wp_trash_meta_time', '1572548511'),
(176, 41, '_edit_lock', '1572548922:1'),
(177, 41, '_wp_trash_meta_status', 'publish'),
(178, 41, '_wp_trash_meta_time', '1572548927'),
(179, 42, '_wp_trash_meta_status', 'publish'),
(180, 42, '_wp_trash_meta_time', '1572549060'),
(181, 43, '_edit_lock', '1572549272:1'),
(182, 43, '_wp_trash_meta_status', 'publish'),
(183, 43, '_wp_trash_meta_time', '1572549298'),
(184, 44, '_wp_trash_meta_status', 'publish'),
(185, 44, '_wp_trash_meta_time', '1572549322'),
(186, 45, '_wp_trash_meta_status', 'publish'),
(187, 45, '_wp_trash_meta_time', '1572549335'),
(188, 46, '_wp_trash_meta_status', 'publish'),
(189, 46, '_wp_trash_meta_time', '1572549394'),
(190, 47, '_edit_lock', '1572549452:1'),
(191, 47, '_wp_trash_meta_status', 'publish'),
(192, 47, '_wp_trash_meta_time', '1572549466'),
(193, 48, '_edit_lock', '1572549632:1'),
(194, 48, '_wp_trash_meta_status', 'publish'),
(195, 48, '_wp_trash_meta_time', '1572549660'),
(234, 54, '_wp_trash_meta_status', 'publish'),
(235, 54, '_wp_trash_meta_time', '1572550508'),
(236, 55, '_wp_trash_meta_status', 'publish'),
(237, 55, '_wp_trash_meta_time', '1572550527'),
(238, 56, '_wp_trash_meta_status', 'publish'),
(239, 56, '_wp_trash_meta_time', '1572550560'),
(242, 58, '_wp_trash_meta_status', 'publish'),
(243, 58, '_wp_trash_meta_time', '1572551183'),
(244, 59, '_menu_item_type', 'custom'),
(245, 59, '_menu_item_menu_item_parent', '0'),
(246, 59, '_menu_item_object_id', '59'),
(247, 59, '_menu_item_object', 'custom'),
(248, 59, '_menu_item_target', ''),
(249, 59, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(250, 59, '_menu_item_xfn', ''),
(251, 59, '_menu_item_url', 'http://www.facebook.com'),
(252, 59, '_menu_item_orphaned', '1572551572'),
(253, 60, '_menu_item_type', 'custom'),
(254, 60, '_menu_item_menu_item_parent', '0'),
(255, 60, '_menu_item_object_id', '60'),
(256, 60, '_menu_item_object', 'custom'),
(257, 60, '_menu_item_target', ''),
(258, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(259, 60, '_menu_item_xfn', ''),
(260, 60, '_menu_item_url', 'http://www.facebook.com'),
(261, 60, '_menu_item_orphaned', '1572551703'),
(298, 65, '_edit_lock', '1572552140:1'),
(299, 66, '_menu_item_type', 'custom'),
(300, 66, '_menu_item_menu_item_parent', '0'),
(301, 66, '_menu_item_object_id', '66'),
(302, 66, '_menu_item_object', 'custom'),
(303, 66, '_menu_item_target', '_blank'),
(304, 66, '_menu_item_classes', 'a:1:{i:0;s:11:\"ti-facebook\";}'),
(305, 66, '_menu_item_xfn', ''),
(306, 66, '_menu_item_url', 'https://www.facebook.com/'),
(307, 67, '_menu_item_type', 'custom'),
(308, 67, '_menu_item_menu_item_parent', '0'),
(309, 67, '_menu_item_object_id', '67'),
(310, 67, '_menu_item_object', 'custom'),
(311, 67, '_menu_item_target', '_blank'),
(312, 67, '_menu_item_classes', 'a:1:{i:0;s:14:\"ti-twitter-alt\";}'),
(313, 67, '_menu_item_xfn', ''),
(314, 67, '_menu_item_url', 'https://twitter.com/'),
(315, 68, '_menu_item_type', 'custom'),
(316, 68, '_menu_item_menu_item_parent', '0'),
(317, 68, '_menu_item_object_id', '68'),
(318, 68, '_menu_item_object', 'custom'),
(319, 68, '_menu_item_target', '_blank'),
(320, 68, '_menu_item_classes', 'a:1:{i:0;s:12:\"ti-instagram\";}'),
(321, 68, '_menu_item_xfn', ''),
(322, 68, '_menu_item_url', 'https://www.instagram.com/'),
(323, 69, '_menu_item_type', 'custom'),
(324, 69, '_menu_item_menu_item_parent', '0'),
(325, 69, '_menu_item_object_id', '69'),
(326, 69, '_menu_item_object', 'custom'),
(327, 69, '_menu_item_target', '_blank'),
(328, 69, '_menu_item_classes', 'a:1:{i:0;s:8:\"ti-skype\";}'),
(329, 69, '_menu_item_xfn', ''),
(330, 69, '_menu_item_url', 'https://www.skype.com/en/'),
(331, 65, '_wp_trash_meta_status', 'publish'),
(332, 65, '_wp_trash_meta_time', '1572552149'),
(333, 70, '_wp_trash_meta_status', 'publish'),
(334, 70, '_wp_trash_meta_time', '1572552171'),
(335, 71, '_wp_trash_meta_status', 'publish'),
(336, 71, '_wp_trash_meta_time', '1572552199'),
(337, 72, '_wp_trash_meta_status', 'publish'),
(338, 72, '_wp_trash_meta_time', '1572552218'),
(341, 74, '_wp_attached_file', '2019/11/logo.png'),
(342, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:122;s:6:\"height\";i:26;s:4:\"file\";s:16:\"2019/11/logo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(343, 75, '_wp_attached_file', '2019/11/cropped-logo.png'),
(344, 75, '_wp_attachment_context', 'custom-logo'),
(345, 75, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:122;s:6:\"height\";i:26;s:4:\"file\";s:24:\"2019/11/cropped-logo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(346, 76, '_edit_lock', '1572638114:1'),
(347, 76, '_wp_trash_meta_status', 'publish'),
(348, 76, '_wp_trash_meta_time', '1572638115'),
(349, 78, '_edit_last', '1'),
(350, 78, '_edit_lock', '1572959031:1'),
(351, 79, '_wp_attached_file', '2019/11/hero-banner.png'),
(352, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1140;s:6:\"height\";i:550;s:4:\"file\";s:23:\"2019/11/hero-banner.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"hero-banner-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"hero-banner-300x145.png\";s:5:\"width\";i:300;s:6:\"height\";i:145;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"hero-banner-768x371.png\";s:5:\"width\";i:768;s:6:\"height\";i:371;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"hero-banner-1024x494.png\";s:5:\"width\";i:1024;s:6:\"height\";i:494;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(353, 80, '_wp_attached_file', '2019/10/blog-slide1.png'),
(354, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:236;s:4:\"file\";s:23:\"2019/10/blog-slide1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"blog-slide1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"blog-slide1-300x197.png\";s:5:\"width\";i:300;s:6:\"height\";i:197;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(355, 81, '_wp_attached_file', '2019/10/blog-slide2.png'),
(356, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:236;s:4:\"file\";s:23:\"2019/10/blog-slide2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"blog-slide2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"blog-slide2-300x197.png\";s:5:\"width\";i:300;s:6:\"height\";i:197;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(357, 82, '_wp_attached_file', '2019/10/blog-slide3.png'),
(358, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:236;s:4:\"file\";s:23:\"2019/10/blog-slide3.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"blog-slide3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"blog-slide3-300x197.png\";s:5:\"width\";i:300;s:6:\"height\";i:197;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(361, 1, '_thumbnail_id', '82'),
(362, 84, '_edit_lock', '1572723182:1'),
(365, 84, '_thumbnail_id', '81'),
(366, 86, '_edit_lock', '1572722844:1'),
(369, 86, '_thumbnail_id', '80'),
(370, 88, '_edit_lock', '1572781117:1'),
(373, 88, '_thumbnail_id', '79'),
(378, 90, '_edit_lock', '1572960269:1'),
(381, 92, '_wp_attached_file', '2019/11/cat-post-1.jpg'),
(382, 92, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:220;s:4:\"file\";s:22:\"2019/11/cat-post-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"cat-post-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"cat-post-1-300x183.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:183;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(385, 90, '_thumbnail_id', '92'),
(394, 93, 'es_template_type', 'newsletter'),
(395, 94, 'es_template_type', 'post_notification'),
(396, 96, '_mc4wp_settings', 'a:9:{s:15:\"required_fields\";s:5:\"EMAIL\";s:12:\"double_optin\";s:1:\"1\";s:15:\"update_existing\";s:1:\"1\";s:17:\"replace_interests\";s:1:\"1\";s:15:\"subscriber_tags\";s:0:\"\";s:18:\"hide_after_success\";s:1:\"1\";s:8:\"redirect\";s:0:\"\";s:3:\"css\";s:1:\"0\";s:5:\"lists\";a:1:{i:0;s:10:\"080d67d4c6\";}}'),
(397, 96, 'text_subscribed', 'Thank you, your sign-up request was successful! Please check your email inbox to confirm.'),
(398, 96, 'text_invalid_email', 'Please provide a valid email address.'),
(399, 96, 'text_required_field_missing', 'Please fill in the required fields.'),
(400, 96, 'text_already_subscribed', 'Given email address is already subscribed, thank you!'),
(401, 96, 'text_error', 'Oops. Something went wrong. Please try again later.'),
(402, 96, 'text_unsubscribed', 'You were successfully unsubscribed.'),
(403, 96, 'text_not_subscribed', 'Given email address is not subscribed.'),
(404, 96, 'text_no_lists_selected', 'Please select at least one list.'),
(405, 96, 'text_updated', 'You are already signed Up. Please follow your email'),
(406, 97, '_edit_lock', '1574237044:1'),
(407, 98, '_wp_attached_file', '2019/11/blog4.png'),
(408, 98, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:400;s:4:\"file\";s:17:\"2019/11/blog4.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"blog4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"blog4-300x160.png\";s:5:\"width\";i:300;s:6:\"height\";i:160;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(411, 97, '_thumbnail_id', '98'),
(412, 97, '_edit_last', '1'),
(415, 96, '_wp_old_date', '2019-11-04'),
(416, 100, '_edit_last', '1'),
(417, 100, '_edit_lock', '1572959027:1'),
(418, 100, '_wp_trash_meta_status', 'publish'),
(419, 100, '_wp_trash_meta_time', '1572959179'),
(420, 100, '_wp_desired_post_slug', 'single-page-banner'),
(423, 90, '_edit_last', '1'),
(442, 86, 'mashsb_timestamp', '1573124248'),
(443, 86, 'mashsb_shares', '0'),
(444, 86, 'mashsb_jsonshares', '{\"total\":0}'),
(453, 97, 'mbsocial_data', 'a:3:{s:7:\"general\";a:1:{s:4:\"hide\";i:0;}s:7:\"network\";a:1:{s:9:\"share_url\";s:0:\"\";}s:7:\"twitter\";a:2:{s:12:\"twitter_hash\";s:0:\"\";s:14:\"twitter_handle\";s:0:\"\";}}'),
(460, 97, '_pingme', '1'),
(461, 97, '_encloseme', '1'),
(462, 8, '_edit_last', '1'),
(463, 8, 'mbsocial_data', 'a:3:{s:7:\"general\";a:1:{s:4:\"hide\";i:0;}s:7:\"network\";a:1:{s:9:\"share_url\";s:0:\"\";}s:7:\"twitter\";a:2:{s:12:\"twitter_hash\";s:0:\"\";s:14:\"twitter_handle\";s:0:\"\";}}'),
(472, 111, '_form', '<div class=\"row\">\n              <div class=\"col-lg-5\">\n                <div class=\"form-group\">\n                  <input class=\"form-control\" name=\"name\" id=\"name\" type=\"text\" placeholder=\"Enter your name\">\n                </div>\n                <div class=\"form-group\">\n                  <input class=\"form-control\" name=\"email\" id=\"email\" type=\"email\" placeholder=\"Enter email address\">\n                </div>\n                <div class=\"form-group\">\n                  <input class=\"form-control\" name=\"subject\" id=\"subject\" type=\"text\" placeholder=\"Enter Subject\">\n                </div>\n              </div>\n              <div class=\"col-lg-7\">\n                <div class=\"form-group\">\n                    <textarea class=\"form-control different-control w-100\" name=\"message\" id=\"message\" cols=\"30\" rows=\"5\" placeholder=\"Enter Message\"></textarea>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group text-center text-md-right mt-3\">\n              <button type=\"submit\" class=\"button button--active button-contactForm\">Send Message</button>\n            </div>'),
(473, 111, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:41:\"My New Wordpress Website \"[your-subject]\"\";s:6:\"sender\";s:45:\"My New Wordpress Website <nmnaba14@gmail.com>\";s:9:\"recipient\";s:18:\"nmnaba14@gmail.com\";s:4:\"body\";s:193:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on My New Wordpress Website (http://localhost/WpLearning_03)\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(474, 111, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:41:\"My New Wordpress Website \"[your-subject]\"\";s:6:\"sender\";s:45:\"My New Wordpress Website <nmnaba14@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:135:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on My New Wordpress Website (http://localhost/WpLearning_03)\";s:18:\"additional_headers\";s:28:\"Reply-To: nmnaba14@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(475, 111, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(476, 111, '_additional_settings', ''),
(477, 111, '_locale', 'en_US'),
(479, 112, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]'),
(480, 112, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:41:\"My New Wordpress Website \"[your-subject]\"\";s:6:\"sender\";s:45:\"My New Wordpress Website <nmnaba14@gmail.com>\";s:9:\"recipient\";s:18:\"nmnaba14@gmail.com\";s:4:\"body\";s:193:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on My New Wordpress Website (http://localhost/WpLearning_03)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(481, 112, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:41:\"My New Wordpress Website \"[your-subject]\"\";s:6:\"sender\";s:45:\"My New Wordpress Website <nmnaba14@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:135:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on My New Wordpress Website (http://localhost/WpLearning_03)\";s:18:\"additional_headers\";s:28:\"Reply-To: nmnaba14@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(482, 112, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(483, 112, '_additional_settings', ''),
(484, 112, '_locale', 'en_US');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-10-30 18:10:04', '2019-10-30 18:10:04', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-11-02 17:56:54', '2019-11-02 17:56:54', '', 0, 'http://localhost/WpLearning_03/?p=1', 0, 'post', '', 2),
(2, 1, '2019-10-30 18:10:04', '2019-10-30 18:10:04', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost/WpLearning_03/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-10-30 18:10:04', '2019-10-30 18:10:04', '', 0, 'http://localhost/WpLearning_03/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-10-30 18:10:04', '2019-10-30 18:10:04', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/WpLearning_03.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2019-10-30 18:10:04', '2019-10-30 18:10:04', '', 0, 'http://localhost/WpLearning_03/?page_id=3', 0, 'page', '', 0),
(6, 1, '2019-10-31 16:09:05', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-10-31 16:09:05', '0000-00-00 00:00:00', '', 0, 'http://localhost/WpLearning_03/?p=6', 1, 'nav_menu_item', '', 0),
(7, 1, '2019-10-31 16:09:06', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-10-31 16:09:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/WpLearning_03/?p=7', 1, 'nav_menu_item', '', 0),
(8, 1, '2019-10-31 16:09:44', '2019-10-31 16:09:44', '<!-- wp:paragraph -->\n<p><a href=\"http://localhost/WpLearning_03/contact.html\">Contact</a> page</p>\n<!-- /wp:paragraph -->', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2019-11-21 05:18:03', '2019-11-21 05:18:03', '', 0, 'http://localhost/WpLearning_03/?page_id=8', 0, 'page', '', 0),
(9, 1, '2019-10-31 16:09:44', '2019-10-31 16:09:44', '<!-- wp:paragraph -->\n<p><a href=\"http://localhost/WpLearning_03/contact.html\">Contact</a> page</p>\n<!-- /wp:paragraph -->', 'Contact', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2019-10-31 16:09:44', '2019-10-31 16:09:44', '', 8, 'http://localhost/WpLearning_03/2019/10/31/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2019-10-31 16:10:17', '2019-10-31 16:10:17', '<!-- wp:paragraph -->\n<p>category page</p>\n<!-- /wp:paragraph -->', 'category', '', 'publish', 'closed', 'closed', '', 'category', '', '', '2019-10-31 16:10:17', '2019-10-31 16:10:17', '', 0, 'http://localhost/WpLearning_03/?page_id=10', 0, 'page', '', 0),
(11, 1, '2019-10-31 16:10:17', '2019-10-31 16:10:17', '<!-- wp:paragraph -->\n<p>category page</p>\n<!-- /wp:paragraph -->', 'category', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2019-10-31 16:10:17', '2019-10-31 16:10:17', '', 10, 'http://localhost/WpLearning_03/2019/10/31/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2019-10-31 16:10:42', '2019-10-31 16:10:42', '<!-- wp:paragraph -->\n<p>archive page</p>\n<!-- /wp:paragraph -->', 'archive', '', 'publish', 'closed', 'closed', '', 'archive', '', '', '2019-10-31 16:10:42', '2019-10-31 16:10:42', '', 0, 'http://localhost/WpLearning_03/?page_id=12', 0, 'page', '', 0),
(13, 1, '2019-10-31 16:10:42', '2019-10-31 16:10:42', '<!-- wp:paragraph -->\n<p>archive page</p>\n<!-- /wp:paragraph -->', 'archive', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-10-31 16:10:42', '2019-10-31 16:10:42', '', 12, 'http://localhost/WpLearning_03/2019/10/31/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2019-10-31 16:11:08', '2019-10-31 16:11:08', '<!-- wp:paragraph -->\n<p>blog-details page</p>\n<!-- /wp:paragraph -->', 'blog-details', '', 'publish', 'closed', 'closed', '', 'blog-details', '', '', '2019-10-31 16:11:08', '2019-10-31 16:11:08', '', 0, 'http://localhost/WpLearning_03/?page_id=14', 0, 'page', '', 0),
(15, 1, '2019-10-31 16:11:08', '2019-10-31 16:11:08', '<!-- wp:paragraph -->\n<p>blog-details page</p>\n<!-- /wp:paragraph -->', 'blog-details', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2019-10-31 16:11:08', '2019-10-31 16:11:08', '', 14, 'http://localhost/WpLearning_03/2019/10/31/14-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2019-10-31 16:11:19', '2019-10-31 16:11:19', '', 'pages', '', 'publish', 'closed', 'closed', '', 'pages', '', '', '2019-10-31 16:11:19', '2019-10-31 16:11:19', '', 0, 'http://localhost/WpLearning_03/?page_id=16', 0, 'page', '', 0),
(17, 1, '2019-10-31 16:11:19', '2019-10-31 16:11:19', '', 'pages', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2019-10-31 16:11:19', '2019-10-31 16:11:19', '', 16, 'http://localhost/WpLearning_03/2019/10/31/16-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2019-10-31 16:13:23', '2019-10-31 16:13:23', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home-2', '', '', '2019-10-31 19:14:58', '2019-10-31 19:14:58', '', 0, 'http://localhost/WpLearning_03/?p=25', 1, 'nav_menu_item', '', 0),
(26, 1, '2019-10-31 16:13:24', '2019-10-31 16:13:24', ' ', '', '', 'publish', 'closed', 'closed', '', '26', '', '', '2019-10-31 19:15:22', '2019-10-31 19:15:22', '', 0, 'http://localhost/WpLearning_03/?p=26', 2, 'nav_menu_item', '', 0),
(27, 1, '2019-10-31 16:13:25', '2019-10-31 16:13:25', ' ', '', '', 'publish', 'closed', 'closed', '', '27', '', '', '2019-10-31 19:21:00', '2019-10-31 19:21:00', '', 0, 'http://localhost/WpLearning_03/?p=27', 5, 'nav_menu_item', '', 0),
(28, 1, '2019-10-31 16:13:24', '2019-10-31 16:13:24', ' ', '', '', 'publish', 'closed', 'closed', '', '28', '', '', '2019-10-31 19:15:35', '2019-10-31 19:15:35', '', 0, 'http://localhost/WpLearning_03/?p=28', 3, 'nav_menu_item', '', 0),
(29, 1, '2019-10-31 16:13:25', '2019-10-31 16:13:25', ' ', '', '', 'publish', 'closed', 'closed', '', '29', '', '', '2019-10-31 19:16:34', '2019-10-31 19:16:34', '', 0, 'http://localhost/WpLearning_03/?p=29', 7, 'nav_menu_item', '', 0),
(30, 1, '2019-10-31 16:13:24', '2019-10-31 16:13:24', ' ', '', '', 'publish', 'closed', 'closed', '', '30', '', '', '2019-10-31 19:21:00', '2019-10-31 19:21:00', '', 0, 'http://localhost/WpLearning_03/?p=30', 4, 'nav_menu_item', '', 0),
(31, 1, '2019-10-31 16:13:25', '2019-10-31 16:13:25', ' ', '', '', 'publish', 'closed', 'closed', '', '31', '', '', '2019-10-31 19:21:00', '2019-10-31 19:21:00', '', 0, 'http://localhost/WpLearning_03/?p=31', 6, 'nav_menu_item', '', 0),
(32, 1, '2019-10-31 17:13:52', '2019-10-31 17:13:52', '{\n    \"nav_menu_item[25]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 25,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"Home\",\n            \"url\": \"http://localhost/WpLearning_03/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 1,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 17:12:11\"\n    },\n    \"nav_menu_item[26]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 12,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/archive/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 2,\n            \"status\": \"publish\",\n            \"original_title\": \"archive\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 17:13:12\"\n    },\n    \"nav_menu_item[28]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 10,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/category/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 3,\n            \"status\": \"publish\",\n            \"original_title\": \"category\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 17:13:12\"\n    },\n    \"nav_menu_item[30]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 16,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/pages/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item submenu dropdown\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 4,\n            \"status\": \"publish\",\n            \"original_title\": \"pages\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 17:13:12\"\n    },\n    \"nav_menu_item[27]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 14,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/blog-details/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"dropdown-menu\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 5,\n            \"status\": \"publish\",\n            \"original_title\": \"blog-details\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 17:13:52\"\n    },\n    \"nav_menu_item[31]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 2,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/sample-page/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"dropdown-menu\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 6,\n            \"status\": \"publish\",\n            \"original_title\": \"Sample Page\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 17:13:52\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '7fd95408-28b6-46e3-9d9b-0c4536732ae3', '', '', '2019-10-31 17:13:52', '2019-10-31 17:13:52', '', 0, 'http://localhost/WpLearning_03/?p=32', 0, 'customize_changeset', '', 0),
(33, 1, '2019-10-31 17:14:48', '2019-10-31 17:14:48', '{\n    \"nav_menu_item[31]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 2,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/sample-page/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"dropdown-menu nav-item \",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 6,\n            \"status\": \"publish\",\n            \"original_title\": \"Sample Page\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 17:14:48\"\n    },\n    \"nav_menu_item[29]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 8,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/contact/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 7,\n            \"status\": \"publish\",\n            \"original_title\": \"Contact\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 17:14:48\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b471295e-6ec7-4f36-90af-451ddd76c51c', '', '', '2019-10-31 17:14:48', '2019-10-31 17:14:48', '', 0, 'http://localhost/WpLearning_03/?p=33', 0, 'customize_changeset', '', 0),
(35, 1, '2019-10-31 18:45:32', '2019-10-31 18:45:32', '{\n    \"nav_menu_item[27]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 14,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/blog-details/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"dropdown-menu nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 5,\n            \"status\": \"publish\",\n            \"original_title\": \"blog-details\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 18:45:32\"\n    },\n    \"nav_menu_item[31]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 2,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/sample-page/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"dropdown-menu nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 6,\n            \"status\": \"publish\",\n            \"original_title\": \"Sample Page\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 18:45:32\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'fcdac50e-a21f-4f66-8d72-cbbc8a4471a9', '', '', '2019-10-31 18:45:32', '2019-10-31 18:45:32', '', 0, 'http://localhost/WpLearning_03/2019/10/31/fcdac50e-a21f-4f66-8d72-cbbc8a4471a9/', 0, 'customize_changeset', '', 0),
(38, 1, '2019-10-31 18:59:27', '2019-10-31 18:59:27', '{\n    \"nav_menu_item[25]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 25,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"Home\",\n            \"url\": \"http://localhost/WpLearning_03/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item nav-link\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 1,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 18:58:50\"\n    },\n    \"nav_menu_item[30]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 16,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/pages/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item submenu dropdown\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 4,\n            \"status\": \"publish\",\n            \"original_title\": \"pages\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 18:59:27\"\n    },\n    \"nav_menu_item[27]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 14,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/blog-details/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"dropdown-menu nav-item nav-link\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 5,\n            \"status\": \"publish\",\n            \"original_title\": \"blog-details\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 18:58:50\"\n    },\n    \"nav_menu_item[31]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 2,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/sample-page/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"dropdown-menu nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 6,\n            \"status\": \"publish\",\n            \"original_title\": \"Sample Page\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 18:59:27\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'a4b976a3-bdd8-4c6b-b23c-cec05979300d', '', '', '2019-10-31 18:59:27', '2019-10-31 18:59:27', '', 0, 'http://localhost/WpLearning_03/?p=38', 0, 'customize_changeset', '', 0),
(39, 1, '2019-10-31 18:59:40', '2019-10-31 18:59:40', '{\n    \"nav_menu_item[25]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 25,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"Home\",\n            \"url\": \"http://localhost/WpLearning_03/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 1,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 18:59:40\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '3a6682a1-a64a-4fa1-a7e1-de10f7ec1ac6', '', '', '2019-10-31 18:59:40', '2019-10-31 18:59:40', '', 0, 'http://localhost/WpLearning_03/2019/10/31/3a6682a1-a64a-4fa1-a7e1-de10f7ec1ac6/', 0, 'customize_changeset', '', 0),
(40, 1, '2019-10-31 19:01:50', '2019-10-31 19:01:50', '{\n    \"nav_menu_item[27]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 14,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/blog-details/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 5,\n            \"status\": \"publish\",\n            \"original_title\": \"blog-details\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:01:50\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'cf40911b-0b20-49a8-a483-9d4b0395d5a7', '', '', '2019-10-31 19:01:50', '2019-10-31 19:01:50', '', 0, 'http://localhost/WpLearning_03/?p=40', 0, 'customize_changeset', '', 0),
(41, 1, '2019-10-31 19:08:46', '2019-10-31 19:08:46', '{\n    \"nav_menu_item[27]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 14,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/blog-details/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-link\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 5,\n            \"status\": \"publish\",\n            \"original_title\": \"blog-details\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:08:42\"\n    },\n    \"nav_menu_item[31]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 2,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/sample-page/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-link\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 6,\n            \"status\": \"publish\",\n            \"original_title\": \"Sample Page\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:07:42\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'c1bf9b96-7bf5-40e7-b42c-c942b1e4ea89', '', '', '2019-10-31 19:08:46', '2019-10-31 19:08:46', '', 0, 'http://localhost/WpLearning_03/?p=41', 0, 'customize_changeset', '', 0),
(42, 1, '2019-10-31 19:10:59', '2019-10-31 19:10:59', '{\n    \"nav_menu_item[25]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 25,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"Home\",\n            \"url\": \"http://localhost/WpLearning_03/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-link\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 1,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:10:59\"\n    },\n    \"nav_menu_item[26]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 12,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/archive/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-link\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 2,\n            \"status\": \"publish\",\n            \"original_title\": \"archive\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:10:59\"\n    },\n    \"nav_menu_item[28]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 10,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/category/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-link\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 3,\n            \"status\": \"publish\",\n            \"original_title\": \"category\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:10:59\"\n    },\n    \"nav_menu_item[30]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 16,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/pages/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-link submenu dropdown\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 4,\n            \"status\": \"publish\",\n            \"original_title\": \"pages\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:10:59\"\n    },\n    \"nav_menu_item[29]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 8,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/contact/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-link\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 7,\n            \"status\": \"publish\",\n            \"original_title\": \"Contact\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:10:59\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '496d21f6-695c-465f-8aa0-55c6305ad316', '', '', '2019-10-31 19:10:59', '2019-10-31 19:10:59', '', 0, 'http://localhost/WpLearning_03/2019/10/31/496d21f6-695c-465f-8aa0-55c6305ad316/', 0, 'customize_changeset', '', 0),
(43, 1, '2019-10-31 19:14:57', '2019-10-31 19:14:57', '{\n    \"nav_menu_item[30]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 16,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/pages/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item submenu dropdown\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 4,\n            \"status\": \"publish\",\n            \"original_title\": \"pages\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:12:29\"\n    },\n    \"nav_menu_item[25]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 25,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"Home\",\n            \"url\": \"http://localhost/WpLearning_03/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 1,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:14:57\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'e5fbc4d2-9648-4fc8-a8de-7dea7dc1920a', '', '', '2019-10-31 19:14:57', '2019-10-31 19:14:57', '', 0, 'http://localhost/WpLearning_03/?p=43', 0, 'customize_changeset', '', 0),
(44, 1, '2019-10-31 19:15:21', '2019-10-31 19:15:21', '{\n    \"nav_menu_item[26]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 12,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/archive/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 2,\n            \"status\": \"publish\",\n            \"original_title\": \"archive\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:15:21\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '2d571866-4d7a-43d5-a5f1-b969bb6de580', '', '', '2019-10-31 19:15:21', '2019-10-31 19:15:21', '', 0, 'http://localhost/WpLearning_03/2019/10/31/2d571866-4d7a-43d5-a5f1-b969bb6de580/', 0, 'customize_changeset', '', 0),
(45, 1, '2019-10-31 19:15:34', '2019-10-31 19:15:34', '{\n    \"nav_menu_item[28]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 10,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/category/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 3,\n            \"status\": \"publish\",\n            \"original_title\": \"category\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:15:34\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '495445e1-ab3e-4d93-a1b2-5ced8a9a8bc5', '', '', '2019-10-31 19:15:34', '2019-10-31 19:15:34', '', 0, 'http://localhost/WpLearning_03/2019/10/31/495445e1-ab3e-4d93-a1b2-5ced8a9a8bc5/', 0, 'customize_changeset', '', 0),
(46, 1, '2019-10-31 19:16:34', '2019-10-31 19:16:34', '{\n    \"nav_menu_item[29]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 8,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/contact/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 7,\n            \"status\": \"publish\",\n            \"original_title\": \"Contact\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:16:34\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'f5305d81-4dfe-485b-8cba-c179038b2ab9', '', '', '2019-10-31 19:16:34', '2019-10-31 19:16:34', '', 0, 'http://localhost/WpLearning_03/2019/10/31/f5305d81-4dfe-485b-8cba-c179038b2ab9/', 0, 'customize_changeset', '', 0),
(47, 1, '2019-10-31 19:17:46', '2019-10-31 19:17:46', '{\n    \"nav_menu_item[27]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 14,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/blog-details/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 5,\n            \"status\": \"publish\",\n            \"original_title\": \"blog-details\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:17:29\"\n    },\n    \"nav_menu_item[31]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 2,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/sample-page/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 6,\n            \"status\": \"publish\",\n            \"original_title\": \"Sample Page\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:17:46\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'eedb7333-60b0-49f4-b48e-b83462778bd7', '', '', '2019-10-31 19:17:46', '2019-10-31 19:17:46', '', 0, 'http://localhost/WpLearning_03/?p=47', 0, 'customize_changeset', '', 0),
(48, 1, '2019-10-31 19:21:00', '2019-10-31 19:21:00', '{\n    \"nav_menu_item[31]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 2,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/sample-page/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 6,\n            \"status\": \"publish\",\n            \"original_title\": \"Sample Page\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:18:29\"\n    },\n    \"nav_menu_item[27]\": {\n        \"value\": {\n            \"menu_item_parent\": 30,\n            \"object_id\": 14,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/blog-details/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 5,\n            \"status\": \"publish\",\n            \"original_title\": \"blog-details\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:21:00\"\n    },\n    \"nav_menu_item[30]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 16,\n            \"object\": \"page\",\n            \"type\": \"post_type\",\n            \"type_label\": \"Page\",\n            \"url\": \"http://localhost/WpLearning_03/pages/\",\n            \"title\": \"\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"nav-item submenu dropdown\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 4,\n            \"status\": \"publish\",\n            \"original_title\": \"pages\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:21:00\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '3a9f47b7-74ae-41d9-b195-fcb8ec38eb78', '', '', '2019-10-31 19:21:00', '2019-10-31 19:21:00', '', 0, 'http://localhost/WpLearning_03/?p=48', 0, 'customize_changeset', '', 0),
(54, 1, '2019-10-31 19:35:07', '2019-10-31 19:35:07', '{\n    \"nav_menu_item[50]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 50,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"\",\n            \"url\": \"https://www.facebook.com/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-facebook\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 3,\n            \"position\": 1,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:35:07\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'f17d18fc-05de-448d-8bf3-82dbf5479a8a', '', '', '2019-10-31 19:35:07', '2019-10-31 19:35:07', '', 0, 'http://localhost/WpLearning_03/2019/10/31/f17d18fc-05de-448d-8bf3-82dbf5479a8a/', 0, 'customize_changeset', '', 0),
(55, 1, '2019-10-31 19:35:27', '2019-10-31 19:35:27', '{\n    \"nav_menu_item[51]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 51,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"\",\n            \"url\": \"https://twitter.com/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-twitter-alt\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 3,\n            \"position\": 2,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:35:27\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'e9f96b27-cedd-44d2-bd1e-d5ddd27cb1a0', '', '', '2019-10-31 19:35:27', '2019-10-31 19:35:27', '', 0, 'http://localhost/WpLearning_03/2019/10/31/e9f96b27-cedd-44d2-bd1e-d5ddd27cb1a0/', 0, 'customize_changeset', '', 0),
(56, 1, '2019-10-31 19:35:59', '2019-10-31 19:35:59', '{\n    \"nav_menu_item[52]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 52,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"\",\n            \"url\": \"https://www.instagram.com/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-instagram\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 3,\n            \"position\": 3,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:35:59\"\n    },\n    \"nav_menu_item[53]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 53,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"\",\n            \"url\": \"https://www.skype.com/en/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-skype\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 3,\n            \"position\": 4,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:35:59\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'eb955f54-a4c0-489c-a68e-1c7358a1eeb0', '', '', '2019-10-31 19:35:59', '2019-10-31 19:35:59', '', 0, 'http://localhost/WpLearning_03/2019/10/31/eb955f54-a4c0-489c-a68e-1c7358a1eeb0/', 0, 'customize_changeset', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(58, 1, '2019-10-31 19:46:22', '2019-10-31 19:46:22', '{\n    \"nav_menu_item[50]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 50,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"\",\n            \"url\": \"https://www.facebook.com/\",\n            \"target\": \"_blank\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-facebook\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 3,\n            \"position\": 1,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:46:22\"\n    },\n    \"nav_menu_item[51]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 51,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"\",\n            \"url\": \"https://twitter.com/\",\n            \"target\": \"_blank\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-twitter-alt\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 3,\n            \"position\": 2,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:46:22\"\n    },\n    \"nav_menu_item[52]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 52,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"\",\n            \"url\": \"https://www.instagram.com/\",\n            \"target\": \"_blank\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-instagram\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 3,\n            \"position\": 3,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:46:22\"\n    },\n    \"nav_menu_item[53]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 53,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"\",\n            \"url\": \"https://www.skype.com/en/\",\n            \"target\": \"_blank\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-skype\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 3,\n            \"position\": 4,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 19:46:22\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4aff552b-2584-4957-9979-87a1af2ce8eb', '', '', '2019-10-31 19:46:22', '2019-10-31 19:46:22', '', 0, 'http://localhost/WpLearning_03/2019/10/31/4aff552b-2584-4957-9979-87a1af2ce8eb/', 0, 'customize_changeset', '', 0),
(59, 1, '2019-10-31 19:52:51', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-10-31 19:52:51', '0000-00-00 00:00:00', '', 0, 'http://localhost/WpLearning_03/?p=59', 1, 'nav_menu_item', '', 0),
(60, 1, '2019-10-31 19:55:02', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-10-31 19:55:02', '0000-00-00 00:00:00', '', 0, 'http://localhost/WpLearning_03/?p=60', 1, 'nav_menu_item', '', 0),
(65, 1, '2019-10-31 20:02:25', '2019-10-31 20:02:25', '{\n    \"nav_menu_item[-2788139507789324300]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 1,\n            \"type\": \"custom\",\n            \"title\": \"\",\n            \"url\": \"https://www.facebook.com/\",\n            \"target\": \"_blank\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-facebook\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"facebook\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 20:02:25\"\n    },\n    \"nav_menu_item[-6606627382355120000]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 2,\n            \"type\": \"custom\",\n            \"title\": \"twitter\",\n            \"url\": \"https://twitter.com/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"twitter\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 20:01:26\"\n    },\n    \"nav_menu_item[-1546888412851968000]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 3,\n            \"type\": \"custom\",\n            \"title\": \"instagram\",\n            \"url\": \"https://www.instagram.com/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"instagram\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 20:01:41\"\n    },\n    \"nav_menu_item[-333795877236336640]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 4,\n            \"type\": \"custom\",\n            \"title\": \"skype\",\n            \"url\": \"https://www.skype.com/en/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"skype\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 20:02:25\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4976e7e6-7fe8-48b1-b051-b6274a7998c6', '', '', '2019-10-31 20:02:25', '2019-10-31 20:02:25', '', 0, 'http://localhost/WpLearning_03/?p=65', 0, 'customize_changeset', '', 0),
(66, 1, '2019-10-31 20:02:26', '2019-10-31 20:02:26', '', '', '', 'publish', 'closed', 'closed', '', '66', '', '', '2019-10-31 20:02:26', '2019-10-31 20:02:26', '', 0, 'http://localhost/WpLearning_03/2019/10/31/66/', 1, 'nav_menu_item', '', 0),
(67, 1, '2019-10-31 20:02:26', '2019-10-31 20:02:26', '', '', '', 'publish', 'closed', 'closed', '', 'twitter', '', '', '2019-10-31 20:02:50', '2019-10-31 20:02:50', '', 0, 'http://localhost/WpLearning_03/2019/10/31/twitter/', 2, 'nav_menu_item', '', 0),
(68, 1, '2019-10-31 20:02:27', '2019-10-31 20:02:27', '', '', '', 'publish', 'closed', 'closed', '', 'instagram', '', '', '2019-10-31 20:03:38', '2019-10-31 20:03:38', '', 0, 'http://localhost/WpLearning_03/2019/10/31/instagram/', 3, 'nav_menu_item', '', 0),
(69, 1, '2019-10-31 20:02:28', '2019-10-31 20:02:28', '', '', '', 'publish', 'closed', 'closed', '', 'skype', '', '', '2019-10-31 20:03:38', '2019-10-31 20:03:38', '', 0, 'http://localhost/WpLearning_03/2019/10/31/skype/', 4, 'nav_menu_item', '', 0),
(70, 1, '2019-10-31 20:02:50', '2019-10-31 20:02:50', '{\n    \"nav_menu_item[67]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 2,\n            \"type\": \"custom\",\n            \"title\": \"\",\n            \"url\": \"https://twitter.com/\",\n            \"target\": \"_blank\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-twitter-alt\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"twitter\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 20:02:50\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '5e2aa09c-b73d-4a1c-a62a-8faf47c773bc', '', '', '2019-10-31 20:02:50', '2019-10-31 20:02:50', '', 0, 'http://localhost/WpLearning_03/2019/10/31/5e2aa09c-b73d-4a1c-a62a-8faf47c773bc/', 0, 'customize_changeset', '', 0),
(71, 1, '2019-10-31 20:03:19', '2019-10-31 20:03:19', '{\n    \"nav_menu_item[68]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 3,\n            \"type\": \"custom\",\n            \"title\": \"\",\n            \"url\": \"https://www.instagram.com/\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-instagram\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"instagram\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 20:03:19\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '3e6ad622-1a43-405a-8235-1461f9bd87fc', '', '', '2019-10-31 20:03:19', '2019-10-31 20:03:19', '', 0, 'http://localhost/WpLearning_03/2019/10/31/3e6ad622-1a43-405a-8235-1461f9bd87fc/', 0, 'customize_changeset', '', 0),
(72, 1, '2019-10-31 20:03:38', '2019-10-31 20:03:38', '{\n    \"nav_menu_item[68]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 3,\n            \"type\": \"custom\",\n            \"title\": \"\",\n            \"url\": \"https://www.instagram.com/\",\n            \"target\": \"_blank\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-instagram\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"instagram\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 20:03:38\"\n    },\n    \"nav_menu_item[69]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 4,\n            \"type\": \"custom\",\n            \"title\": \"\",\n            \"url\": \"https://www.skype.com/en/\",\n            \"target\": \"_blank\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"ti-skype\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"skype\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-31 20:03:38\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '5489ec68-8e9c-492c-84ec-81bc5e40e159', '', '', '2019-10-31 20:03:38', '2019-10-31 20:03:38', '', 0, 'http://localhost/WpLearning_03/2019/10/31/5489ec68-8e9c-492c-84ec-81bc5e40e159/', 0, 'customize_changeset', '', 0),
(74, 1, '2019-11-01 19:54:53', '2019-11-01 19:54:53', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2019-11-01 19:54:53', '2019-11-01 19:54:53', '', 0, 'http://localhost/WpLearning_03/wp-content/uploads/2019/11/logo.png', 0, 'attachment', 'image/png', 0),
(75, 1, '2019-11-01 19:55:08', '2019-11-01 19:55:08', 'http://localhost/WpLearning_03/wp-content/uploads/2019/11/cropped-logo.png', 'cropped-logo.png', '', 'inherit', 'open', 'closed', '', 'cropped-logo-png', '', '', '2019-11-01 19:55:08', '2019-11-01 19:55:08', '', 0, 'http://localhost/WpLearning_03/wp-content/uploads/2019/11/cropped-logo.png', 0, 'attachment', 'image/png', 0),
(76, 1, '2019-11-01 19:55:15', '2019-11-01 19:55:15', '{\n    \"sensive::header_text\": {\n        \"value\": true,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-11-01 19:55:14\"\n    },\n    \"sensive::custom_logo\": {\n        \"value\": 75,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-11-01 19:55:14\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'dec4d7c7-6763-49e9-bd43-6f063f614e49', '', '', '2019-11-01 19:55:15', '2019-11-01 19:55:15', '', 0, 'http://localhost/WpLearning_03/?p=76', 0, 'customize_changeset', '', 0),
(78, 1, '2019-11-01 20:03:32', '2019-11-01 20:03:32', '<img class=\"alignnone size-medium wp-image-79\" src=\"http://localhost/WpLearning_03/wp-content/uploads/2019/11/hero-banner-300x145.png\" alt=\"\" width=\"300\" height=\"145\" />\r\n<div class=\"hero-banner__content\">\r\n<h3>Tours &amp; Travels</h3>\r\n<h1>Amazing Places on earth</h1>\r\n<h4>December 12, 2019</h4>\r\n</div>', 'Banner-1', '', 'publish', 'closed', 'closed', '', 'banner-1', '', '', '2019-11-02 12:56:35', '2019-11-02 12:56:35', '', 0, 'http://localhost/WpLearning_03/?post_type=banner&#038;p=78', 0, 'banner', '', 0),
(79, 1, '2019-11-01 20:03:26', '2019-11-01 20:03:26', '', 'hero-banner', '', 'inherit', 'open', 'closed', '', 'hero-banner', '', '', '2019-11-01 20:03:26', '2019-11-01 20:03:26', '', 78, 'http://localhost/WpLearning_03/wp-content/uploads/2019/11/hero-banner.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2019-11-02 17:56:35', '2019-11-02 17:56:35', '', 'blog-slide1', '', 'inherit', 'open', 'closed', '', 'blog-slide1', '', '', '2019-11-02 17:56:35', '2019-11-02 17:56:35', '', 1, 'http://localhost/WpLearning_03/wp-content/uploads/2019/10/blog-slide1.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2019-11-02 17:56:38', '2019-11-02 17:56:38', '', 'blog-slide2', '', 'inherit', 'open', 'closed', '', 'blog-slide2', '', '', '2019-11-02 17:56:38', '2019-11-02 17:56:38', '', 1, 'http://localhost/WpLearning_03/wp-content/uploads/2019/10/blog-slide2.png', 0, 'attachment', 'image/png', 0),
(82, 1, '2019-11-02 17:56:39', '2019-11-02 17:56:39', '', 'blog-slide3', '', 'inherit', 'open', 'closed', '', 'blog-slide3', '', '', '2019-11-02 17:56:39', '2019-11-02 17:56:39', '', 1, 'http://localhost/WpLearning_03/wp-content/uploads/2019/10/blog-slide3.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2019-11-02 17:56:54', '2019-11-02 17:56:54', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-11-02 17:56:54', '2019-11-02 17:56:54', '', 1, 'http://localhost/WpLearning_03/2019/11/02/1-revision-v1/', 0, 'revision', '', 0),
(84, 1, '2019-11-02 19:28:14', '2019-11-02 19:28:14', '<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->', 'New york fashion week\'s continued the evolution', '', 'publish', 'open', 'open', '', 'new-york-fashion-weeks-continued-the-evolution', '', '', '2019-11-02 19:35:20', '2019-11-02 19:35:20', '', 0, 'http://localhost/WpLearning_03/?p=84', 0, 'post', '', 0),
(85, 1, '2019-11-02 19:28:14', '2019-11-02 19:28:14', '<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->', 'New york fashion week\'s continued the evolution', '', 'inherit', 'closed', 'closed', '', '84-revision-v1', '', '', '2019-11-02 19:28:14', '2019-11-02 19:28:14', '', 84, 'http://localhost/WpLearning_03/2019/11/02/84-revision-v1/', 0, 'revision', '', 0),
(86, 1, '2019-11-02 19:29:01', '2019-11-02 19:29:01', '<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->', 'New york fashion week\'s continued the evolution', '', 'publish', 'open', 'open', '', 'new-york-fashion-weeks-continued-the-evolution-2', '', '', '2019-11-02 19:29:01', '2019-11-02 19:29:01', '', 0, 'http://localhost/WpLearning_03/?p=86', 0, 'post', '', 0),
(87, 1, '2019-11-02 19:29:01', '2019-11-02 19:29:01', '<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->', 'New york fashion week\'s continued the evolution', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2019-11-02 19:29:01', '2019-11-02 19:29:01', '', 86, 'http://localhost/WpLearning_03/2019/11/02/86-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2019-11-02 19:30:10', '2019-11-02 19:30:10', '<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->', 'New york fashion week\'s continued the evolution', '', 'publish', 'open', 'open', '', 'new-york-fashion-weeks-continued-the-evolution-3', '', '', '2019-11-03 11:35:51', '2019-11-03 11:35:51', '', 0, 'http://localhost/WpLearning_03/?p=88', 0, 'post', '', 0),
(89, 1, '2019-11-02 19:30:10', '2019-11-02 19:30:10', '<!-- wp:paragraph -->\n<p> New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution  New york fashion week\'s continued the evolution </p>\n<!-- /wp:paragraph -->', 'New york fashion week\'s continued the evolution', '', 'inherit', 'closed', 'closed', '', '88-revision-v1', '', '', '2019-11-02 19:30:10', '2019-11-02 19:30:10', '', 88, 'http://localhost/WpLearning_03/2019/11/02/88-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2019-11-02 19:46:31', '2019-11-02 19:46:31', '<!-- wp:paragraph -->\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of “de Finibus Bonorum et Malorum” (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32.\n\n</p>\n<!-- /wp:paragraph -->', 'Where does it come from?', '', 'publish', 'open', 'open', '', 'where-does-it-come-from', '', '', '2019-11-05 13:24:28', '2019-11-05 13:24:28', '', 0, 'http://localhost/WpLearning_03/?p=90', 0, 'post', '', 0),
(91, 1, '2019-11-02 19:46:31', '2019-11-02 19:46:31', '<!-- wp:paragraph -->\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\n<!-- /wp:paragraph -->', 'Where does it come from?', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2019-11-02 19:46:31', '2019-11-02 19:46:31', '', 90, 'http://localhost/WpLearning_03/2019/11/02/90-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2019-11-02 19:47:01', '2019-11-02 19:47:01', '', 'cat-post-1', '', 'inherit', 'open', 'closed', '', 'cat-post-1', '', '', '2019-11-02 19:47:01', '2019-11-02 19:47:01', '', 90, 'http://localhost/WpLearning_03/wp-content/uploads/2019/11/cat-post-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(93, 1, '2019-11-04 09:29:35', '2019-11-04 09:29:35', '<strong style=\"color: #990000\">What can you achieve using Email Subscribers?</strong><p>Add subscription forms on website, send HTML newsletters & automatically notify subscribers about new blog posts once it is published. You can also Import or Export subscribers from any list to Email Subscribers.</p> <strong style=\"color: #990000\">Plugin Features</strong><ol> <li>Send notification emails to subscribers when new blog posts are published.</li> <li>Subscribe form available with 3 options to setup.</li> <li>Double Opt-In and Single Opt-In support.</li> <li>Email notification to admin when a new user signs up (Optional).</li> <li>Automatic welcome email to subscriber.</li> <li>Auto add unsubscribe link in the email.</li> <li>Import/Export subscriber emails to migrate to any lists.</li> <li>Default WordPress editor to create emails.</li> </ol> <strong>Thanks & Regards,</strong><br>Admin', 'Welcome To Email Subscribers', '', 'publish', 'closed', 'closed', '', 'welcome-to-email-subscribers', '', '', '2019-11-04 09:29:35', '2019-11-04 09:29:35', '', 0, 'http://localhost/WpLearning_03/es_template/welcome-to-email-subscribers/', 0, 'es_template', '', 0),
(94, 1, '2019-11-04 09:29:40', '2019-11-04 09:29:40', 'Hello {{NAME}},\r\n\r\nWe have published a new blog article on our website : {{POSTTITLE}}\r\n{{POSTIMAGE}}\r\n\r\nYou can view it from this link : {{POSTLINK}}\r\n\r\nThanks & Regards,\r\nAdmin\r\n\r\nYou received this email because in the past you have provided us your email address : {{EMAIL}} to receive notifications when new updates are posted.', 'New Post Published - {{POSTTITLE}}', '', 'publish', 'closed', 'closed', '', 'new-post-published-posttitle', '', '', '2019-11-04 09:29:40', '2019-11-04 09:29:40', '', 0, 'http://localhost/WpLearning_03/es_template/new-post-published-posttitle/', 0, 'es_template', '', 0),
(95, 1, '2019-11-04 10:13:47', '2019-11-04 10:13:47', '[newsletter]', 'Newsletter', '', 'publish', 'closed', 'closed', '', 'newsletter', '', '', '2019-11-04 10:13:47', '2019-11-04 10:13:47', '', 0, 'http://localhost/WpLearning_03/newsletter/', 0, 'page', '', 0),
(96, 1, '2019-11-05 12:09:49', '2019-11-05 12:09:49', '\r\n<div class=\"d-flex flex-row\">\r\n\r\n                  <input class=\"form-control\" name=\"EMAIL\" placeholder=\"Enter Email\" onfocus=\"this.placeholder = \'\'\" onblur=\"this.placeholder = \'Enter Email \'\"\r\n                    required=\"\" type=\"email\">\r\n\r\n\r\n                  <button class=\"click-btn btn btn-default\"><span class=\"lnr lnr-arrow-right\"></span></button>\r\n                  <div style=\"position: absolute; left: -5000px;\">\r\n                    <input name=\"b_36c4fd991d266f23781ded980_aefe40901a\" tabindex=\"-1\" value=\"\" type=\"text\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"info\"></div>\r\n', 'Sign Up Form', '', 'publish', 'closed', 'closed', '', '96', '', '', '2019-11-05 12:09:49', '2019-11-05 12:09:49', '', 0, 'http://localhost/WpLearning_03/mc4wp-form/96/', 0, 'mc4wp-form', '', 0),
(97, 1, '2019-11-05 08:17:49', '2019-11-05 08:17:49', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living  Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living  Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p> Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of “de Finibus Bonorum et Malorum” (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32. </p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p> Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living  Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living </p>\n<!-- /wp:paragraph -->', 'Tourist deaths in Costa Rica jeopardize safe dest ination reputation all time.', '', 'publish', 'open', 'open', '123456', 'tourist-deaths-in-costa-rica-jeopardize-safe-dest-ination-reputation-all-time', '', '', '2019-11-20 06:07:56', '2019-11-20 06:07:56', '', 0, 'http://localhost/WpLearning_03/?p=97', 0, 'post', '', 6),
(98, 1, '2019-11-05 08:17:42', '2019-11-05 08:17:42', '', 'blog4', '', 'inherit', 'open', 'closed', '', 'blog4', '', '', '2019-11-05 08:17:42', '2019-11-05 08:17:42', '', 97, 'http://localhost/WpLearning_03/wp-content/uploads/2019/11/blog4.png', 0, 'attachment', 'image/png', 0),
(99, 1, '2019-11-05 08:17:49', '2019-11-05 08:17:49', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living  Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living  Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living  Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living </p>\n<!-- /wp:paragraph -->', 'Tourist deaths in Costa Rica jeopardize safe dest ination reputation all time.', '', 'inherit', 'closed', 'closed', '', '97-revision-v1', '', '', '2019-11-05 08:17:49', '2019-11-05 08:17:49', '', 97, 'http://localhost/WpLearning_03/2019/11/05/97-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2019-11-05 13:05:10', '2019-11-05 13:05:10', '<img class=\"alignnone size-medium wp-image-98\" src=\"http://localhost/WpLearning_03/wp-content/uploads/2019/11/blog4-300x160.png\" alt=\"\" width=\"300\" height=\"160\" />\r\n<div class=\"hero-banner__content\">\r\n<h1>Blog Details</h1>\r\n<nav class=\"banner-breadcrumb\" aria-label=\"breadcrumb\">\r\n<ol class=\"breadcrumb\">\r\n 	<li class=\"breadcrumb-item\"><a href=\"#\">Home</a></li>\r\n 	<li class=\"breadcrumb-item active\" aria-current=\"page\">Blog Details</li>\r\n</ol>\r\n</nav></div>', 'Single Page Banner', '', 'trash', 'closed', 'closed', '', 'single-page-banner__trashed', '', '', '2019-11-05 13:06:19', '2019-11-05 13:06:19', '', 0, 'http://localhost/WpLearning_03/?post_type=banner&#038;p=100', 0, 'banner', '', 0),
(101, 1, '2019-11-05 13:23:57', '2019-11-05 13:23:57', '<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\n<!-- /wp:paragraph -->', 'Where does it come from?', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2019-11-05 13:23:57', '2019-11-05 13:23:57', '', 90, 'http://localhost/WpLearning_03/2019/11/05/90-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2019-11-05 13:24:17', '2019-11-05 13:24:17', '<!-- wp:paragraph -->\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>\n\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of “de Finibus Bonorum et Malorum” (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32.\n\n</p>\n<!-- /wp:paragraph -->', 'Where does it come from?', '', 'inherit', 'closed', 'closed', '', '90-revision-v1', '', '', '2019-11-05 13:24:17', '2019-11-05 13:24:17', '', 90, 'http://localhost/WpLearning_03/2019/11/05/90-revision-v1/', 0, 'revision', '', 0),
(103, 1, '2019-11-05 13:25:18', '2019-11-05 13:25:18', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living  Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living  Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p> Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of “de Finibus Bonorum et Malorum” (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, “Lorem ipsum dolor sit amet..”, comes from a line in section 1.10.32. </p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p> Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living  Over yielding doesn\'t so moved green saw meat hath fish he him from given yielding lesser cattle were fruitful lights. Given let have, lesser their made him above gathered dominion sixth. Creeping deep said can\'t called second. Air created seed heaven sixth created living </p>\n<!-- /wp:paragraph -->', 'Tourist deaths in Costa Rica jeopardize safe dest ination reputation all time.', '', 'inherit', 'closed', 'closed', '', '97-revision-v1', '', '', '2019-11-05 13:25:18', '2019-11-05 13:25:18', '', 97, 'http://localhost/WpLearning_03/2019/11/05/97-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2019-11-21 05:13:50', '2019-11-21 05:13:50', '<!-- wp:shortcode -->\n[pw_map address=\"New York City\" key=\"AIzaSyCQRIIju0It_namVD3g5OcCv9tYdctoL8A\"]\n<!-- /wp:shortcode -->', 'Contact', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2019-11-21 05:13:50', '2019-11-21 05:13:50', '', 8, 'http://localhost/WpLearning_03/2019/11/21/8-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2019-11-21 05:17:54', '2019-11-21 05:17:54', '<!-- wp:paragraph -->\n<p><a href=\"http://localhost/WpLearning_03/contact.html\">Contact</a> page</p>\n<!-- /wp:paragraph -->', 'Contact', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2019-11-21 05:17:54', '2019-11-21 05:17:54', '', 8, 'http://localhost/WpLearning_03/2019/11/21/8-revision-v1/', 0, 'revision', '', 0),
(111, 1, '2019-11-21 13:22:48', '2019-11-21 13:22:48', '<div class=\"row\">\r\n              <div class=\"col-lg-5\">\r\n                <div class=\"form-group\">\r\n                  <input class=\"form-control\" name=\"name\" id=\"name\" type=\"text\" placeholder=\"Enter your name\">\r\n                </div>\r\n                <div class=\"form-group\">\r\n                  <input class=\"form-control\" name=\"email\" id=\"email\" type=\"email\" placeholder=\"Enter email address\">\r\n                </div>\r\n                <div class=\"form-group\">\r\n                  <input class=\"form-control\" name=\"subject\" id=\"subject\" type=\"text\" placeholder=\"Enter Subject\">\r\n                </div>\r\n              </div>\r\n              <div class=\"col-lg-7\">\r\n                <div class=\"form-group\">\r\n                    <textarea class=\"form-control different-control w-100\" name=\"message\" id=\"message\" cols=\"30\" rows=\"5\" placeholder=\"Enter Message\"></textarea>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group text-center text-md-right mt-3\">\r\n              <button type=\"submit\" class=\"button button--active button-contactForm\">Send Message</button>\r\n            </div>\n1\nMy New Wordpress Website \"[your-subject]\"\nMy New Wordpress Website <nmnaba14@gmail.com>\nnmnaba14@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on My New Wordpress Website (http://localhost/WpLearning_03)\n\n\n\n\n\nMy New Wordpress Website \"[your-subject]\"\nMy New Wordpress Website <nmnaba14@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on My New Wordpress Website (http://localhost/WpLearning_03)\nReply-To: nmnaba14@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'contact', '', 'publish', 'closed', 'closed', '', 'untitled', '', '', '2019-11-21 13:55:47', '2019-11-21 13:55:47', '', 0, 'http://localhost/WpLearning_03/?post_type=wpcf7_contact_form&#038;p=111', 0, 'wpcf7_contact_form', '', 0),
(112, 1, '2019-11-21 13:48:00', '2019-11-21 13:48:00', '<label> Your Name (required)\r\n    [text* your-name] </label>\r\n\r\n<label> Your Email (required)\r\n    [email* your-email] </label>\r\n\r\n<label> Subject\r\n    [text your-subject] </label>\r\n\r\n<label> Your Message\r\n    [textarea your-message] </label>\r\n\r\n[submit \"Send\"]\n1\nMy New Wordpress Website \"[your-subject]\"\nMy New Wordpress Website <nmnaba14@gmail.com>\nnmnaba14@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on My New Wordpress Website (http://localhost/WpLearning_03)\nReply-To: [your-email]\n\n\n\n\nMy New Wordpress Website \"[your-subject]\"\nMy New Wordpress Website <nmnaba14@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on My New Wordpress Website (http://localhost/WpLearning_03)\nReply-To: nmnaba14@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Untitled', '', 'publish', 'closed', 'closed', '', 'untitled-2', '', '', '2019-11-21 13:48:00', '2019-11-21 13:48:00', '', 0, 'http://localhost/WpLearning_03/?post_type=wpcf7_contact_form&p=112', 0, 'wpcf7_contact_form', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_responsive_menu`
--

CREATE TABLE `wp_responsive_menu` (
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_responsive_menu`
--

INSERT INTO `wp_responsive_menu` (`name`, `value`) VALUES
('accordion_animation', 'off'),
('active_arrow_font_icon', ''),
('active_arrow_font_icon_type', 'font-awesome'),
('active_arrow_image', ''),
('active_arrow_image_alt', ''),
('active_arrow_shape', '▲'),
('admin_theme', 'dark'),
('animation_speed', '0.5'),
('animation_type', 'slide'),
('arrow_position', 'right'),
('auto_expand_all_submenus', 'off'),
('auto_expand_current_submenus', 'off'),
('breakpoint', '8000'),
('button_background_colour', '#000'),
('button_background_colour_active', '#000'),
('button_background_colour_hover', '#000'),
('button_click_animation', 'boring'),
('button_click_trigger', '#responsive-menu-button'),
('button_distance_from_side', '5'),
('button_distance_from_side_unit', '%'),
('button_font', NULL),
('button_font_icon', NULL),
('button_font_icon_type', 'font-awesome'),
('button_font_icon_when_clicked', NULL),
('button_font_icon_when_clicked_type', 'font-awesome'),
('button_font_size', '14'),
('button_font_size_unit', 'px'),
('button_height', '55'),
('button_height_unit', 'px'),
('button_image', NULL),
('button_image_alt', NULL),
('button_image_alt_when_clicked', NULL),
('button_image_when_clicked', NULL),
('button_left_or_right', 'right'),
('button_line_colour', '#fff'),
('button_line_colour_active', '#fff'),
('button_line_colour_hover', '#fff'),
('button_line_height', '3'),
('button_line_height_unit', 'px'),
('button_line_margin', '5'),
('button_line_margin_unit', 'px'),
('button_line_width', '25'),
('button_line_width_unit', 'px'),
('button_position_type', 'fixed'),
('button_push_with_animation', 'off'),
('button_text_colour', '#fff'),
('button_title', NULL),
('button_title_line_height', '13'),
('button_title_line_height_unit', 'px'),
('button_title_open', NULL),
('button_title_position', 'left'),
('button_top', '15'),
('button_top_unit', 'px'),
('button_transparent_background', 'off'),
('button_trigger_type', 'click'),
('button_width', '55'),
('button_width_unit', 'px'),
('custom_css', NULL),
('custom_walker', NULL),
('desktop_menu_options', '{}'),
('desktop_menu_positioning', 'fixed'),
('desktop_menu_side', ''),
('desktop_menu_to_hide', ''),
('desktop_menu_width', ''),
('desktop_menu_width_unit', 'px'),
('enable_touch_gestures', ''),
('excluded_pages', NULL),
('external_files', 'off'),
('fade_submenus', 'off'),
('fade_submenus_delay', '100'),
('fade_submenus_side', 'left'),
('fade_submenus_speed', '500'),
('header_bar_adjust_page', NULL),
('header_bar_background_color', '#ffffff'),
('header_bar_breakpoint', '800'),
('header_bar_font', NULL),
('header_bar_font_size', '14'),
('header_bar_font_size_unit', 'px'),
('header_bar_height', '80'),
('header_bar_height_unit', 'px'),
('header_bar_html_content', NULL),
('header_bar_items_order', '{\"logo\":\"on\",\"title\":\"on\",\"search\":\"on\",\"html content\":\"on\"}'),
('header_bar_logo', NULL),
('header_bar_logo_alt', ''),
('header_bar_logo_height', NULL),
('header_bar_logo_height_unit', 'px'),
('header_bar_logo_link', NULL),
('header_bar_logo_width', NULL),
('header_bar_logo_width_unit', '%'),
('header_bar_position_type', 'fixed'),
('header_bar_text_color', '#ffffff'),
('header_bar_title', NULL),
('hide_on_desktop', 'off'),
('hide_on_mobile', 'off'),
('inactive_arrow_font_icon', ''),
('inactive_arrow_font_icon_type', 'font-awesome'),
('inactive_arrow_image', ''),
('inactive_arrow_image_alt', ''),
('inactive_arrow_shape', '▼'),
('items_order', '{\"title\":\"on\",\"menu\":\"on\",\"search\":\"on\",\"additional content\":\"on\"}'),
('keyboard_shortcut_close_menu', '27,37'),
('keyboard_shortcut_open_menu', '32,39'),
('menu_additional_content', NULL),
('menu_additional_content_colour', '#fff'),
('menu_adjust_for_wp_admin_bar', 'off'),
('menu_appear_from', 'left'),
('menu_auto_height', 'off'),
('menu_background_colour', '#212121'),
('menu_background_image', ''),
('menu_border_width', '1'),
('menu_border_width_unit', 'px'),
('menu_close_on_body_click', 'off'),
('menu_close_on_link_click', 'off'),
('menu_close_on_scroll', 'off'),
('menu_container_background_colour', '#212121'),
('menu_current_item_background_colour', '#212121'),
('menu_current_item_background_hover_colour', '#3f3f3f'),
('menu_current_item_border_colour', '#212121'),
('menu_current_item_border_hover_colour', '#3f3f3f'),
('menu_current_link_colour', '#fff'),
('menu_current_link_hover_colour', '#fff'),
('menu_depth', '5'),
('menu_depth_0', '5'),
('menu_depth_0_unit', '%'),
('menu_depth_1', '10'),
('menu_depth_1_unit', '%'),
('menu_depth_2', '15'),
('menu_depth_2_unit', '%'),
('menu_depth_3', '20'),
('menu_depth_3_unit', '%'),
('menu_depth_4', '25'),
('menu_depth_4_unit', '%'),
('menu_depth_5', '30'),
('menu_depth_5_unit', '%'),
('menu_depth_side', 'left'),
('menu_disable_scrolling', 'off'),
('menu_font', NULL),
('menu_font_icons', ''),
('menu_font_size', '13'),
('menu_font_size_unit', 'px'),
('menu_item_background_colour', '#212121'),
('menu_item_background_hover_colour', '#3f3f3f'),
('menu_item_border_colour', '#212121'),
('menu_item_border_colour_hover', '#212121'),
('menu_item_click_to_trigger_submenu', 'off'),
('menu_link_colour', '#fff'),
('menu_link_hover_colour', '#fff'),
('menu_links_height', '40'),
('menu_links_height_unit', 'px'),
('menu_links_line_height', '40'),
('menu_links_line_height_unit', 'px'),
('menu_maximum_width', NULL),
('menu_maximum_width_unit', 'px'),
('menu_minimum_width', NULL),
('menu_minimum_width_unit', 'px'),
('menu_overlay', 'off'),
('menu_overlay_colour', 'rgba(0,0,0,0.7)'),
('menu_search_box_background_colour', '#fff'),
('menu_search_box_border_colour', '#dadada'),
('menu_search_box_placeholder_colour', '#C7C7CD'),
('menu_search_box_text', 'Search'),
('menu_search_box_text_colour', '#333'),
('menu_sub_arrow_background_colour', '#212121'),
('menu_sub_arrow_background_colour_active', '#212121'),
('menu_sub_arrow_background_hover_colour', '#3f3f3f'),
('menu_sub_arrow_background_hover_colour_active', '#3f3f3f'),
('menu_sub_arrow_border_colour', '#212121'),
('menu_sub_arrow_border_colour_active', '#212121'),
('menu_sub_arrow_border_hover_colour', '#3f3f3f'),
('menu_sub_arrow_border_hover_colour_active', '#3f3f3f'),
('menu_sub_arrow_shape_colour', '#fff'),
('menu_sub_arrow_shape_colour_active', '#fff'),
('menu_sub_arrow_shape_hover_colour', '#fff'),
('menu_sub_arrow_shape_hover_colour_active', '#fff'),
('menu_text_alignment', 'left'),
('menu_theme', NULL),
('menu_title', NULL),
('menu_title_alignment', 'left'),
('menu_title_background_colour', '#212121'),
('menu_title_background_hover_colour', '#212121'),
('menu_title_colour', '#fff'),
('menu_title_font_icon', NULL),
('menu_title_font_icon_type', 'font-awesome'),
('menu_title_font_size', '13'),
('menu_title_font_size_unit', 'px'),
('menu_title_hover_colour', '#fff'),
('menu_title_image', NULL),
('menu_title_image_alt', NULL),
('menu_title_image_height', NULL),
('menu_title_image_height_unit', 'px'),
('menu_title_image_width', NULL),
('menu_title_image_width_unit', '%'),
('menu_title_link', NULL),
('menu_title_link_location', '_self'),
('menu_to_hide', ''),
('menu_to_use', ''),
('menu_width', '75'),
('menu_width_unit', '%'),
('menu_word_wrap', 'off'),
('minify_scripts', 'off'),
('mobile_only', 'off'),
('page_wrapper', NULL),
('remove_bootstrap', ''),
('remove_fontawesome', ''),
('scripts_in_footer', 'off'),
('shortcode', 'off'),
('show_menu_on_page_load', ''),
('single_menu_font', NULL),
('single_menu_font_size', '14'),
('single_menu_font_size_unit', 'px'),
('single_menu_height', '80'),
('single_menu_height_unit', 'px'),
('single_menu_item_background_colour', '#ffffff'),
('single_menu_item_background_colour_hover', '#ffffff'),
('single_menu_item_link_colour', '#000000'),
('single_menu_item_link_colour_hover', '#000000'),
('single_menu_item_submenu_background_colour', '#ffffff'),
('single_menu_item_submenu_background_colour_hover', '#ffffff'),
('single_menu_item_submenu_link_colour', '#000000'),
('single_menu_item_submenu_link_colour_hover', '#000000'),
('single_menu_line_height', '80'),
('single_menu_line_height_unit', 'px'),
('single_menu_submenu_font', NULL),
('single_menu_submenu_font_size', '12'),
('single_menu_submenu_font_size_unit', 'px'),
('single_menu_submenu_height', NULL),
('single_menu_submenu_height_unit', 'auto'),
('single_menu_submenu_line_height', '40'),
('single_menu_submenu_line_height_unit', 'px'),
('slide_effect_back_to_text', 'Back'),
('smooth_scroll_on', 'off'),
('smooth_scroll_speed', '500'),
('sub_menu_speed', '0.2'),
('submenu_arrow_height', '39'),
('submenu_arrow_height_unit', 'px'),
('submenu_arrow_position', 'right'),
('submenu_arrow_width', '40'),
('submenu_arrow_width_unit', 'px'),
('submenu_border_width', '1'),
('submenu_border_width_unit', 'px'),
('submenu_current_item_background_colour', '#212121'),
('submenu_current_item_background_hover_colour', '#3f3f3f'),
('submenu_current_item_border_colour', '#212121'),
('submenu_current_item_border_hover_colour', '#3f3f3f'),
('submenu_current_link_colour', '#fff'),
('submenu_current_link_hover_colour', '#fff'),
('submenu_descriptions_on', ''),
('submenu_font', NULL),
('submenu_font_size', '13'),
('submenu_font_size_unit', 'px'),
('submenu_item_background_colour', '#212121'),
('submenu_item_background_hover_colour', '#3f3f3f'),
('submenu_item_border_colour', '#212121'),
('submenu_item_border_colour_hover', '#212121'),
('submenu_link_colour', '#fff'),
('submenu_link_hover_colour', '#fff'),
('submenu_links_height', '40'),
('submenu_links_height_unit', 'px'),
('submenu_links_line_height', '40'),
('submenu_links_line_height_unit', 'px'),
('submenu_sub_arrow_background_colour', '#212121'),
('submenu_sub_arrow_background_colour_active', '#212121'),
('submenu_sub_arrow_background_hover_colour', '#3f3f3f'),
('submenu_sub_arrow_background_hover_colour_active', '#3f3f3f'),
('submenu_sub_arrow_border_colour', '#212121'),
('submenu_sub_arrow_border_colour_active', '#212121'),
('submenu_sub_arrow_border_hover_colour', '#3f3f3f'),
('submenu_sub_arrow_border_hover_colour_active', '#3f3f3f'),
('submenu_sub_arrow_shape_colour', '#fff'),
('submenu_sub_arrow_shape_colour_active', '#fff'),
('submenu_sub_arrow_shape_hover_colour', '#fff'),
('submenu_sub_arrow_shape_hover_colour_active', '#fff'),
('submenu_submenu_arrow_height', '39'),
('submenu_submenu_arrow_height_unit', 'px'),
('submenu_submenu_arrow_width', '40'),
('submenu_submenu_arrow_width_unit', 'px'),
('submenu_text_alignment', 'left'),
('theme_location_menu', ''),
('transition_speed', '0.5'),
('use_desktop_menu', ''),
('use_header_bar', 'off'),
('use_slide_effect', 'off');

-- --------------------------------------------------------

--
-- Table structure for table `wp_sbi_instagram_feeds_posts`
--

CREATE TABLE `wp_sbi_instagram_feeds_posts` (
  `record_id` int(11) UNSIGNED NOT NULL,
  `id` int(11) UNSIGNED NOT NULL,
  `instagram_id` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `feed_id` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_sbi_instagram_feeds_posts`
--

INSERT INTO `wp_sbi_instagram_feeds_posts` (`record_id`, `id`, `instagram_id`, `feed_id`) VALUES
(1, 1, '2096651942797224840_15243772486', 'sbi_15243772486');

-- --------------------------------------------------------

--
-- Table structure for table `wp_sbi_instagram_posts`
--

CREATE TABLE `wp_sbi_instagram_posts` (
  `id` int(11) UNSIGNED NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `instagram_id` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `time_stamp` datetime DEFAULT NULL,
  `top_time_stamp` datetime DEFAULT NULL,
  `json_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `media_id` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sizes` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `aspect_ratio` decimal(4,2) NOT NULL DEFAULT 0.00,
  `images_done` tinyint(1) NOT NULL DEFAULT 0,
  `last_requested` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_sbi_instagram_posts`
--

INSERT INTO `wp_sbi_instagram_posts` (`id`, `created_on`, `instagram_id`, `time_stamp`, `top_time_stamp`, `json_data`, `media_id`, `sizes`, `aspect_ratio`, `images_done`, `last_requested`) VALUES
(1, '2019-11-04 08:15:08', '2096651942797224840_15243772486', '2019-11-04 08:17:08', NULL, '{\"id\":\"2096651942797224840_15243772486\",\"user\":{\"id\":\"15243772486\",\"full_name\":\"Abdullah Al Noman\",\"profile_picture\":\"https:\\/\\/scontent.cdninstagram.com\\/vp\\/0b3f469d38f40bd8f330cd1a6eef82dd\\/5E41C390\\/t51.2885-19\\/s150x150\\/64379235_2267993340181031_4712706986418896896_n.jpg?_nc_ht=scontent.cdninstagram.com\",\"username\":\"alnoman141\"},\"images\":{\"thumbnail\":{\"width\":150,\"height\":150,\"url\":\"https:\\/\\/scontent.cdninstagram.com\\/vp\\/7cbfabe58c12dd909bc66874de30b243\\/5E585FDC\\/t51.2885-15\\/e35\\/s150x150\\/66421366_111804106814964_7995586576395090816_n.jpg?_nc_ht=scontent.cdninstagram.com\"},\"low_resolution\":{\"width\":320,\"height\":320,\"url\":\"https:\\/\\/scontent.cdninstagram.com\\/vp\\/70ed936620428882aa515c875b7e03b1\\/5E59372C\\/t51.2885-15\\/e35\\/s320x320\\/66421366_111804106814964_7995586576395090816_n.jpg?_nc_ht=scontent.cdninstagram.com\"},\"standard_resolution\":{\"width\":640,\"height\":640,\"url\":\"https:\\/\\/scontent.cdninstagram.com\\/vp\\/f17de9a7c6655fef9841246c5a053a64\\/5E5AA47B\\/t51.2885-15\\/sh0.08\\/e35\\/s640x640\\/66421366_111804106814964_7995586576395090816_n.jpg?_nc_ht=scontent.cdninstagram.com\"}},\"created_time\":\"1564160410\",\"caption\":null,\"user_has_liked\":false,\"likes\":{\"count\":4},\"tags\":[],\"filter\":\"Perpetua\",\"comments\":{\"count\":0},\"type\":\"image\",\"link\":\"https:\\/\\/www.instagram.com\\/p\\/B0YzdVcD6uI\\/\",\"location\":null,\"attribution\":null,\"users_in_photo\":[]}', '2096651942797224840_15243772486', 'a:1:{s:3:\"low\";i:320;}', '1.00', 1, '2019-11-07');

-- --------------------------------------------------------

--
-- Table structure for table `wp_socialsnap_stats`
--

CREATE TABLE `wp_socialsnap_stats` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `count` bigint(20) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `network` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(2083) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_subscribe_reloaded_subscribers`
--

CREATE TABLE `wp_subscribe_reloaded_subscribers` (
  `stcr_id` int(11) NOT NULL,
  `subscriber_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` int(15) NOT NULL,
  `subscriber_unique_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main Menu', 'main-menu', 0),
(3, 'Social Menu', 'social-menu', 0),
(4, 'News', 'news', 0),
(5, 'Sports', 'sports', 0),
(6, 'Demo', 'demo', 0),
(7, 'Hello', 'hello', 0),
(8, 'sports', 'sports', 0),
(9, 'health', 'health', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(25, 2, 0),
(26, 2, 0),
(27, 2, 0),
(28, 2, 0),
(29, 2, 0),
(30, 2, 0),
(31, 2, 0),
(66, 3, 0),
(67, 3, 0),
(68, 3, 0),
(69, 3, 0),
(84, 5, 0),
(86, 1, 0),
(88, 4, 0),
(88, 8, 0),
(88, 9, 0),
(90, 4, 0),
(90, 6, 0),
(90, 7, 0),
(97, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'nav_menu', '', 0, 7),
(3, 3, 'nav_menu', '', 0, 4),
(4, 4, 'category', '', 0, 2),
(5, 5, 'category', '', 0, 1),
(6, 6, 'post_tag', '', 0, 1),
(7, 7, 'post_tag', '', 0, 1),
(8, 8, 'post_tag', '', 0, 1),
(9, 9, 'post_tag', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_top_ten`
--

CREATE TABLE `wp_top_ten` (
  `postnumber` bigint(20) NOT NULL,
  `cntaccess` bigint(20) NOT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_top_ten`
--

INSERT INTO `wp_top_ten` (`postnumber`, `cntaccess`, `blog_id`) VALUES
(84, 16, 1),
(86, 10, 1),
(88, 2, 1),
(90, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_top_ten_daily`
--

CREATE TABLE `wp_top_ten_daily` (
  `postnumber` bigint(20) NOT NULL,
  `cntaccess` bigint(20) NOT NULL,
  `dp_date` datetime NOT NULL,
  `blog_id` bigint(20) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_top_ten_daily`
--

INSERT INTO `wp_top_ten_daily` (`postnumber`, `cntaccess`, `dp_date`, `blog_id`) VALUES
(84, 3, '2019-11-03 12:00:00', 1),
(84, 13, '2019-11-03 13:00:00', 1),
(86, 7, '2019-11-03 12:00:00', 1),
(86, 3, '2019-11-04 06:00:00', 1),
(88, 1, '2019-11-03 11:00:00', 1),
(88, 1, '2019-11-04 07:00:00', 1),
(90, 2, '2019-11-03 12:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_ulike`
--

CREATE TABLE `wp_ulike` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL,
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ulike_activities`
--

CREATE TABLE `wp_ulike_activities` (
  `id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL,
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ulike_comments`
--

CREATE TABLE `wp_ulike_comments` (
  `id` bigint(20) NOT NULL,
  `comment_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL,
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ulike_forums`
--

CREATE TABLE `wp_ulike_forums` (
  `id` bigint(20) NOT NULL,
  `topic_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL,
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'text_widget_custom_html,addtoany_settings_pointer,theme_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"c568bd9959e385551769bd3c26e99bd628e2d261d8449cc64906a968f1311202\";a:4:{s:10:\"expiration\";i:1575183158;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36\";s:5:\"login\";i:1573973558;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '106'),
(18, 1, 'managenav-menuscolumnshidden', 'a:3:{i:0;s:15:\"title-attribute\";i:1;s:3:\"xfn\";i:2;s:11:\"description\";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(20, 1, 'nav_menu_recently_edited', '3'),
(21, 1, 'wp_user-settings', 'editor=tinymce&libraryContent=browse'),
(22, 1, 'wp_user-settings-time', '1574342389'),
(23, 1, 'jetpack_tracks_anon_id', 'jetpack:RG1s+OutGf31CdhKL2b/zxWA'),
(24, 1, 'es_forms_per_page', '20'),
(27, 1, 's2_authors', ''),
(32, 1, 'maxbuttons_review_notice', 'off'),
(33, 1, 'mbsocial_review_notice', 'off'),
(34, 1, 'closedpostboxes_page', 'a:1:{i:0;s:12:\"mbsocial-box\";}'),
(35, 1, 'metaboxhidden_page', 'a:0:{}'),
(36, 1, 'sbi_ignore_bfcm_sale_notice', '2019'),
(37, 1, 'sbi_ignore_new_user_sale_notice', 'always'),
(38, 1, 'wpcf7_hide_welcome_panel_on', 'a:1:{i:0;s:3:\"5.1\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BKV9aVRKmeWOJHEvqzV6/Gx0EHgbOH1', 'admin', 'nmnaba14@gmail.com', '', '2019-10-30 18:10:02', '', 0, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza`
--

CREATE TABLE `wp_wpgmza` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `address` varchar(700) NOT NULL,
  `description` mediumtext NOT NULL,
  `pic` varchar(700) NOT NULL,
  `link` varchar(2083) NOT NULL,
  `icon` varchar(700) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lng` varchar(100) NOT NULL,
  `anim` varchar(3) NOT NULL,
  `title` varchar(700) NOT NULL,
  `infoopen` varchar(3) NOT NULL,
  `category` varchar(500) NOT NULL,
  `approved` tinyint(1) DEFAULT 1,
  `retina` tinyint(1) DEFAULT 0,
  `type` tinyint(1) DEFAULT 0,
  `did` varchar(500) NOT NULL,
  `sticky` tinyint(1) DEFAULT 0,
  `other_data` longtext NOT NULL,
  `latlng` point DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_wpgmza`
--

INSERT INTO `wp_wpgmza` (`id`, `map_id`, `address`, `description`, `pic`, `link`, `icon`, `lat`, `lng`, `anim`, `title`, `infoopen`, `category`, `approved`, `retina`, `type`, `did`, `sticky`, `other_data`, `latlng`) VALUES
(1, 1, 'California', '', '', '', '', '36.778261', '-119.4179323999', '0', '', '', '', 1, 0, 0, '', 0, '', 0x0000000001010000004a60730e9e63424098608967bfda5dc0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_circles`
--

CREATE TABLE `wp_wpgmza_circles` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `center` point DEFAULT NULL,
  `radius` float DEFAULT NULL,
  `color` varchar(16) DEFAULT NULL,
  `opacity` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_maps`
--

CREATE TABLE `wp_wpgmza_maps` (
  `id` int(11) NOT NULL,
  `map_title` varchar(55) NOT NULL,
  `map_width` varchar(6) NOT NULL,
  `map_height` varchar(6) NOT NULL,
  `map_start_lat` varchar(700) NOT NULL,
  `map_start_lng` varchar(700) NOT NULL,
  `map_start_location` varchar(700) NOT NULL,
  `map_start_zoom` int(10) NOT NULL,
  `default_marker` varchar(700) NOT NULL,
  `type` int(10) NOT NULL,
  `alignment` int(10) NOT NULL,
  `directions_enabled` int(10) NOT NULL,
  `styling_enabled` int(10) NOT NULL,
  `styling_json` mediumtext NOT NULL,
  `active` int(1) NOT NULL,
  `kml` varchar(700) NOT NULL,
  `bicycle` int(10) NOT NULL,
  `traffic` int(10) NOT NULL,
  `dbox` int(10) NOT NULL,
  `dbox_width` varchar(10) NOT NULL,
  `listmarkers` int(10) NOT NULL,
  `listmarkers_advanced` int(10) NOT NULL,
  `filterbycat` tinyint(1) NOT NULL,
  `ugm_enabled` int(10) NOT NULL,
  `ugm_category_enabled` tinyint(1) NOT NULL,
  `fusion` varchar(100) NOT NULL,
  `map_width_type` varchar(3) NOT NULL,
  `map_height_type` varchar(3) NOT NULL,
  `mass_marker_support` int(10) NOT NULL,
  `ugm_access` int(10) NOT NULL,
  `order_markers_by` int(10) NOT NULL,
  `order_markers_choice` int(10) NOT NULL,
  `show_user_location` int(3) NOT NULL,
  `default_to` varchar(700) NOT NULL,
  `other_settings` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_wpgmza_maps`
--

INSERT INTO `wp_wpgmza_maps` (`id`, `map_title`, `map_width`, `map_height`, `map_start_lat`, `map_start_lng`, `map_start_location`, `map_start_zoom`, `default_marker`, `type`, `alignment`, `directions_enabled`, `styling_enabled`, `styling_json`, `active`, `kml`, `bicycle`, `traffic`, `dbox`, `dbox_width`, `listmarkers`, `listmarkers_advanced`, `filterbycat`, `ugm_enabled`, `ugm_category_enabled`, `fusion`, `map_width_type`, `map_height_type`, `mass_marker_support`, `ugm_access`, `order_markers_by`, `order_markers_choice`, `show_user_location`, `default_to`, `other_settings`) VALUES
(1, 'My first map', '100', '400', '45.950464', '-109.815505', '45.950464398418106,-109.81550500000003', 2, '0', 1, 2, 1, 0, '', 0, '', 2, 2, 1, '100', 0, 0, 0, 0, 0, '', '\\%', 'px', 1, 0, 1, 2, 0, '', 'a:13:{s:21:\"store_locator_enabled\";i:2;s:22:\"store_locator_distance\";i:2;s:28:\"store_locator_default_radius\";i:10;s:31:\"store_locator_not_found_message\";s:52:\"No results found in this location. Please try again.\";s:20:\"store_locator_bounce\";i:1;s:26:\"store_locator_query_string\";s:14:\"ZIP / Address:\";s:29:\"wpgmza_store_locator_restrict\";s:0:\"\";s:33:\"wpgmza_store_locator_radius_style\";s:6:\"modern\";s:12:\"map_max_zoom\";s:1:\"1\";s:15:\"transport_layer\";i:2;s:17:\"wpgmza_theme_data\";s:0:\"\";s:30:\"wpgmza_show_points_of_interest\";i:1;s:17:\"wpgmza_auto_night\";i:0;}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_polygon`
--

CREATE TABLE `wp_wpgmza_polygon` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `polydata` longtext NOT NULL,
  `description` text NOT NULL,
  `innerpolydata` longtext NOT NULL,
  `linecolor` varchar(7) NOT NULL,
  `lineopacity` varchar(7) NOT NULL,
  `fillcolor` varchar(7) NOT NULL,
  `opacity` varchar(3) NOT NULL,
  `title` varchar(250) NOT NULL,
  `link` varchar(700) NOT NULL,
  `ohfillcolor` varchar(7) NOT NULL,
  `ohlinecolor` varchar(7) NOT NULL,
  `ohopacity` varchar(3) NOT NULL,
  `polyname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_polylines`
--

CREATE TABLE `wp_wpgmza_polylines` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `polydata` longtext NOT NULL,
  `linecolor` varchar(7) NOT NULL,
  `linethickness` varchar(3) NOT NULL,
  `opacity` varchar(3) NOT NULL,
  `polyname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_rectangles`
--

CREATE TABLE `wp_wpgmza_rectangles` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `cornerA` point DEFAULT NULL,
  `cornerB` point DEFAULT NULL,
  `color` varchar(16) DEFAULT NULL,
  `opacity` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wti_like_post`
--

CREATE TABLE `wp_wti_like_post` (
  `id` bigint(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `value` int(2) NOT NULL,
  `date_time` datetime NOT NULL,
  `ip` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_wti_like_post`
--

INSERT INTO `wp_wti_like_post` (`id`, `post_id`, `value`, `date_time`, `ip`, `user_id`) VALUES
(1, 97, 4, '2019-11-07 16:39:31', '::1', 1),
(2, 78, 1, '2019-11-07 16:45:48', '::1', 1),
(3, 90, 1, '2019-11-07 16:45:58', '::1', 1),
(4, 88, 1, '2019-11-08 06:55:30', '::1', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_b2s_posts`
--
ALTER TABLE `wp_b2s_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `blog_user_id` (`blog_user_id`),
  ADD KEY `sched_details_id` (`sched_details_id`),
  ADD KEY `sched_date` (`sched_date`),
  ADD KEY `sched_date_utc` (`sched_date_utc`),
  ADD KEY `publish_date` (`publish_date`),
  ADD KEY `relay_primary_post_id` (`relay_primary_post_id`),
  ADD KEY `hook_action` (`hook_action`),
  ADD KEY `hide` (`hide`);

--
-- Indexes for table `wp_b2s_posts_drafts`
--
ALTER TABLE `wp_b2s_posts_drafts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_user_id` (`blog_user_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `wp_b2s_posts_network_details`
--
ALTER TABLE `wp_b2s_posts_network_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_b2s_posts_sched_details`
--
ALTER TABLE `wp_b2s_posts_sched_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_b2s_user`
--
ALTER TABLE `wp_b2s_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_user_id` (`blog_user_id`),
  ADD KEY `token` (`token`),
  ADD KEY `feature` (`feature`);

--
-- Indexes for table `wp_b2s_user_contact`
--
ALTER TABLE `wp_b2s_user_contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_user_id` (`blog_user_id`);

--
-- Indexes for table `wp_b2s_user_network_settings`
--
ALTER TABLE `wp_b2s_user_network_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_user_id` (`blog_user_id`),
  ADD KEY `mandant_id` (`mandant_id`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_fblb`
--
ALTER TABLE `wp_fblb`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_hustle_entries`
--
ALTER TABLE `wp_hustle_entries`
  ADD PRIMARY KEY (`entry_id`),
  ADD KEY `entry_type` (`entry_type`),
  ADD KEY `entry_module_id` (`module_id`);

--
-- Indexes for table `wp_hustle_entries_meta`
--
ALTER TABLE `wp_hustle_entries_meta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `meta_key` (`meta_key`),
  ADD KEY `meta_entry_id` (`entry_id`),
  ADD KEY `meta_key_object` (`entry_id`,`meta_key`);

--
-- Indexes for table `wp_hustle_modules`
--
ALTER TABLE `wp_hustle_modules`
  ADD PRIMARY KEY (`module_id`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `wp_hustle_modules_meta`
--
ALTER TABLE `wp_hustle_modules_meta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `meta_key` (`meta_key`);

--
-- Indexes for table `wp_hustle_tracking`
--
ALTER TABLE `wp_hustle_tracking`
  ADD PRIMARY KEY (`tracking_id`),
  ADD KEY `tracking_module_id` (`module_id`),
  ADD KEY `action` (`action`),
  ADD KEY `tracking_module_object` (`action`,`module_id`,`module_type`),
  ADD KEY `tracking_module_object_ip` (`module_id`,`tracking_id`,`ip`);

--
-- Indexes for table `wp_ig_actions`
--
ALTER TABLE `wp_ig_actions`
  ADD UNIQUE KEY `id` (`contact_id`,`message_id`,`campaign_id`,`type`,`link_id`,`list_id`),
  ADD KEY `contact_id` (`contact_id`),
  ADD KEY `message_id` (`message_id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `wp_ig_blocked_emails`
--
ALTER TABLE `wp_ig_blocked_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_ig_campaigns`
--
ALTER TABLE `wp_ig_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `status` (`status`),
  ADD KEY `base_template_id` (`base_template_id`);

--
-- Indexes for table `wp_ig_contactmeta`
--
ALTER TABLE `wp_ig_contactmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `contact_id` (`contact_id`),
  ADD KEY `meta_ley` (`meta_key`);

--
-- Indexes for table `wp_ig_contacts`
--
ALTER TABLE `wp_ig_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wp_user_id` (`wp_user_id`),
  ADD KEY `email` (`email`),
  ADD KEY `status` (`status`),
  ADD KEY `form_id` (`form_id`);

--
-- Indexes for table `wp_ig_contacts_ips`
--
ALTER TABLE `wp_ig_contacts_ips`
  ADD PRIMARY KEY (`created_on`,`ip`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `wp_ig_contact_meta`
--
ALTER TABLE `wp_ig_contact_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contact_id` (`contact_id`),
  ADD KEY `meta_ley` (`meta_key`);

--
-- Indexes for table `wp_ig_forms`
--
ALTER TABLE `wp_ig_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_ig_links`
--
ALTER TABLE `wp_ig_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `message_id` (`message_id`),
  ADD KEY `link` (`link`(100));

--
-- Indexes for table `wp_ig_lists`
--
ALTER TABLE `wp_ig_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_ig_lists_contacts`
--
ALTER TABLE `wp_ig_lists_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_ig_mailing_queue`
--
ALTER TABLE `wp_ig_mailing_queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campaign_id` (`campaign_id`);

--
-- Indexes for table `wp_ig_queue`
--
ALTER TABLE `wp_ig_queue`
  ADD UNIQUE KEY `id` (`contact_id`,`campaign_id`,`requeued`,`options`),
  ADD KEY `contact_id` (`contact_id`),
  ADD KEY `campaign_id` (`campaign_id`),
  ADD KEY `requeued` (`requeued`),
  ADD KEY `timestamp` (`timestamp`),
  ADD KEY `priority` (`priority`),
  ADD KEY `count` (`count`),
  ADD KEY `error` (`error`),
  ADD KEY `ignore_status` (`ignore_status`);

--
-- Indexes for table `wp_ig_sending_queue`
--
ALTER TABLE `wp_ig_sending_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_like_dislike_btn_details`
--
ALTER TABLE `wp_like_dislike_btn_details`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_maxbuttonsv3`
--
ALTER TABLE `wp_maxbuttonsv3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_maxbuttons_collections`
--
ALTER TABLE `wp_maxbuttons_collections`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `wp_newsletter`
--
ALTER TABLE `wp_newsletter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `wp_user_id` (`wp_user_id`);

--
-- Indexes for table `wp_newsletter_emails`
--
ALTER TABLE `wp_newsletter_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_newsletter_sent`
--
ALTER TABLE `wp_newsletter_sent`
  ADD PRIMARY KEY (`email_id`,`user_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `email_id` (`email_id`);

--
-- Indexes for table `wp_newsletter_stats`
--
ALTER TABLE `wp_newsletter_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_id` (`email_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_newsletter_user_logs`
--
ALTER TABLE `wp_newsletter_user_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_nls_subscribers`
--
ALTER TABLE `wp_nls_subscribers`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_responsive_menu`
--
ALTER TABLE `wp_responsive_menu`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `wp_sbi_instagram_feeds_posts`
--
ALTER TABLE `wp_sbi_instagram_feeds_posts`
  ADD PRIMARY KEY (`record_id`),
  ADD KEY `feed_id` (`feed_id`(100));

--
-- Indexes for table `wp_sbi_instagram_posts`
--
ALTER TABLE `wp_sbi_instagram_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_socialsnap_stats`
--
ALTER TABLE `wp_socialsnap_stats`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_subscribe_reloaded_subscribers`
--
ALTER TABLE `wp_subscribe_reloaded_subscribers`
  ADD PRIMARY KEY (`stcr_id`),
  ADD UNIQUE KEY `uk_subscriber_email` (`subscriber_email`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_top_ten`
--
ALTER TABLE `wp_top_ten`
  ADD PRIMARY KEY (`postnumber`,`blog_id`);

--
-- Indexes for table `wp_top_ten_daily`
--
ALTER TABLE `wp_top_ten_daily`
  ADD PRIMARY KEY (`postnumber`,`dp_date`,`blog_id`);

--
-- Indexes for table `wp_ulike`
--
ALTER TABLE `wp_ulike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_ulike_activities`
--
ALTER TABLE `wp_ulike_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_ulike_comments`
--
ALTER TABLE `wp_ulike_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_ulike_forums`
--
ALTER TABLE `wp_ulike_forums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wpgmza`
--
ALTER TABLE `wp_wpgmza`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_circles`
--
ALTER TABLE `wp_wpgmza_circles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_maps`
--
ALTER TABLE `wp_wpgmza_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_polygon`
--
ALTER TABLE `wp_wpgmza_polygon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_polylines`
--
ALTER TABLE `wp_wpgmza_polylines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_rectangles`
--
ALTER TABLE `wp_wpgmza_rectangles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wti_like_post`
--
ALTER TABLE `wp_wti_like_post`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_b2s_posts`
--
ALTER TABLE `wp_b2s_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_b2s_posts_drafts`
--
ALTER TABLE `wp_b2s_posts_drafts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_b2s_posts_network_details`
--
ALTER TABLE `wp_b2s_posts_network_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_b2s_posts_sched_details`
--
ALTER TABLE `wp_b2s_posts_sched_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_b2s_user`
--
ALTER TABLE `wp_b2s_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_b2s_user_contact`
--
ALTER TABLE `wp_b2s_user_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_b2s_user_network_settings`
--
ALTER TABLE `wp_b2s_user_network_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wp_fblb`
--
ALTER TABLE `wp_fblb`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_hustle_entries`
--
ALTER TABLE `wp_hustle_entries`
  MODIFY `entry_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_hustle_entries_meta`
--
ALTER TABLE `wp_hustle_entries_meta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_hustle_modules`
--
ALTER TABLE `wp_hustle_modules`
  MODIFY `module_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_hustle_modules_meta`
--
ALTER TABLE `wp_hustle_modules_meta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wp_hustle_tracking`
--
ALTER TABLE `wp_hustle_tracking`
  MODIFY `tracking_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_ig_blocked_emails`
--
ALTER TABLE `wp_ig_blocked_emails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_ig_campaigns`
--
ALTER TABLE `wp_ig_campaigns`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_ig_contactmeta`
--
ALTER TABLE `wp_ig_contactmeta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_ig_contacts`
--
ALTER TABLE `wp_ig_contacts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_ig_contact_meta`
--
ALTER TABLE `wp_ig_contact_meta`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_ig_forms`
--
ALTER TABLE `wp_ig_forms`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_ig_links`
--
ALTER TABLE `wp_ig_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_ig_lists`
--
ALTER TABLE `wp_ig_lists`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_ig_lists_contacts`
--
ALTER TABLE `wp_ig_lists_contacts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_ig_mailing_queue`
--
ALTER TABLE `wp_ig_mailing_queue`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_ig_sending_queue`
--
ALTER TABLE `wp_ig_sending_queue`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_like_dislike_btn_details`
--
ALTER TABLE `wp_like_dislike_btn_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_maxbuttonsv3`
--
ALTER TABLE `wp_maxbuttonsv3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_maxbuttons_collections`
--
ALTER TABLE `wp_maxbuttons_collections`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wp_newsletter`
--
ALTER TABLE `wp_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_newsletter_emails`
--
ALTER TABLE `wp_newsletter_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_newsletter_stats`
--
ALTER TABLE `wp_newsletter_stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_newsletter_user_logs`
--
ALTER TABLE `wp_newsletter_user_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_nls_subscribers`
--
ALTER TABLE `wp_nls_subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2936;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=488;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `wp_sbi_instagram_feeds_posts`
--
ALTER TABLE `wp_sbi_instagram_feeds_posts`
  MODIFY `record_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_sbi_instagram_posts`
--
ALTER TABLE `wp_sbi_instagram_posts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_socialsnap_stats`
--
ALTER TABLE `wp_socialsnap_stats`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_subscribe_reloaded_subscribers`
--
ALTER TABLE `wp_subscribe_reloaded_subscribers`
  MODIFY `stcr_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wp_ulike`
--
ALTER TABLE `wp_ulike`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_ulike_activities`
--
ALTER TABLE `wp_ulike_activities`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_ulike_comments`
--
ALTER TABLE `wp_ulike_comments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_ulike_forums`
--
ALTER TABLE `wp_ulike_forums`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wpgmza`
--
ALTER TABLE `wp_wpgmza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wpgmza_circles`
--
ALTER TABLE `wp_wpgmza_circles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza_maps`
--
ALTER TABLE `wp_wpgmza_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wpgmza_polygon`
--
ALTER TABLE `wp_wpgmza_polygon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza_polylines`
--
ALTER TABLE `wp_wpgmza_polylines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza_rectangles`
--
ALTER TABLE `wp_wpgmza_rectangles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wti_like_post`
--
ALTER TABLE `wp_wti_like_post`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
