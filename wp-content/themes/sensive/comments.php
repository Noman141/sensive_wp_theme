<div class="comment-list">
    <?php
    if (!post_password_required()):
        wp_list_comments(array(
            'style'   => 'ul',
            'avatar_size'  => 60,
        ));
    endif;
    ?>
    <hr>
</div>

<div class="comment-form">
    <?php
    if (comments_open()):
        comment_form();
    else:
        _e('Comments are closed');
    endif;
    ?>
</div>
