<!DOCTYPE html>
<html lang="<?php language_attributes()?>">
<head>
    <meta charset="<?php bloginfo('charset')?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name') ;?></title>
    <link rel="icon" href="<?php echo get_template_directory_uri();?>/img/Fevicon.png" type="image/png">

    <?php wp_head();?>
</head>
<body <?php body_class() ;?>>
<!--================Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container box_1620">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="<?php home_url()?>"><?php
                    if ( function_exists( 'the_custom_logo' ) ) {
                        the_custom_logo();
                    }

                    ?></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav justify-content-center">
                        <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'mainmenu',
                                'depth'             => 5,
                                'container'         => 'div',
                                'container_class'   => 'collapse navbar-collapse offset',
                                'container_id'      => 'navbarSupportedContent',
                                'menu_class'        => 'nav navbar-nav menu_nav justify-content-center',
                                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                                'walker'            => new WP_Bootstrap_Navwalker(),
                            ) );
                        ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right navbar-social">
                        <?php
                        wp_nav_menu( array(
                            'theme_location'    => 'socialmenu',
                            'depth'             => 2,
                            'container'         => 'div',
                            'container_class'   => 'collapse navbar-collapse offset',
                            'container_id'      => 'navbarSupportedContent',
                            'menu_class'        => 'nav navbar-nav navbar-right navbar-social',
                            'fallback_cb'       => 'WP_Social_Navwalker::fallback',
                            'walker'            => new WP_Social_Navwalker(),
                        ) );
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!--================Header Menu Area =================-->