<?php get_header();?>

<!--================ Hero sm Banner start =================-->
<?php get_template_part('banner')?>
<!--================ Hero sm Banner end =================-->


<!--================ Start Blog Post Area =================-->
<section class="blog-post-area section-margin">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="main_blog_details">
                    <?php the_post_thumbnail();?>
                    <h4><?php the_title() ?></h4>
                    <div class="user_details">
                        <div class="float-left">
                            <?php the_tags()?>
                        </div>
                        <div class="float-right mt-sm-0 mt-3">
                            <div class="media">
                                <div class="media-body">
                                    <h5><?php the_author_posts_link();?></h5>
                                    <p><?php the_time('M d, Y');?></p>
                                </div>
                                <div class="d-flex">
                                    <?php echo get_avatar( get_the_author_meta( 'ID' ) , 42 ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php the_content()?>
                    <div class="news_d_footer flex-column flex-sm-row ">
                        <div class="row">
                            <div class="col-md-4">
                                <?php echo fb_like_button(); ?>
                            </div>
                            <div class="col-md-4">
                                <a class="justify-content-sm-center ml-sm-auto mt-sm-0 mt-2" href="#"><span class="align-middle mr-2"><i class="ti-themify-favicon"></i></span><?php comments_popup_link('No Comments','1 Comment','% Comments','comment_class','Comments off') ;?></a>

                            </div>
                            <div class="col-md-4">
                                <div class="news_socail ml-sm-auto mt-sm-0 mt-2" style="margin-right: 20px">
                                    <?php dynamic_sidebar('socialshare_widget')?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="navigation-area" id="next-previous">
                    <div class="row">
                        <?php

                        $previous = get_previous_post();
                        $next = get_next_post();
                        ?>
                        <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                            <?php

                                if ( get_previous_post($previous) ) { ?>

                            <div class="thumb">
                                <?php echo get_avatar( get_the_author_meta('ID') , 60 ); ?>
                            </div>
                            <div class="arrow">
                                <a href="<?php the_permalink($previous) ?>"><span class="lnr text-white lnr-arrow-left"></span></a>
                            </div>
                            <div class="detials">
                                <p>Prev Post</p>
                                <h4><?php echo get_the_author_posts_link($previous) ?></h4>
                            </div>
                          <?php }  ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                            <?php

                            if ( get_previous_post($next) ) { ?>
                            <div class="detials">
                                <p>Next Post</p>
                                <h4><?php echo get_the_author_posts_link($next) ?></h4>
                            </div>
                            <div class="arrow">
                                <a href="<?php the_permalink($next) ?>"><span class="lnr text-white lnr-arrow-right"></span></a>
                            </div>
                            <div class="thumb">
                                <a href="<?php the_permalink($next) ?>"> <?php echo get_avatar( get_the_author_meta('ID') , 60 ); ?></a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="comments-section">
                    <div class="comments-area">
                        <h4><?php comments_popup_link('No Comments','1 Comment','% Comments','comment_class','Comments off') ;?></h4>
                            <?php comments_template();?>

                    </div>

                </div>
            </div>

            <!-- Start Blog Post Siddebar -->
            <?php get_sidebar()?>
        <!-- End Blog Post Siddebar -->
    </div>
</section>
<!--================ End Blog Post Area =================-->

<?php get_footer()?>