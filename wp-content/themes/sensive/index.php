<?php get_header();?>

    <main class="site-main">
        <?php get_template_part('banner')?>
        <?php get_template_part('slider')?>


    <!--================ Start Blog Post Area =================-->
    <section class="blog-post-area section-margin mt-4">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
          <?php
            if (have_posts()):
                while (have_posts()): the_post(); ?>
                    <div class="single-recent-blog-post">
                        <div class="thumb">
                            <a href="<?php the_permalink() ?>"><?php the_post_thumbnail();?></a>
                            <ul class="thumb-info">
                                <li><i class="ti-user"></i><?php the_author_posts_link();?></li>
                                <li><i class="ti-notepad"></i><?php the_time('M d, Y');?></li>
                                <li><?php comments_popup_link('No Comments','1 Comment','% Comments','comment_class','Comments off') ;?></li>
                            </ul>
                        </div>
                        <div class="details mt-20">
                            <a href="<?php the_permalink() ?>">
                                <h3><?php the_title() ?></h3>
                            </a>
                            <p class="tag-list-inline"><?php the_tags()?></p>
                            <span><?php the_excerpt();?></span>
                            <a class="button" href="<?php the_permalink() ?>">Read More <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
          <?php endwhile;
            endif;
          ?>


            <div class="row">
              <div class="col-lg-12">
                  <nav class="blog-pagination justify-content-center d-flex">
                    <?php if (function_exists("pagination")) {pagination($additional_loop->max_num_pages);}?>

                  </nav>

              </div>
            </div>
          </div>

          <?php get_sidebar()?>
        </div>
    </section>
    <!--================ End Blog Post Area =================-->
  </main>

<?php get_footer()?>