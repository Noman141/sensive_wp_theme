<!--================ Blog slider start =================-->
<section>
    <div class="container">
        <div class="owl-carousel owl-theme blog-slider">
            <!-- query the posts to be displayed -->
            <?php $loop = new WP_Query(array('post_type' => 'post', 'posts_per_page' => -1, 'orderby'=> 'ASC')); //Note, you can change the post_type to a CPT and set the number to pull and order to display them in here. ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="card blog__slide text-center">
                <div class="blog__slide__img">
                    <a href="<?php the_permalink() ?>"><?php the_post_thumbnail()?></a>
                </div>
                <div class="blog__slide__content">
                    <span class="blog__slide__label"> <?php the_category(', ')?></span>
                    <h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
                    <p>2 days ago</p>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<!--================ Blog slider end =================-->