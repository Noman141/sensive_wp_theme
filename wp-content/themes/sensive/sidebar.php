<!-- Start Blog Post Siddebar -->
<div class="col-lg-4 sidebar-widgets">
    <div class="widget-wrap">
        <div class="single-sidebar-widget newsletter-widget">
            <h4 class="single-sidebar-widget__title">Newsletter</h4>
            <div class="form-group mt-30">
                <div class="col-autos">
                    <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email" onfocus="this.placeholder = ''"
                           onblur="this.placeholder = 'Enter email'">
                </div>
            </div>
            <button class="bbtns d-block mt-20 w-100">Subcribe</button>
        </div>

         <?php dynamic_sidebar('category_widget')?>
         <?php dynamic_sidebar('popular_post_widget')?>
         <?php dynamic_sidebar('tags_widget')?>
</div>
</div>
<!-- End Blog Post Siddebar -->