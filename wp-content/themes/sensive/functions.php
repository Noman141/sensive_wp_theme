<?php

function callingResources(){
    wp_enqueue_style('style',get_stylesheet_uri(),'','2.3.3');
    wp_enqueue_style('bootstrap',get_template_directory_uri().'/vendors/bootstrap/bootstrap.min.css');
    wp_enqueue_style('fontawesome',get_template_directory_uri().'/vendors/fontawesome/css/all.min.css');
    wp_enqueue_style('themify-icons',get_template_directory_uri().'/vendors/themify-icons/themify-icons.css');
    wp_enqueue_style('linericon',get_template_directory_uri().'/vendors/linericon/style.css');
    wp_enqueue_style('owl-carousel-default',get_template_directory_uri().'/vendors/owl-carousel/owl.theme.default.min.css');
    wp_enqueue_style('owl-carousel',get_template_directory_uri().'/vendors/owl-carousel/owl.carousel.min.css');
    wp_enqueue_script('jquery',get_template_directory_uri().'/vendors/jquery/jquery-3.2.1.min.js');
    wp_enqueue_script('bootstrap-bundle',get_template_directory_uri().'/vendors/bootstrap/bootstrap.bundle.min.js');
    wp_enqueue_script('owlCarousel',get_template_directory_uri().'/vendors/owl-carousel/owl.carousel.min.js');
    wp_enqueue_script('jquery-ajaxchimp',get_template_directory_uri().'/js/jquery.ajaxchimp.min.js');
    wp_enqueue_script('mail-script',get_template_directory_uri().'/js/mail-script.js','','1.0.0');
    wp_enqueue_script('main-script',get_template_directory_uri().'/js/main.js','','1.0.5');
}
add_action('wp_enqueue_scripts','callingResources');

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
    register_nav_menus( array(
        'mainmenu' => __('Main Menu', 'THEMENAME'),
    ) );

}
add_action( 'after_setup_theme', 'register_navwalker' );


function ourThemeSetUp(){
    add_theme_support('post-thumbnails');
}
add_action('after_setup_theme','ourThemeSetUp');

function register_social_navwalker(){
    require_once get_template_directory() . '/class-wp-social_navwalker.php';
    register_nav_menus( array(
        'socialmenu' => __('Social Menu'),
    ) );

}
add_action( 'after_setup_theme', 'register_social_navwalker' );

function custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'custom_logo_setup' );

// register a custom post type called 'banner'
function homePageBanner() {
    $labels = array(
        'name' => __( 'Banners' ),
        'singular_name' => __( 'banner' ),
        'add_new' => __( 'New banner' ),
        'add_new_item' => __( 'Add New banner' ),
        'edit_item' => __( 'Edit banner' ),
        'new_item' => __( 'New banner' ),
        'view_item' => __( 'View banner' ),
        'search_items' => __( 'Search banners' ),
        'not_found' =>  __( 'No banners Found' ),
        'not_found_in_trash' => __( 'No banners found in Trash' ),
    );
    $args = array(
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'custom-fields',
            'thumbnail',
            'page-attributes'
        ),
        'taxonomies' => array( 'post_tag', 'category'),
    );
    register_post_type( 'banner', $args );
}
add_action( 'init', 'homePageBanner' );



// function to show home page banner using query of banner post type
function display_home_page_banner() {

    // start by setting up the query
    $query = new WP_Query( array(
        'post_type' => 'banner',
    ));

    // now check if the query has posts and if so, output their content in a banner-box div
    if ( $query->have_posts() ) {

             while ( $query->have_posts() ) : $query->the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>

    <?php }
    wp_reset_postdata();

}



/* Custom Pagination */
function pagination($pages = '', $range = 4){
    $showitems = ($range * 2)+1;
    global $paged;
    if(empty($paged)) $paged = 1;
    if($pages == ''){
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages){$pages = 1;}
    }
    if(1 < $pages){
        echo "<ul class=\"pagination\">";

//        if($paged > 2 && $paged > $range+1 && $showitems < $pages)
//            echo "<li class=\"page-item\"><a class=\"page-link\" href='".get_pagenum_link(1)."'></a></li>";

        if($paged > 1 )
            echo "<li class=\"page-item\"><a class=\"page-link\" href='".get_pagenum_link($paged - 1)."'  aria-label=\"Previous\"><span aria-hidden=\"true\"><i class=\"ti-angle-left\"></i></span></a></li>";

        for ($i=1; $i <= $pages; $i++){
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
                echo ($paged == $i)? "<li class=\"page-item active\"><a href='".get_pagenum_link($i)."' class=\"active page-link\">$i</a></li>":"<li class=\"page-item \"><a href='".get_pagenum_link($i)."' class=\"inactive page-link\">".$i."</a></li>";
            }
        }
        if ($paged < $pages )
            echo "<li class=\"page-item\"><a class=\"page-link\" href='".get_pagenum_link($paged + 1)."'  aria-label=\"Next\"><span aria-hidden=\"true\"><i class=\"ti-angle-right\"></i></span></a></li>";
//        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages)
//            echo "<li class=\"page-item\"><a class=\"page-link\" href='".get_pagenum_link($pages)."'>Last Page &raquo;</a></li>";
       echo "</ul>";
    }
}

function ourWidgetRegister(){
    register_sidebar(array(
        'name' => 'Category Widget',
        'id'   => 'category_widget',
        'before_widget' =>'<div class="single-sidebar-widget post-category-widget category_widget">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="single-sidebar-widget__title">',
        'after_title'  => '</h2>'
    ));
    register_sidebar(array(
        'name' => 'Popular Posts Widget',
        'id'   => 'popular_post_widget',
        'before_widget' =>'<div class="single-sidebar-widget popular-post-widget">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="single-sidebar-widget__title">',
        'after_title'  => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Tags Widget',
        'id'   => 'tags_widget',
        'before_widget' =>'<div class="single-sidebar-widget tag_cloud_widget">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="single-sidebar-widget__title">',
        'after_title'  => '</h2>',
    ));

    register_sidebar(array(
        'name' => 'Footer About Widget',
        'id'   => 'about_widget',
        'before_widget' =>'<div class="single-footer-widget">',
        'after_widget' => '</div>',
        'before_title' => '<h6>',
        'after_title'  => '</h6>',
    ));

    register_sidebar(array(
        'name' => 'Instagram Feed Widget',
        'id'   => 'instagram_widget',
        'before_widget' =>'<div class="single-footer-widget mail-chimp">',
        'after_widget' => '</div>',
        'before_title' => '<h6 class="mb-20">',
        'after_title'  => '</h6>',
    ));

    register_sidebar(array(
        'name' => 'SubsCribe Widget',
        'id'   => 'subscribe_widget',
        'before_widget' =>'<div class="single-footer-widget">',
        'after_widget' => '</div>',
        'before_title' => '<h6 >',
        'after_title'  => '</h6>',
    ));

    register_sidebar(array(
        'name' => 'Copyright Widget',
        'id'   => 'copyright_widget',
        'before_widget' =>'<p class="footer-text m-0">',
        'after_widget' => '</p>',
    ));

    register_sidebar(array(
        'name' => 'Social Share Widget',
        'id'   => 'socialshare_widget',
        'before_widget' =>'<div class="news_socail ml-sm-auto mt-sm-0 mt-2">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'Google Map Widget',
        'id'   => 'google_map_widget',
        'before_widget' =>'<div id="map" style="height: 420px;">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'Address Widget',
        'id'   => 'address_widget',
        'before_widget' =>'<div >',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name' => 'Contact Form Widget',
        'id'   => 'contact_form_widget',
        'before_widget' =>'<div class="form-contact contact_form" id="contactForm">',
        'after_widget' => '</div>',
    ));
}
add_action('widgets_init','ourWidgetRegister');
