<!--================ Start Footer Area =================-->
<footer class="footer-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-3  col-md-6 col-sm-6">
                <?php dynamic_sidebar('about_widget')?>
            </div>
            <div class="col-lg-4  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Newsletter</h6>
                    <p>Stay update with our latest</p>
                    <div class="" id="mc_embed_signup">
                       <?php dynamic_sidebar('subscribe_widget')?>
                    </div>

                </div>
            </div>
            <div class="col-lg-3  col-md-6 col-sm-6">
                    <ul class="instafeed d-flex flex-wrap">
                        <?php dynamic_sidebar('instagram_widget')?>
                    </ul>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6">

                    <h6>Follow Us</h6>
                    <p>Let us be social</p>
                <ul class="nav navbar-nav navbar-right navbar-social">
                    <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'socialmenu',
                        'depth'             => 2,
                        'container'         => 'div',
//                        'container_class'   => 'collapse navbar-collapse offset',
//                        'container_id'      => 'navbarSupportedContent',
//                        'menu_class'        => 'nav navbar-nav navbar-right navbar-social',
                        'fallback_cb'       => 'WP_Social_Navwalker::fallback',
                        'walker'            => new WP_Social_Navwalker(),
                    ) );
                    ?>
                </ul>
                </div>
            </div>
        </div>
        <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
            <?php dynamic_sidebar('copyright_widget') ?>
        </div>
    </div>
</footer>
<!--================ End Footer Area =================-->


<?php wp_footer();?>
</body>
</html>