<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'WpLearning_03' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'M`&>hUx43W*EO<{}.$U9qR_LDe1Fh=HFfJevq?Ja5.<yWv K`$E=l1?V#r~oUI#^' );
define( 'SECURE_AUTH_KEY',  'Nm%%1w[*G|F_&aS.lmb*Q,Vb}kYqic%*h%(u(gYG}iDI4u@z6E. X@V*K6&LR.Ik' );
define( 'LOGGED_IN_KEY',    '1(e&;=o&fzs${rQcJ~CzI4i@lyXb2M>M`}dRk(-rd*E%B-%8h4dZk1D&rJ*?g`0/' );
define( 'NONCE_KEY',        '0ND :z*{OqXR!v2CHzUIak)&,lQJp|l|q7,m28|Ji6b?~hU-ejEdKcqd`;rtk Vw' );
define( 'AUTH_SALT',        '<p=Q[F8=+]7q!ZaUAI&6M{AJ.|hCKb`wLp{NkVo}{WO5hF&sDBHiz|U}F~6#kbrY' );
define( 'SECURE_AUTH_SALT', '=4LgYI@Ux<#8t:32>eGeW3PC@CRS8)/;)(3+qLDaIQtd0hAN1[W]$D<d,ada0y6y' );
define( 'LOGGED_IN_SALT',   'vkSLg5R-=!OIsufXqi6Bs3p0K^{bb)m?1X_?c+0Iz*)8`KN>o)m6ELMOs0ISuJ`)' );
define( 'NONCE_SALT',       '*Yc.MEiBkpP4u;|8{dg`@w2K0-1^>CPp]hp+Wwa:AgY4ZWbf`+wc?DU2((.(wJmw' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
